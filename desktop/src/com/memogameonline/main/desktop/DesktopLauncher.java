package com.memogameonline.main.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.memogameonline.externalAPI.AdsController;
import com.memogameonline.main.MemoGameOnline;




public class DesktopLauncher {
	private static boolean rebuildAtlas = false;
	private static boolean drawDebugOutline = false;

	public static void main(String[] arg) {

		if (rebuildAtlas) {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.maxHeight = 2048;
			settings.maxWidth = 2048;
			settings.debug = drawDebugOutline;

			TexturePacker.process(settings, "D:/Lavoro/MemoGameOnline/desktop/asset-raw/images-menu",
					"D:/Lavoro/MemoGameOnline/android/assets/images", "memo");


			TexturePacker.process(settings, "D:/Lavoro/MemoGameOnline/desktop/asset-raw/cards",
					"D:/Lavoro/MemoGameOnline/android/assets/cards", "cards");


			TexturePacker
					.process(
							settings,
							"D:/Lavoro/MemoGameOnline/desktop/asset-raw/GeneralAssetsPatched",
							"D:/Lavoro/MemoGameOnline/android/assets/GeneralAssetsPatched",
							"GeneralAssets");


			TexturePacker.process(settings, "D:/Lavoro/MemoGameOnline/desktop/asset-raw/game screen",
					"D:/Lavoro/MemoGameOnline/android/assets/game screen",
					"game screen");

			TexturePacker.process(settings, "D:/Lavoro/MemoGameOnline/desktop/asset-raw/SmallIcons",
					"D:/Lavoro/MemoGameOnline/android/assets/SmallIcons",
					"smallIcons");
			TexturePacker.process(settings, "D:/Lavoro/MemoGameOnline/desktop/asset-raw/BigIcons",
					"D:/Lavoro/MemoGameOnline/android/assets/BigIcons",
					"bigIcons");



		}
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Memo";
		config.width = 540;
		config.height = 885;
		AdsController a=new AdsController() {
			@Override
			public void showBannerAd() {

			}

			@Override
			public void hideBannerAd() {

			}

			@Override
			public void showOrLoadInterstital() {
				System.out.println("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- MOSTRO MOSTRO MOSTRO MOSTRO");
			}

			@Override
			public void loadInterstital() {
				System.out.println("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- CARICO CARICO CAICO CARICO!!!!!");
			}
		};
		new LwjglApplication(new MemoGameOnline(a), config);
	}
}

