package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.SettingCell;
import com.memogameonline.main.MemoGameOnline;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class SendToFriendsView extends AbstractScreen {

    private Image topBar;
    private ScrollPane scrollPane;

    SendToFriendsView(final MemoGameOnline memo, final MOCDataCarrier mocdc) {

        super(memo, mocdc);
        TAG = "SendToFriendsView";

        multiplexer = new InputMultiplexer();
        InputAdapter backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new Settings(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));


    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {

        Table table = new Table();

        table.addActor(buildScrollPane());
        table.addActor(buildImagesLayer());

        stage.addActor(table);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();


        topBar = new Image(memo.patchedSkin, "topBar");
        topBar.setPosition(-9, 783);
        Label title = new Label("Share MemoGameOnline with your friends!", memo.patchedSkin, "40black");
        title.setPosition((540-title.getWidth())/2, 800);
        // layer.addActor(topBar);
        layer.addActor(title);

        return layer;
    }

    public Table buildCells() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        table.center().top();
        table.add(header).padTop(50);
        Label title = new Label("Choose an option", memo.patchedSkin, "30white");
        table.row();
        table.add(title).padTop(-68);
        table.row();


        String name1 = "What's App";
        String legend1 = "";
        SettingCell whatsApp = new SettingCell(memo, name1, legend1);
        whatsApp.getCellName().setPosition(30, 40);
        whatsApp.getCellNamePressed().setPosition(30, 40);
        whatsApp.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onWhatsAppClicked();
            }

        });
        table.add(whatsApp.getWhiteCell()).padTop(-9);
        table.row();

        String name2 = "SMS";
        String legend2 = "";
        SettingCell sms = new SettingCell(memo, name2, legend2);
        sms.getCellName().setPosition(30, 40);
        sms.getCellNamePressed().setPosition(30, 40);
        sms.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSmsClicked();
            }

        });
        table.add(sms.getWhiteCell()).padTop(-14);
        table.row();


        String name3 = "Twitter";
        String legend3 = "";
        SettingCell twitter = new SettingCell(memo, name3, legend3);
        twitter.getCellName().setPosition(30, 40);
        twitter.getCellNamePressed().setPosition(30, 40);
        twitter.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onTwitterClicked();
            }

        });
        table.add(twitter.getWhiteCell()).padTop(-14);
        table.row();

        String name4 = "e-Mail";
        String legend4 = "";
        SettingCell email = new SettingCell(memo, name4, legend4);
        email.getCellName().setPosition(30, 40);
        email.getCellNamePressed().setPosition(30, 40);
        email.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onEmailClicked();
            }

        });
        table.add(email.getWhiteCell()).padTop(-14);
        table.row();

        return table;

    }

    private Table buildScrollPane() {

        Table scrollContainer = new Table(memo.patchedSkin);
        Table table = new Table();

        table.add(buildCells()).row();

        scrollPane = new ScrollPane(table, memo.patchedSkin, "scrollPane");

        scrollPane.setScrollBarPositions(true, false);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollbarsOnTop(false);
        scrollPane.setOverscroll(false, false);

        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(6, -2, 530, 630);
        scrollContainer.addActor(background);

        scrollContainer.setBounds(0, 135, 540, 630);
        // scrollContainer.setBackground("scrollPaneContainer");
        scrollContainer.add(scrollPane).expand().fill().colspan(4);

        return scrollContainer;
    }

    public void onWhatsAppClicked() {
        memo.getWhatsApp().openWhatsApp();
    }

    public void onSmsClicked() {
        memo.getSms().openSMS();
    }

    public void onTwitterClicked() {
        // memo.getTwitter().openEmail();
    }

    public void onEmailClicked() {
        memo.getEmail().openEmail();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

}
