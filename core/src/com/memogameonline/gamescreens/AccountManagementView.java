package com.memogameonline.gamescreens;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamehelpers.TaskCallback;
import com.memogameonline.gameobjects.SettingCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

import java.io.IOException;

public class AccountManagementView extends AbstractScreen {

    private TextField emailField;
    private TextField newPasswordField;
    private TextField confirmNewPasswordField;

    private SettingCell memoGameIconsCell;
    private Image heroIcon;
    private SettingCell facebookPictureCell;
    private Image facebookHeroIcon;

    private TextureRegion image;
    private boolean pictureDownloadRequested;

    AccountManagementView(MemoGameOnline memo, MOCDataCarrier mocdc) {

        super(memo, mocdc);
        TAG = "AccountMenagement";

        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        checkServerMessage();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

        checkFacebookLogIn();

    }

    public void checkFacebookLogIn() {

        if (User.data.comingFromFacebookLogIn) {
            User.data.comingFromFacebookLogIn = false;
            String[] strings = {"0603", User.data.facebookID, User.data.facebookPictureURL};
            mocdc.sendToServer(strings);
            onFacebookPictureClicked();
        }

    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub
    }

    @Override
    public void show() {


        rebuildStage();

        //onPictureProfileClicked();

    }

    public void rebuildStage() {


        stage.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("PowerUpBackground");
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.addActor(buildImagesLayer());
        screen.addActor(buildAccountData());
        screen.addActor(buildPictureChoose());
        screen.addActor(buildButtonsLayer());
        stage.addActor(screen);

        //stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();
        Label title = new Label("My Account", memo.patchedSkin, "40black");
        title.setPosition(200, 800);
        layer.addActor(title);

        return layer;
    }

    public Table buildAccountData() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        table.center().top();
        table.add(header).padTop(50);
        table.row();
        Label title = new Label("Account Data", memo.patchedSkin, "30white");
        table.add(title).padTop(-65);
        table.row();

        Button disconnectCell = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(disconnectCell).padTop(-9);
        Label name4 = new Label("" + User.data.userName, memo.patchedSkin,
                "34black");
        disconnectCell.addActor(name4);
        name4.setPosition(23, 30);


        Button disconnectButton = new Button(memo.patchedSkin, "WhiteButton");
        disconnectButton.add(new Label("disconnect", memo.patchedSkin,
                "28black"));
        disconnectButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onDisconnectClicked();
            }
        });
        disconnectButton.setBounds(355, 20, 110, 50);

        disconnectCell.addActor(disconnectButton);
        table.row();

        Button emailCell = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(emailCell).padTop(-14);
        Label name1 = new Label("e-mail:", memo.patchedSkin, "32black");
        emailCell.addActor(name1);
        name1.setPosition(23, 30);
        emailField = new TextField(User.data.email, memo.patchedSkin);
        emailField.setMessageText("write here your e-mail");
        emailField.setBounds(140, 27, 350, 60);
        emailCell.addActor(emailField);
        emailCell.setTouchable(Touchable.childrenOnly);
        table.row();

        Button newPassword = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(newPassword).padTop(-14);
        Label name2 = new Label("New Password:", memo.patchedSkin, "32black");
        newPassword.addActor(name2);
        name2.setPosition(23, 30);
        newPasswordField = new TextField("", memo.patchedSkin);
        newPasswordField.setBounds(200, 24, 290, 60);
        newPasswordField.setMessageText("new password");
        newPasswordField.setPasswordMode(true);
        newPasswordField.setPasswordCharacter('"');
        newPassword.addActor(newPasswordField);
        table.row();

        Button confirmNewPassword = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(confirmNewPassword).padTop(-14);
        Label name3 = new Label("Confirm new Password:", memo.patchedSkin, "32black");
        confirmNewPassword.addActor(name3);
        name3.setPosition(23, 30);
        confirmNewPasswordField = new TextField("", memo.patchedSkin);
        confirmNewPasswordField.setBounds(260, 27, 230, 60);
        confirmNewPasswordField.setMessageText("Confirm new password");
        confirmNewPasswordField.setPasswordMode(true);
        confirmNewPasswordField.setPasswordCharacter('"');
        confirmNewPassword.addActor(confirmNewPasswordField);

        table.setBounds(110, 0, 320, 800);

        return table;

    }

    public Table buildPictureChoose() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        table.center().top();
        table.add(header).padTop(30);
        table.row();
        Label title = new Label("Picture Profile", memo.patchedSkin, "30white");
        table.add(title).padTop(-65);
        table.row();


        String name2 = "Choose a MemoGame icon";
        String legend2 = "";
        memoGameIconsCell = new SettingCell(memo, name2, legend2);
        memoGameIconsCell.getCellName().setPosition(23, 30);
        memoGameIconsCell.getCellNamePressed().setPosition(23, 30);
        table.add(memoGameIconsCell.getWhiteCell()).padTop(-9);

        memoGameIconsCell.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onPictureProfileClicked();
            }
        });

        if (User.data.imageId != 48) {
            heroIcon = AvatarManager.instance.getBigAvatar(User.data.imageId);
            heroIcon.setBounds(400, 18, 80, 70);
            memoGameIconsCell.getWhiteCell().addActor(heroIcon);
        }
        table.row();


        String name3 = "Use your Facebook picture";
        String legend3 = "";
        facebookPictureCell = new SettingCell(memo, name3, legend3);
        facebookPictureCell.getCellName().setPosition(23, 30);
        facebookPictureCell.getCellNamePressed().setPosition(23, 30);
        table.add(facebookPictureCell.getWhiteCell()).padTop(-14);


        facebookPictureCell.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onFacebookPictureClicked();
            }
        });

        if (User.data.imageId == 48) {
            if (User.data.facebookPicture != null) {
                facebookHeroIcon = User.data.facebookPicture;
                facebookHeroIcon.setBounds(400, 22, 60, 60);
                facebookPictureCell.getWhiteCell().addActor(facebookHeroIcon);
                Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                pictureFrame.setBounds(398, 20, 64, 64);
                facebookPictureCell.getWhiteCell().addActor(pictureFrame);
            } else {
                onFacebookPictureClicked();
            }
        }
        ;
        table.row();

        table.setBounds(110, 0, 320, 400);
        return table;

    }

    public Table buildButtonsLayer() {
        Table table = new Table();

        Button confirm = new Button(memo.patchedSkin, "GreenButton");
        confirm.add(new Label("Accept", memo.patchedSkin, "48white"));
        confirm.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onConfirmClicked();
            }
        });
        confirm.setBounds(275, 1, 235, 112);
        table.addActor(confirm);

        Button cancel = new Button(memo.patchedSkin, "WhiteButton");
        cancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onCancelClicked();
            }
        });
        cancel.add(new Label("Cancel", memo.patchedSkin, "40black"));
        cancel.setBounds(30, 5, 190, 90);
        table.addActor(cancel);

        return table;

    }

    public void onCancelClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.RIGHT, false, interpolation);
        memo.setScreen(new Settings(this.memo, this.mocdc), transition);
    }

    public void onConfirmClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (emailField.getText().equals("")
                || emailField.getText().equals(User.data.email)) {
            User.data.email = "-";
        } else {
            System.out.println("Inviata nuova mail!!! : "
                    + emailField.getText());
            User.data.email = emailField.getText();
            String[] msg = {"0600", User.data.email};
            mocdc.sendToServer(msg);
        }

        if (!newPasswordField.getText().equals("")
                && newPasswordField.getText().equals(
                confirmNewPasswordField.getText())) {
            User.data.password = newPasswordField.getText();
            String[] strings = {"0601", User.data.password};
            mocdc.sendToServer(strings);

        } else
            System.out.println("Le due password  non coincidono!!!");

        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.RIGHT, false, interpolation);
        DataManager.instance.saveData();
        memo.setScreen(new Settings(this.memo, this.mocdc), transition);
    }

    public void onDisconnectClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        GamePreferences.instance.autoLogIn = false;
        GamePreferences.instance.autoLogInFacebook = false;
        User.data.activeMatches.clear();
        User.data.finishedMatches.clear();
        switch (Gdx.app.getType()) {
            case Android:
                if (memo.getFacebookService().isLoggedIn()) {
                    memo.getFacebookService().logOut();
                }
        }

        GamePreferences.instance.save();

        String[] msg = {"0109", User.data.userName};
        mocdc.sendToServer(msg);
        DataManager.instance.saveData();
        memo.setScreen(new SignIn(this.memo, this.mocdc));

    }


    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
    }

    public void onFacebookPictureClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        System.out
                .println("----------------------------------SCARICO LA FOTO PROFILO!!!");
        String[] strings = {"0602", String.valueOf(48)};
        mocdc.sendToServer(strings);

        System.out.println("Facebook URL  " + User.data.facebookPictureURL);
        System.out.println("Facebook Image  " + User.data.facebookPicture);

        if (User.data.facebookPicture != null) {
            System.out.println("IMPOSTO LA FOTO!!!!!  " + User.data.facebookPicture);
            User.data.imageId = 48;
            Image facebookHeroAvatar = User.data.facebookPicture;
            facebookHeroAvatar.setBounds(400, 22, 60, 60);
            facebookPictureCell.getWhiteCell().addActor(facebookHeroAvatar);
            memoGameIconsCell.getWhiteCell().removeActor(heroIcon);

            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
            pictureFrame.setBounds(398, 20, 64, 64);
            facebookPictureCell.getWhiteCell().addActor(pictureFrame);
            //TODO aggiungere la negazione all'iF!!!
        } else if (!User.data.facebookPictureURL.equals("")) {
            System.out.println("SCARICO LA FOTO!!!!!  " + User.data.facebookPictureURL);
            new Thread(new Runnable() {
                /** Downloads the content of the specified url to the array. The array has to be big enough. */
                private int download(byte[] out, String url) {
                    InputStream in = null;
                    try {
                        HttpURLConnection conn = null;
                        conn = (HttpURLConnection) new URL(url).openConnection();
                        conn.setDoInput(true);
                        conn.setDoOutput(false);
                        conn.setUseCaches(true);
                        conn.connect();
                        in = conn.getInputStream();
                        int readBytes = 0;
                        while (true) {
                            int length = in.read(out, readBytes, out.length - readBytes);
                            if (length == -1) break;
                            readBytes += length;
                        }
                        return readBytes;
                    } catch (Exception ex) {
                        return 0;
                    } finally {
                        StreamUtils.closeQuietly(in);
                    }
                }

                @Override
                public void run() {
                    byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                    //TODO TOGLIERE COMMENTO!!!
                    int numBytes = download(bytes, "https://scontent.xx.fbcdn.net/hprofile-xpa1/v/t1.0-1/" +
                            "p50x50/12115705_1650716381878140_2621404005692089800_n.jpg?oh=393ae634f765a231c4382a29aed353eb&oe=5740545F");
                    //int numBytes = download(bytes, User.data.facebookPictureURL);
                    if (numBytes != 0) {
                        // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                        Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                        final int originalWidth = pixmap.getWidth();
                        final int originalHeight = pixmap.getHeight();
                        int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                        int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                        final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                        potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                        pixmap.dispose();
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                Texture tex = new Texture(potPixmap);
                                tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                                image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                                User.data.facebookPicture = new Image(image);
                                User.data.imageId = 48;
                                facebookHeroIcon = User.data.facebookPicture;
                                facebookHeroIcon.setBounds(400, 22, 60, 60);
                                facebookPictureCell.getWhiteCell().addActor(facebookHeroIcon);
                                memoGameIconsCell.getWhiteCell().removeActor(heroIcon);
                                Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                                pictureFrame.setBounds(398, 20, 64, 64);
                                facebookPictureCell.getWhiteCell().addActor(pictureFrame);
                            }
                        });
                    }
                }
            }).start();
        } else {

            TaskCallback taskCallback = new TaskCallback(memo, mocdc, this);
            taskCallback.setCalledToLogIn(true);
            try {
                memo.getFacebookService().logIn(taskCallback);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    private void onPictureProfileClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);

        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);

        memo.setScreen(new PictureProfileView(this.memo, this.mocdc), transition);
    }

    public void renderImage() {

        if (image != null) {
            batcher.begin();
            batcher.draw(image, 100, 100);
            batcher.end();
            System.out.println("RENDERIZZO IMMAGINE!!!!!");
        } else {
            batcher.begin();
            zighia36.draw(batcher, "Downloading...", 10, 881);
            batcher.end();
            stage.addActor(new Label("DOWNLOADING!!!!!", memo.patchedSkin, "50black"));

        }
    }


}
