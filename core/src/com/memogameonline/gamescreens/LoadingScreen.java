package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;

public class LoadingScreen extends AbstractScreen {

	private SpriteBatch batcher;
	private Texture background;
	private boolean assetLoadingStarted;
	private boolean userDataLoadingStarted;

	public LoadingScreen(MemoGameOnline memo, MOCDataCarrier mocdc) {
		super(memo,mocdc);

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void render(float runTime) {

		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batcher.begin();
		batcher.draw(background, 0, 0);
		batcher.end();


	}

	private void loadAsset() {


	}

	@Override
	public void show() {
		batcher = new SpriteBatch();
		background = new Texture(
				Gdx.files.internal("loadingScreen/loading.png"));
		assetLoadingStarted = false;
		userDataLoadingStarted = false;
		System.out.println("LOADING SCREEN HIDEEE");
		loadAsset();

	}

	@Override
	public void hide() {
		System.out.println("LOADING SCREEN HIDEEE");
		batcher.dispose();

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	public void checkCompeltedAssetsLoading() {

		//if (CardsAsset.instance.isCompletedLoading()) {
			//memo.setScreen(new SignIn(memo, mocdc));
			// memo.setScreen(new Home(memo,mocdc));
		//}
	}

	@Override
	public void dispose() {
		System.out.println("LOADING SCREEN DISPOSE");
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

}
