package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.Achievement;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class PowerUp extends AbstractScreen {


    private Window waitingForServerConfirmationPopUp;
    private Table powerUpTab;
    private ScrollPane powerUpScrollPane;
    private Table powerUpScrollPaneContainer;

    private Table achievementTab;
    private ScrollPane achievementScrollPane;
    private Table achievementScrollPaneContainer;

    private Window purchaseWindow;
    private int selectedPowerUpType;
    private int selectedPowerUpPrice;

    private Image powerUpLabel;
    private Image achievementLabel;


    private Image doubleCouple;
    private Image time;
    private Image faceUp;
    private Image stop;
    private Image chain;
    private Image bonus;

    private Label numberOfDoubleCouple;
    private Label numberOfTime;
    private Label numberOfFaceUp;
    private Label numberOfStop;
    private Label numberOfChain;
    private Label numberOfBonus;

    private Button doubleCoupleBuyingButton;
    private Button timeBuyingButton;
    private Button faceUpBuyingButton;
    private Button stopBuyingButton;
    private Button chainBuyingButton;
    private Button bonusBuyingButton;

    private final static int powerUpDoubleCouplePrice = 1;
    private final static int powerUpTimePrice = 2;
    private final static int powerUpFaceUpPrice = 4;
    private final static int powerUpStopPrice = 3;
    private final static int powerUpChainPrice = 2;
    private final static int powerUpBonusPrice = 1;

    private Label textPriceLabel;

    private String doubleCoupleDescriptionText = "Activate  the         \n             in  the  1st\nRound and another\n couple is removed!";
    private String timeDescriptionText = " Activate     \n to gain 10 extra seconds!";
    private String faceUpDescriptionText = " Activate the       \n  and all cards become \n face up for 2 seconds";
    private String stopDescriptionText = " Activate the             in\nthe Second Round and the\n cards will stop moving!";
    private String chainDescriptionText = " Activate the             in the\nThird Round and create\n chains of different cards!";
    private String bonusDescriptionText = " Activate the      \nin the Third Round to get\nextra points!";

    private String[] achievementName = new String[17];
    private String[] achievementDescription = new String[17];

    private Achievement winMatch;
    private Achievement level5;
    private Achievement streak3;
    private Achievement win10Match;
    private Achievement level10;
    private Achievement streak6;
    private Achievement win25Match;
    private Achievement level25;
    private Achievement streak10;
    private Achievement win50Match;
    private Achievement level50;
    private Achievement streak15;
    private Achievement win100Match;
    private Achievement level100;
    private Achievement streak25;
    private Achievement win200Match;
    private Achievement win300Match;

    private final static float popInTimeDuration = 0.1f;

    private int cheatCounter;
    private boolean waitingForServerConfirmation;
    private float confirmationServerTime;
    private int maxWaitingGiveUpConfirmationTime = 10;
    private float minWaitingGiveUpConfirmationTime = 1.5f;

    public PowerUp(final MemoGameOnline memo, final MOCDataCarrier mocdc, final Long matchId, final Opponent opponent) {
        super(memo, mocdc);
        TAG = "PowerUp";
        this.matchID = matchId;
        this.opponent = opponent;
        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    if (matchID == -1) {
                        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                                ScreenTransitionSlide.RIGHT, false, interpolation);
                        memo.setScreen(new Home(memo, mocdc), transition);
                    } else {
                        ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                                ScreenTransitionSlide.UP, true, interpolation);
                        memo.setScreen(new GameStatus(memo, matchID, opponent, mocdc, null), transition);
                    }
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);

        //User.data.pills += 100;
    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));


    }

    @Override
    public void show() {

        defineAchievementName();
        rebuildStage();


    }

    public void defineAchievementName() {
        achievementName[0] = "Reach level 5";
        achievementName[1] = "Reach level 10";
        achievementName[2] = "Reach level 25";
        achievementName[3] = "Reach level 50";
        achievementName[4] = "Reach level 100";
        achievementName[5] = "Win a Match";
        achievementName[6] = "Win 10 Matches";
        achievementName[7] = "Win 25 Matches";
        achievementName[8] = "Win 50 Matches";
        achievementName[9] = "Win 100 Matches";
        achievementName[10] = "Win 200 Matches";
        achievementName[11] = "Win 300 Matches";
        achievementName[12] = "3 Match winning streaks";
        achievementName[13] = "6 Match winning streaks";
        achievementName[14] = "10 Match winning streaks";
        achievementName[15] = "15 Match winning streaks";
        achievementName[16] = "25 Match winning streaks";

    }

    private Table buildSwitchingLabels() {

        Table layer = new Table();
        powerUpLabel = new Image(memo.patchedSkin, "PowerUpPowerUpFrameLabel");
        powerUpLabel.setPosition(22, 702);
        powerUpLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switchToPowerUpTab();

            }
        });
        layer.addActor(powerUpLabel);

        achievementLabel = new Image(memo.patchedSkin, "PowerUpAchievementFrameLabel");
        achievementLabel.setPosition(270, 702);
        achievementLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                switchToAchievementTab();
                //screen.removeActor(achievementLabel);
                // screen.addActor(achievementLabel);

            }
        });
        layer.addActor(achievementLabel);

        return layer;

    }

    private void rebuildStage() {

        screen.setBackground("PowerUpBackground");
        screen.addActor(buildImagesLayer());
        screen.addActor(buildAchievementTab());
        screen.addActor(buildPowerUpTab());
        screen.addActor(buildSwitchingLabels());

        // table.addActor(buildPurchaseWindow());
        stage.clear();
        stage.addActor(screen);
        // stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        Label logInName = new Label("Loggato come: " + User.data.userName,
                memo.patchedSkin, "28black");
        logInName.setPosition(95, 900);
        layer.addActor(logInName);

        return layer;
    }

    private Table buildPowerUpTab() {

        System.out.println("COSTRUISCO TAB POWERUP!!!");

        powerUpTab = new Table(memo.patchedSkin);
        powerUpTab.setBackground("PowerUpPowerUpBackground");
        // powerUpTab.setBounds(20, 78, 481, 800);

        // PRIMO POWER UP !!!
        doubleCouple = new Image(memo.patchedSkin, "PowerUpDoubleCouple");
        numberOfDoubleCouple = new Label("" + User.data.powerUp_DoubleCouple, memo.patchedSkin,
                "45red");
        numberOfDoubleCouple.setPosition(152, 1315);

        doubleCoupleBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        doubleCoupleBuyingButton.add(new Label("" + powerUpDoubleCouplePrice, memo.patchedSkin,
                "60white")).padLeft(-30);
        doubleCoupleBuyingButton.setPosition(30, 1125);
        doubleCoupleBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_DoubleCouple < 10)
                    onBuyClicked(powerUpDoubleCouplePrice, 1, x, y);
                if (cheatCounter == 0 || cheatCounter == 11)
                    cheatCounter++;
                else cheatCounter = 0;
                if (cheatCounter == 12)
                    User.data.pills = +100;
            }
        });
        Label doubleCoupleDescription = new Label(doubleCoupleDescriptionText,
                memo.patchedSkin, "40white");
        doubleCoupleDescription.setPosition(195, 1230);
        doubleCouple.setPosition(35, 1205);

        Label doubleCoupleName1 = new Label("Double", memo.patchedSkin, "40red");
        Label doubleCoupleName2 = new Label("Couple", memo.patchedSkin, "40red");

        // SECONDO POWER UP !!!
        time = new Image(memo.patchedSkin, "PowerUpTime");
        numberOfTime = new Label("" + User.data.powerUp_Time, memo.patchedSkin, "45red");
        numberOfTime.setPosition(340, 1090);

        timeBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        timeBuyingButton.add(new Label("" + powerUpTimePrice, memo.patchedSkin, "60white")).padLeft(-30);
        timeBuyingButton.setPosition(320, 900);

        timeBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_Time < 10)
                    onBuyClicked(powerUpTimePrice, 2, x, y);
                if (cheatCounter == 1 || cheatCounter == 10)
                    cheatCounter++;
                else cheatCounter = 0;
            }
        });
        Label timeDescription = new Label(timeDescriptionText, memo.patchedSkin,
                "40white");
        timeDescription.setPosition(30, 1005);
        time.setPosition(325, 980);
        Label timeName = new Label("Time", memo.patchedSkin, "40red");

        // THIRD POWER UP !!!
        faceUp = new Image(memo.patchedSkin, "PowerUpFaceUp");
        numberOfFaceUp = new Label("" + User.data.powerUp_FaceUp, memo.patchedSkin,
                "45red");
        numberOfFaceUp.setPosition(152, 875);

        faceUpBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        faceUpBuyingButton.add(new Label("" + powerUpFaceUpPrice, memo.patchedSkin,
                "60white")).padLeft(-30);
        faceUpBuyingButton.setPosition(30, 685);
        faceUpBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_FaceUp < 10)
                    onBuyClicked(powerUpFaceUpPrice, 3, x, y);
                if (cheatCounter == 2 || cheatCounter == 9)
                    cheatCounter++;
                else cheatCounter = 0;
            }
        });
        Label faceUpDescription = new Label(faceUpDescriptionText, memo.patchedSkin,
                "40white");
        faceUpDescription.setPosition(195, 790);
        faceUp.setPosition(35, 765);
        Label faceUpName = new Label("FaceUp", memo.patchedSkin, "40red");

        // FOURTH POWER UP
        stop = new Image(memo.patchedSkin, "PowerUpStop");
        numberOfStop = new Label("" + User.data.powerUp_Stop, memo.patchedSkin, "45red");
        numberOfStop.setPosition(340, 650);

        stopBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        stopBuyingButton.add(new Label("" + powerUpStopPrice, memo.patchedSkin, "60white")).padLeft(-30);
        stopBuyingButton.setPosition(320, 450);
        stopBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_Stop < 10)
                    onBuyClicked(powerUpStopPrice, 4, x, y);
                if (cheatCounter == 3 || cheatCounter == 8)
                    cheatCounter++;
                else cheatCounter = 0;
            }
        });

        Label stopDescription = new Label(stopDescriptionText,
                memo.patchedSkin, "40white");
        stopDescription.setPosition(30, 565);
        stop.setPosition(325, 540);
        Label stopName = new Label("Stop", memo.patchedSkin, "40red");

        // FIFTH POWER UP
        chain = new Image(memo.patchedSkin, "PowerUpChain");
        numberOfChain = new Label("" + User.data.powerUp_Chain, memo.patchedSkin, "45red");
        numberOfChain.setPosition(152, 425);
        chainBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        chainBuyingButton.add(new Label("" + powerUpChainPrice, memo.patchedSkin, "60white")).padLeft(-30);
        chainBuyingButton.setPosition(30, 230);
        chainBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_Chain < 10)
                    onBuyClicked(powerUpChainPrice, 5, x, y);
                if (cheatCounter == 4 || cheatCounter == 7)
                    cheatCounter++;
                else cheatCounter = 0;
            }
        });
        Label chainDescription = new Label(chainDescriptionText, memo.patchedSkin,
                "40white");
        chainDescription.setPosition(195, 340);
        chain.setPosition(35, 315);
        Label chainName = new Label("Chain", memo.patchedSkin, "40red");

        // SIXTH POWER UP
        bonus = new Image(memo.patchedSkin, "PowerUpBonus");
        numberOfBonus = new Label("" + User.data.powerUp_Bonus, memo.patchedSkin, "45red");
        numberOfBonus.setPosition(340, 200);

        bonusBuyingButton = new Button(memo.patchedSkin, "BuyPowerUpButton");
        bonusBuyingButton.add(new Label("" + powerUpBonusPrice, memo.patchedSkin, "60white")).padLeft(-30);
        bonusBuyingButton.setPosition(320, 10);
        bonusBuyingButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (User.data.powerUp_Bonus < 10)
                    onBuyClicked(powerUpBonusPrice, 6, x, y);
                if (cheatCounter == 5 || cheatCounter == 6)
                    cheatCounter++;
                else cheatCounter = 0;
            }
        });
        Label bonusDescription = new Label(bonusDescriptionText, memo.patchedSkin,
                "40white");
        bonusDescription.setPosition(30, 115);
        bonus.setPosition(325, 90);
        Label bonusName = new Label("Bonus", memo.patchedSkin, "40red");

        powerUpTab.add(doubleCouple).padRight(270).row();
        powerUpTab.add(doubleCoupleBuyingButton).padRight(288).padTop(4).row();
        powerUpTab.add(doubleCoupleDescription).padLeft(100).padTop(-295).row();
        powerUpTab.add(numberOfDoubleCouple).padRight(167).padTop(-415).row();
        powerUpTab.add(doubleCoupleName1).padLeft(290).padTop(-420).row();
        powerUpTab.add(doubleCoupleName2).padLeft(-30).padTop(-335).row();
        powerUpTab.add(time).padLeft(270).row();
        powerUpTab.add(timeBuyingButton).padLeft(288).padTop(4).row();
        powerUpTab.add(timeDescription).padRight(165).padTop(-265).row();
        powerUpTab.add(numberOfTime).padLeft(167).padTop(-415).row();
        powerUpTab.add(timeName).padRight(155).padTop(-307).row();
        powerUpTab.add(faceUp).padRight(270).row();
        powerUpTab.add(faceUpBuyingButton).padRight(288).padTop(4).row();
        powerUpTab.add(faceUpDescription).padLeft(165).padTop(-300).row();
        powerUpTab.add(numberOfFaceUp).padRight(167).padTop(-415).row();
        powerUpTab.add(faceUpName).padLeft(315).padTop(-382).row();
        powerUpTab.add(stop).padLeft(270).row();
        powerUpTab.add(stopBuyingButton).padLeft(288).padTop(4).row();
        powerUpTab.add(stopDescription).padRight(165).padTop(-300).row();
        powerUpTab.add(stopName).padRight(75).padTop(-382).row();
        powerUpTab.add(numberOfStop).padLeft(167).padTop(-415).row();
        powerUpTab.add(chain).padRight(270).row();
        powerUpTab.add(chainBuyingButton).padRight(288).padTop(4).row();
        powerUpTab.add(chainDescription).padLeft(165).padTop(-300).row();
        powerUpTab.add(chainName).padLeft(250).padTop(-382).row();
        powerUpTab.add(numberOfChain).padRight(167).padTop(-415).row();
        powerUpTab.add(bonus).padLeft(270).row();
        powerUpTab.add(bonusBuyingButton).padLeft(288).padTop(4).row();
        powerUpTab.add(bonusDescription).padRight(165).padTop(-300).row();
        powerUpTab.add(numberOfBonus).padLeft(167).padTop(-415).row();
        powerUpTab.add(bonusName).padRight(50).padTop(-382).row();

        // powerUpTab.addActor(numberOfDoubleCouple);
        // powerUpTab.addActor(numberOfTime);
        // powerUpTab.addActor(numberOfFaceUp);
        // powerUpTab.addActor(numberOfStop);
        // powerUpTab.addActor(numberOfChain);
        // powerUpTab.addActor(numberOfBonus);

        powerUpScrollPane = new ScrollPane(powerUpTab, memo.patchedSkin,
                "scrollPane");
        // powerUpScrollPane.setScrollBarPositions(true, false);
        // powerUpScrollPane.setFadeScrollBars(false);
        // powerUpScrollPane.setScrollbarsOnTop(false);
        // powerUpScrollPane.setClamp(true);
        powerUpScrollPane.setupOverscroll(0, 0, 0);
        // powerUpScrollPane.setTransform(true);

        powerUpScrollPaneContainer = new Table(memo.patchedSkin);
        powerUpScrollPaneContainer.setBounds(26, 14, 489, 703);

        powerUpScrollPaneContainer.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("CLICCO!!!!!!");
            }
        });

        Image frame = new Image(memo.patchedSkin, "PowerUpPowerUpFrame");
        frame.setBounds(-4, -6.5f, 502, 715);
        frame.setTouchable(Touchable.disabled);

        powerUpScrollPaneContainer.add(powerUpScrollPane).expand().fill()
                .colspan(9);
        powerUpScrollPaneContainer.addActor(frame);


        return powerUpScrollPaneContainer;

    }

    private Table buildAchievementTab() {

        achievementTab = new Table(memo.patchedSkin);
        achievementTab.setBackground("PowerUpAchievementBackground");
        //achievementTab.setBounds(20, 78, 500, 800);
        // Button firstCell = new Button(patchedSkin, "achievementCell");
        // firstCell.setPosition(10, 1350);
        // firstCell.addActor(new Label("First Cell", memo.patchedSkin,
        // "36blackBold"));
        // achievementTab.addActor(firstCell);

        winMatch = new Achievement(memo, memo.patchedSkin,
                achievementName[5], User.data.wonMatches, 1);
        achievementTab.add(winMatch.getCell()).padBottom(-13).row();
        achievementTab.row();

        level5 = new Achievement(memo, memo.patchedSkin,
                achievementName[0], User.data.level, 5);
        achievementTab.add(level5.getCell()).padBottom(-13).row();
        achievementTab.row();

        streak3 = new Achievement(memo, memo.patchedSkin,
                achievementName[12], User.data.bestMatchWinningStreak, 3);
        achievementTab.add(streak3.getCell()).padBottom(-13).row();
        achievementTab.row();


        win10Match = new Achievement(memo, memo.patchedSkin,
                achievementName[6], User.data.wonMatches, 10);
        achievementTab.add(win10Match.getCell()).padBottom(-13).row();
        achievementTab.row();

        level10 = new Achievement(memo, memo.patchedSkin,
                achievementName[1], User.data.level, 10);
        achievementTab.add(level10.getCell()).padBottom(-13).row();
        achievementTab.row();

        streak6 = new Achievement(memo, memo.patchedSkin,
                achievementName[13], User.data.bestMatchWinningStreak, 6);
        achievementTab.add(streak6.getCell()).padBottom(-13).row();
        achievementTab.row();


        win25Match = new Achievement(memo, memo.patchedSkin,
                achievementName[7], User.data.wonMatches, 25);
        achievementTab.add(win25Match.getCell()).padBottom(-13).row();
        achievementTab.row();

        level25 = new Achievement(memo, memo.patchedSkin,
                achievementName[2], User.data.level, 25);
        achievementTab.add(level25.getCell()).padBottom(-13).row();
        achievementTab.row();

        streak10 = new Achievement(memo, memo.patchedSkin,
                achievementName[14], User.data.bestMatchWinningStreak, 10);
        achievementTab.add(streak10.getCell()).padBottom(-13).row();
        achievementTab.row();


        win50Match = new Achievement(memo, memo.patchedSkin,
                achievementName[8], User.data.wonMatches, 50);
        achievementTab.add(win50Match.getCell()).padBottom(-13).row();
        achievementTab.row();

        level50 = new Achievement(memo, memo.patchedSkin,
                achievementName[3], User.data.level, 50);
        achievementTab.add(level50.getCell()).padBottom(-13).row();
        achievementTab.row();

        streak15 = new Achievement(memo, memo.patchedSkin,
                achievementName[15], User.data.bestMatchWinningStreak, 15);
        achievementTab.add(streak15.getCell()).padBottom(-13).row();
        achievementTab.row();

        win100Match = new Achievement(memo, memo.patchedSkin,
                achievementName[9], User.data.wonMatches, 100);
        achievementTab.add(win100Match.getCell()).padBottom(-13).row();
        achievementTab.row();

        level100 = new Achievement(memo, memo.patchedSkin,
                achievementName[4], User.data.level, 100);
        achievementTab.add(level100.getCell()).padBottom(-13).row();
        achievementTab.row();

        streak25 = new Achievement(memo, memo.patchedSkin,
                achievementName[16], User.data.bestMatchWinningStreak, 25);
        achievementTab.add(streak25.getCell()).padBottom(-13).row();
        achievementTab.row();

        win200Match = new Achievement(memo, memo.patchedSkin,
                achievementName[10], User.data.wonMatches, 200);
        achievementTab.add(win200Match.getCell()).padBottom(-13).row();
        achievementTab.row();

        win300Match = new Achievement(memo, memo.patchedSkin,
                achievementName[11], User.data.wonMatches, 300);
        achievementTab.add(win300Match.getCell()).padBottom(-13).row();
        achievementTab.row();


        //Costruisco lo ScrollPane
        achievementScrollPane = new ScrollPane(achievementTab,
                memo.patchedSkin, "scrollPane");
        // achievementScrollPane.setScrollBarPositions(true, false);
        // achievementScrollPane.setFadeScrollBars(false);
        //achievementScrollPane.setScrollbarsOnTop(false);
        achievementScrollPane.setupOverscroll(0, 0, 0);
        achievementScrollPaneContainer = new Table();
        achievementScrollPaneContainer.setBounds(26, 14, 496, 703);

        Image frame = new Image(memo.patchedSkin, "PowerUpAchievementFrame");
        frame.setBounds(-4, -6.5f, 502, 715);
        frame.setTouchable(Touchable.disabled);

        achievementScrollPaneContainer.add(achievementScrollPane).expand().fill().colspan(9);
        achievementScrollPaneContainer.addActor(frame);
        achievementScrollPaneContainer.setVisible(false);
        return achievementScrollPaneContainer;

    }

    public Table buildPurchaseWindow(int price) {

        purchaseWindow = new Window("", memo.patchedSkin);
        purchaseWindow.setBounds(62, 374, 416, 280);
        purchaseWindow.setBackground("BlackBackground");
        purchaseWindow.setOrigin(purchaseWindow.getWidth() / 2, purchaseWindow.getHeight() / 2);
        purchaseWindow.setTransform(true);

        Image pills = new Image(memo.patchedSkin, "pillsCan");
        pills.setBounds(235, 125, 65, 75);
        purchaseWindow.addActor(pills);

        Label questionLabel = new Label("  Do  you  want  to  buy  the", memo.patchedSkin, "50white");
        purchaseWindow.addActor(questionLabel);
        questionLabel.setPosition(10, 190);
        textPriceLabel = new Label("Power Up  for      " + price + "     ?", memo.patchedSkin, "50white");
        purchaseWindow.addActor(textPriceLabel);
        textPriceLabel.setPosition(10, 130);


        Button accept = new Button(memo.patchedSkin, "RedButton");
        accept.add(new Label("Accept", memo.patchedSkin, "50white"));
        accept.setBounds(184, 3, 185, 100);
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onConfirmClicked();
            }
        });

        Button cancel = new Button(memo.patchedSkin, "GreyButton");
        cancel.add(new Label("cancel", memo.patchedSkin, "36white"));
        cancel.setBounds(44, 10, 120, 78);
        cancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onCancelClicked();
            }
        });


        purchaseWindow.addActor(accept);
        purchaseWindow.addActor(cancel);
        purchaseWindow.setScale(0, 0);
        purchaseWindow.setModal(true);
        return purchaseWindow;
    }

    public void onConfirmClicked() {

        if (selectedPowerUpType == 1) {
            String[] strings = {"0605", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_DoubleCouple += 1;
            numberOfDoubleCouple.setText("" + User.data.powerUp_DoubleCouple);
        } else if (selectedPowerUpType == 2) {
            String[] strings = {"0604", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_Time += 1;
            numberOfTime.setText("" + User.data.powerUp_Time);
        } else if (selectedPowerUpType == 3) {
            String[] strings = {"0606", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_FaceUp += 1;
            numberOfFaceUp.setText("" + User.data.powerUp_FaceUp);
        } else if (selectedPowerUpType == 4) {
            String[] strings = {"0607", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_Stop += 1;
            numberOfStop.setText("" + User.data.powerUp_Stop);
        } else if (selectedPowerUpType == 5) {
            String[] strings = {"0608", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_Chain += 1;
            numberOfChain.setText("" + User.data.powerUp_Chain);
        } else if (selectedPowerUpType == 6) {
            String[] strings = {"0609", String.valueOf(selectedPowerUpPrice)};
            mocdc.sendToServer(strings);
            User.data.powerUp_Bonus += 1;
            numberOfBonus.setText("" + User.data.powerUp_Bonus);
        }

        purchaseWindow.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                purchaseWindow.setModal(false);
                if (waitingForServerConfirmationPopUp == null)
                    stage.addActor(buildWaitingForServerConfirmationPopUp());
                waitingForServerConfirmationPopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                waitingForServerConfirmation = true;
                waitingForServerConfirmationPopUp.setModal(true);


            }
        })));

    }

    public void onCancelClicked() {
        purchaseWindow.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                purchaseWindow.setVisible(false);
                purchaseWindow.setModal(false);
            }
        })));
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    public void switchToAchievementTab() {
        AudioManager.instance
                .play(CardsAsset.instance.sounds.pressedButton);
        achievementScrollPaneContainer.setVisible(true);
        powerUpScrollPaneContainer.setVisible(false);
        winMatch.startBarAnimation();
        level5.startBarAnimation();
        streak3.startBarAnimation();
        win10Match.startBarAnimation();
        level10.startBarAnimation();
        streak6.startBarAnimation();
        win25Match.startBarAnimation();
        level25.startBarAnimation();
        streak10.startBarAnimation();
        win50Match.startBarAnimation();
        level50.startBarAnimation();
        streak15.startBarAnimation();
        win100Match.startBarAnimation();
        level100.startBarAnimation();
        streak25.startBarAnimation();
        win200Match.startBarAnimation();
        win300Match.startBarAnimation();

    }

    public void switchToPowerUpTab() {
        AudioManager.instance
                .play(CardsAsset.instance.sounds.pressedButton);
        achievementScrollPaneContainer.setVisible(false);
        powerUpScrollPaneContainer.setVisible(true);

        winMatch.resetBar();
        level5.resetBar();
        streak3.resetBar();
        win10Match.resetBar();
        level10.resetBar();
        streak6.resetBar();
        win25Match.resetBar();
        level25.resetBar();
        streak10.resetBar();
        win50Match.resetBar();
        level50.resetBar();
        streak15.resetBar();
        win100Match.resetBar();
        level100.resetBar();
        streak25.resetBar();
        win200Match.resetBar();
        win300Match.resetBar();
    }

    public void onBuyClicked(int price, int powerUpType, float x, float y) {
        if (price <= User.data.pills) {
            this.selectedPowerUpType = powerUpType;
            this.selectedPowerUpPrice = price;
            if (purchaseWindow == null) {
                stage.addActor(buildPurchaseWindow(price));
            }
            purchaseWindow.setVisible(true);
            purchaseWindow.addAction(scaleTo(1, 1, popInTimeDuration));
            purchaseWindow.setModal(true);
            textPriceLabel.setText("  Power  Up  for      "
                    + price + "       ?");

            selectedPowerUpType = powerUpType;
            this.selectedPowerUpPrice = price;
        }

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    public Table buildWaitingForServerConfirmationPopUp() {

        waitingForServerConfirmationPopUp = new Window("", memo.patchedSkin);
        waitingForServerConfirmationPopUp.setSize(200,150);
        waitingForServerConfirmationPopUp.setPosition((540-waitingForServerConfirmationPopUp.getWidth())/2, 374);
        waitingForServerConfirmationPopUp.setBackground("BlackBackground");
        waitingForServerConfirmationPopUp.setOrigin(waitingForServerConfirmationPopUp.getWidth() / 2, waitingForServerConfirmationPopUp.getHeight() / 2);
        waitingForServerConfirmationPopUp.setTransform(true);
        Label info = new Label("Wait...", memo.patchedSkin, "50white");
        waitingForServerConfirmationPopUp.add(info);
        info.setPosition(40, 50);
        waitingForServerConfirmationPopUp.setScale(0, 0);

        waitingForServerConfirmationPopUp.addAction(sequence(scaleTo(1, 1, popInTimeDuration), run(new Runnable() {
            public void run() {
                waitingForServerConfirmationPopUp.setVisible(true);

            }
        })));

        waitingForServerConfirmationPopUp.setModal(true);
        return waitingForServerConfirmationPopUp;
    }

    @Override
    public void checkServerMessage() {


        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80 || mocdc.cmdToExecute.first() == 98 || mocdc.cmdToExecute.first() == 99) {
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH
                // SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 3) { // OPPONENT REFUSE A CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 2) { // OPPONENT FOUND, CREATE A MATCH!!!

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);


                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA compsota di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////

                User.data.searchingForRandomOpponent = false;
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 6) { // Match CLOSED BY OTHER PLAYER
                Long matchID = Long.valueOf(mocdc.dataToExecute.first()[0]);
                if (User.data.getMatch(matchID) != null) {
                    User.data.closeMatch(matchID);
                    User.data.getMatch(matchID).setMatchWon(false);
                    User.data.getMatch(matchID).setFinished(true);
                    User.data.getMatch(matchID).setFinishedForGiveUp(true);
                    DataManager.instance.saveData();
                }
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        User.data.challengeFromFriendReceived = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27
                    || mocdc.cmdToExecute.first() >= 80 && mocdc.cmdToExecute.first() <= 99) { // DISCARD MESSAGE!!!
                System.out.println("ABStract!!!!");
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }


        }

        if (waitingForServerConfirmation) {
            confirmationServerTime = confirmationServerTime
                    + Gdx.graphics.getDeltaTime();
            System.out.println(confirmationServerTime);
            if (mocdc.cmdToExecute.size > 0 && mocdc.cmdToExecute.first() == 8 && this.confirmationServerTime > minWaitingGiveUpConfirmationTime) {

                waitingForServerConfirmationPopUp.addAction(scaleTo(0, 0, popInTimeDuration));
                waitingForServerConfirmationPopUp.setModal(false);
                User.data.pills -= Float.valueOf(mocdc.dataToExecute.first()[0]);
                waitingForServerConfirmation = false;
                confirmationServerTime = 0;
                mocdc.removeMessage();
            } else if (this.confirmationServerTime > maxWaitingGiveUpConfirmationTime) {
                confirmationServerTime = 0;
                waitingForServerConfirmation = false;
                waitingForServerConfirmationPopUp.setVisible(false);
                waitingForServerConfirmationPopUp.setModal(false);
                // if (waitingForGiveUpServerConfirmationERRORPopUp == null)
                // stage.addActor(buildWaitingForServerConfirmationPopUp());
                // waitingForGiveUpServerConfirmationERRORPopUp.setVisible(true);
            }

        }

        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }

}
