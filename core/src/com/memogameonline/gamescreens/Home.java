package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Match;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;


public class Home extends AbstractScreen {

    private Image logo;

    private Button powerUp;
    private Button settings;
    private Button tournament;
    private Button play;

    private Table tournamentPopUp;
    private final static float popInTimeDuration = 0.1f;

    public Home(MemoGameOnline memo, MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "HOME";
        String[] strings = {"9992"};
        mocdc.sendToServer(strings);


        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    Gdx.app.exit();
                }
                return true;
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);

        //memo.getAdsController().showOrLoadInterstital();


    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));


    }

    @Override
    public void show() {
        System.out.println("SHOW SHOW SHOW!!!!!!!!");
        Gdx.input.setOnscreenKeyboardVisible(false);
        rebuildStage();
    }

    @Override
    public void pause() {

        System.out.println("PAUSE PAUSE PAUSE!!!!!!!!");
    }

    public void rebuildStage() {

        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("HomeBackground");
        screen.addActor(buildButtonsLayer());
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        if (randomOpponentChallengePopUp != null)
            stage.addActor(buildRandomOpponentChallengePopUp());

        stage.clear();
        stage.addActor(screen);

        // stage.setDebugAll(true);

        //onSettingsClicked();

    }

    private Table buildButtonsLayer() {

        Table layer = new Table();

        powerUp = new Button(memo.patchedSkin, "VioletButton");
        powerUp.add(new Label("Power  Up", memo.patchedSkin, "60white"));
        powerUp.setBounds(88, 153, 365, 100);
        powerUp.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                ScreenTransition transition = ScreenTransitionSlide.init(duration,
                        ScreenTransitionSlide.LEFT, false, interpolation);
                memo.setScreen(new PowerUp(memo, mocdc, -1l, null), transition);
            }
        });
        layer.addActor(powerUp);

        settings = new Button(memo.patchedSkin, "SettingsButton");
        settings.setBounds(94, 40, 175, 100);
        settings.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSettingsClicked();
            }
        });
        layer.addActor(settings);

        tournament = new Button(memo.patchedSkin, "TournamentButton");
        tournament.setBounds(274, 39, 175, 100);
        tournament.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onTournamentClicked();
            }
        });
        layer.addActor(tournament);

        play = new Button(memo.patchedSkin, "GreenButton");
        play.add(new Label("PLAY", memo.patchedSkin, "100white"));
        play.setBounds(88, 266, 365, 155);
        play.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onPlayClicked();
            }
        });
        layer.addActor(play);

        return layer;
    }

    private void onPlayClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);

        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);

        memo.setScreen(new GameSelection(this.memo, this.mocdc), transition);

    }

    private void onSettingsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);

        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);

        memo.setScreen(new Settings(this.memo, this.mocdc), transition);

    }


    private void onTournamentClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (tournamentPopUp == null)
            stage.addActor(buildTournamentPopUp());
        tournamentPopUp.setVisible(true);
        tournamentPopUp.addAction(sequence(scaleTo(1, 1, popInTimeDuration), run(new Runnable() {
            public void run() {
            }
        })));

    }

    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }

    public Table buildTournamentPopUp() {

        tournamentPopUp = new Table(memo.patchedSkin);
        tournamentPopUp.setBounds(120, 350, 300, 200);
        tournamentPopUp.setBackground("AzureBackground");
        tournamentPopUp.setTransform(true);
        System.out.println(tournament.getX() + tournament.getWidth() / 2);
        System.out.println(tournament.getY() + tournament.getHeight() / 2);
        tournamentPopUp.setOrigin(-tournamentPopUp.getX() + tournament.getX() + tournament.getWidth() / 2, -tournamentPopUp.getY() + tournament.getY() + tournament.getHeight() / 2);
        Label label = new Label("Coming soon!", memo.patchedSkin, "60white");
        tournamentPopUp.addActor(label);
        label.setPosition((tournamentPopUp.getWidth() - label.getWidth()) / 2, 115);

        Button continueButton = new TextButton("Continue", memo.patchedSkin);
        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                tournamentPopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
                    public void run() {
                        tournamentPopUp.setVisible(false);
                    }
                })));
            }
        });
        continueButton.setBounds((tournamentPopUp.getWidth() - continueButton.getWidth()) / 2, 14, 193, 95);


        tournamentPopUp.addActor(continueButton);
        tournamentPopUp.setScale(0, 0);
        tournamentPopUp.setVisible(false);

        return tournamentPopUp;
    }


    @Override
    public void resume() {
        System.out.println("RESUME RESUME RESUME!!!!!!!!");

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    Gdx.app.exit();
                }
                return true;
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);


    }

}
