package com.memogameonline.gamescreens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gameobjects.ParticleEffect_PowerUpSelectionEffect;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class GameStatusPowerUpPopUp {

    private GameStatus gameStatus;
    private Window popUp;
    private Skin skin;

    private Button firstPowerUp;
    private Label firstPowerUpNumber;
    private Button secondPowerUp;
    private Label secondPowerUpNumber;
    private Button thirdPowerUp;
    private Label thirdPowerUpNumber;
    private Button confirm;
    private Button cancel;
    private Button buy;


    private long matchID;
    private int currentRound;

    private ParticleEffect_PowerUpSelectionEffect specialEffectPool;
    private PooledEffect firstPowerUpEffect;
    private PooledEffect secondPowerUpEffect;
    private PooledEffect thirdPowerUpEffect;

    private boolean firstPowerUpSelected;
    private boolean secondPowerUpSelected;
    private boolean thirdPowerUpSelected;

    private final static float popInTimeDuration = 0.1f;

    public GameStatusPowerUpPopUp(GameStatus gameStatus, Skin skin,
                                  long matchID) {

        this.gameStatus = gameStatus;
        this.skin = skin;
        this.matchID = matchID;
        currentRound = User.data.getMatch(matchID).getCurrentRound();

        popUp = new Window("",skin);

        //popUp.setModal(true);

        popUp.setBounds(10, 150, 520, 540);
        popUp.setBackground("VioletBackground");

        popUp.setTransform(true);
        popUp.setOrigin(gameStatus.getPowerUpButton().getX() + gameStatus.getPowerUpButton().getWidth() / 2 - popUp.getX(),
                gameStatus.getPowerUpButton().getY() + gameStatus.getPowerUpButton().getHeight() / 2 - popUp.getY());

        specialEffectPool = new ParticleEffect_PowerUpSelectionEffect();

        Table t = buildButtonLayer();
        popUp.center().bottom();
        popUp.addActor(t);

        popUp.setVisible(false);
        popUp.setScale(0, 0);
        popUp.setModal(true);

        // stage.setDebugAll(true);

        System.out.println(firstPowerUpSelected);
        System.out.println(secondPowerUpSelected);
        System.out.println(thirdPowerUpSelected);



    }

    public Table buildButtonLayer() {

        Table layer = new Table();

        Label windowTitle = new Label("   Power  Up  for  Round  "
                + User.data.getMatch(matchID).getCurrentRound(), skin,
                "48white");
        windowTitle.setPosition(80, 450);
        popUp.addActor(windowTitle);
        Label outOfPowerUp = new Label("Run   out   of   Power   Up?", skin,
                "36white");
        outOfPowerUp.setPosition(150, 85);
        popUp.addActor(outOfPowerUp);

        firstPowerUp = new Button(skin, "PowerUpTimeWithSticker");
        firstPowerUpNumber = new Label("  " + User.data.powerUp_Time,
                skin, "powerUpCounter");

        firstPowerUpNumber.setPosition(51, -6);
        firstPowerUp.addActor(firstPowerUpNumber);
        firstPowerUp.setPosition(36, 261);
        firstPowerUp.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.pressedButton);
                selectFirstPowerUp();
                return true;
            }
        });
        layer.addActor(firstPowerUp);

        if (currentRound == 1 || currentRound == 2) {
            secondPowerUp = new Button(skin, "PowerUpFaceUpWithSticker");
            secondPowerUpNumber = new Label("  " + User.data.powerUp_FaceUp,
                    skin, "powerUpCounter");
        } else if (currentRound == 3) {
            secondPowerUp = new Button(skin, "PowerUpChainWithSticker");
            secondPowerUpNumber = new Label("  " + User.data.powerUp_Chain,
                    skin, "powerUpCounter");
        }

        secondPowerUpNumber.setPosition(51, -6);
        secondPowerUp.addActor(secondPowerUpNumber);
        secondPowerUp.setPosition(188, 261);
        secondPowerUp.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.pressedButton);
                selectSecondPowerUp();
                return true;
            }

        });

        layer.addActor(secondPowerUp);

        if (currentRound == 1) {
            thirdPowerUp = new Button(skin, "PowerUpDoubleCoupleWithSticker");
            thirdPowerUpNumber = new Label("  "
                    + User.data.powerUp_DoubleCouple, skin,
                    "powerUpCounter");
        }
        if (currentRound == 2) {
            thirdPowerUp = new Button(skin, "PowerUpStopWithSticker");
            thirdPowerUpNumber = new Label("  " + User.data.powerUp_Stop,
                    skin, "powerUpCounter");
        }
        if (currentRound == 3) {
            thirdPowerUp = new Button(skin, "PowerUpBonusWithSticker");
            thirdPowerUpNumber = new Label("  " + User.data.powerUp_Bonus,
                    skin, "powerUpCounter");
        }

        thirdPowerUpNumber.setPosition(51, -6);
        thirdPowerUp.addActor(thirdPowerUpNumber);
        thirdPowerUp.setPosition(342, 261);
        thirdPowerUp.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.pressedButton);
                selectThirdPowerUp();
                return true;
            }
        });
        layer.addActor(thirdPowerUp);

        confirm = new Button(skin, "GreenButton");
        confirm.add(new Label("ok", skin, "70white"));
        confirm.setBounds(248, 144, 235, 92);
        confirm.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onConfirmClicked();
            }
        });
        popUp.addActor(confirm);


        cancel = new Button(skin, "RedButton");
        cancel.add(new Label("Cancel", skin, "40white"));
        cancel.setBounds(42, 150, 168, 77);
        cancel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onCancelClicked();
            }
        });
        popUp.addActor(cancel);

        buy = new Button(skin, "RedButton");
        buy.add(new Label("Buy  it!", skin, "40white"));
        buy.setBounds(188, 17, 140, 70);
        buy.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onBuyPowerUpClicked();
            }
        });
        popUp.addActor(buy);

        return layer;

    }

    private void selectFirstPowerUp() {

        if (firstPowerUpSelected) {
            firstPowerUpSelected = false;
            setFirstPowerUpEffect(null);
            User.data.powerUp_Time += 1;
            firstPowerUpNumber.setText("  " + User.data.powerUp_Time);
        } else if (User.data.powerUp_Time > 0) {
            firstPowerUpSelected = true;
            PooledEffect effect = getSpecialEffectPool()
                    .getStarsPool().obtain();
            effect.setPosition(popUp.getX() + firstPowerUp.getX() + 10, popUp.getY() + firstPowerUp.getY() + 43);
            System.out.println("Aggiungo POwerUp in: " + (popUp.getX() + firstPowerUp.getX()) + "   " + (popUp.getY() + firstPowerUp.getY()));
            setFirstPowerUpEffect(effect);
            User.data.powerUp_Time -= 1;
            firstPowerUpNumber.setText("  " + User.data.powerUp_Time);
        }

    }

    private void selectSecondPowerUp() {

        if (secondPowerUpSelected) {
            secondPowerUpSelected = false;
            setSecondPowerUpEffect(null);
            if (currentRound == 1 || currentRound == 2) {
                User.data.powerUp_FaceUp += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_FaceUp);
            } else {
                User.data.powerUp_Chain += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_Chain);
            }

        } else if (User.data.getMatch(matchID).getCurrentRound() == 1 && User.data.powerUp_FaceUp > 0 ||
                User.data.getMatch(matchID).getCurrentRound() == 2 && User.data.powerUp_FaceUp > 0 ||
                User.data.getMatch(matchID).getCurrentRound() == 3 && User.data.powerUp_Chain > 0) {
            secondPowerUpSelected = true;
            PooledEffect effect = getSpecialEffectPool()
                    .getStarsPool().obtain();
            effect.setPosition(popUp.getX() + secondPowerUp.getX() + 10, popUp.getY() + secondPowerUp.getY() + 43);
            setSecondPowerUpEffect(effect);
            System.out.println("Aggiungo POwerUp in: " + (popUp.getX() + secondPowerUp.getX() + 2) + "   " + (popUp.getY() + secondPowerUp.getY()));
            if (currentRound == 1 || currentRound == 2) {
                User.data.powerUp_FaceUp -= 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_FaceUp);
            } else {
                User.data.powerUp_Chain -= 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_Chain);
            }

        }

    }

    private void selectThirdPowerUp() {

        if (thirdPowerUpSelected) {
            thirdPowerUpSelected = false;
            setThirdPowerUpEffect(null);
            if (currentRound == 1) {
                User.data.powerUp_DoubleCouple += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_DoubleCouple);
            } else if (currentRound == 2) {
                User.data.powerUp_Stop += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Stop);
            } else {
                User.data.powerUp_Bonus += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Bonus);
            }

        } else if (User.data.getMatch(matchID).getCurrentRound() == 1 && User.data.powerUp_DoubleCouple > 0 ||
                User.data.getMatch(matchID).getCurrentRound() == 2 && User.data.powerUp_Stop > 0 ||
                User.data.getMatch(matchID).getCurrentRound() == 3 && User.data.powerUp_Bonus > 0) {
            thirdPowerUpSelected = true;
            PooledEffect effect = getSpecialEffectPool()
                    .getStarsPool().obtain();
            effect.setPosition(popUp.getX() + thirdPowerUp.getX() + 10, popUp.getY() + thirdPowerUp.getY() + 43);
            System.out.println("Aggiungo POwerUp in: " + (popUp.getX() + thirdPowerUp.getX() + 2) + "   " + (popUp.getY() + thirdPowerUp.getY()));
            setThirdPowerUpEffect(effect);
            if (currentRound == 1) {
                User.data.powerUp_DoubleCouple -= 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_DoubleCouple);
            } else if (currentRound == 2) {
                User.data.powerUp_Stop -= 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Stop);
            } else {
                User.data.powerUp_Bonus -= 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Bonus);
            }
        }

    }

    private void onConfirmClicked() {

        if (firstPowerUpSelected) {
            gameStatus.setFirstPowerUp(true);
            firstPowerUpEffect.free();
            firstPowerUpEffect = null;
            //getFirstPowerUpEffect().free();
            //setFirstPowerUpEffect(null);
        } else {
            gameStatus.setFirstPowerUp(false);
        }
        if (secondPowerUpSelected) {
            gameStatus.setSecondPowerUp(true);
            secondPowerUpEffect.free();
            secondPowerUpEffect = null;
            //getSecondPowerUpEffect().free();
            // setSecondPowerUpEffect(null);
        } else {
            gameStatus.setSecondPowerUp(false);
        }
        if (thirdPowerUpSelected) {
            gameStatus.setThirdPowerUp(true);
            thirdPowerUpEffect.free();
            thirdPowerUpEffect = null;
            //getThirdPowerUpEffect().free();
            // setThirdPowerUpEffect(null);
        } else {
            gameStatus.setThirdPowerUp(false);
        }

        popUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                popUp.setVisible(false);
            }
        })));

    }

    private void onCancelClicked() {

        if (firstPowerUpSelected) {
            User.data.powerUp_Time += 1;
            firstPowerUpNumber.setText("  " + User.data.powerUp_Time);
            getFirstPowerUpEffect().free();
            setFirstPowerUpEffect(null);
        }

        if (secondPowerUpSelected) {
            if (currentRound == 1 || currentRound == 2) {
                User.data.powerUp_FaceUp += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_FaceUp);
            } else {
                User.data.powerUp_Chain += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_Chain);
            }
            getSecondPowerUpEffect().free();
            setSecondPowerUpEffect(null);
        }

        if (thirdPowerUpSelected) {
            if (currentRound == 1) {
                User.data.powerUp_DoubleCouple += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_DoubleCouple);
            } else if (currentRound == 2) {
                User.data.powerUp_Stop += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Stop);
            } else {
                User.data.powerUp_Bonus += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Bonus);
            }
            getThirdPowerUpEffect().free();
            setThirdPowerUpEffect(null);
        }

        firstPowerUpSelected = false;
        secondPowerUpSelected = false;
        thirdPowerUpSelected = false;
        gameStatus.setFirstPowerUp(false);
        gameStatus.setSecondPowerUp(false);
        gameStatus.setThirdPowerUp(false);

        popUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                popUp.setVisible(false);
            }
        })));

    }

    public void onBuyPowerUpClicked() {

        if (firstPowerUpSelected) {
            User.data.powerUp_Time += 1;
            firstPowerUpNumber.setText("  " + User.data.powerUp_Time);
            getFirstPowerUpEffect().free();
            setFirstPowerUpEffect(null);
        }

        if (secondPowerUpSelected) {
            if (currentRound == 1 || currentRound == 2) {
                User.data.powerUp_FaceUp += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_FaceUp);
            } else {
                User.data.powerUp_Chain += 1;
                secondPowerUpNumber.setText("  " + User.data.powerUp_Chain);
            }
            getSecondPowerUpEffect().free();
            setSecondPowerUpEffect(null);
        }

        if (thirdPowerUpSelected) {
            if (currentRound == 1) {
                User.data.powerUp_DoubleCouple += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_DoubleCouple);
            } else if (currentRound == 2) {
                User.data.powerUp_Stop += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Stop);
            } else {
                User.data.powerUp_Bonus += 1;
                thirdPowerUpNumber.setText("  " + User.data.powerUp_Bonus);
            }
            getThirdPowerUpEffect().free();
            setThirdPowerUpEffect(null);
        }

        firstPowerUpSelected = false;
        secondPowerUpSelected = false;
        thirdPowerUpSelected = false;
        gameStatus.setFirstPowerUp(false);
        gameStatus.setSecondPowerUp(false);
        gameStatus.setThirdPowerUp(false);


        ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                ScreenTransitionSlide.DOWN, false, gameStatus.interpolation);
        gameStatus.memo.setScreen(new PowerUp(gameStatus.memo, gameStatus.mocdc, gameStatus.matchID, gameStatus.opponent), transition);

    }

    public Button getFirstPowerUp() {
        return firstPowerUp;
    }

    public void setFirstPowerUp(Button firstPowerUp) {
        this.firstPowerUp = firstPowerUp;
    }

    public Button getSecondPowerUp() {
        return secondPowerUp;
    }

    public void setSecondPowerUp(Button secondPowerUp) {
        this.secondPowerUp = secondPowerUp;
    }

    public Button getThirdPowerUp() {
        return thirdPowerUp;
    }

    public void setThirdPowerUp(Button thirdPowerUp) {
        this.thirdPowerUp = thirdPowerUp;
    }

    public Table getPopUp() {
        return popUp;
    }

    public Skin getSkin() {
        return skin;
    }

    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    public Label getFirstPowerUpNumber() {
        return firstPowerUpNumber;
    }

    public void setFirstPowerUpNumber(Label firstPowerUpNumber) {
        this.firstPowerUpNumber = firstPowerUpNumber;
    }

    public Label getSecondPowerUpNumber() {
        return secondPowerUpNumber;
    }

    public void setSecondPowerUpNumber(Label secondPowerUpNumber) {
        this.secondPowerUpNumber = secondPowerUpNumber;
    }

    public Label getThirdPowerUpNumber() {
        return thirdPowerUpNumber;
    }

    public void setThirdPowerUpNumber(Label thirdPowerUpNumber) {
        this.thirdPowerUpNumber = thirdPowerUpNumber;
    }

    public Button getConfirm() {
        return confirm;
    }

    public void setConfirm(Button confirm) {
        this.confirm = confirm;
    }

    public Button getCancel() {
        return cancel;
    }

    public void setCancel(Button cancel) {
        this.cancel = cancel;
    }

    public Button getBuy() {
        return buy;
    }

    public void setBuy(Button buy) {
        this.buy = buy;
    }

    public long getMatchID() {
        return matchID;
    }

    public void setMatchID(long matchID) {
        this.matchID = matchID;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public ParticleEffect_PowerUpSelectionEffect getSpecialEffectPool() {
        return specialEffectPool;
    }

    public void setSpecialEffectPool(ParticleEffect_PowerUpSelectionEffect specialEffectPool) {
        this.specialEffectPool = specialEffectPool;
    }

    public PooledEffect getFirstPowerUpEffect() {
        return firstPowerUpEffect;
    }

    public void setFirstPowerUpEffect(PooledEffect firstPowerUpEffect) {
        this.firstPowerUpEffect = firstPowerUpEffect;
    }

    public PooledEffect getSecondPowerUpEffect() {
        return secondPowerUpEffect;
    }

    public void setSecondPowerUpEffect(PooledEffect secondPowerUpEffect) {
        this.secondPowerUpEffect = secondPowerUpEffect;
    }

    public PooledEffect getThirdPowerUpEffect() {
        return thirdPowerUpEffect;
    }

    public void setThirdPowerUpEffect(PooledEffect thirdPowerUpEffect) {
        this.thirdPowerUpEffect = thirdPowerUpEffect;
    }

    public boolean isFirstPowerUpSelected() {
        return firstPowerUpSelected;
    }

    public void setFirstPowerUpSelected(boolean firstPowerUpSelected) {
        this.firstPowerUpSelected = firstPowerUpSelected;
    }

    public boolean isSecondPowerUpSelected() {
        return secondPowerUpSelected;
    }

    public void setSecondPowerUpSelected(boolean secondPowerUpSelected) {
        this.secondPowerUpSelected = secondPowerUpSelected;
    }

    public boolean isThirdPowerUpSelected() {
        return thirdPowerUpSelected;
    }

    public void setThirdPowerUpSelected(boolean thirdPowerUpSelected) {
        this.thirdPowerUpSelected = thirdPowerUpSelected;
    }
}
