package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.ClassicInputHandler;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamehelpers.MemoFiendsInputHandler;
import com.memogameonline.gamehelpers.MemobileInputHandler;
import com.memogameonline.gametable.Classic;
import com.memogameonline.gametable.ClassicRenderer;
import com.memogameonline.gametable.MemoFiends;
import com.memogameonline.gametable.MemoFiendsRenderer;
import com.memogameonline.gametable.Memobile;
import com.memogameonline.gametable.MemobileRenderer;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

public class GameScreen extends AbstractScreen {
    public static final String TAG = GameScreen.class.getName();

    private long matchID;
    private Classic classicTable;
    private Memobile memobileTable;
    private MemoFiends memofiendsTable;

    private ClassicRenderer classicRenderer;
    private MemobileRenderer memobileRenderer;
    private MemoFiendsRenderer memofiendsRenderer;

    private float time;
    private int gameMode;
    private boolean[] powerUp;
    private boolean offlineMode;

    public GameScreen(MemoGameOnline memo, MOCDataCarrier mocdc, long matchID, Opponent opponent,
                      int gameMode, boolean[] powerUp, boolean offlineMode) {
        super(memo, mocdc);
        this.matchID = matchID;
        this.opponent = opponent;
        this.gameMode = gameMode;
        this.powerUp = powerUp;
        this.offlineMode = offlineMode;

        multiplexer.addProcessor(stage);

        if (!offlineMode && User.data.getMatch(matchID).getCurrentRound() == 3) {
            memo.getAdsController().loadInterstital();
        }

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (gameMode == 1) {
            classicTable.update(delta);
            classicRenderer.render(delta);
        } else if (gameMode == 2) {
            memobileTable.update(delta);
            memobileRenderer.render(delta);
        } else {
            memofiendsTable.update(delta);
            memofiendsRenderer.render(delta);
        }

        checkServerMessage();

    }

    @Override
    public void resize(int width, int height) {

        if (gameMode == 1) {
            classicRenderer.resize(width, height);
        } else if (gameMode == 2) {
            memobileRenderer.resize(width, height);
        } else {
            memofiendsRenderer.resize(width, height);
        }

    }

    @Override
    public void show() {
        // Gdx.app.log("GameScreen", "show called");
        stage.dispose();

        float classicTime = 40;
        float memobileTime = 45;
        float memoFiendsTime = 40;

        if (gameMode == 1) {

            if (powerUp[0]) {
                String[] strings = {"0610"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[1]) {
                String[] strings = {"0612"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[2]) {
                String[] strings = {"0611"};
                mocdc.sendToServer(strings);
            }

            classicTable = new Classic(memo, mocdc, matchID, opponent, gameMode, classicTime,
                    powerUp, offlineMode);
            classicRenderer = new ClassicRenderer(classicTable, cam, batcher);
            multiplexer.addProcessor(new ClassicInputHandler(memo, mocdc, classicTable));
        } else if (gameMode == 2) {

            if (powerUp[0]) {
                String[] strings = {"0610"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[1]) {
                String[] strings = {"0612"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[2]) {
                String[] strings = {"0613"};
                mocdc.sendToServer(strings);
            }

            memobileTable = new Memobile(memo, mocdc, matchID, opponent, gameMode, memobileTime,
                    powerUp, offlineMode);
            memobileRenderer = new MemobileRenderer(memobileTable, cam, batcher);
            multiplexer.addProcessor(new MemobileInputHandler(memo, mocdc, memobileTable));
        } else {

            if (powerUp[0]) {
                String[] strings = {"0610"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[1]) {
                String[] strings = {"0614"};
                mocdc.sendToServer(strings);
            }
            if (powerUp[2]) {
                String[] strings = {"0615"};
                mocdc.sendToServer(strings);

            }

            memofiendsTable = new MemoFiends(memo, mocdc, matchID, opponent, gameMode,
                    memoFiendsTime, powerUp, 5, offlineMode);
            memofiendsRenderer = new MemoFiendsRenderer(memofiendsTable, cam,
                    batcher);
            multiplexer
                    .addProcessor(new MemoFiendsInputHandler(memo, mocdc, memofiendsTable));
        }

    }

    @Override
    public void pause() {

    }

    @Override
    public void dispose() {

    }


    @Override
    public void resume() {

    }

    @Override
    public void checkServerMessage() {


        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80 || mocdc.cmdToExecute.first() == 98 || mocdc.cmdToExecute.first() == 99) {
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH
                // SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 3) { // OPPONENT REFUSE A CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 2) { // OPPONENT FOUND, CREATE A MATCH!!!

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                User.data.searchingForRandomOpponent = false;
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                String[] sendMsg = {"0207", String.valueOf(challengeMatchID)};
                mocdc.sendToServer(sendMsg);
                User.data.resetAndStopTimerEvent();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED


            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27
                    || mocdc.cmdToExecute.first() >= 80 && mocdc.cmdToExecute.first() <= 99) { // DISCARD MESSAGE!!!
                System.out.println("ABStract!!!!");
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }


        }

    }

}