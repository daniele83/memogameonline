package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.FacebookFriendCell;
import com.memogameonline.gameobjects.MemoFriendCell;
import com.memogameonline.gameobjects.ProfilePictureCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class PictureProfileView extends AbstractScreen {

    private Table friendsCellPanel;
    private ProfilePictureCell[] cellList = new ProfilePictureCell[48];


    public PictureProfileView(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "FacebookFriendsScreen";

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new AccountManagementView(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        checkServerMessage();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

    }

    @Override
    public void show() {
        rebuildStage();
    }

    @Override
    public void pause() {

    }

    public void rebuildStage() {


        friendsCellPanel = buildButtonsLayer();
        stage.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("PowerUpBackground");
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.addActor(friendsCellPanel);
        screen.addActor(buildImagesLayer());
        stage.addActor(screen);

        // stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        Label title = new Label("Choose your icon!", memo.patchedSkin, "40black");
        title.setPosition(170, 800);
        // layer.addActor(topBar);
        layer.addActor(title);

        return layer;
    }


    private Table buildButtonsLayer() {

        Table friendsListGrid = new Table();
        //friendsListGrid.defaults().expandX().fillX();
        boolean firstCellAdded = false;
        int imageIndex = 0;

        for (int i = 0; i < 12; i++) {

            for (int j = 0; j < 4; j++) {
                ProfilePictureCell cell = new ProfilePictureCell(memo, imageIndex);
                cellList[imageIndex] = cell;
                imageIndex++;
                final int tempIndex = imageIndex - 1;
                if (User.data.imageId == tempIndex)
                    cell.getWhiteCell().toggle();
                cell.getWhiteCell().addListener(new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                        User.data.imageId = tempIndex;

                        String[] strings = {"0602", String.valueOf(User.data.imageId)};
                        mocdc.sendToServer(strings);

                        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                                ScreenTransitionSlide.RIGHT, false, interpolation);
                        memo.setScreen(new AccountManagementView(memo, mocdc), transition);
                    }
                });


                if (!firstCellAdded) {
                    friendsListGrid.add(cell.getWhiteCell());
                    firstCellAdded = true;
                } else {
                    friendsListGrid.add(cell.getWhiteCell()).padLeft(25);
                    if (j == 3) {
                        if (i > 2) {
                            Label lab = new Label("Requieres  Level  " + (i * 4) + "  to  be  unlocked!", memo.patchedSkin, "50red");
                            lab.setPosition(-375, 15);
                            cell.getWhiteCell().addActor(lab);
                            for (int k = (7 + (4 * (i - 1))); k > (7 + (4 * (i - 1) - 4)); k--) {
                                cellList[k].removeImage();
                                cellList[k].getWhiteCell().setDisabled(true);
                                System.out.println("i= " + i);
                                System.out.println("k= " + k);
                            }
                        }
                        friendsListGrid.row();
                    }
                }

            }
            firstCellAdded = false;
        }


        ScrollPane scrollPane = new ScrollPane(friendsListGrid,
                memo.patchedSkin, "scrollPane");
        scrollPane.setScrollBarPositions(true, false);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollbarsOnTop(false);


        Table scrollContainer = new Table(memo.patchedSkin);
        scrollContainer.setBounds(0, 10, 540, 726);
        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(4, -2, 530, 730);
        scrollContainer.addActor(background);
        scrollContainer.add(scrollPane).padBottom(0).expand().fill()
                .colspan(4);


        return scrollContainer;

    }


    private void onPlayClicked(FacebookFriendCell cell) {

        String[] strings = {"0400", cell.getFriendFacebookID()};
        mocdc.sendToServer(strings);
        System.out.println("SFIDO " + cell.getFriendName() + " !!!");
        System.out.println("ID FACEBOOK " + cell.getFriendFacebookID() + " !!!");

        User.data.challengedFriendList.add(cell.getFriendName());
        cell.friendHasBeenChallenged();
    }


    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

}

