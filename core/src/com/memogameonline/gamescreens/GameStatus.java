package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.LevelManagement;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.ExperienceBar;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class GameStatus extends AbstractScreen {

    private Table mainLayer;
    private GameStatusPowerUpPopUp powerUpPopUp;
    private Table giveUpConfirmationPopUp;
    private Table waitingForGiveUpServerConfirmationPopUp;
    private Table waitingForGiveUpServerConfirmationERRORPopUp;
    private Table opponentHasGivenUpPopUp;
    private Window roundEndPopUp;
    private Button continuePopUpButton;
    private boolean[] powerUpCharged = new boolean[3];
    private Label heroId;
    private Label opponentId;

    private int currentRound;

    private Button chat;
    private Button powerUpButton;
    private Image heroIcon;
    private Image opponentIcon;
    private Button play;
    private Button facebook;
    private Button twitter;

    protected Image popUpExperienceBarContainer;
    protected Image popUpExperienceBarSeparetor;
    protected ExperienceBar popUpExperienceBar = null;

    private Label heroTotalScore;
    private Label opponentTotalScore;
    private Label totalScoreText;

    private boolean waitignForGiveUpServerConfirmation;
    private float giveUpWaitingTime;

    private int maxWaitingGiveUpConfirmationTime = 10;
    private final static float popInTimeDuration = 0.1f;

    private boolean drwaPopUpExperienceBar = false;

    private int popUpPills;
    private int bonusXP;

    private int startingExperienceForAnimation = User.data.experience;

    private boolean isExperienceAnimationStarted;
    private boolean isExperienceAnimationFinished;
    private boolean isPillsAnimationStarted;
    private boolean isPillsAnimationFinished;
    private float pillsAnimationBarMultiplier;
    private float pillsAnimationTreshold = 0.2f;
    private float pillsAnimationSpeed;
    private final int experienceAnimationSpeed = 9;

    private int exprienceForLEvelUp;
    private boolean levelUp;
    private boolean experienceBarResetted;


    public GameStatus(final MemoGameOnline memo, long matchID, Opponent opponent, final MOCDataCarrier mocdc, boolean[] powerUpCharged) {

        super(memo, mocdc);
        this.matchID = matchID;
        this.opponent = opponent;
        TAG = "GameStatus";
        User.data.refreshTotalScore(matchID);

        if (powerUpCharged != null)
            this.powerUpCharged = powerUpCharged;

        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    if (powerUpPopUp == null || powerUpPopUp != null && !powerUpPopUp.getPopUp().isVisible()) {
                        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                                ScreenTransitionSlide.RIGHT, false, interpolation);
                        memo.setScreen(new GameSelection(memo, mocdc), transition);
                        resetPowerUp();
                    } else if (powerUpPopUp != null && powerUpPopUp.getPopUp().isVisible()) {

                        powerUpPopUp.getPopUp().addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
                            public void run() {
                                powerUpPopUp.getPopUp().setVisible(false);
                            }
                        })));
                    }

                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(stage);

        currentRound = User.data.getMatch(matchID).getCurrentRound();

        if (User.data.level <= 9)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 8);
        else if (User.data.level > 9 && User.data.level <= 19)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 15);
        else if (User.data.level > 19 && User.data.level <= 29)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 15);
        else if (User.data.level > 29 && User.data.level <= 39)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 20);
        else if (User.data.level > 39 && User.data.level <= 49)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 20);
        else if (User.data.level > 49 && User.data.level <= 59)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 20);
        else if (User.data.level > 59 && User.data.level <= 69)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 25);
        else if (User.data.level > 69 && User.data.level <= 79)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 25);
        else if (User.data.level > 79 && User.data.level <= 89)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 25);
        else if (User.data.level > 89 && User.data.level <= 99)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 30);
        if (User.data.level > 99)
            bonusXP = (LevelManagement.instance.getCurrentExpRange() / 40);
    }

    @Override
    public void show() {

        waitignForGiveUpServerConfirmation = false;
        giveUpWaitingTime = 0;

        rebuildStage();

        if (User.data.getMatch(matchID).isReadyToDiscoverResult()) {
            //User.data.getMatch(matchID).setMatchWon(true);
            if (roundEndPopUp == null)
                stage.addActor(buildRoundEndPopUp());
            if (!popUpShowed) {
                popUpShowed = true;
                roundEndPopUp.setVisible(true);
                final Interpolation interpolation = Interpolation.elasticOut;
                // roundEndPopUp.addAction(scaleTo(1, 1, 1, interpolation));
                roundEndPopUp.addAction(sequence(scaleTo(1, 1, popInTimeDuration), run(new Runnable() {
                    public void run() {
                        drwaPopUpExperienceBar = true;
                        System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                    }
                })));
            }

        }

    }

    public void rebuildStage() {

        //User.data.refreshAllMatchesStatus();

        screen.clear();
        stage.clear();

        screen = new Table(memo.patchedSkin);
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.setBackground("GameStatusBackground");
        screen.addActor(buildImagesLayer());
        screen.addActor(buildButtonsLayer());

        if (powerUpPopUp != null)
            screen.addActor(buildPowerUpPopUp());

        if (giveUpConfirmationPopUp != null)
            screen.addActor(buildGiveUpConfirmationPopUp());
        if (waitingForGiveUpServerConfirmationPopUp != null)
            screen.addActor(buildWaitingForGiveUpServerConfirmationPopUp());
        if (waitingForGiveUpServerConfirmationERRORPopUp != null)
            screen.addActor(buildWaitingForGiveUpServerConfirmationERRORPopUp());
        if (opponentHasGivenUpPopUp != null)
            screen.addActor(buildOpponentHasGivenUpPopUp());
        if (roundEndPopUp != null) {
            stage.addActor(buildRoundEndPopUp());
        }

        stage.addActor(screen);

        //stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        layer.addActor(buildFirstRoundInfo());
        layer.addActor(buildSecondRoundInfo());
        layer.addActor(buildThirdRoundInfo());

        layer.addActor(buildHeroTotalScore());
        layer.addActor(buildOpponentTotalScore());


        return layer;
    }

    private Table buildButtonsLayer() {

        mainLayer = new Table();

        chat = new Button(memo.patchedSkin, "chat");
        chat.setPosition(222, 791);
        chat.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onChatClicked();
            }
        });
        mainLayer.addActor(chat);
        mainLayer.row();


        if (User.data.getMatch(matchID).isYourTurn() && !User.data.getMatch(matchID).isFinished()) {
            powerUpButton = new Button(memo.patchedSkin, "VioletButton");
            powerUpButton.setBounds(205, 570, 130, 60);
            powerUpButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onPowerUpClicked();
                }
            });
            Label text = new Label("Power Up", memo.patchedSkin, "38white");
            powerUpButton.add(text).padTop(-10);
            mainLayer.addActor(powerUpButton);
        }

        mainLayer.row();

        // Costruisco Avatar,Livello e nome di HERO
        if (User.data.imageId != 48) {
            heroIcon = AvatarManager.instance.getBigAvatar(User.data.imageId);
            heroIcon.setBounds(10, 626, 160, 140);
            mainLayer.addActor(heroIcon);
            String name;
            if (User.data.userName.length() > 13)
                name = User.data.userName.substring(0, 13);
            else
                name = User.data.userName;
            heroId = new Label(name, memo.patchedSkin, "40blue");
            heroId.setPosition(heroIcon.getX() + (heroIcon.getWidth() - heroId.getWidth()) / 2, heroIcon.getY() - 40);
            mainLayer.addActor(heroId);
        } else {
            if (User.data.facebookPicture != null) {
                heroIcon = User.data.facebookPicture;
                heroIcon.clearListeners();
                heroIcon.setTouchable(Touchable.enabled);
                heroIcon.setBounds(30, 637, 120, 120);
                Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                pictureFrame.setBounds(28, 635, 124, 124);
                pictureFrame.setTouchable(Touchable.disabled);
                mainLayer.addActor(heroIcon);
                mainLayer.addActor(pictureFrame);
                String name;
                if (User.data.userName.length() > 13)
                    name = User.data.userName.substring(0, 13);
                else
                    name = User.data.userName;
                heroId = new Label(name, memo.patchedSkin, "40blue");
                heroId.setPosition(heroIcon.getX() + (heroIcon.getWidth() - heroId.getWidth()) / 2, heroIcon.getY() - 51);
                mainLayer.addActor(heroId);
            } else {
                downloadHeroFacebookIcon();

            }
        }

        heroIcon.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                        ScreenTransitionSlide.DOWN, false, interpolation);
                memo.setScreen(new HeroView(memo, mocdc, matchID, opponent, powerUpCharged), transition);
                return true;
            }
        });
        //powerUpButton.setBounds(heroIcon.getX(), heroIcon.getY()-80, 140, 60);
        Label heroLevel;
        if (User.data.level <= 9) {
            heroLevel = new Label("" + User.data.level, memo.patchedSkin, "36white");
            heroLevel.setPosition(188, 665);//188,665 (36white)
        } else {
            heroLevel = new Label("" + User.data.level, memo.patchedSkin, "34white");
            heroLevel.setPosition(180.5f, 666);
        }


        mainLayer.addActor(heroLevel);

        // Costruisco Avatar,Livello e nome di OPPO

        if (opponent.getImageID() != 48) {
            opponentIcon = AvatarManager.instance.getBigAvatar(opponent.getImageID());
            opponentIcon.setBounds(370, 626, 160, 140);
            mainLayer.addActor(opponentIcon);
            String name;
            if (opponent.getUserName().length() > 13)
                name = opponent.getUserName().substring(0, 13);
            else
                name = opponent.getUserName();
            opponentId = new Label(name, memo.patchedSkin,
                    "40blue");
            opponentId.setPosition(opponentIcon.getX() + (opponentIcon.getWidth() - opponentId.getWidth()) / 2, opponentIcon.getY() - 40);
            mainLayer.addActor(opponentId);
        } else {
            if (opponent.getFacebookPicture() != null) {
                opponentIcon = opponent.getFacebookPicture();
                opponentIcon.clearListeners();
                opponentIcon.setTouchable(Touchable.enabled);
                opponentIcon.setBounds(387, 637, 120, 120);
                Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                pictureFrame.setBounds(385, 635, 124, 124);
                pictureFrame.setTouchable(Touchable.disabled);
                mainLayer.addActor(opponentIcon);
                mainLayer.addActor(pictureFrame);
                String name;
                if (opponent.getUserName().length() > 13)
                    name = opponent.getUserName().substring(0, 13);
                else
                    name = opponent.getUserName();
                opponentId = new Label(name, memo.patchedSkin,
                        "40blue");
                opponentId.setPosition(opponentIcon.getX() + (opponentIcon.getWidth() - opponentId.getWidth()) / 2, opponentIcon.getY() - 51);
                mainLayer.addActor(opponentId);
            } else {
                downloadOpponentFacebookIcon();
            }
        }

        opponentIcon.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                        ScreenTransitionSlide.DOWN, false, interpolation);
                memo.setScreen(new OpponentView(memo, mocdc, matchID, opponent, powerUpCharged), transition);
                return true;
            }
        });

        Label opponentLevel;
        if (opponent.getLevel() <= 9) {
            opponentLevel = new Label("" + opponent.getLevel(), memo.patchedSkin, "36white");
            opponentLevel.setPosition(342, 665);//188,665 (36white)
        } else {
            opponentLevel = new Label("" + opponent.getLevel(), memo.patchedSkin, "34white");
            opponentLevel.setPosition(334.5f, 666);
        }

        mainLayer.addActor(opponentIcon);
        mainLayer.addActor(opponentLevel);

        // if (User.data.getMatch(matchID).getCurrentRound() == 1 && User.data.getMatch(matchID).getFirstRoundHeroScore() == -1 ||
        // User.data.getMatch(matchID).getCurrentRound() == 2 && User.data.getMatch(matchID).getSecondRoundHeroScore() == -1 ||
        //  User.data.getMatch(matchID).getCurrentRound() == 3 && User.data.getMatch(matchID).getThirdRoundHeroScore() == -1 ||
        // User.data.getMatch(matchID).getCurrentRound() == 3 && User.data.getMatch(matchID).getThirdRoundHeroScore() != -1 && User.data.getMatch(matchID).getThirdRoundOpponentScore() == -1) {


        if (User.data.getMatch(matchID).isFinished() && !User.data.matchAlreadyExist(opponent.getUserName())
                && !User.data.challengedFriendList.contains(opponent.getUserName(), false)) {
            System.out.println("REMATCH?????????????????");
            for (String s : User.data.challengedFriendList)
                System.out.println(s);
            play = new Button(memo.patchedSkin, "GreenButton");
            play.setBounds(255, -2, 273, 114);
            play.add(new Label("Re-Match", memo.patchedSkin, "70white"));
            play.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onRematchClicked();
                }
            });
            mainLayer.addActor(play);
        } else if (!User.data.getMatch(matchID).isFinished() &&
                (User.data.getMatch(matchID).getCurrentRound() == 1 && User.data.getMatch(matchID).getFirstRoundHeroScore() == -1 ||
                        User.data.getMatch(matchID).getCurrentRound() == 2 && User.data.getMatch(matchID).getSecondRoundHeroScore() == -1 ||
                        User.data.getMatch(matchID).getCurrentRound() == 3 && User.data.getMatch(matchID).getThirdRoundHeroScore() == -1)
                ) {
            System.out.println("PLAY");
            play = new Button(memo.patchedSkin, "GreenButton");
            play.setBounds(255, -2, 273, 114);
            play.add(new Label("PLAY", memo.patchedSkin, "70white"));
            play.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    onPlayClicked();
                }
            });
            mainLayer.addActor(play);
        } else if (User.data.getMatch(matchID).isFinished() && User.data.matchAlreadyExist(opponent.getUserName())) {
            play = new Button(memo.patchedSkin,
                    "GreenDisabledButton");
            play.setVisible(false);
            mainLayer.addActor(play);

        } else {
            play = new Button(memo.patchedSkin,
                    "GreenDisabledButton");
            play.add(new Label("WAIT", memo.patchedSkin, "70white"));
            play.setBounds(255, -2, 273, 114);
            mainLayer.addActor(play);

        }

        mainLayer.row();

        if (!User.data.getMatch(matchID).isFinished() && !User.data.getMatch(matchID).isReadyToDiscoverResult()) {
            Button giveUp = new Button(memo.patchedSkin, "WhiteButton");
            giveUp.add(new Label("Give Up", memo.patchedSkin, "48black"));
            giveUp.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (powerUpPopUp == null || powerUpPopUp != null && !powerUpPopUp.getPopUp().isVisible()) {
                        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                        stage.addActor(buildGiveUpConfirmationPopUp());
                        giveUpConfirmationPopUp.setVisible(true);
                    }
                }
            });
            giveUp.setBounds(28, -4, 189, 105);
            mainLayer.addActor(giveUp);
            mainLayer.row();
        }

        mainLayer.setDebug(true);
        return mainLayer;
    }

    public Table buildPowerUpPopUp() {
        powerUpPopUp = new GameStatusPowerUpPopUp(this, memo.patchedSkin, matchID);
        powerUpPopUp.getPopUp().setVisible(false);
        return powerUpPopUp.getPopUp();

    }

    public Table buildGiveUpConfirmationPopUp() {

        giveUpConfirmationPopUp = new Table(memo.patchedSkin);
        giveUpConfirmationPopUp.setBounds(81, 300, 384, 206);
        giveUpConfirmationPopUp.setBackground("BlackBackground");
        Label confirmationRequest = new Label(
                "Are you sure you want to give up \nthe match?", memo.patchedSkin,
                "40white");
        giveUpConfirmationPopUp.addActor(confirmationRequest);
        confirmationRequest.setPosition(15, 115);

        Button accept = new Button(memo.patchedSkin, "RedButton");
        accept.add(new Label("Confirm", memo.patchedSkin, "48white"));
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);

                String[] sendMsg = {"1000", String.valueOf(matchID)};
                mocdc.sendToServer(sendMsg);
                if (waitingForGiveUpServerConfirmationPopUp == null)
                    stage.addActor(buildWaitingForGiveUpServerConfirmationPopUp());
                waitingForGiveUpServerConfirmationPopUp.setVisible(true);
                giveUpConfirmationPopUp.setVisible(false);
                waitignForGiveUpServerConfirmation = true;
            }
        });
        accept.setBounds(175, 3, 194, 98);

        Button refuse = new Button(memo.patchedSkin, "GreyButton");
        refuse.add(new Label("Refuse", memo.patchedSkin, "28white"));
        refuse.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                giveUpConfirmationPopUp.setVisible(false);

            }
        });
        refuse.setBounds(22, 3, 128, 69);


        giveUpConfirmationPopUp.addActor(accept);
        giveUpConfirmationPopUp.addActor(refuse);
        giveUpConfirmationPopUp.setVisible(false);
        return giveUpConfirmationPopUp;
    }

    public Table buildRoundEndPopUp() {

        roundEndPopUp = new Window("", memo.patchedSkin);
        roundEndPopUp.setBounds(56.5f, 300, 427, 455);
        roundEndPopUp.setTransform(true);
        roundEndPopUp.setOrigin(roundEndPopUp.getWidth() / 2, roundEndPopUp.getHeight() / 2);
        Label confirmationRequest;
        if (User.data.getMatch(matchID).isMatchWon()) {
            confirmationRequest = new Label(
                    "YOU WON!", memo.patchedSkin,
                    "60white");
            //bonusXP = 54300;
            roundEndPopUp.setBackground("BlueBackgroundGameWon");
            Label bonusXP = new Label("+" + this.bonusXP, memo.patchedSkin, "50yellow");
            Label bonusXP2 = new Label(" xp!", memo.patchedSkin, "50yellow");
            if (this.bonusXP < 10000)
                bonusXP.setPosition(34, 291);
            else
                bonusXP.setPosition(25, 291);
            bonusXP2.setPosition(46, 262);
            roundEndPopUp.addActor(bonusXP);
            roundEndPopUp.addActor(bonusXP2);
        } else {
            confirmationRequest = new Label(
                    "You lost!", memo.patchedSkin,
                    "60white");
            roundEndPopUp.setBackground("BlueBackgroundGameLost");
        }

        if (User.data.getMatch(matchID).getHeroCoins() == -1)
            User.data.calculateMatchPills(matchID);
        System.out.println("Hero ha pillole: " + User.data.pills);
        System.out.println("Hero riceve: " + User.data.getMatch(matchID).getHeroCoins()
                + " pillole");
        // User.data.pills = User.data.pills + User.data.getMatch(matchID).getHeroCoins();
        //System.out.println("Hero ha ora: " + User.data.pills);
        popUpPills = User.data.getMatch(matchID).getHeroCoins();

        if (popUpPills >= 7)
            pillsAnimationSpeed = 0.2f;
        else if (popUpPills == 6)
            pillsAnimationSpeed = (float) (7 / popUpPills) * 0.2f;
        else if (popUpPills == 4)
            pillsAnimationSpeed = (float) (7 / popUpPills) * 0.2f;
        else if (popUpPills == 2)
            pillsAnimationSpeed = (float) (7 / popUpPills) * 0.2f;


        roundEndPopUp.addActor(confirmationRequest);
        confirmationRequest.setPosition((roundEndPopUp.getWidth() - confirmationRequest.getWidth()) / 2, 365);

        pillsCan = new Image(memo.patchedSkin, "pillsCan");
        pillsCan.setPosition(455, 790);

        continuePopUpButton = new Button(memo.patchedSkin, "GreenButton");
        continuePopUpButton.add(new Label("Continue", memo.patchedSkin, "60white"));
        continuePopUpButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                onRoundEndPopUpContinueClicked();

            }
        });
        continuePopUpButton.setSize(272, 105);
        continuePopUpButton.setPosition((roundEndPopUp.getWidth() - continuePopUpButton.getWidth()) / 2, 15);

        facebook = new Button(memo.patchedSkin, "FacebookButton");
        facebook.setPosition(continuePopUpButton.getX() + 24, 130);
        facebook.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Condividi su Facebook!!!");
            }
        });
        roundEndPopUp.addActor(facebook);


        twitter = new Button(memo.patchedSkin, "TwitterButton");
        twitter.setPosition(continuePopUpButton.getX() + continuePopUpButton.getWidth() - twitter.getWidth() - 24, 130);
        twitter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Condividi su Twitter!!!");
            }
        });
        roundEndPopUp.addActor(twitter);

        popUpExperienceBarContainer = new Image(memo.patchedSkin, "experienceBarContainer");
        popUpExperienceBarContainer.setPosition(129 + roundEndPopUp.getX(), 273.5f + roundEndPopUp.getY());
        popUpExperienceBar = new ExperienceBar();
        popUpExperienceBarSeparetor = new Image(memo.patchedSkin, "experienceBarSeparetorBlue");
        popUpExperienceBarSeparetor.setPosition(129 + roundEndPopUp.getX(), 273.5f + roundEndPopUp.getY());


        roundEndPopUp.addActor(continuePopUpButton);

        // roundEndPopUp.addActor(popUpExperienceBarContainer);
        // roundEndPopUp.addActor(popUpExperienceBarSeparetor);
        roundEndPopUp.setVisible(false);
        roundEndPopUp.setScale(0, 0);
        roundEndPopUp.setModal(true);
        return roundEndPopUp;
    }

    public Table buildWaitingForGiveUpServerConfirmationPopUp() {

        waitingForGiveUpServerConfirmationPopUp = new Window("", memo.patchedSkin,
                "popup");
        waitingForGiveUpServerConfirmationPopUp.setBounds(145, 425, 250, 120);
        waitingForGiveUpServerConfirmationPopUp.setBackground("BlackBackground");
        Label info = new Label("Please wait...", memo.patchedSkin, "40white");
        waitingForGiveUpServerConfirmationPopUp.addActor(info);
        info.setPosition(40, 50);
        waitingForGiveUpServerConfirmationPopUp.setVisible(false);
        return waitingForGiveUpServerConfirmationPopUp;
    }

    public Table buildWaitingForGiveUpServerConfirmationERRORPopUp() {

        waitingForGiveUpServerConfirmationERRORPopUp = new Table(
                memo.patchedSkin);
        waitingForGiveUpServerConfirmationERRORPopUp.setBounds(81, 300, 384,
                206);
        waitingForGiveUpServerConfirmationERRORPopUp.setBackground("BlackBackground");
        Label continueRequest = new Label(
                "Timeout server error.\n Please try later", memo.patchedSkin,
                "40white");
        waitingForGiveUpServerConfirmationERRORPopUp.addActor(continueRequest);
        continueRequest.setPosition(15, 115);

        Button accept = new Button(memo.patchedSkin, "RedButton");
        accept.add(new Label("Continue", memo.patchedSkin, "48white"));
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                waitingForGiveUpServerConfirmationERRORPopUp.setVisible(false);

            }
        });
        accept.setBounds(90, 3, 194, 98);

        waitingForGiveUpServerConfirmationERRORPopUp.addActor(accept);
        waitingForGiveUpServerConfirmationERRORPopUp.setVisible(false);

        return waitingForGiveUpServerConfirmationERRORPopUp;
    }

    public Table buildOpponentHasGivenUpPopUp() {

        opponentHasGivenUpPopUp = new Table(memo.patchedSkin);
        opponentHasGivenUpPopUp.setBounds(81, 300, 384, 206);
        opponentHasGivenUpPopUp.setBackground("BlackBackground");
        Label continueRequest = new Label(""
                + User.data.getMatch(matchID).getOpponent().getUserName()
                + "  has given up.\n The match will be closed.", memo.patchedSkin,
                "40white");
        opponentHasGivenUpPopUp.addActor(continueRequest);
        continueRequest.setPosition(15, 115);

        Button accept = new Button(memo.patchedSkin, "RedButton");
        accept.add(new Label("Continue", memo.patchedSkin, "48white"));
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                opponentHasGivenUpPopUp.setVisible(false);
                User.data.getMatch(matchID).setFinishedForGiveUp(true);
                User.data.closeMatch(matchID);
                User.data.getMatch(matchID).setMatchWon(true);
                User.data.getMatch(matchID).setFinished(true);
                DataManager.instance.saveData();
                resetPowerUp();
                ScreenTransition transition = ScreenTransitionSlide.init(duration,
                        ScreenTransitionSlide.RIGHT, false, interpolation);
                memo.setScreen(new GameSelection(memo, mocdc), transition);
            }
        });
        accept.setBounds(90, 3, 194, 98);

        opponentHasGivenUpPopUp.addActor(accept);
        opponentHasGivenUpPopUp.setVisible(false);

        return opponentHasGivenUpPopUp;
    }

    private void onPlayClicked() {
        if (powerUpPopUp == null || powerUpPopUp != null && !powerUpPopUp.getPopUp().isVisible()) {
            AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
            int gameMode = User.data.getMatch(matchID).getCurrentRound();
            ScreenTransition transition = ScreenTransitionSlide.init(duration,
                    ScreenTransitionSlide.LEFT, false, interpolation);
            memo.setScreen(new GameScreen(this.memo, mocdc, matchID, opponent, gameMode,
                    powerUpCharged, false), transition);
        }
    }

    private void onRematchClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        System.out.println("Inviata richiesta di rivincita!!!");
        String opponentID = User.data.getMatch(matchID).getOpponent().getUserName();
        User.data.challengedFriendList.add(opponentID);
        String[] sendMsg = {"0300", opponentID};
        mocdc.sendToServer(sendMsg);
        rebuildStage();
        DataManager.instance.saveData();

    }

    private void onRoundEndPopUpContinueClicked() {
        continuePopUpButton.setDisabled(true);
        System.out.println("");
        if (User.data.getMatch(matchID).isMatchWon()) {
            String[] sendMsg = {"1008", String.valueOf(matchID), String.valueOf(this.bonusXP)};
            mocdc.sendToServer(sendMsg);
        } else {
            String[] sendMsg = {"1008", String.valueOf(matchID), "0"};
            mocdc.sendToServer(sendMsg);
        }

        User.data.getMatch(matchID).setCoinsAlreadyAssigned(true);
        User.data.getMatch(matchID).setReadyToDiscoverResult(false);

        DataManager.instance.saveData();

        if (User.data.getMatch(matchID).isMatchWon()) {
            if (User.data.experience + bonusXP > LevelManagement.instance.getCurrentExpRange()) {
                System.out.println("______________________________________________________________________");
                System.out.println("Livello Attuale: " + User.data.level);
                System.out.println("Range Attuale: " + LevelManagement.instance.getCurrentExpRange());
                System.out.println("Exp attuali: " + User.data.experience);
                System.out.println("Bonus Attuale: " + bonusXP);
                exprienceForLEvelUp = LevelManagement.instance.getCurrentExpRange() - User.data.experience;
                System.out.println("Exp per  il Level Up: " + exprienceForLEvelUp);
                levelUp = true;

            }
            isExperienceAnimationStarted = true;
            AudioManager.instance.play(CardsAsset.instance.sounds.scoreCount);
        } else {
            isPillsAnimationStarted = true;
        }

        User.data.closeMatch(matchID);
        if (mocdc.cmdToExecute.size > 0 && mocdc.cmdToExecute.first() == 4)

        {
            mocdc.removeMessage();
            System.out.println("RIMUOVO IL MOCC!!!!");
        }


    }

    private void onChatClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                ScreenTransitionSlide.DOWN, false, interpolation);
        memo.setScreen(new ChatScreen(this.memo, this.mocdc, matchID, opponent, powerUpCharged), transition);
    }


    private void onPowerUpClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (powerUpPopUp == null)
            stage.addActor(buildPowerUpPopUp());
        powerUpPopUp.getPopUp().setVisible(true);
        powerUpPopUp.getPopUp().addAction(sequence(scaleTo(1, 1, popInTimeDuration), run(new Runnable() {
            public void run() {
                addSpecialEffect();
            }
        })));


    }

    private void addSpecialEffect() {

        if (powerUpCharged[0]) {
            powerUpPopUp.setFirstPowerUpSelected(true);
            PooledEffect effect = powerUpPopUp.getSpecialEffectPool().getStarsPool().obtain();
            effect.setPosition(powerUpPopUp.getFirstPowerUp().getX()
                    + powerUpPopUp.getPopUp().getX() + 10, powerUpPopUp
                    .getFirstPowerUp().getY()
                    + powerUpPopUp.getPopUp().getY() + 43);
            powerUpPopUp.setFirstPowerUpEffect(effect);
        }
        if (powerUpCharged[1]) {
            powerUpPopUp.setSecondPowerUpSelected(true);
            PooledEffect effect = powerUpPopUp.getSpecialEffectPool().getStarsPool().obtain();
            effect.setPosition(powerUpPopUp.getSecondPowerUp().getX()
                    + powerUpPopUp.getPopUp().getX() + 10, powerUpPopUp
                    .getSecondPowerUp().getY()
                    + powerUpPopUp.getPopUp().getY() + 43);
            powerUpPopUp.setSecondPowerUpEffect(effect);
        }
        if (powerUpCharged[2]) {
            powerUpPopUp.setThirdPowerUpSelected(true);
            PooledEffect effect = powerUpPopUp.getSpecialEffectPool().getStarsPool().obtain();
            effect.setPosition(powerUpPopUp.getThirdPowerUp().getX()
                    + powerUpPopUp.getPopUp().getX() + 10, powerUpPopUp
                    .getThirdPowerUp().getY()
                    + powerUpPopUp.getPopUp().getY() + 43);
            powerUpPopUp.setThirdPowerUpEffect(effect);
        }

    }


    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        batcher.begin();
        batcher.enableBlending();
        drawExperienceBar();
        renderSpecialEffect(delta);
        batcher.end();

        if (popUpExperienceBar != null && drwaPopUpExperienceBar) {
            batcher.begin();
            batcher.enableBlending();
            renderPopUpExperienceBar();
            batcher.end();
        }


        checkServerMessage();
        if (isExperienceAnimationStarted && !isExperienceAnimationFinished)
            renderExperienceAnimation();

        if (isPillsAnimationStarted && !isPillsAnimationFinished)
            renderPillsAnimation();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

    }

    private void renderExperienceAnimation() {

        if (!levelUp) {
            //System.out.println("NON LEVEL UP!!!!");
            User.data.experience += experienceAnimationSpeed * User.data.level;
            if (User.data.experience > startingExperienceForAnimation + bonusXP) {
                User.data.experience = startingExperienceForAnimation + bonusXP;
                isExperienceAnimationFinished = true;
                isPillsAnimationStarted = true;
                AudioManager.instance.stopSound(CardsAsset.instance.sounds.scoreCount);
            }
        } else {
            //System.out.println("LEVEL UP!!!!");
            User.data.experience += experienceAnimationSpeed * User.data.level;
            if (User.data.experience > (startingExperienceForAnimation + exprienceForLEvelUp) && !experienceBarResetted) {
                //System.out.println("LEVEL UP COMPIUTO RESETTO IL TUTTO!!!!!");
                User.data.experience = 1;
                User.data.level++;
                experienceBarResetted = true;
                levelUp = false;
                startingExperienceForAnimation = 1;
                bonusXP -= exprienceForLEvelUp;
            }
            /*if ((User.data.experience > bonusXP - exprienceForLEvelUp) && experienceBarResetted) {
                System.out.println("ANIMAZIONE TERMINATA!!!!");
                User.data.experience = bonusXP - exprienceForLEvelUp;
                isExperienceAnimationFinished = true;
                isPillsAnimationStarted = true;
                AudioManager.instance.stopSound(CardsAsset.instance.sounds.scoreCount);
            }*/
        }
    }

    private void renderPillsAnimation() {
        pillsAnimationBarMultiplier += Gdx.graphics.getDeltaTime();
        if (pillsAnimationBarMultiplier > pillsAnimationTreshold) {
            AudioManager.instance.play(CardsAsset.instance.sounds.pill);
            pillsAnimationTreshold += pillsAnimationSpeed;
            popUpPills--;
            User.data.pills += 1;
            if (popUpPills == 0) {
                isPillsAnimationFinished = true;
                drwaPopUpExperienceBar = false;
                if (powerUpButton != null)
                    powerUpButton.setVisible(false);
                roundEndPopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration),
                        run(new Runnable() {
                                public void run() {
                                    System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                                    roundEndPopUp.setVisible(false);
                                    popUpShowed = false;
                                    rebuildStage();
                                }
                            }

                        )));
            }
        }
    }

    private void renderPopUpExperienceBar() {

        if (User.data.getMatch(matchID).isMatchWon()) {
            popUpExperienceBarContainer.draw(batcher, 1);
            popUpExperienceBar.draw(batcher, roundEndPopUp.getX() + 129, roundEndPopUp.getY() + 273.5f,
                    360 - 360 * LevelManagement.instance
                            .getPercentageIncrease(User.data.experience));
            popUpExperienceBarSeparetor.draw(batcher, 1);
            //System.out.println(User.data.experience);

            if (User.data.level > 9)
                zighia37.draw(batcher, "" + User.data.level, roundEndPopUp.getX() + 129 + 28.2f, roundEndPopUp.getY() + 273.5f + 52);
            else
                zighia48.draw(batcher, "" + User.data.level, roundEndPopUp.getX() + 129 + 33.7f, roundEndPopUp.getY() + 273.5f + 55);


            //System.out.println("Pillle vinte: " + popUpPills);
            if (popUpPills < 10) {
                zighia48.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 131, roundEndPopUp.getY() + 273.5f + 52);
                // System.out.println("Renderizzo < 10");
            } else if (popUpPills > 9 && popUpPills < 100) {
                zighia48.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 123, roundEndPopUp.getY() + 273.5f + 52);
                //System.out.println("Renderizzo > 9 && < 100");
            } else if (popUpPills > 99) {
                zighia45.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 117, roundEndPopUp.getY() + 273.5f + 52);
                // System.out.println("Renderizzo > 100");
            }
        } else {
            if (popUpPills < 10) {
                zighia48.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 79, roundEndPopUp.getY() + 273.5f + 52);
                // System.out.println("Renderizzo < 10");
            } else if (popUpPills > 9 && popUpPills < 100) {
                zighia48.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 71, roundEndPopUp.getY() + 273.5f + 52);
                //System.out.println("Renderizzo > 9 && < 100");
            } else if (popUpPills > 99) {
                zighia45.draw(batcher, "" + popUpPills, roundEndPopUp.getX() + 129 + 111, roundEndPopUp.getY() + 273.5f + 52);
                //System.out.println("Renderizzo > 100");
            }
        }
    }

    public void renderSpecialEffect(float delta) {

        //if (powerUpPopUp != null)
        // System.out.println(powerUpPopUp.getFirstPowerUpEffect());

        if (powerUpPopUp != null && powerUpPopUp.getPopUp().isVisible()) {
            if (powerUpPopUp.getFirstPowerUpEffect() != null) {
                PooledEffect effect = powerUpPopUp.getFirstPowerUpEffect();
                effect.draw(batcher, delta);
                //System.out.println("RENDERIZZO EFFETTO");
                if (effect.isComplete()) {
                    effect.free();
                }
            }

            if (powerUpPopUp.getSecondPowerUpEffect() != null) {
                PooledEffect effect = powerUpPopUp.getSecondPowerUpEffect();
                effect.draw(batcher, delta);
                if (effect.isComplete()) {
                    effect.free();
                }
            }

            if (powerUpPopUp.getThirdPowerUpEffect() != null) {
                PooledEffect effect = powerUpPopUp.getThirdPowerUpEffect();
                effect.draw(batcher, delta);
                if (effect.isComplete()) {
                    effect.free();
                }
            }
        }
    }

    private void resetPowerUp() {

        if (powerUpCharged[0]) {
            User.data.powerUp_Time += 1;
        }

        if (powerUpCharged[1]) {
            if (currentRound == 1 || currentRound == 2) {
                User.data.powerUp_FaceUp += 1;
            } else {
                User.data.powerUp_Chain += 1;
            }
        }

        if (powerUpCharged[2]) {
            if (currentRound == 1) {
                User.data.powerUp_DoubleCouple += 1;
            } else if (currentRound == 2) {
                User.data.powerUp_Stop += 1;
            } else {
                User.data.powerUp_Bonus += 1;
            }
        }

    }

    @Override
    public void resize(int width, int height) {

        // use true here to center the camera
        // that's what you probably want in case of a UI
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    public Button getPowerUpButton() {
        return powerUpButton;
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }


    public boolean[] getPowerUpCharged() {
        return powerUpCharged;
    }

    public void setFirstPowerUp(boolean firstPowerUp) {
        this.powerUpCharged[0] = firstPowerUp;
    }

    public void setSecondPowerUp(boolean firstPowerUp) {
        this.powerUpCharged[1] = firstPowerUp;
    }

    public void setThirdPowerUp(boolean firstPowerUp) {
        this.powerUpCharged[2] = firstPowerUp;
    }

    private void downloadHeroFacebookIcon() {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                int numBytes = download(bytes, User.data.facebookPictureURL);
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            User.data.facebookPicture = new Image(image);

                            heroIcon = User.data.facebookPicture;
                            heroIcon.clearListeners();
                            heroIcon.setTouchable(Touchable.enabled);
                            heroIcon.setBounds(30, 637, 120, 120);
                            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                            pictureFrame.setBounds(28, 635, 124, 124);
                            pictureFrame.setTouchable(Touchable.disabled);
                            mainLayer.addActor(heroIcon);
                            mainLayer.addActor(pictureFrame);
                            heroId = new Label("" + User.data.userName, memo.patchedSkin, "40blue");
                            heroId.setPosition(heroIcon.getX() + (heroIcon.getWidth() - heroId.getWidth()) / 2, heroIcon.getY() - 48);
                            mainLayer.addActor(heroId);

                        }
                    });
                }
            }
        }).start();
    }

    private void downloadOpponentFacebookIcon() {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));

                            opponentIcon = opponent.getFacebookPicture();
                            opponentIcon.clearListeners();
                            opponentIcon.setTouchable(Touchable.enabled);
                            opponentIcon.setBounds(387, 637, 120, 120);
                            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                            pictureFrame.setBounds(385, 635, 124, 124);
                            pictureFrame.setTouchable(Touchable.disabled);
                            mainLayer.addActor(opponentIcon);
                            mainLayer.addActor(pictureFrame);
                            opponentId = new Label("" + opponent.getUserName(), memo.patchedSkin, "40blue");
                            opponentId.setPosition(opponentIcon.getX() + (opponentIcon.getWidth() - opponentId.getWidth()) / 2, opponentIcon.getY() - 48);
                            mainLayer.addActor(opponentId);

                        }
                    });
                }
            }
        }).start();
    }

    public Table buildFirstRoundInfo() {


        Table roundCell = new Table();
        roundCell.setBounds(20, 445, 500, 103);
        Image heroScoreBar;
        Label heroScore;
        Image opponentScoreBar = null;
        Label opponentScore;

        roundCell.center().top().add(new Label("Round  1", memo.patchedSkin, "34blue")).padTop(-30);

        if (User.data.getMatch(matchID).getFirstRoundHeroScore() == -1) {

            heroScore = new Label("YOUR TURN", memo.patchedSkin, "36grey");
            heroScore.setPosition(26, 26);
            opponentScore = new Label("HIDE", memo.patchedSkin, "40beige");
            opponentScore.setPosition(432, 26);

            roundCell.addActor(heroScore);
            roundCell.addActor(opponentScore);

            // Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            //scoreContainer.setBounds(0, -1, 501, 103);
            //roundCell.addActor(scoreContainer);

            return roundCell;
        } else {

            // HERO AND OPPO PLAYED ROUND
            if (User.data.getMatch(matchID).getFirstRoundHeroScore() != -1
                    && User.data.getMatch(matchID).getFirstRoundOpponentScore() != -1) {
                if (User.data.getMatch(matchID).getFirstRoundHeroScore() >= User.data
                        .getMatch(matchID).getFirstRoundOpponentScore()) {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarRight");

                } else {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarRight");
                }
                int score = User.data.getMatch(matchID).getFirstRoundHeroScore()
                        + User.data.getMatch(matchID).getFirstRoundOpponentScore();

                heroScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getFirstRoundHeroScore() / score + 5);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getFirstRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getFirstRoundOpponentScore() / score);
                opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                opponentScore = new Label(""
                        + User.data.getMatch(matchID)
                        .getFirstRoundOpponentScore(), memo.patchedSkin,
                        "48white");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 22);

                if (heroScoreBar.getWidth() < 80) {
                    heroScoreBar.setWidth(80);
                    opponentScoreBar.setWidth(roundCell.getWidth() - heroScoreBar.getWidth() + 5);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                }
                if (opponentScoreBar.getWidth() < 80) {
                    opponentScoreBar.setWidth(80);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                    heroScoreBar.setWidth(roundCell.getWidth() - opponentScoreBar.getWidth() + 5);
                }

            }

            // HERO IS WAITING OPPONENT HERO SCORE
            else {
                heroScoreBar = new Image(memo.patchedSkin,
                        "GameStatusGreenBarLeft");
                heroScoreBar.setWidth(roundCell.getWidth() / 2);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getFirstRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScore = new Label("WAITING", memo.patchedSkin, "36grey");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 30);

            }

            heroScore.setPosition(15, 22);

            roundCell.addActor(heroScoreBar);
            roundCell.addActor(heroScore);
            if (!(User.data.getMatch(matchID).getFirstRoundOpponentScore() == -1))
                roundCell.addActor(opponentScoreBar);
            roundCell.addActor(opponentScore);

            // Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            // scoreContainer.setBounds(0, -1, 501, 103);
            //roundCell.addActor(scoreContainer);
            return roundCell;

        }

    }

    public Table buildSecondRoundInfo() {

        Table roundCell = new Table();
        roundCell.setBounds(20, 316, 500, 103);
        Image heroScoreBar;
        Label heroScore;
        Image opponentScoreBar = null;
        Label opponentScore;

        roundCell.center().top().add(new Label("Round  2", memo.patchedSkin, "34blue")).padTop(-30);

        if (User.data.getMatch(matchID).getSecondRoundHeroScore() == -1) {

            if (User.data.getMatch(matchID).getCurrentRound() == 2) {
                heroScore = new Label("YOUR TURN", memo.patchedSkin, "40beige");
                heroScore.setPosition(26, 26);
                opponentScore = new Label("HIDE", memo.patchedSkin, "40beige");
                opponentScore.setPosition(432, 26);
                roundCell.addActor(heroScore);
                roundCell.addActor(opponentScore);
            } else {
                Image blackStars = new Image(memo.patchedSkin, "blackStars");
                blackStars.setPosition(13, 30);
                roundCell.addActor(blackStars);
            }
            // Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            //scoreContainer.setBounds(0, -1, 501, 103);
            //roundCell.addActor(scoreContainer);
            return roundCell;
        } else {
            // HERO AND OPPO PLAYED ROUND
            if (User.data.getMatch(matchID).getSecondRoundHeroScore() != -1
                    && User.data.getMatch(matchID).getSecondRoundOpponentScore() != -1) {
                if (User.data.getMatch(matchID).getSecondRoundHeroScore() >= User.data
                        .getMatch(matchID).getSecondRoundOpponentScore()) {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarRight");

                } else {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarRight");
                }
                int score = User.data.getMatch(matchID).getSecondRoundHeroScore()
                        + User.data.getMatch(matchID).getSecondRoundOpponentScore();

                heroScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getSecondRoundHeroScore() / score + 5);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getSecondRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getSecondRoundOpponentScore() / score);
                opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                opponentScore = new Label(""
                        + User.data.getMatch(matchID)
                        .getSecondRoundOpponentScore(), memo.patchedSkin,
                        "48white");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 22);

                if (heroScoreBar.getWidth() < 80) {
                    heroScoreBar.setWidth(80);
                    opponentScoreBar.setWidth(roundCell.getWidth() - heroScoreBar.getWidth() + 5);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                }
                if (opponentScoreBar.getWidth() < 80) {
                    opponentScoreBar.setWidth(80);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                    heroScoreBar.setWidth(roundCell.getWidth() - opponentScoreBar.getWidth() + 5);
                }

            }

            // HERO IS WAITING OPPONENT HERO SCORE
            else {
                heroScoreBar = new Image(memo.patchedSkin,
                        "GameStatusGreenBarLeft");
                heroScoreBar.setWidth(roundCell.getWidth() / 2);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getSecondRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScore = new Label("WAITING", memo.patchedSkin, "40beige");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 30);

            }

            heroScore.setPosition(15, 22);

            roundCell.addActor(heroScoreBar);
            roundCell.addActor(heroScore);
            if (!(User.data.getMatch(matchID).getSecondRoundOpponentScore() == -1))
                roundCell.addActor(opponentScoreBar);
            roundCell.addActor(opponentScore);

            //Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            //scoreContainer.setBounds(0, -1, 501, 103);
            // roundCell.addActor(scoreContainer);
            return roundCell;

        }

    }

    public Table buildThirdRoundInfo() {

        Table roundCell = new Table();
        roundCell.setBounds(19, 187, 500, 103);
        Image heroScoreBar;
        Label heroScore;
        Image opponentScoreBar = null;
        Label opponentScore;

        roundCell.center().top().add(new Label("Round  3", memo.patchedSkin, "34blue")).padTop(-30);

        if (User.data.getMatch(matchID).getThirdRoundHeroScore() == -1) {

            if (User.data.getMatch(matchID).getCurrentRound() == 1
                    || User.data.getMatch(matchID).getCurrentRound() == 2) {
                Image blackStars = new Image(memo.patchedSkin, "blackStars");
                blackStars.setPosition(13, 30);
                roundCell.addActor(blackStars);
            } else {
                heroScore = new Label("YOUR TURN", memo.patchedSkin, "40beige");
                heroScore.setPosition(26, 26);
                opponentScore = new Label("HIDE", memo.patchedSkin, "40beige");
                opponentScore.setPosition(432, 26);
                roundCell.addActor(heroScore);
                roundCell.addActor(opponentScore);
            }
            // Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            //scoreContainer.setBounds(0, -1, 501, 103);
            // roundCell.addActor(scoreContainer);
            return roundCell;
        } else {
            // HERO AND OPPO PLAYED ROUND
            if (User.data.getMatch(matchID).getThirdRoundHeroScore() != -1
                    && User.data.getMatch(matchID).getThirdRoundOpponentScore() != -1) {
                if (User.data.getMatch(matchID).getThirdRoundHeroScore() >= User.data
                        .getMatch(matchID).getThirdRoundOpponentScore()) {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarRight");

                } else {
                    heroScoreBar = new Image(memo.patchedSkin,
                            "GameStatusRedBarLeft");
                    opponentScoreBar = new Image(memo.patchedSkin,
                            "GameStatusGreenBarRight");
                }
                int score = User.data.getMatch(matchID).getThirdRoundHeroScore()
                        + User.data.getMatch(matchID).getThirdRoundOpponentScore();

                heroScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getThirdRoundHeroScore() / score + 5);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getThirdRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScoreBar.setWidth(roundCell.getWidth() * User.data.getMatch(matchID).getThirdRoundOpponentScore() / score);
                opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                opponentScore = new Label(""
                        + User.data.getMatch(matchID)
                        .getThirdRoundOpponentScore(), memo.patchedSkin,
                        "48white");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 22);

                if (heroScoreBar.getWidth() < 80) {
                    heroScoreBar.setWidth(80);
                    opponentScoreBar.setWidth(roundCell.getWidth() - heroScoreBar.getWidth() + 5);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                }
                if (opponentScoreBar.getWidth() < 80) {
                    opponentScoreBar.setWidth(80);
                    opponentScoreBar.setPosition(roundCell.getWidth() - opponentScoreBar.getWidth(), 0);
                    heroScoreBar.setWidth(roundCell.getWidth() - opponentScoreBar.getWidth() + 5);
                }

            }

            // HERO IS WAITING OPPONENT HERO SCORE
            else {
                heroScoreBar = new Image(memo.patchedSkin,
                        "GameStatusGreenBarLeft");
                heroScoreBar.setWidth(roundCell.getWidth() / 2);
                heroScore = new Label(""
                        + User.data.getMatch(matchID).getThirdRoundHeroScore(),
                        memo.patchedSkin, "48white");
                opponentScore = new Label("WAITING", memo.patchedSkin, "40beige");
                opponentScore.setPosition(roundCell.getWidth()
                        - opponentScore.getWidth() - 20, 30);

            }

            heroScore.setPosition(15, 22);

            roundCell.addActor(heroScoreBar);
            roundCell.addActor(heroScore);
            if (!(User.data.getMatch(matchID).getThirdRoundOpponentScore() == -1))
                roundCell.addActor(opponentScoreBar);
            roundCell.addActor(opponentScore);

            // Image scoreContainer = new Image(memo.patchedSkin, "TrasparentFrame");
            //scoreContainer.setBounds(0, -1, 501, 103);
            // roundCell.addActor(scoreContainer);
            return roundCell;

        }

    }

    private Label buildHeroTotalScore() {
        int score = 0;
        if (User.data.getMatch(matchID).getFirstRoundHeroScore() != -1)
            score += User.data.getMatch(matchID).getFirstRoundHeroScore();
        if (User.data.getMatch(matchID).getSecondRoundHeroScore() != -1)
            score += User.data.getMatch(matchID).getSecondRoundHeroScore();
        if (User.data.getMatch(matchID).getSecondRoundHeroScore() != -1)
            score += User.data.getMatch(matchID).getThirdRoundHeroScore();

        heroTotalScore = new Label("" + score, memo.patchedSkin,
                "80blue");
        heroTotalScore.setPosition(33, 107);
        return heroTotalScore;

    }

    private Label buildOpponentTotalScore() {

        int score = 0;
        if (User.data.getMatch(matchID).getFirstRoundHeroScore() != -1
                && User.data.getMatch(matchID).getFirstRoundOpponentScore() != -1)
            score += User.data.getMatch(matchID).getFirstRoundOpponentScore();
        if (User.data.getMatch(matchID).getSecondRoundHeroScore() != -1
                && User.data.getMatch(matchID).getSecondRoundOpponentScore() != -1)
            score += User.data.getMatch(matchID).getSecondRoundOpponentScore();
        if (User.data.getMatch(matchID).getThirdRoundHeroScore() != -1
                && User.data.getMatch(matchID).getSecondRoundOpponentScore() != -1)
            score += User.data.getMatch(matchID).getThirdRoundOpponentScore();

        opponentTotalScore = new Label("" + score,
                memo.patchedSkin, "80blue");
        opponentTotalScore.setPosition(510 - opponentTotalScore.getWidth(), 107);
        return opponentTotalScore;

    }


    public void checkServerMessage() {

        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.get(0) == 2) { // OPPONENT FOUND, CREATE A MATCH

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);

                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA compsota di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////
                User.data.searchingForRandomOpponent = false;
                mocdc.removeMessage();
                DataManager.instance.saveData();
            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        User.data.challengeFromFriendReceived = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.get(0) == 3) { // OPPONENT REFUSED THE CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                mocdc.removeMessage();
                DataManager.instance.saveData();
            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH SCORE

                if (Integer.parseInt(mocdc.dataToExecute.first()[0]) == matchID) {
                    if (!User.data.getMatch(matchID).isReadyToDiscoverResult()) {
                        int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                        User.data.updateOpponentScore(matchID, score);
                        User.data.updateMatchStatus(matchID);
                        System.out.println("COGLIONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        this.rebuildStage();
                        if (User.data.getMatch(matchID).isReadyToDiscoverResult()) {
                            if (roundEndPopUp == null)
                                stage.addActor(buildRoundEndPopUp());
                            if (!popUpShowed) {
                                popUpShowed = true;
                                roundEndPopUp.setVisible(true);
                                final Interpolation interpolation = Interpolation.elasticOut;
                                // roundEndPopUp.addAction(scaleTo(1, 1, 1, interpolation));
                                roundEndPopUp.addAction(sequence(scaleTo(1, 1, popInTimeDuration), run(new Runnable() {
                                    public void run() {
                                        drwaPopUpExperienceBar = true;
                                        System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                                    }
                                })));
                            }

                        } else
                            mocdc.removeMessage();
                    }
                } else {
                    int matchID = Integer
                            .parseInt(mocdc.dataToExecute.first()[0]);
                    int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                    User.data.updateOpponentScore(matchID, score);
                    User.data.updateMatchStatus(matchID);
                    mocdc.removeMessage();
                }

            } else if (mocdc.cmdToExecute.first() == 5) { // CLOSE MATCH

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int coins = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                // User.data.calculateMatchPills(matchID);
                this.rebuildStage();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 6) { // GIVE UP MATCH
                // BY OTHER PLAYER
                if (opponentHasGivenUpPopUp == null)
                    stage.addActor(buildOpponentHasGivenUpPopUp());
                opponentHasGivenUpPopUp.setVisible(true);
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27) { // DISCARD MESSAGE!!!
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }
        }

        if (this.waitignForGiveUpServerConfirmation) {
            this.giveUpWaitingTime = this.giveUpWaitingTime
                    + Gdx.graphics.getDeltaTime();
            System.out.println(giveUpWaitingTime);
            if (mocdc.cmdToExecute.size > 0 && mocdc.cmdToExecute.first() == 7) {

                User.data.closeMatch(matchID);
                User.data.getMatch(matchID).setMatchWon(false);
                User.data.getMatch(matchID).setFinished(true);
                User.data.getMatch(matchID).setFinishedForGiveUp(true);
                waitignForGiveUpServerConfirmation = false;
                waitingForGiveUpServerConfirmationPopUp.setVisible(false);
                giveUpWaitingTime = 0;
                DataManager.instance.saveData();
                ScreenTransition transition = ScreenTransitionSlide.init(
                        duration, ScreenTransitionSlide.RIGHT, false,
                        interpolation);
                mocdc.removeMessage();
                memo.setScreen(new GameSelection(this.memo, mocdc), transition);
            } else if (this.giveUpWaitingTime > maxWaitingGiveUpConfirmationTime) {
                giveUpWaitingTime = 0;
                waitignForGiveUpServerConfirmation = false;
                waitingForGiveUpServerConfirmationPopUp.setVisible(false);
                if (waitingForGiveUpServerConfirmationERRORPopUp == null)
                    stage.addActor(buildWaitingForGiveUpServerConfirmationERRORPopUp());
                waitingForGiveUpServerConfirmationERRORPopUp.setVisible(true);
            }

        }
        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }

}