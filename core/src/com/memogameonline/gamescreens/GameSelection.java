package com.memogameonline.gamescreens;

//COMMENTO DI PROVA ASDASDASDASDASDASDASD

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamehelpers.TaskCallback;
import com.memogameonline.gameobjects.MatchCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Match;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class GameSelection extends AbstractScreen {


    private Image backgroundStep;
    private Label playWithFriendsText;
    private Button facebook;
    private Button offlineModeButton;
    private Button memoFriends;
    private Button randomOpponent;
    private Label randomOpponentButtonLabel;

    private Table activeMatches;
    private Button buttonNoActiveMatches;
    private Table finishedMatches;
    private Table scrollContainer;


    private final static float popInTimeDuration = 0.1f;

    private boolean DEBUG = false;

    public GameSelection(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "GameSelection";

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new Home(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);

    }

    @Override
    public void show() {

        rebuildStage();

    }

    public void rebuildStage() {

        User.data.refreshAllMatchesStatus();

        stage.clear();
        if (activeMatches != null)
            activeMatches.clearChildren();
        if (finishedMatches != null)
            finishedMatches.clearChildren();

        screen = new Table(memo.patchedSkin);
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.setBackground("GameSelectionBackground");

        screen.addActor(buildScrollPane(buildRunningMatches(),
                buildFinishedeMatches()));
        screen.addActor(buildImagesLayer());
        screen.addActor(buildButtonsLayer());
        stage.addActor(screen);

        if (randomOpponentChallengePopUp != null)
            stage.addActor(randomOpponentChallengePopUp);

        //stage.setDebugAll(true);

    }

    private void refreshStageAfterGameHasBeenAddedOrDeleted(long matchID, final Opponent opponent) {


        String name;

        if (opponent.getUserName().length() > 12)
            name = opponent.getUserName().substring(0, 12);
        else
            name = opponent.getUserName();

        String playerInfo = "Play  vs  " + name;
        String matchStatus = "It's your turn!";

        final MatchCell cell = new MatchCell(memo, matchID,
                opponent,
                memo.patchedSkin, "matchCell", playerInfo, matchStatus);


        cell.getButton().addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                onMatchCellClicked(challengeMatchID, opponent);
            }
        });

        System.out.println("DEBUG!!!!DEBUG!!!! ENTRO!!!!!!!!!!!!!!!!!!!!!!  NUMERO DI PARTITE ATTIVE: " + User.data.activeMatches.size);
        if (User.data.activeMatches.size == 1) {
            activeMatches.removeActor(buttonNoActiveMatches);
            activeMatches.add(cell.getButton()).padTop(-10).row();

        } else {
            Array<Actor> children = new Array<Actor>(activeMatches.getChildren());
            System.out.println("Numero di Celle: " + (activeMatches.getChildren().size - 2));
            for (int i = activeMatches.getChildren().size - 1; i >= 0; i--) {
                if (activeMatches.getChildren().get(i) instanceof Button) {
                    activeMatches.getChildren().get(i).remove();
                } else System.out.println("\n");
            }
            //activeMatches.clearChildren();
            //children.insert(2, cell.getWhiteCell());
            activeMatches.add(cell.getButton()).padTop(-8).row();
            for (int i = 2; i < children.size; i++) {
                activeMatches.add(children.get(i)).padTop(-8).row();
            }
        }
        // for (int i = children.size-1; i >= 0; i--)
        // activeMatches.removeActor(children.get(i));
        // for (Actor actor : activeMatches.getChildren())
        // actor.remove();

        activeMatches.center().top();
        // activeMatches.add(header).padTop(60).row();
        //activeMatches.add(headerText).padTop(-65).row();

    }


    private Table buildImagesLayer() {

        Table layer = new Table();

        backgroundStep = new Image(memo.patchedSkin, "backgroundStep");
        backgroundStep.setPosition(0, 507);
        // layer.addActor(backgroundStep);

        playWithFriendsText = new Label("Play with your friends!", memo.patchedSkin,
                "45blue");
        playWithFriendsText.setPosition((540 - playWithFriendsText.getWidth()) / 2, 805);
        layer.addActor(playWithFriendsText);


        return layer;
    }

    private Table buildButtonsLayer() {

        Table layer = new Table();

        offlineModeButton = new Button(memo.patchedSkin, "OfflineButton");
        offlineModeButton.setPosition(97, 656);
        offlineModeButton.add(new Label("Training\n  Mode", memo.patchedSkin, "40white"));
        offlineModeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onOfflineModeClicked();
            }
        });
        layer.addActor(offlineModeButton);
        layer.row();

        facebook = new Button(memo.patchedSkin, "FacebookButton");
        facebook.setPosition(215, 656);
        facebook.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                try {
                    onFacebookClicked();
                } catch (IOException e) {
                }
            }
        });
        layer.addActor(facebook);
        layer.row();

        memoFriends = new Button(memo.patchedSkin, "MemoFriendsButton");
        memoFriends.setPosition(333, 656);
        memoFriends.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onMemoFriendsClicked();
            }
        });
        layer.addActor(memoFriends);
        layer.row();

        if (User.data.searchingForRandomOpponent) {
            randomOpponent = new Button(memo.patchedSkin, "GreenButton");
            randomOpponentButtonLabel = new Label("Searching for opponent...",
                    memo.patchedSkin, "45white");
            randomOpponent.add(randomOpponentButtonLabel);
        } else {
            randomOpponent = new Button(memo.patchedSkin, "GreenButton");
            randomOpponentButtonLabel = new Label("Random Opponent",
                    memo.patchedSkin, "54white");
            randomOpponent.add(randomOpponentButtonLabel);
        }

        randomOpponent.setBounds(91, 560, 355, 90);
        randomOpponent.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onRandomOpponentClicked();
            }
        });
        layer.addActor(randomOpponent);
        layer.row();

        layer.setDebug(true);
        return layer;
    }

    @Override
    public Table buildRandomOpponentChallengePopUp() {

        randomOpponentChallengePopUp = new Window("", memo.patchedSkin);
        randomOpponentChallengePopUp.setBounds(70, 300, 400, 280);
        randomOpponentChallengePopUp.setBackground("BlueBackground");
        randomOpponentChallengePopUp.setTransform(true);
        randomOpponentChallengePopUp.setOrigin(randomOpponentChallengePopUp.getWidth() / 2, randomOpponentChallengePopUp.getHeight() / 2);
        playerChallengingLabel = new Label("" + challengeOpponentID
                + " vuole \n giocare con te!", memo.patchedSkin, "40white");
        randomOpponentChallengePopUp.addActor(playerChallengingLabel);
        playerChallengingLabel.setPosition(202, 138);

        accept = new TextButton("Play (" + (int) User.data.timerEvent + ")", memo.patchedSkin);
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onAcceptRandomChallengeClicked();
            }
        });
        accept.setBounds(173, 14, 193, 95);

        Button refuse = new Button(memo.patchedSkin, "RedButton");
        refuse.add(new Label("Refuse", memo.patchedSkin, "36white"));
        refuse.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onRefuseRandomChallengeClicked();
            }
        });
        refuse.setBounds(28, 14, 140, 68);


        randomOpponentChallengePopUp.addActor(accept);
        randomOpponentChallengePopUp.addActor(refuse);
        randomOpponentChallengePopUp.setScale(0, 0);
        randomOpponentChallengePopUp.setVisible(false);
        randomOpponentChallengePopUp.setModal(true);
        return randomOpponentChallengePopUp;
    }

    @Override
    public void onAcceptRandomChallengeClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        String[] sendMsg = {"0208", String.valueOf(challengeMatchID)};
        mocdc.sendToServer(sendMsg);


        User.data.addActiveMatch(challengeMatchID, challengingOpponent);
        refreshStageAfterGameHasBeenAddedOrDeleted(challengeMatchID, challengingOpponent);

        DataManager.instance.saveData();

        System.out.println("AGGIUNGO L?AZIONE??????????????????????");
        randomOpponentChallengePopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                randomOpponentChallengePopUp.setVisible(false);
                popUpShowed = false;
            }
        })));

        System.out.println(randomOpponentChallengePopUp.getActions());
        User.data.resetAndStopTimerEvent();
        mocdc.removeMessage();
        opponentForChallengePopUpHasBeenBuilded = false;
        downloadingRandomOpponentFacebookPictureHasFinished = false;

    }

    @Override
    public void onRefuseRandomChallengeClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);

        if (User.data.challengeFromFriendReceived) {
            String[] sendMsg = {"0303", challengeOpponentID};
            User.data.challengeFromFriendReceived = false;
            mocdc.sendToServer(sendMsg);
            System.out.println("RIFIUTO INVITO PERSONALIZZATO!!!!!!!!!!!!!!!!!!!!!");
        } else {
            String[] sendMsg = {"0207", String.valueOf(challengeMatchID)};
            mocdc.sendToServer(sendMsg);
        }
        randomOpponentChallengePopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                randomOpponentChallengePopUp.setVisible(false);
                popUpShowed = false;
            }
        })));
        User.data.resetAndStopTimerEvent();
        mocdc.removeMessage();
        opponentForChallengePopUpHasBeenBuilded = false;
        downloadingRandomOpponentFacebookPictureHasFinished = false;

    }

    private void onRandomOpponentClicked() {
        if (!User.data.searchingForRandomOpponent) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.pressedButton);
            User.data.searchingForRandomOpponent = true;
            String[] sendMsg = {"0200"};
            mocdc.sendToServer(sendMsg);
        }

        Label.LabelStyle style = new Label.LabelStyle(memo.patchedSkin.getFont("zighia45"), Color.WHITE);
        randomOpponentButtonLabel.setStyle(style);
        randomOpponentButtonLabel.setText("Searching for opponent...");

    }

    private void onOfflineModeClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);
        memo.setScreen(new OfflineMode(this.memo, mocdc), transition);
    }

    private void onFacebookClicked() throws IOException {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (!memo.getFacebookService().isLoggedIn()) {
            System.out
                    .println("----------------------------------EFFETTUO IL LOG IN!!!");
            TaskCallback taskCallback = new TaskCallback(memo, mocdc, this);
            taskCallback.setCalledToLogIn(true);
            memo.getFacebookService().logIn(taskCallback);
            System.out
                    .println("----------------------------------TORNO QUI ALL?USCITA DEL TASK CALL BACK?????????????");
        } else if ((User.data.facebookFriendsList.size == 0 && !User.data.comingFromFacebookFriendsListRequest)) {
            User.data.comingFromFacebookLogIn = false;
            System.out
                    .println("----------------------------------RICERCO LA LSITA DI AMICI!!!");
            TaskCallback taskCallback = new TaskCallback(memo, mocdc, this);
            taskCallback.setCalledToChallengeFriend(true);
            memo.getFacebookService().getFriendList(taskCallback);
        } else {


        /*Opponent oppo= new Opponent();
        oppo.setFacebookName("Valerio Poetafreddo Manzo");
        oppo.setFacebookID("3124512342343124234");
        oppo.setFacebookPictureURL("https://scontent.xx.fbcdn.net/hprofile-xfp1/v/t1.0-1/c41.41.517.517/s50x50/" +
                "970079_554231147963043_510477155_n.jpg?oh=bde4e0f7bb4ebc69fd069a173cd16dad&oe=575BE86B");


        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);
        User.data.facebookFriendsList.add(oppo);*/


            System.out
                    .println("----------------------------------CREO LA SCHERMATA AMICI DI FACEBOOK!!!");
            User.data.comingFromFacebookFriendsListRequest = false;
            ScreenTransition transition = ScreenTransitionSlide.init(duration,
                    ScreenTransitionSlide.LEFT, false, interpolation);
            memo.setScreen(new FacebookFriends(this.memo, this.mocdc),
                    transition);
        }
    }

    private void onMemoFriendsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);
        memo.setScreen(new MemoGameFriends(this.memo, this.mocdc), transition);

    }

    private void onMatchCellClicked(long matchID, Opponent opponent) {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);
        memo.setScreen(new GameStatus(this.memo, matchID, opponent, this.mocdc, null), transition);

    }

    @Override
    public void render(float delta) {


        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //TODO controllar che  non causi problemi (vedi blocco dello sccroll pane)
        stage.act(delta);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        // System.out.println("sprites inviate alla CPU: " + batcher.maxSpritesInBatch);
        // System.out.println("render calls: " + batcher.renderCalls);


        checkServerMessage();
        try {
            checkFacebookLoop();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

    }

    public void checkFacebookLoop() throws IOException {

        if (User.data.comingFromFacebookLogIn) {
            String[] strings = {"0603", User.data.facebookID, User.data.facebookPictureURL};
            mocdc.sendToServer(strings);
            this.onFacebookClicked();
        } else if (User.data.comingFromFacebookFriendsListRequest) {
            this.onFacebookClicked();
        }
    }

    @Override
    public void resize(int width, int height) {

        // use true here to center the camera
        // that's what you probably want in case of a UI
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    public void downloadOpponentFacebookIcon(final Long matchID, final Opponent opponent) {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                System.out.println("FOTO URL:  " + opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            User.data.addActiveMatch(challengeMatchID, opponent);
                            refreshStageAfterGameHasBeenAddedOrDeleted(matchID, opponent);
                            System.out.print("FOTO SCARICATA!!!");
                        }
                    });
                }
            }
        }).start();
    }


    public Table buildRunningMatches() {

        activeMatches = new Table();
        activeMatches.defaults().expandX();
        boolean firstCellbuilded = false;

        Image header = new Image(memo.patchedSkin, "BlueHeader");
       /* header.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                activeMatches.clear();
                User.data.activeMatches.clear();
                DataManager.instance.saveData();
                return true;
            }
        });*/
        Label headerText = new Label("Running  Matches", memo.patchedSkin,
                "38white");
        activeMatches.center().top();
        activeMatches.add(header).padTop(20).row();
        activeMatches.add(headerText).padTop(-65).row();

        if (User.data.activeMatches.size > 0) {

            for (int i = 0; i < User.data.activeMatches.size; i++) {
                if (User.data.activeMatches.get(i).isReadyToDiscoverResult()) {
                    if (User.data.activeMatches.get(i).getOpponent().getImageID() != 48 ||
                            User.data.activeMatches.get(i).getOpponent().getImageID() == 48 &&
                                    User.data.activeMatches.get(i).getOpponent().getFacebookPicture() != null
                            ) {
                        final long matchID = User.data.activeMatches.get(i).getMatchID();

                        String playerInfo = "MATCH COMPLETED!";

                        final MatchCell cell = new MatchCell(memo, matchID,
                                User.data.activeMatches.get(i).getOpponent(),
                                memo.patchedSkin, "matchCell", playerInfo, "");

                        cell.getButton().addListener(new ChangeListener() {
                            public void changed(ChangeEvent event, Actor actor) {
                                onMatchCellClicked(matchID, cell.getOpponent());
                            }
                        });
                        System.out.println("CELLA COMPELTATA!!!");
                        if (!firstCellbuilded) {
                            activeMatches.add(cell.getButton()).padTop(-8).row();
                            firstCellbuilded = true;
                        } else
                            activeMatches.add(cell.getButton()).padTop(-8).row();
                    } else {
                        System.out.println("AGGIUNDO DOPO AVER SCARICATO LA FOTO!!!");
                        downloadOpponentFacebookIcon(matchID, User.data.activeMatches.get(i).getOpponent());
                    }
                }
            }
            for (int i = 0; i < User.data.activeMatches.size; i++) {
                if (User.data.activeMatches.get(i).isYourTurn() && !User.data.activeMatches.get(i).isReadyToDiscoverResult()) {

                    if (User.data.activeMatches.get(i).getOpponent().getImageID() != 48 ||
                            User.data.activeMatches.get(i).getOpponent().getImageID() == 48 &&
                                    User.data.activeMatches.get(i).getOpponent().getFacebookPicture() != null
                            ) {
                        final long matchID = User.data.activeMatches.get(i).getMatchID();

                        String name;

                        if (User.data.activeMatches.get(i).getOpponent().getUserName().length() > 14)
                            name = User.data.activeMatches.get(i).getOpponent().getUserName().substring(0, 14);
                        else
                            name = User.data.activeMatches.get(i).getOpponent().getUserName();

                        String playerInfo = "Play  vs  " + name;
                        String matchStatus = "It's  your  turn!";

                        final MatchCell cell = new MatchCell(memo, matchID,
                                User.data.activeMatches.get(i).getOpponent(),
                                memo.patchedSkin, "matchCell", playerInfo, matchStatus);

                        cell.getButton().addListener(new ChangeListener() {
                            public void changed(ChangeEvent event, Actor actor) {
                                onMatchCellClicked(matchID, cell.getOpponent());
                            }
                        });
                        if (!firstCellbuilded) {
                            activeMatches.add(cell.getButton()).padTop(-8).row();
                            firstCellbuilded = true;
                        } else
                            activeMatches.add(cell.getButton()).padTop(-8).row();

                    } else {
                        System.out.println("AGGIUNDO DOPO AVER SCARICATO LA FOTO!!!");
                        downloadOpponentFacebookIcon(matchID, User.data.activeMatches.get(i).getOpponent());
                    }
                }
            }
            for (int i = 0; i < User.data.activeMatches.size; i++) {
                if (User.data.activeMatches.get(i).isWaitingForOpponentTurn() &&
                        !User.data.activeMatches.get(i).isReadyToDiscoverResult()) {

                    if (User.data.activeMatches.get(i).getOpponent().getImageID() != 48 ||
                            User.data.activeMatches.get(i).getOpponent().getImageID() == 48 &&
                                    User.data.activeMatches.get(i).getOpponent().getFacebookPicture() != null
                            ) {
                        final long matchID = User.data.activeMatches.get(i).getMatchID();

                        String name;

                        if (User.data.activeMatches.get(i).getOpponent().getUserName().length() > 14)
                            name = User.data.activeMatches.get(i).getOpponent().getUserName().substring(0, 14);
                        else
                            name = User.data.activeMatches.get(i).getOpponent().getUserName();

                        String playerInfo = "Play  vs  " + name;
                        String matchStatus = "Waiting  for  opponent!";

                        final MatchCell cell = new MatchCell(memo, matchID,
                                User.data.activeMatches.get(i).getOpponent(),
                                memo.patchedSkin, "matchCell", playerInfo, matchStatus);

                        cell.getButton().addListener(new ChangeListener() {
                            public void changed(ChangeEvent event, Actor actor) {
                                onMatchCellClicked(matchID, cell.getOpponent());
                            }
                        });
                        System.out.println("WAITING FOR OPPONENT!!!!");
                        if (i == 0 && User.data.activeMatches.size == 0)
                            activeMatches.add(cell.getButton()).padTop(-8).row();
                        else
                            activeMatches.add(cell.getButton()).padTop(-8).row();

                    } else {
                        System.out.println("AGGIUNDO DOPO AVER SCARICATO LA FOTO!!!");
                        downloadOpponentFacebookIcon(matchID, User.data.activeMatches.get(i).getOpponent());

                    }
                }
            }


        } else {

            buttonNoActiveMatches = new Button(memo.patchedSkin, "constantWhiteCell");
            buttonNoActiveMatches.add(new Label("You don't have active matches!",
                    memo.patchedSkin, "30black"));
            activeMatches.add(buttonNoActiveMatches).padTop(-8).row();
        }

        activeMatches.top();
        activeMatches.setDebug(DEBUG);
        return activeMatches;
    }

    public Table buildFinishedeMatches() {

        finishedMatches = new Table();
        finishedMatches.defaults().expandX();

        if (User.data.finishedMatches.size > 0) {

            Image header = new Image(memo.patchedSkin, "BlueHeaderWithX");
            Label headerText = new Label("Completed  Matches", memo.patchedSkin,
                    "38white");

            Image x = new Image(memo.patchedSkin, "matchCellTimeContainer");
            x.addListener(new ClickListener() {
                public boolean touchDown(InputEvent event, float x, float y,
                                         int pointer, int button) {

                    String[] msg = new String[2];
                    msg[0] = "1003";
                    for (int i = 0; i < User.data.finishedMatches.size; i++) {
                        msg[1] = String.valueOf(User.data.finishedMatches.get(i).getMatchID());
                        mocdc.sendToServer(msg);
                    }


                    finishedMatches.clear();
                    User.data.finishedMatches.clear();
                    DataManager.instance.saveData();
                    return true;
                }
            });

            finishedMatches.add(header).padTop(33).row();
            finishedMatches.add(x).padTop(-61).padLeft(421).row();//442
            finishedMatches.add(headerText).padTop(-65).row();

            for (int i = 0; i < User.data.finishedMatches.size; i++) {

                String playerInfo;
                String matchStatus;
                String name;

                if (User.data.finishedMatches.get(i).getOpponent().getUserName().length() > 12)
                    name = User.data.finishedMatches.get(i).getOpponent().getUserName().substring(0, 12);
                else
                    name = User.data.finishedMatches.get(i).getOpponent().getUserName();

                if (User.data.finishedMatches.get(i).isMatchWon()) {
                    playerInfo = "Won  vs  " + name;
                    if (User.data.finishedMatches.get(i).isFinishedForGiveUp())
                        matchStatus = "Opponent  gave  up";
                    else
                        matchStatus = "match  completed";
                } else {
                    playerInfo = "Lost  vs  " + name;
                    if (User.data.finishedMatches.get(i).isFinishedForGiveUp())
                        matchStatus = "You  gave  up";
                    else
                        matchStatus = "match  completed";
                }

                final long matchID = User.data.finishedMatches.get(i).getMatchID();
                final MatchCell cell = new MatchCell(memo, matchID,
                        User.data.finishedMatches.get(i).getOpponent(),
                        memo.patchedSkin, "matchCell", playerInfo, matchStatus);

                cell.getButton().addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        onMatchCellClicked(matchID, cell.getOpponent());
                    }
                });
                if (i == 0)
                    finishedMatches.add(cell.getButton()).padTop(-8).row();
                else
                    finishedMatches.add(cell.getButton()).padTop(-8).row();
            }

        }
        finishedMatches.top();
        finishedMatches.setDebug(DEBUG);
        return finishedMatches;
    }

    protected void onDeleteFinishedMatchesButtonClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        User.data.finishedMatches.clear();
        this.rebuildStage();

    }

    public Table buildScrollPane(Table table1, Table table2) {

        scrollContainer = new Table(memo.patchedSkin);

        Table table = new Table();
        table.add(table1).row();
        table.top();
        table.add(table2).row();
        table.top();

        ScrollPane scrollPane = new ScrollPane(table, memo.patchedSkin,
                "scrollPane");

        scrollPane.setScrollBarPositions(true, false);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollbarsOnTop(false);
        scrollPane.setSmoothScrolling(true);

        scrollContainer.setBounds(0, 19, 540, 536);
        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(6, -2, 528, 525);
        // scrollContainer.addActor(background);

        scrollContainer.add(scrollPane).padTop(0).expand().fill()
                .colspan(4);

        // scrollContainer.setDebug(DEBUG);
        return scrollContainer;

    }


    public void checkServerMessage() {
        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.get(0) == 2) { // OPPONENT FOUND, CREATE A MATCh
                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                if (opponent.getImageID() != 48) {
                    User.data.addActiveMatch(challengeMatchID, opponent);
                    refreshStageAfterGameHasBeenAddedOrDeleted(challengeMatchID, opponent);
                } else {
                    downloadOpponentFacebookIcon(challengeMatchID, opponent);
                    System.out.println("NON HO LA FOTO!!! DEVO SCARICARLA!!!!!");
                }


                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA compsota di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////
                User.data.searchingForRandomOpponent = false;
                // this.rebuildStage();
                Label.LabelStyle style = new Label.LabelStyle(memo.patchedSkin.getFont("zighia48"), Color.WHITE);
                randomOpponentButtonLabel.setStyle(style);
                randomOpponentButtonLabel.setText("Random Opponent");
                DataManager.instance.saveData();
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.get(0) == 3) { // OPPONENT REFUSE CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                this.rebuildStage();
                mocdc.removeMessage();
                DataManager.instance.saveData();
            } else if (mocdc.cmdToExecute.first() == 5) { // CHECK COINS OF THE MATCH

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int coins = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                this.rebuildStage();
                mocdc.removeMessage();
                DataManager.instance.saveData();
            } else if (mocdc.cmdToExecute.first() == 6) { // CLOSE MATCH BECAUSE A GIVE UP OF OPPONENT

                Long matchID = Long.valueOf(mocdc.dataToExecute.first()[0]);
                if (User.data.getMatch(matchID) != null) {
                    User.data.closeMatch(matchID);
                    User.data.getMatch(matchID).setMatchWon(false);
                    User.data.getMatch(matchID).setFinished(true);
                    User.data.getMatch(matchID).setFinishedForGiveUp(true);
                    DataManager.instance.saveData();
                }
                mocdc.removeMessage();
                this.rebuildStage();
            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {

                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        popUpShowed = true;
                        randomOpponentChallengePopUp.setVisible(true);
                        randomOpponentChallengePopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        User.data.challengeFromFriendReceived = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        popUpShowed = true;
                        randomOpponentChallengePopUp.setVisible(true);
                        randomOpponentChallengePopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27) { // DISCARD MESSAGE!!!
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }
        }

        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }

}