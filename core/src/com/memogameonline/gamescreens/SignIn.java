package com.memogameonline.gamescreens;


import java.io.IOException;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamehelpers.TaskCallback;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SignIn extends AbstractScreen {

    private int currentPanel = 0;
    private Table mainPanel;
    private Table signInPanel;
    private Table signUpPanel;
    private Table forgottenPasswordPanel;

    private Button credentialsSignInPanel;
    private TextField userNameFieldSignInPanel;
    private TextField passwordFieldSignInPanel;
    private Button signInButtonSignInPanel;
    private Button forgottenPasswordButtonSignInPanel;

    private Button credentialsSignUpPanel;
    private TextField userNameFieldSignUpPanel;
    private TextField passwordFieldSignUpPanel;
    private TextField emailFieldSignUpPanel;
    private Button signUpButtonSignUpPanel;
    private int imageIdTEMP;

    private Button credentialsForgottenPasswordPanel;
    private TextField emailFieldForgottenPasswordPanel;
    private Button emailButtonForgottenPasswordPanel;

    private Button signIn;
    private Button signUp;
    private Button facebook;

    private boolean alreadyExistError;
    private boolean alreadyLoggedError;
    private boolean wrongCredentialsError;

    public TaskCallback taskCallback;

    private float mainPanelHeight = 0;
    private float signInPanelHeight = 0;
    private float signUpPanelHeight = 0;
    private float forgotPasswordPanelHeight = 0;

    private boolean buttonsBuilded = false;

    public SignIn(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "SignIn";
        //memo.getAdsController().showBannerAd();

        multiplexer = new InputMultiplexer();
        InputAdapter backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    resetPanels();
                }
                return true;
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);

        //String s="https://scontent.xx.fbcdn.net/hprofile-xpa1/v/t1.0-1/p50x50/12115705_1650716381878140_2621404005692089800_n.jpg?oh=393ae634f765a231c4382a29aed353eb&oe=5740545F";
        // String[] strings = {s};
        // mocdc.sendToServer(strings);

        //memo.getAdsController().loadInterstital();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);

        stage.draw();

        checkServerMessage();

        checkFacebookLogIn();

        if (!memo.socket.isConnected()) {
            memo.setScreen(new ReconnectionScreen(memo));
        }

    }

    @Override
    public void show() {

        GamePreferences.instance.load();

        if (GamePreferences.instance.autoLogIn) {
            autoSignIn();
            screen.setBackground("SignInBackground");
            stage.addActor(screen);
        } else if (GamePreferences.instance.autoLogInFacebook) {
            autoSignInFacebook();
            screen.setBackground("SignInBackground");
            stage.addActor(screen);
        } else {
            screen.clear();
            screen.setBackground("SignInBackground");
            screen.addActor(buildButtonAndFields());


            ///////////////////////////////DISPLAY VERSION NAME
            Label versionCode = new Label("versionCode: " + String.valueOf(memo.versionCode), memo.patchedSkin, "30black");
            versionCode.setPosition(5, 35);
            Label versionName = new Label("versionName: " + memo.versionName, memo.patchedSkin, "30black");
            versionName.setPosition(5, 5);
            screen.addActor(versionCode);
            screen.addActor(versionName);
            //////////////////////////////////////////////////

            stage.clear();
            stage.addActor(screen);
            buttonsBuilded = true;
        }

    }

    public void checkFacebookLogIn() {

        if (User.data.comingFromFacebookLogIn) {
            screen.clear();
            screen.setBackground("SignInBackground");
            User.data.comingFromFacebookLogIn = false;
            String[] strings = {"0115", User.data.facebookID};
            mocdc.sendToServer(strings);
            System.out.println("VENGO DAL LOG IN E SCRIVO AL SERVER!!!");
        }

    }


    @Override
    public void pause() {


    }

    @Override
    public void resume() {

    }

    private void catchKeyboardInput() {

        if (stage.getKeyboardFocus() == userNameFieldSignInPanel) {
            System.out.println("Passo al password Field SIGN IN!!!");
            stage.setKeyboardFocus(passwordFieldSignInPanel);
        } else if (stage.getKeyboardFocus() == passwordFieldSignInPanel) {
            Gdx.input.setOnscreenKeyboardVisible(false);
            screen.addAction(moveTo(0, 0, 0.2f));
            System.out
                    .println("ESCO dal password Field e SIGN IN PANEL!!!  Abbasso bg e tolgo KEYBOARD!!!");
        } else if (stage.getKeyboardFocus() == userNameFieldSignUpPanel) {
            stage.setKeyboardFocus(passwordFieldSignUpPanel);
            System.out.println("Passo al password Field SIGN UP!!!");
        } else if (stage.getKeyboardFocus() == passwordFieldSignUpPanel) {
            stage.setKeyboardFocus(emailFieldSignUpPanel);
            System.out.println("Passo al EMail Field SIGN Up!!!");
        } else if (stage.getKeyboardFocus() == emailFieldSignUpPanel) {
            Gdx.input.setOnscreenKeyboardVisible(false);
            screen.addAction(moveTo(0, 0, 0.2f));
            System.out
                    .println("ESCO dall'email Field SIGN UP PANEL!!!  Abbasso bg e tolgo KEYBOARD!!!");
        } else if (stage.getKeyboardFocus() == emailFieldForgottenPasswordPanel) {
            Gdx.input.setOnscreenKeyboardVisible(false);
            screen.addAction(moveTo(0, 0, 0.2f));
            System.out
                    .println("ESCO dal forgotten Panel!!!  Abbasso bg e tolgo KEYBOARD!!!");
        }

        // nextField.setText("");

    }

    public Table buildButtonAndFields() {

        System.out
                .println("COSTRUISCO!!!!!!!!");
        // PANNELLO SIGN IN: SIGN IN (BOTTONE E DUE CAMPI)
        signInPanel = new Table();
        credentialsSignInPanel = new Button(memo.patchedSkin, "SignIn2TextFields");
        credentialsSignInPanel.setPosition(58, 263);
        userNameFieldSignInPanel = new TextField("", memo.patchedSkin);
        userNameFieldSignInPanel.setBounds(15, 70, 300, 45);
        userNameFieldSignInPanel.setMessageText("User Name");
        userNameFieldSignInPanel.setText("");

        userNameFieldSignInPanel.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER
                        || event.getKeyCode() == Keys.TAB)
                    stage.setKeyboardFocus(passwordFieldSignInPanel);
                return true;
            }
        });

        passwordFieldSignInPanel = new TextField("textField", memo.patchedSkin);
        passwordFieldSignInPanel.setBounds(15, 8, 300, 45);
        passwordFieldSignInPanel.setPasswordMode(true);
        passwordFieldSignInPanel.setPasswordCharacter('"');
        passwordFieldSignInPanel.setMessageText("Password");
        passwordFieldSignInPanel.setText("");
        passwordFieldSignInPanel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER) {
                    onSignInButtonSignInPanelClicked();
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    // screen.addAction(moveTo(0, 0, 0.2f));
                }
                return true;
            }
        });

        credentialsSignInPanel.addActor(userNameFieldSignInPanel);
        credentialsSignInPanel.addActor(passwordFieldSignInPanel);

        signInButtonSignInPanel = new Button(memo.patchedSkin, "GreenButton");
        signInButtonSignInPanel.add(new Label("Sign in", memo.patchedSkin,
                "48white"));
        signInButtonSignInPanel.setBounds(53, 163, 416, 86);

        signInButtonSignInPanel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSignInButtonSignInPanelClicked();
            }
        });

        forgottenPasswordButtonSignInPanel = new Button(memo.patchedSkin, "GreenButton");
        forgottenPasswordButtonSignInPanel.add(new Label(
                "Forgot  your  password?", memo.patchedSkin, "40white"));
        forgottenPasswordButtonSignInPanel.setBounds(53, 66, 416, 86);

        forgottenPasswordButtonSignInPanel.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        onForgottenPasswordButtonSignInPanelClicked();
                    }
                });

        signInPanel.addActor(credentialsSignInPanel);
        signInPanel.addActor(signInButtonSignInPanel);
        signInPanel.addActor(forgottenPasswordButtonSignInPanel);

        // PANNELLO FORGOTTEN PASSWORD
        forgottenPasswordPanel = new Table();
        credentialsForgottenPasswordPanel = new Button(memo.patchedSkin,
                "SignInTextFields");
        credentialsForgottenPasswordPanel.setPosition(58, 350);
        emailFieldForgottenPasswordPanel = new TextField("textField", memo.patchedSkin);
        emailFieldForgottenPasswordPanel.setBounds(15, 8, 300, 45);
        emailFieldForgottenPasswordPanel.setMessageText("   Write here your email addres");
        emailFieldForgottenPasswordPanel.setText("");
        emailFieldForgottenPasswordPanel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                // forgottenPasswordPanel.addAction(moveTo(
                // forgottenPasswordPanel.getX(),
                // forgottenPasswordPanel.getY() + 100, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER) {
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    // screen.addAction(moveTo(0, 0, 0.2f));
                    // forgottenPasswordPanel.addAction(moveTo(
                    // forgottenPasswordPanel.getX(), 0, 0.2f));
                }
                return true;
            }
        });

        credentialsForgottenPasswordPanel
                .addActor(emailFieldForgottenPasswordPanel);

        emailButtonForgottenPasswordPanel = new Button(memo.patchedSkin, "GreenButton");
        emailButtonForgottenPasswordPanel.add(new Label("Recover  password", memo.patchedSkin, "40white"));
        emailButtonForgottenPasswordPanel.setBounds(53, 260, 416, 86);
        emailButtonForgottenPasswordPanel.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        onForgottenPasswordClicked();
                    }
                });

        forgottenPasswordPanel.addActor(credentialsForgottenPasswordPanel);
        forgottenPasswordPanel.addActor(emailButtonForgottenPasswordPanel);

        // PANNELLO SIGN UP
        signUpPanel = new Table();
        credentialsSignUpPanel = new Button(memo.patchedSkin, "SignIn3TextFields");
        credentialsSignUpPanel.setPosition(58, 262);

        userNameFieldSignUpPanel = new TextField("textField", memo.patchedSkin);
        userNameFieldSignUpPanel.setBounds(15, 132, 300, 45);
        userNameFieldSignUpPanel.setMessageText("User Name");
        userNameFieldSignUpPanel.setText("");
        userNameFieldSignUpPanel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER
                        || event.getKeyCode() == Keys.TAB)
                    stage.setKeyboardFocus(passwordFieldSignUpPanel);
                return true;
            }
        });
        passwordFieldSignUpPanel = new TextField("textField", memo.patchedSkin);
        passwordFieldSignUpPanel.setBounds(15, 70, 300, 45);
        passwordFieldSignUpPanel.setPasswordMode(true);
        passwordFieldSignUpPanel.setMessageText("Password");
        passwordFieldSignUpPanel.setPasswordCharacter('"');
        passwordFieldSignUpPanel.setText("");
        passwordFieldSignUpPanel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER
                        || event.getKeyCode() == Keys.TAB)
                    stage.setKeyboardFocus(emailFieldSignUpPanel);
                return true;
            }
        });
        emailFieldSignUpPanel = new TextField("textField", memo.patchedSkin);
        emailFieldSignUpPanel.setBounds(15, 8, 300, 45);
        emailFieldSignUpPanel.setMessageText("email (optional)");
        emailFieldSignUpPanel.setText("");
        emailFieldSignUpPanel.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                // screen.addAction(moveTo(0, 170, 0.2f));
                return true;
            }

            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                // userNameFieldSignInPanel.next(false);
                if (event.getKeyCode() == Keys.ENTER) {
                    onSignUpButtonSignUpPanelClicked();
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    // screen.addAction(moveTo(0, 0, 0.2f));
                }
                return true;
            }
        });

        credentialsSignUpPanel.addActor(userNameFieldSignUpPanel);
        credentialsSignUpPanel.addActor(passwordFieldSignUpPanel);
        credentialsSignUpPanel.addActor(emailFieldSignUpPanel);


        signUpButtonSignUpPanel = new Button(memo.patchedSkin, "GreenButton");
        signUpButtonSignUpPanel.add(new Label("Create  Account",
                memo.patchedSkin, "42white"));
        signUpButtonSignUpPanel.setBounds(53, 156, 416, 86);
        signUpButtonSignUpPanel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSignUpButtonSignUpPanelClicked();
            }
        });
        signUpPanel.addActor(credentialsSignUpPanel);
        signUpPanel.addActor(signUpButtonSignUpPanel);

        // PANNELLO INIZIALE !!!!!!!!!!!!!!!!!!!!!!!!
        mainPanel = new Table();
        mainPanel.setWidth(540);
        mainPanel.setHeight(300);

        signUp = new Button(memo.patchedSkin, "GreenButton");
        signUp.add(new Label("Create  a  MemoGame  Account", memo.patchedSkin,
                "38white"));
        signUp.setBounds(59, 118, 416, 86);
        signUp.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSignUpClicked();
            }
        });

        signIn = new Button(memo.patchedSkin, "GreenButton");
        signIn.add(new Label("Sign  in  to  your  MemoGame\n                   account", memo.patchedSkin,
                "38white"));
        signIn.setBounds(59, 214, 416, 86);
        signIn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSignInClicked();
            }
        });

        facebook = new Button(memo.patchedSkin, "BlueButton");
        facebook.add(new Label("Log  in  with  Facebook", memo.patchedSkin,
                "40white"));
        facebook.setBounds(59, 309, 416, 86);
        facebook.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onFacebookClicked();
            }
        });

        mainPanel.addActor(signIn);
        mainPanel.addActor(signUp);
        mainPanel.addActor(facebook);

        Table table = new Table();
        table.addActor(mainPanel);
        mainPanel.setPosition(0, mainPanelHeight);
        table.addActor(signUpPanel);
        signUpPanel.setPosition(1080, signUpPanelHeight);
        table.addActor(signInPanel);
        signInPanel.setPosition(1080, signInPanelHeight);
        table.addActor(forgottenPasswordPanel);
        forgottenPasswordPanel.setPosition(1080, forgotPasswordPanelHeight);

        return table;

    }

    public void autoSignIn() {

        User.data.userName = GamePreferences.instance.lastLoggedUser;
        String[] SignInMsg = new String[3];
        SignInMsg[0] = "0105";
        if (DataManager.instance.loadData()) {
            SignInMsg[1] = User.data.userName;
            SignInMsg[2] = User.data.password;
        } else {
            System.out.println("FILE  NON trovato!!!");
            SignInMsg[1] = "asdasd";
            SignInMsg[2] = "asdasd";
        }


        mocdc.sendToServer(SignInMsg);
        currentPanel = 1;
    }

    public void autoSignInFacebook() {

        User.data.userName = GamePreferences.instance.lastLoggedUser;
        DataManager.instance.loadData();
        System.out.println("Provo il log  in automatico con facebook!!!! ID FACEBOOK: " + User.data.facebookID);
        String[] SignInMsg = new String[2];
        SignInMsg[0] = "0115";
        if (DataManager.instance.loadData()) {
            SignInMsg[1] = User.data.facebookID;
        } else {
            SignInMsg[1] = "ERRORENELCARICAMENTOFILE!!!";
        }
        mocdc.sendToServer(SignInMsg);
    }

    public void onSignInButtonSignInPanelClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        String[] strings = {"0105", userNameFieldSignInPanel.getText(),
                passwordFieldSignInPanel.getText()};
        mocdc.sendToServer(strings);
        User.data.userName = userNameFieldSignInPanel.getText();
        User.data.password = passwordFieldSignInPanel.getText();

    }

    public void onSignUpButtonSignUpPanelClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (userNameFieldSignUpPanel.getText().equals(""))
            System.out.println("Inserire  un NOME UTENTE!!!");
        else if (passwordFieldSignUpPanel.getText().equals(""))
            System.out.println("Inserire una PASSWORD!!!");
        else {
            Random r = new Random();
            imageIdTEMP = r.nextInt(12);
            String[] strings = {"0102", userNameFieldSignUpPanel.getText(),
                    passwordFieldSignUpPanel.getText(),
                    emailFieldSignUpPanel.getText(),
                    String.valueOf(imageIdTEMP)};
            mocdc.sendToServer(strings);
        }

    }

    public void onSignInClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        signInPanel.addAction(moveTo(10, signInPanelHeight, 0.5f));
        mainPanel.addAction(sequence(
                parallel(moveTo(-1080, mainPanelHeight, 0.5f), fadeOut(0.2f)),
                fadeIn(0.1f)));
        currentPanel = 1;

    }

    public void onSignUpClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        signUpPanel.addAction(moveTo(10, signUpPanelHeight, 0.5f));
        mainPanel.addAction(sequence(
                parallel(moveTo(-1080, mainPanelHeight, 0.5f), fadeOut(0.2f)),
                fadeIn(0.1f)));
        currentPanel = 1;
    }

    public void onForgottenPasswordButtonSignInPanelClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        forgottenPasswordPanel.addAction(moveTo(10, forgotPasswordPanelHeight,
                0.5f));
        signInPanel
                .addAction(sequence(
                        parallel(moveTo(-1080, signInPanelHeight, 0.5f),
                                fadeOut(0.2f)), fadeIn(0.1f)));
        currentPanel = 2;

    }


    public void onForgottenPasswordClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        String[] strings = {"0120", emailFieldForgottenPasswordPanel.getText()};
        mocdc.sendToServer(strings);

    }

    public void resetPanels() {

		/*
         * if (stage.getKeyboardFocus() == userNameFieldSignInPanel ||
		 * stage.getKeyboardFocus() == passwordFieldSignInPanel ||
		 * stage.getKeyboardFocus() == emailFieldForgottenPasswordPanel ||
		 * stage.getKeyboardFocus() == userNameFieldSignUpPanel ||
		 * stage.getKeyboardFocus() == passwordFieldSignUpPanel ||
		 * stage.getKeyboardFocus() == emailFieldSignUpPanel) {
		 * screen.addAction(moveTo(0, 0, 0.2f)); }
		 */
        if (currentPanel == 1) {
            signUpPanel.addAction(moveTo(1080, signUpPanelHeight, 0.5f));
            signInPanel.addAction(moveTo(1080, signInPanelHeight, 0.5f));
            mainPanel.addAction(moveTo(0, mainPanelHeight, 0.5f));
            currentPanel = 0;
        } else if (currentPanel == 2) {
            signInPanel.addAction(moveTo(10, signInPanelHeight, 0.5f));
            forgottenPasswordPanel.addAction(moveTo(1080,
                    forgotPasswordPanelHeight, 0.5f));
            currentPanel = 1;
        }

    }

    public void onFacebookClicked() {

        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        taskCallback = new TaskCallback(memo, mocdc, this);
        taskCallback.setCalledToLogIn(true);
        try {
            memo.getFacebookService().logIn(taskCallback);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void displayError() {

        //System.out.println(this.alreadyLoggedError);
        if (this.wrongCredentialsError) {
            Label errorMessage = new Label("Wrong User Name/Password!",
                    memo.patchedSkin, "28red");

            errorMessage.setPosition(230, 340);
            errorMessage.setColor(Color.RED);
            errorMessage.addAction(fadeOut(4));

            if ((GamePreferences.instance.autoLogIn == true || GamePreferences.instance.autoLogInFacebook) && !buttonsBuilded) {
                screen.addActor(buildButtonAndFields());
                signInPanel.addAction(moveTo(10, signInPanelHeight, 0.5f));
                mainPanel.addAction(sequence(
                        parallel(moveTo(-1080, mainPanelHeight, 0.5f), fadeOut(0.2f)),
                        fadeIn(0.1f)));
                currentPanel = 1;
                buttonsBuilded = true;
            }
            userNameFieldSignInPanel.setText(User.data.userName);
            passwordFieldSignInPanel.setText(User.data.password);
            signInPanel.addActor(errorMessage);
            wrongCredentialsError = false;
        } else if (this.alreadyLoggedError) {
            Label errorMessage = new Label("User already logged in!",
                    memo.patchedSkin, "28red");
            errorMessage.setPosition(260, 340);
            errorMessage.setColor(Color.RED);
            errorMessage.addAction(fadeOut(4));

            if ((GamePreferences.instance.autoLogIn == true || GamePreferences.instance.autoLogInFacebook) && !buttonsBuilded) {
                screen.addActor(buildButtonAndFields());
                signInPanel.addAction(moveTo(10, signInPanelHeight, 0.5f));
                mainPanel.addAction(sequence(
                        parallel(moveTo(-1080, mainPanelHeight, 0.5f), fadeOut(0.2f)),
                        fadeIn(0.1f)));
                currentPanel = 1;
                buttonsBuilded = true;
            }

            signInPanel.addActor(errorMessage);
            userNameFieldSignInPanel.setText(User.data.userName);
            passwordFieldSignInPanel.setText(User.data.password);
            System.out.println("GESTIONE CORRETTA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            alreadyLoggedError = false;

        } else if (this.alreadyExistError) {
            Label errorMessage = new Label("User Name already exists",
                    memo.patchedSkin, "28red");
            errorMessage.setPosition(255, 400);
            errorMessage.setColor(Color.RED);
            errorMessage.addAction(fadeOut(4));
            signUpPanel.addActor(errorMessage);
            System.out.println("GESTIONE CORRETTA DELL'ERRORE SIGN UP!!!!");
            alreadyExistError = false;

        }
    }

    @Override
    public void checkServerMessage() {
        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80 || mocdc.cmdToExecute.first() == 98 || mocdc.cmdToExecute.first() == 99) {
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.first() == 21) { // SIGN UP SUCCESS

                User.data.resetData();
                User.data.userName = userNameFieldSignUpPanel.getText();
                User.data.password = passwordFieldSignUpPanel.getText();
                if (!emailFieldSignUpPanel.getText().equals("")) {
                    User.data.email = emailFieldSignUpPanel.getText();
                }

                User.data.imageId = imageIdTEMP;
                DataManager.instance.saveData();

                GamePreferences.instance.lastLoggedUser = User.data.userName;
                GamePreferences.instance.autoLogIn = true;
                GamePreferences.instance.save();
                mocdc.removeMessage();
                memo.setScreen(new SynchronizingScreen(memo, mocdc));

                System.out.println("EMAIL!!!!!!" + User.data.email);

            } else if (mocdc.cmdToExecute.first() == 22) { // SIGN UP FAILED, NAME ALREADY EXISTS
                mocdc.removeMessage();
                this.alreadyExistError = true;
                this.displayError();
            } else if (mocdc.cmdToExecute.first() == 23) { // SIGN IN SUCCESS
                GamePreferences.instance.lastLoggedUser = User.data.userName;
                if (!GamePreferences.instance.autoLogIn)
                    DataManager.instance.saveData();
                GamePreferences.instance.autoLogIn = true;
                GamePreferences.instance.autoLogInFacebook = false;
                GamePreferences.instance.save();
                // DA TOGLIERE!!!!!!!!!!!!!!!!!!!!!!!!
                // DataManager.instance.saveData();
                // ///////////////////////////////////
                //DataManager.instance.loadData();
                //TODO Da controllare!
                User.data.facebookPictureURL = String.valueOf(mocdc.dataToExecute.first()[0]);
                User.data.level = Integer.valueOf(mocdc.dataToExecute.first()[1]);
                User.data.experience = Integer.valueOf(mocdc.dataToExecute.first()[2]);
                User.data.imageId = Integer.valueOf(mocdc.dataToExecute.first()[3]);
                User.data.playedMatches = Integer.valueOf(mocdc.dataToExecute.first()[4]);
                User.data.wonMatches = Integer.valueOf(mocdc.dataToExecute.first()[5]);
                User.data.bestMatchWinningStreak = Integer.valueOf(mocdc.dataToExecute.first()[6]);
                User.data.highestMatchScore = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                User.data.highestFirstRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                User.data.bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[9]);
                User.data.highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[10]);
                User.data.bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[11]);
                User.data.highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[12]);
                User.data.bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[13]);

                User.data.powerUp_Time = Integer.valueOf(mocdc.dataToExecute.first()[14]);
                User.data.powerUp_DoubleCouple = Integer.valueOf(mocdc.dataToExecute.first()[15]);
                User.data.powerUp_FaceUp = Integer.valueOf(mocdc.dataToExecute.first()[16]);
                User.data.powerUp_Stop = Integer.valueOf(mocdc.dataToExecute.first()[17]);
                User.data.powerUp_Chain = Integer.valueOf(mocdc.dataToExecute.first()[18]);
                User.data.powerUp_Bonus = Integer.valueOf(mocdc.dataToExecute.first()[19]);

                User.data.pills=Integer.valueOf(mocdc.dataToExecute.first()[20]);

                mocdc.removeMessage();
                memo.setScreen(new SynchronizingScreen(memo, mocdc));
            } else if (mocdc.cmdToExecute.first() == 27) { // SIGN IN SUCCESS FACEBOOK
                System.out.println("LOG  IN FACEBOOK CON SUCCESSO!!! AUTOMATIC LOG IN ATTIVATO?  " + GamePreferences.instance.autoLogInFacebook);
                User.data.userName = mocdc.dataToExecute.first()[0];
                System.out.println("SETTO USER NAME: " + User.data.userName);
                if (!GamePreferences.instance.autoLogInFacebook) {
                    System.out.println("CREO IL BENEDETTO FILE!!!!");
                    DataManager.instance.saveData();
                }
                GamePreferences.instance.lastLoggedUser = User.data.userName;
                GamePreferences.instance.autoLogIn = false;
                GamePreferences.instance.autoLogInFacebook = true;
                GamePreferences.instance.save();
                // DA TOGLIERE!!!!!!!!!!!!!!!!!!!!!!!!
                // DataManager.instance.saveData();
                // ///////////////////////////////////
                // DataManager.instance.loadData();
                User.data.facebookPictureURL = mocdc.dataToExecute.first()[1];
                User.data.level = Integer.valueOf(mocdc.dataToExecute.first()[2]);
                User.data.experience = Integer.valueOf(mocdc.dataToExecute.first()[3]);
                User.data.imageId = Integer.valueOf(mocdc.dataToExecute.first()[4]);
                User.data.playedMatches = Integer.valueOf(mocdc.dataToExecute.first()[5]);
                User.data.wonMatches = Integer.valueOf(mocdc.dataToExecute.first()[6]);
                User.data.bestMatchWinningStreak = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                User.data.highestMatchScore = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                User.data.highestFirstRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                User.data.bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                User.data.highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                User.data.bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                User.data.highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                User.data.bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);

                User.data.powerUp_Time = Integer.valueOf(mocdc.dataToExecute.first()[15]);
                User.data.powerUp_DoubleCouple = Integer.valueOf(mocdc.dataToExecute.first()[16]);
                User.data.powerUp_FaceUp = Integer.valueOf(mocdc.dataToExecute.first()[17]);
                User.data.powerUp_Stop = Integer.valueOf(mocdc.dataToExecute.first()[18]);
                User.data.powerUp_Chain = Integer.valueOf(mocdc.dataToExecute.first()[19]);
                User.data.powerUp_Bonus = Integer.valueOf(mocdc.dataToExecute.first()[20]);

                User.data.pills=Integer.valueOf(mocdc.dataToExecute.first()[21]);

                mocdc.removeMessage();
                memo.setScreen(new SynchronizingScreen(memo, mocdc));

            } else if (mocdc.cmdToExecute.first() == 24) { // SIGN IN FAILED (WRONG USERNAME/PASSWORD)

                mocdc.removeMessage();
                this.wrongCredentialsError = true;
                this.displayError();

            } else if (mocdc.cmdToExecute.first() == 25) { // SIGN IN FAILED (USER ALREADY LOGGED IN)
                mocdc.removeMessage();
                this.alreadyLoggedError = true;
                this.displayError();

            } else if (mocdc.cmdToExecute.first() == 26) { // SIGN UP FACEBOOK

                ScreenTransition transition = ScreenTransitionSlide.init(duration,
                        ScreenTransitionSlide.LEFT, false, interpolation);
                memo.setScreen(new UsernameChooseFacebookSignUp(this.memo, this.mocdc), transition);

                mocdc.removeMessage();

            }
        }
    }
}
