package com.memogameonline.gamescreens;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.LevelManagement;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class RoundEndPopUp extends AbstractScreen {


    private Table popUp;
    private Button facebook;
    private Button twitter;

    private long matchID;
    private float time;
    private int score;
    private int totalScore;
    private int timeBonusScore;
    private float remainingTime;

    private boolean offlineMode;
    private int round;

    public RoundEndPopUp(MemoGameOnline memo, MOCDataCarrier mocdc,

                         long matchID, Opponent opponent, int totalScore, int timeBonusScore, float remainingTime, boolean offlineMode, int round) {


        super(memo, mocdc);

        this.matchID = matchID;
        this.opponent = opponent;
        this.totalScore = totalScore;
        this.timeBonusScore = timeBonusScore;
        this.remainingTime = remainingTime;
        this.offlineMode = offlineMode;

        this.round = round;
        popUp = new Table(memo.patchedSkin);
        // popUp.setColor(1f, 1f, 1f, 0.8f);
        popUp.setBounds(62.5f, 258, 415, 300);
        popUp.setBackground("BlueBackground");

        popUp.addActor(buildButtonLayer());
        popUp.addActor(buildTextLayer());

        stage.addActor(popUp);

    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public Table buildTextLayer() {

        Table layer = new Table();
        Label text = new Label("Condividi con i tuoi amici!", memo.patchedSkin,
                "36white");
        text.setPosition(78, 265);
        // layer.addActor(text);
        return layer;
    }

    public Table buildButtonLayer() {

        Table layer = new Table();

        Button continueButton = new Button(memo.patchedSkin, "GreenButton");
        continueButton.add(new Label("Continue", memo.patchedSkin, "60white"));

        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onContinueClicked();
            }
        });
        continueButton.setBounds(72.5f, 8, 272, 105);

        facebook = new Button(memo.patchedSkin, "FacebookButton");
        facebook.setPosition(continueButton.getX() + 24, 125);
        facebook.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Condividi su Facebook!!!");
            }
        });
        //layer.addActor(facebook);


        twitter = new Button(memo.patchedSkin, "TwitterButton");
        twitter.setPosition(continueButton.getX() + continueButton.getWidth() - twitter.getWidth() - 24, 125);
        twitter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                System.out.println("Condividi su Twitter!!!");
            }
        });
        //layer.addActor(twitter);

        //Label debug = new Label("Score: " + totalScore + "\nTime Bonus Exp: " + timeBonusScore, memo.patchedSkin, "40white");
        Label debug = new Label("Score:  " + 1054, memo.patchedSkin, "40white");
        debug.setPosition(45, 236);
        layer.addActor(debug);

        Label debug2 = new Label("Time  Bonus  Score:  " + 354, memo.patchedSkin, "40white");
        debug2.setPosition(45, 201);
        layer.addActor(debug2);


       // Label debug3 = new Label("Total Exp: " + (totalScore + timeBonusScore), memo.patchedSkin, "60white");
        Label debug3 = new Label("Total  Score:  " + 1456, memo.patchedSkin, "64yellow");
        debug3.setPosition((540-debug3.getWidth())/2-popUp.getX(), 125);
        layer.addActor(debug3);

        layer.addActor(continueButton);

        return layer;

    }

    private void onContinueClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (!offlineMode) {
            System.out.println("Terminato  un round del match con ID: " + matchID);
            /*if (totalScore == 0) {
                Random random = new Random();
                totalScore = 1750 + random.nextInt(300);
                if (totalScore + User.data.experience > LevelManagement.instance.getCurrentExpRange()) {
                    System.out.println("1 Range ATTUALE: " + LevelManagement.instance.getCurrentExpRange());
                    System.out.println("2 Esperienza attuale: " + User.data.experience);
                    System.out.println("3 Punteggio nuovo round: " + totalScore);
                    User.data.experience = totalScore + User.data.experience - LevelManagement.instance.getCurrentExpRange();
                    System.out.println("4 Nuova esperienza: " + User.data.experience);
                    User.data.level += 1;
                    System.out.println("LevelUP Grazie allo Score!! Nuovo Livello: "
                            + User.data.level);
                } else {
                    User.data.experience += totalScore;
                }
            }*/
            String[] sendMsg = {"1005", String.valueOf(matchID),
                    String.valueOf(totalScore + timeBonusScore), String.valueOf(remainingTime)};
            User.data.updateHeroScore(matchID, totalScore + timeBonusScore, remainingTime);
            User.data.updateMatchStatus(matchID);
            mocdc.sendToServer(sendMsg);
            DataManager.instance.saveData();
            stage.dispose();
            batcher.dispose();

            System.out.println("ROUND CORRENTE!!!!! :  " + User.data.getMatch(matchID).getCurrentRound());

            if (User.data.getMatch(matchID).getCurrentRound() == 3) {
                memo.getAdsController().showOrLoadInterstital();
            }

            memo.setScreen(new GameStatus(memo, matchID, opponent, this.mocdc, null));

        } else {

            User.data.refreshOfflineRecords(round, (totalScore + timeBonusScore), remainingTime);
            DataManager.instance.saveData();
            stage.dispose();
            batcher.dispose();
            memo.setScreen(new OfflineMode(memo, this.mocdc));

        }

    }

    @Override
    public void resize(int width, int height) {

        // use true here to center the camera
        // that's what you probably want in case of a UI
        stage.getViewport().update(width, height, true);

    }

    @Override
    public void render(float delta) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }
}
