package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;

public class ReconnectionScreen extends AbstractScreen {

    private Table popUp;
    private Label popUpLabel;


    public ReconnectionScreen(MemoGameOnline memo) {
        super(memo, null);
        TAG = "ReconnectionScreen";
        multiplexer.addProcessor(stage);

    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();
    }

    @Override
    public void show() {
        Gdx.input.setOnscreenKeyboardVisible(false);
        rebuildStage();

        // Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void pause() {

    }

    public void rebuildStage() {

        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("SignInBackground");
        screen.addActor(buildPopUp());
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);

        stage.clear();
        stage.addActor(screen);

        // stage.setDebugAll(true);

    }

    private Table buildPopUp() {
        popUp = new Table(memo.patchedSkin);
        popUp.setBounds(81, 250, 384, 300);
        popUp.setBackground("BlackBackground");
        popUp.setTransform(true);
        popUp.setOrigin(popUp.getWidth() / 2, popUp.getHeight() / 2);


        if (memo.getInternetService()!=null && memo.getInternetService().isNetworkConnected()) {

            popUpLabel = new Label("Connection lost with\n remote server", memo.patchedSkin, "40white");
            Button reconnect = new Button(memo.patchedSkin, "GreenButton");
            reconnect.add(new Label("Reconnect\n to server", memo.patchedSkin, "48white"));
            reconnect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    memo.resetConnection();
                }
            });
            reconnect.setBounds(72, 3, 240, 120);
            popUp.addActor(reconnect);
        } else {
            popUpLabel = new Label("There's problem with\n your internet connection", memo.patchedSkin, "40white");
            Button reconnect = new Button(memo.patchedSkin, "GreenButton");
            reconnect.add(new Label("Continue", memo.patchedSkin, "48white"));
            reconnect.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                  Gdx.app.exit();
                }
            });
            reconnect.setBounds(72, 3, 240, 120);
            popUp.addActor(reconnect);
        }

        popUpLabel.setPosition(50, 160);
        popUp.addActor(popUpLabel);

        return popUp;
    }


    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

}
