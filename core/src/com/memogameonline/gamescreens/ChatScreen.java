package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.ChatCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;


public class ChatScreen extends AbstractScreen {

    private ScrollPane scrollPane;
    private Button searchButton;
    private long matchID;
    private TextField searchField;

    private boolean[] powerUpCharged = new boolean[3];

    public ChatScreen(final MemoGameOnline memo, final MOCDataCarrier mocdc, final long matchID, final Opponent opponent, final boolean[] powerUpCharged) {
        super(memo, mocdc);
        TAG = "HOME";

        this.matchID = matchID;
        this.opponent = opponent;
        this.powerUpCharged = powerUpCharged;

        multiplexer = new InputMultiplexer();
        InputAdapter backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                            ScreenTransitionSlide.UP, true, interpolation);
                    memo.setScreen(new GameStatus(memo, matchID, opponent, mocdc,powerUpCharged), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);


    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

        // if (scrollPane != null)
        //  scrollPane.setScrollPercentY(100);
        //System.out.println("Dopo: "+scrollPane.getScrollY());
    }

    @Override
    public void show() {
        Gdx.input.setOnscreenKeyboardVisible(false);
        rebuildStage();

        // Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void pause() {

    }

    public void rebuildStage() {

        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("PowerUpBackground");
        screen.addActor(buildScrollChatLayer());
        screen.addActor(buildSendMessagePanel());
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        if (randomOpponentChallengePopUp != null)
            stage.addActor(buildRandomOpponentChallengePopUp());

        stage.clear();
        stage.addActor(screen);

        if (scrollPane != null) {
            System.out.println("Prima: " + scrollPane.getScrollY());
            scrollPane.layout();
            scrollPane.setScrollPercentY(100);
            System.out.println("Dopo: " + scrollPane.getScrollY());
        }
        //stage.setDebugAll(true);

    }

    private Table buildScrollChatLayer() {

        Table friendsListGrid = new Table();
        // friendsListGrid.defaults().expandX().fillX();
        friendsListGrid.setBounds(10, 10, 520, 600);
        friendsListGrid.top();
        boolean firstCellAdded = false;
        int numberOfCells = User.data.getMatch(matchID).getChatMessages().size;

        for (int i = 0; i < numberOfCells; i++) {

            ChatCell cell = new ChatCell(memo, memo.patchedSkin, "" + User.data.getMatch(matchID).getChatMessages().get(i).getMessage()
                    , User.data.getMatch(matchID).getChatMessages().get(i).isMessageFromOpponent());

            if (!firstCellAdded) {
                firstCellAdded = true;
                if (numberOfCells <= 5) {
                    if (cell.isMessageFromOpponent()) {
                        friendsListGrid.add(cell.getCell()).expandX().right().padTop(10).row();
                    } else {
                        friendsListGrid.add(cell.getCell()).expandX().left().padTop(10).row();
                    }

                } else {
                    if (cell.isMessageFromOpponent()) {
                        friendsListGrid.right().add(cell.getCell()).expand().right().top().padTop(10).row();
                        // System.out.println("Messaggio dell'avversario!!!");
                    } else {
                        friendsListGrid.left().add(cell.getCell()).expand().left().top().padTop(10).row();
                        // System.out.println("Messaggio di HERO");
                    }

                }
            } else {
                if (numberOfCells <= 5) {
                    if (cell.isMessageFromOpponent()) {
                        friendsListGrid.add(cell.getCell()).expandX().right().row();
                    } else {
                        friendsListGrid.add(cell.getCell()).expandX().left().row();
                    }

                } else {
                    if (cell.isMessageFromOpponent()) {
                        friendsListGrid.right().add(cell.getCell()).expand().right().row();
                        // System.out.println("Messaggio dell'avversario!!!");
                    } else {
                        friendsListGrid.left().add(cell.getCell()).expand().left().row();
                        // System.out.println("Messaggio di HERO");
                    }

                }
            }
        }


        Table scrollContainer = new Table(memo.patchedSkin);
        scrollContainer.setBounds(10, 10, 520, 600);
        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(-6, -3, 530, 605);
        scrollContainer.addActor(background);


        if (numberOfCells <= 5) {
            //scrollContainer.add(friendsListGrid);
            // scrollContainer.top();
            friendsListGrid.addActor(background);
            return friendsListGrid;
        } else {
            scrollPane = new ScrollPane(friendsListGrid,
                    memo.patchedSkin, "scrollPane");
            scrollPane.setScrollBarPositions(true, false);
            scrollPane.setFadeScrollBars(false);
            scrollPane.setScrollbarsOnTop(false);
            scrollPane.setOverscroll(false, false);
            scrollContainer.add(scrollPane).padBottom(0).expand().fill()
                    .colspan(4);
            // scrollPane.layout();
            // scrollPane.setScrollPercentY(100);
        }

        return scrollContainer;

    }


    public Table buildSendMessagePanel() {

        Table table = new Table();
        Button cell = new Button(memo.patchedSkin, "SignInTextFields");
        table.setY(680);
        table.addActor(cell);
        cell.setSize(450, 77);

        searchButton = new Button(memo.patchedSkin, "GreenButton");
        searchButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSendMessageClicked();
            }
        });
        searchButton.setBounds(445, 0, 100, 70);
        searchButton.add(new Label("Send", memo.patchedSkin, "34white"));

        searchField = new TextField("", memo.patchedSkin);
        searchField.setMessageText("                     Write a message");
        searchField.setBounds(18, 10, 425, 60);
        searchField.addListener(new InputListener() {
            @Override
            public boolean keyTyped(InputEvent event, char character) {
                // System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                if (event.getKeyCode() == Input.Keys.ENTER) {
                    Gdx.input.setOnscreenKeyboardVisible(false);
                    onSendMessageClicked();
                }
                return true;
            }
        });

        cell.addActor(searchButton);
        cell.addActor(searchField);

        Label heroID = new Label("" + User.data.userName, memo.patchedSkin, "36blue");
        heroID.setPosition(40, -65);
        Label opponentID = new Label(""
                + opponent.getUserName(), memo.patchedSkin,
                "36blue");
        opponentID.setPosition(540 - opponentID.getWidth() - 40, -65);

        table.addActor(heroID);
        table.addActor(opponentID);

        return table;

    }

    private void onSendMessageClicked() {

        if (searchField.getText().equals("")) {
            System.out.println("Campo vuoto!!!");
        } else {
            AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
            Gdx.input.setOnscreenKeyboardVisible(false);
            String msg = searchField.getText();
            Pattern pattern = Pattern.compile("\\s");
            Matcher matcher = pattern.matcher(msg);
            boolean found = matcher.find();
            if (found)
                msg = msg.concat(" ");
            String[] strings = {"2000", String.valueOf(matchID), msg};
            //System.out.println("MESAGGIO INVIATO!!!!!");
             mocdc.sendToServer(strings);
            User.data.getMatch(matchID).addHeroChatMessages("" + searchField.getText());
            rebuildStage();
        }

        //System.out.println("TESTO:   " + searchField.getText());
    }

    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    public void checkServerMessage() {


        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80 || mocdc.cmdToExecute.first() == 98 || mocdc.cmdToExecute.first() == 99) {
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH
                // SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 3) { // OPPONENT REFUSE A CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 2) { // OPPONENT FOUND, CREATE A MATCH!!!

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);


                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA compsota di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////

                User.data.searchingForRandomOpponent = false;
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 6) { // Match CLOSED BY OTHER PLAYER
                ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                        ScreenTransitionSlide.UP, true, interpolation);
                memo.setScreen(new GameStatus(memo, matchID, opponent, mocdc,powerUpCharged), transition);
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        User.data.challengeFromFriendReceived = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27) { // DISCARD MESSAGE!!!
                System.out.println("ABStract!!!!");
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();
                rebuildStage();
            }


        }

        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }

}
