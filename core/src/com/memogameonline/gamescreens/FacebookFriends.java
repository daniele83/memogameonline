package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.FacebookFriendCell;
import com.memogameonline.gameobjects.MatchCell;
import com.memogameonline.gameobjects.MemoFriendCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class FacebookFriends extends AbstractScreen {

    private Table friendsCellPanel;
    private Table friendsListGrid;
    private Button searchButton;
    private Button searchPanel;
    private Image searchPanelLeftPart;
    private TextField searchField;

    private int numberOfPictureToDownload = User.data.facebookFriendsList.size;
    private boolean stageIsBuilded = false;

    public FacebookFriends(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "FacebookFriendsScreen";

        multiplexer = new InputMultiplexer();
        InputAdapter backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new GameSelection(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

        checkFinishedDownloads();

    }

    @Override
    public void show() {
        rebuildStage(false);
        //for (Opponent oppo : User.data.facebookFriendsList)
        //downloadOpponentFacebookIcon(oppo);
    }

    @Override
    public void pause() {

    }

    private void checkFinishedDownloads() {

        if (numberOfPictureToDownload == 0 && !stageIsBuilded) {
            rebuildStage(false);
            stageIsBuilded = true;
        }
    }

    public void rebuildStage(boolean activedSearch) {


        if (!activedSearch) {
            friendsCellPanel = buildButtonsLayer(activedSearch);
            Table searchPanel = buildSearchPanel();

            stage.clear();
            screen = new Table(memo.patchedSkin);
            screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                    Constants.VIRTUAL_VIEWPORT_HEIGHT);
            screen.setBackground("PowerUpBackground");
            if (searchPanel == null)
                screen.addActor(buildSearchPanel());
            screen.addActor(searchPanel);
            screen.addActor(friendsCellPanel);
            stage.addActor(screen);
        } else {
            screen.setBackground("PowerUpBackground");
            screen.removeActor(friendsCellPanel);
            friendsCellPanel = buildButtonsLayer(activedSearch);
            screen.addActor(friendsCellPanel);
        }

        //stage.setDebugAll(true);

    }

    private void refreshStageAfterFacebookFriendCellHasBeenAdded(final Opponent opponent) {


        final FacebookFriendCell cell = new FacebookFriendCell(memo, memo.patchedSkin,
                opponent);

        System.out.println("AGGIUNTA CELLA per il player: " + opponent.getFacebookName());

        cell.getPlayButton().addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                onPlayClicked(cell);
            }
        });


        Array<Actor> children = new Array<Actor>(friendsListGrid.getChildren());
        System.out.println("Numero di Celle: " + (friendsListGrid.getChildren().size - 2));
        for (int i = friendsListGrid.getChildren().size - 1; i >= 0; i--) {
            if (friendsListGrid.getChildren().get(i) instanceof Button) {
                friendsListGrid.getChildren().get(i).remove();
            } else System.out.println("\n");
        }
        //activeMatches.clearChildren();
        //children.insert(2, cell.getWhiteCell());
        friendsListGrid.add(cell.getCell()).padTop(10).row();
        for (int i = 2; i < children.size; i++) {
            friendsListGrid.add(children.get(i)).padTop(-15).row();
        }

        // for (int i = children.size-1; i >= 0; i--)
        // activeMatches.removeActor(children.get(i));
        // for (Actor actor : activeMatches.getChildren())
        // actor.remove();

        friendsListGrid.center().top();
        // activeMatches.add(header).padTop(60).row();
        //activeMatches.add(headerText).padTop(-65).row();

    }


    private Table buildButtonsLayer(boolean activedSearch) {

        friendsListGrid = new Table();
        friendsListGrid.defaults().expandX().fillX();
        boolean firstCellAdded = false;

        int numberOfCells;
        if (!activedSearch)
            numberOfCells = User.data.facebookFriendsList.size;
        else
            numberOfCells = 0;

        for (int i = 0; i < User.data.facebookFriendsList.size; i++) {

            if (User.data.facebookFriendsList.get(i).getFacebookPicture() != null) {
                if (!activedSearch) {
                    final FacebookFriendCell cell = new FacebookFriendCell(memo, memo.patchedSkin,
                            User.data.facebookFriendsList.get(i));

                    //System.out.println("AGGIUNTA CELLA NUMERO " + i + " per il player: " + User.data.facebookFriendsList.get(i).getFacebookName());

                    cell.getPlayButton().addListener(new ChangeListener() {
                        public void changed(ChangeEvent event, Actor actor) {
                            onPlayClicked(cell);
                        }
                    });


                    if (!firstCellAdded) {
                        friendsListGrid.add(cell.getCell()).padTop(10).row();
                        firstCellAdded = true;
                    } else
                        friendsListGrid.add(cell.getCell()).padTop(-15).row();
                } else {

                    if (User.data.facebookFriendsList.get(i).getFacebookName().toLowerCase().contains(
                            searchField.getText())) {
                        numberOfCells++;
                        System.out.println("AGGIUNTA CELLA");
                        final FacebookFriendCell cell = new FacebookFriendCell(memo, memo.patchedSkin,
                                User.data.facebookFriendsList.get(i));
                        if (!User.data.challengedFriendList.contains(User.data.facebookFriendsList.get(i).getFacebookName(), true)) {
                            cell.getPlayButton().addListener(new ChangeListener() {
                                public void changed(ChangeEvent event, Actor actor) {
                                    onPlayClicked(cell);
                                }
                            });
                        }

                        if (!firstCellAdded) {
                            friendsListGrid.add(cell.getCell()).padTop(10).row();
                            firstCellAdded = true;
                        } else
                            friendsListGrid.add(cell.getCell()).padTop(-15).row();
                    }
                }

            } else {
                downloadOpponentFacebookIcon(User.data.facebookFriendsList.get(i));
            }

        }

        Table scrollContainer = new Table(memo.patchedSkin);
        scrollContainer.setBounds(10, 10, 520, 700);
        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(-6, -3, 530, 705);
        scrollContainer.addActor(background);

        Image header = new Image(memo.patchedSkin, "BlueHeader");
        header.setPosition(13, 694);
        Label headerText = new Label("Facebook Friends", memo.patchedSkin, "48white");
        headerText.setPosition(header.getX() + (header.getWidth() - headerText.getWidth()) / 2, 690);
        scrollContainer.addActor(header);
        scrollContainer.addActor(headerText);


        System.out.println("nUMERO DI celle:  " + numberOfCells);
        if (numberOfCells < 9) {
            scrollContainer.add(friendsListGrid);
            scrollContainer.top();
        } else {
            ScrollPane scrollPane = new ScrollPane(friendsListGrid,
                    memo.patchedSkin, "scrollPane");
            scrollPane.setScrollBarPositions(true, false);
            scrollPane.setFadeScrollBars(false);
            scrollPane.setScrollbarsOnTop(false);
            scrollContainer.add(scrollPane).padBottom(0).expand().fill()
                    .colspan(4);
        }

        return scrollContainer;

    }

    public Table buildSearchPanel() {

        Table table = new Table();

        searchPanel = new Button(memo.patchedSkin, "FacebookFriendsSearchField");
        /*searchButton = new Button(memo.patchedSkin, "FacebookFriendsSearchButton");
        searchButton.addListener(new InputListener() {
                                     @Override
                                     public boolean touchDown(InputEvent event, float x, float y,
                                                              int pointer, int button) {
                                         onSearchClicked();
                                         System.out.println("CLICCATO SULLA RICERCA!!!");
                                         return true;
                                     }

                                 }

        );*/

        searchPanelLeftPart = new Image(memo.patchedSkin,
                "FacebookFriendsSearchFieldLeftPart");
        searchField = new TextField("textField", memo.patchedSkin);
        searchField.addListener(new InputListener() {
                                    @Override
                                    public boolean keyTyped(InputEvent event, char character) {
                                        System.out.println("PREMUTO COSA??? " + event.getKeyCode());
                                        onSearchClicked();

                                        return true;
                                    }
                                }

        );
        searchField.setMessageText("       Search for a friend");
        searchField.setText("");
        searchPanel.setPosition(115, 787);
        //searchPanel.addActor(searchButton);
        //searchButton.setPosition(271, 17);
        searchPanel.addActor(searchPanelLeftPart);
        searchPanelLeftPart.setPosition(-14, 0);
        searchPanel.addActor(searchField);
        searchField.setPosition(10, 6);
        searchField.setWidth(210);

        table.addActor(searchPanel);
        return table;

    }


    private void onPlayClicked(FacebookFriendCell cell) {

        String[] strings = {"0400", cell.getFriendFacebookID()};
        mocdc.sendToServer(strings);
        System.out.println("SFIDO " + cell.getFriendName() + " !!!");
        System.out.println("ID FACEBOOK " + cell.getFriendFacebookID() + " !!!");

        User.data.challengedFriendList.add(cell.getFriendName());
        cell.friendHasBeenChallenged();
    }

    private void onSearchClicked() {

        if (searchField.getText().equals("")) {
            rebuildStage(false);
        } else
            rebuildStage(true);
    }


    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    private void downloadOpponentFacebookIcon(final Opponent opponent) {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            System.out.println("HO DOVUTO SCARICARE LA FOTO DI:  " + opponent.getFacebookName());
                            numberOfPictureToDownload--;
                            refreshStageAfterFacebookFriendCellHasBeenAdded(opponent);


                        }
                    });
                }
            }
        }).start();
    }


}
