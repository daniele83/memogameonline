package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;

public class ScreenTransitionSlide implements ScreenTransition {
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int UP = 3;
    public static final int DOWN = 4;
    private static final ScreenTransitionSlide instance = new ScreenTransitionSlide();
    private float duration;
    private int direction;
    private boolean slideOut;
    private Interpolation easing;

    int width = Gdx.graphics.getWidth();
    int height = Gdx.graphics.getHeight();
    float xCurr = 0;
    float xNext = 0;
    float yCurr = 0;
    float yNext = 0;

    public static ScreenTransitionSlide init(float duration, int direction,
                                             boolean slideOut, Interpolation easing) {
        instance.duration = duration;
        instance.direction = direction;
        instance.slideOut = slideOut;
        instance.easing = easing;
        return instance;
    }

    @Override
    public float getDuration() {
        return duration;
    }

    @Override
    public void render(SpriteBatch batch, Texture currScreen,
                       Texture nextScreen, float alpha) {

        if (easing != null)
            alpha = easing.apply(alpha);
        // calculate position offset
        switch (direction) {
            case LEFT:
                xNext = width * (1 - alpha);
                xCurr = -width * alpha;
               // yCurr=0;
               // yNext=0;
                break;
            case RIGHT:
                xNext = -width * (1 - alpha);
                xCurr = width * alpha;
                //yCurr=0;
               // yNext=0;
                break;
            case DOWN:
                yNext = height * (1 - alpha);
               // yCurr=0;
               // xCurr=0;
                break;
            case UP:
               // xCurr = 100;
               // yNext=0;
                yNext = height * alpha;
                break;
        }// drawing order depends on slide type ('in' or 'out')
        Texture texBottom = slideOut ? nextScreen : currScreen;
        Texture texTop = slideOut ? currScreen : nextScreen;
        // finally, draw both screens
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(texBottom, xCurr, yCurr, 0, 0, width, height, 1, 1, 0, 0, 0, width, height, false,
                true);
        batch.draw(texTop, xNext, yNext, 0, 0, width, height, 1, 1, 0, 0, 0, width, height, false,
                true);
        batch.end();
    }

    @Override
    public void resetScreensPosition(){
        xCurr = 0;
        xNext = 0;
        yCurr = 0;
        yNext = 0;

    }
}
