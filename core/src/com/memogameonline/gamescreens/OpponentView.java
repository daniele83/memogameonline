package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.Achievement;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;


/**
 * Created by danie_000 on 02/11/2015.
 */
public class OpponentView extends AbstractScreen {

    private Table buttonsLayer;
    private Button play;
    private Button addToFriends;
    private Button block;
    private final Interpolation interpolation = Interpolation.elasticOut;

    private boolean[] powerUpCharged = new boolean[3];

    public OpponentView(final MemoGameOnline memo, final MOCDataCarrier mocdc, final long matchID, final Opponent opponent, final boolean[] powerUpCharged) {
        super(memo, mocdc);
        TAG = "OpponentView";
        this.matchID = matchID;
        this.opponent = opponent;
        this.powerUpCharged = powerUpCharged;

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    Interpolation interpolation = Interpolation.pow5Out;
                    ScreenTransition transition = ScreenTransitionSlide.init(0.4f,
                            ScreenTransitionSlide.UP, true, interpolation);
                    memo.setScreen(new GameStatus(memo, matchID, opponent, mocdc, powerUpCharged), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        checkServerMessage();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

        rebuildStage();
    }

    private void rebuildStage() {
        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("OpponentViewBackground");
        screen.addActor(buildImagesLayer());
        screen.addActor(buildButtonsLayer());
        screen.addActor(buildInfoScrollPane());
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.setTransform(true);

        stage.clear();
        stage.addActor(screen);

        //stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        // Costruisco Avatar,Livello e nome di OPPO

        Image opponentIcon;
        if (opponent.getImageID() != 48) {
            opponentIcon = AvatarManager.instance.getBigAvatar(opponent.getImageID());
            System.out.println(opponentIcon);
            opponentIcon.setPosition(10, 626);
            layer.addActor(opponentIcon);

        } else {
            opponentIcon = opponent.getFacebookPicture();
            opponentIcon.setBounds(20, 634, 135, 135);
            opponentIcon.setTouchable(Touchable.disabled);
            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
            pictureFrame.setBounds(18, 632, 139, 139);
            pictureFrame.setTouchable(Touchable.enabled);
            layer.addActor(opponentIcon);
            layer.addActor(pictureFrame);

        }
        Label opponentLevel = new Label("" + User.data.level, memo.patchedSkin, "30white");

        if (opponent.getLevel() > 9)
            opponentLevel.setPosition(39, 580);
        else
            opponentLevel.setPosition(45, 580);//45,582

        layer.addActor(opponentLevel);


        Label opponentId = new Label("" + opponent.getUserName(), memo.patchedSkin, "36blue");
        opponentId.setPosition(85, 574);
        layer.addActor(opponentId);

        /*Label matchesPlayed = new Label("Mathes Played: " + opponent.getMatchesPlayed(), memo.patchedSkin, "36black");
        matchesPlayed.setPosition(50, 550);
        layer.addActor(matchesPlayed);

        Label matchesWon = new Label("Mathes Won: " + opponent.getMatchesWon(), memo.patchedSkin, "36black");
        matchesWon.setPosition(50, 500);
        layer.addActor(matchesWon);

        Label winningPercentage = new Label("Winning percentage: " + Math.round(
                ((float) opponent.getMatchesWon() / opponent.getMatchesPlayed()) * 100) + "%", memo.patchedSkin, "36black");
        winningPercentage.setPosition(50, 450);
        layer.addActor(winningPercentage);*/

        return layer;
    }

    private Table buildButtonsLayer() {

        buttonsLayer = new Table();
        buttonsLayer.setTransform(true);
        if (!User.data.matchAlreadyExist(opponent.getUserName())
                && !User.data.challengedFriendList.contains(opponent.getUserName(), false)) {
            play = new Button(memo.patchedSkin, "GreenCircleButton");
            play.setTransform(true);
            play.setOrigin(play.getWidth() / 2, play.getHeight() / 2);
            play.add(new Label("Play", memo.patchedSkin, "36white"));
            play.setPosition(223, 632);
            play.setScale(0, 0);
            play.addAction(scaleTo(1, 1, 1f, interpolation));
            play.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                    play.clearActions();
                    play.addAction(sequence(scaleTo(0, 0, 0.1f), run(new Runnable() {
                        public void run() {
                            play.setVisible(false);
                        }
                    })));
                    User.data.challengedFriendList.add(opponent.getUserName());
                    String[] sendMsg = {"0300", opponent.getUserName()};
                    mocdc.sendToServer(sendMsg);
                    //rebuildStage();
                    DataManager.instance.saveData();
                }
            });
        } else {
            play = new Button(memo.patchedSkin, "emptyButton");
            // play = new Button(memo.patchedSkin, "GreenCircleButton");
            // play.add(new Label("Play", memo.patchedSkin, "30white"));
            play.setPosition(223, 632);
        }
        buttonsLayer.addActor(play);

        if (!User.data.isAlreadyFriend(opponent.getUserName())) {
            addToFriends = new Button(memo.patchedSkin, "BlueCircleButton");
            addToFriends.setTransform(true);
            addToFriends.setOrigin(addToFriends.getWidth() / 2, addToFriends.getHeight() / 2);
            addToFriends.add(new Label("Friend+", memo.patchedSkin, "34white"));
            addToFriends.setPosition(316, 632);
            addToFriends.setScale(0, 0);
            addToFriends.addAction(scaleTo(1, 1, 1f, interpolation));
            addToFriends.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                    addToFriends.clearActions();
                    addToFriends.addAction(scaleTo(0, 0, 0.1f));
                    User.data.addFriend(opponent);
                    //DataManager.instance.saveData();

                }
            });
        } else {
            addToFriends = new Button(memo.patchedSkin, "emptyButton");
            // play = new Button(memo.patchedSkin, "GreenCircleButton");
            // play.add(new Label("Play", memo.patchedSkin, "30white"));
            addToFriends.setPosition(316, 632);
        }
        buttonsLayer.addActor(addToFriends);
        if (!User.data.isBlockedPlayer(opponent.getUserName())) {
            block = new Button(memo.patchedSkin, "RedCircleButton");
            block.setTransform(true);
            block.setOrigin(block.getWidth() / 2, block.getHeight() / 2);
            block.add(new Label("Block", memo.patchedSkin, "36white"));
            block.setPosition(404, 632);
            block.setScale(0, 0);
            block.addAction(scaleTo(1, 1, 1f, interpolation));
            block.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                    block.clearActions();
                    block.addAction(sequence(scaleTo(0, 0, 0.1f), run(new Runnable() {
                        public void run() {
                            onBlockPlayerClicked();
                        }
                    })));

                    User.data.blockedUsersList.add(opponent.getUserName());
                    DataManager.instance.saveData();
                    System.out.println(User.data.blockedUsersList);
                }
            });

        } else {
            block = new Button(memo.patchedSkin, "StopSignal");
            buttonsLayer.addActor(block);
            block.setTransform(true);
            block.setOrigin(block.getWidth() / 2, block.getHeight() / 2);
            block.setPosition(407, 639);
            block.setScale(0, 0);
            block.addAction(scaleTo(1, 1, 1f, interpolation));
            block.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                    block.addAction(sequence(scaleTo(0, 0, 0.1f), run(new Runnable() {
                        public void run() {
                            onUnblockPlayerClicked();
                        }
                    })));

                    for (int i = 0; i < User.data.blockedUsersList.size; i++) {
                        if (User.data.blockedUsersList.get(i).equals(opponent.getUserName())) {
                            System.out.println("Confronto:" + User.data.blockedUsersList.get(i));
                            System.out.println("Confronto:" + opponent.getUserName());
                            System.out.println(User.data.blockedUsersList);
                            System.out.println(i);
                            User.data.blockedUsersList.removeIndex(i);
                            System.out.println(User.data.blockedUsersList);
                        }

                    }
                    DataManager.instance.saveData();
                }
            });
        }
        buttonsLayer.addActor(block);

        return buttonsLayer;

    }

    private Table buildInfoScrollPane() {

        Table table = new Table();

        Button matchesPlayed = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description1 = new Label("Matches completed", memo.patchedSkin, "38blue");
        Label value1 = new Label("" + opponent.getMatchesPlayed(), memo.patchedSkin, "36beige");
        description1.setPosition(32, 29);
        value1.setPosition(422, 31);
        matchesPlayed.addActor(description1);
        matchesPlayed.addActor(value1);
        table.add(matchesPlayed).padBottom(-13).row();

        Button matchesWon = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description2 = new Label("Matches won", memo.patchedSkin, "38blue");
        Label value2 = new Label("" + opponent.getMatchesWon() + " (" + Math.round(((float)
                opponent.getMatchesWon() / opponent.getMatchesPlayed()) * 100) + "%" + ")", memo.patchedSkin, "36beige");
        description2.setPosition(32, 29);
        value2.setPosition(422, 31);
        matchesWon.addActor(description2);
        matchesWon.addActor(value2);
        table.add(matchesWon).padBottom(-13).row();

        Button maxConsecutiveMatchesWon = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description3 = new Label("Most consecutive matches won", memo.patchedSkin, "38blue");
        Label value3 = new Label("" + opponent.getConsecutiveMatchWon(), memo.patchedSkin, "36beige");
        description3.setPosition(32, 29);
        value3.setPosition(422, 31);
        maxConsecutiveMatchesWon.addActor(description3);
        maxConsecutiveMatchesWon.addActor(value3);
        table.add(maxConsecutiveMatchesWon).padBottom(-13).row();

        Button highestMatchScore = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description4 = new Label("Highest match score", memo.patchedSkin, "38blue");
        Label value4 = new Label("" + opponent.getHighestmatchScore(), memo.patchedSkin, "36beige");
        description4.setPosition(32, 29);
        value4.setPosition(422, 31);
        highestMatchScore.addActor(description4);
        highestMatchScore.addActor(value4);
        table.add(highestMatchScore).padBottom(-13).row();

        Button highestFirstRoundScore = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description10 = new Label("Highest first round score", memo.patchedSkin, "38blue");
        Label value10 = new Label("" + opponent.getHighestFirstRoundScore(), memo.patchedSkin, "36beige");
        description10.setPosition(32, 29);
        value10.setPosition(422, 31);
        highestFirstRoundScore.addActor(description10);
        highestFirstRoundScore.addActor(value10);
        table.add(highestFirstRoundScore).padBottom(-13).row();

        Button bestFirstRoundTime = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description5 = new Label("Best first round time", memo.patchedSkin, "38blue");
        Label value5 = new Label("" + opponent.getBestFirstRoundTime() + " s", memo.patchedSkin, "36beige");
        description5.setPosition(32, 29);
        value5.setPosition(422, 31);
        bestFirstRoundTime.addActor(description5);
        bestFirstRoundTime.addActor(value5);
        table.add(bestFirstRoundTime).padBottom(-13).row();

        Button highestSecondRoundScore = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description6 = new Label("Highest second round score", memo.patchedSkin, "38blue");
        Label value6 = new Label("" + opponent.getHighestSecondRoundScore(), memo.patchedSkin, "36beige");
        description6.setPosition(32, 29);
        value6.setPosition(422,31);
        highestSecondRoundScore.addActor(description6);
        highestSecondRoundScore.addActor(value6);
        table.add(highestSecondRoundScore).padBottom(-13).row();

        Button bestSecondRoundTime = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description7 = new Label("Best second round time", memo.patchedSkin, "38blue");
        Label value7 = new Label("" + opponent.getBestSecondRoundTime() + " s", memo.patchedSkin, "36beige");
        description7.setPosition(32, 29);
        value7.setPosition(422, 31);
        bestSecondRoundTime.addActor(description7);
        bestSecondRoundTime.addActor(value7);
        table.add(bestSecondRoundTime).padBottom(-13).row();

        Button highestThirdRoundScore = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description8 = new Label("Highest third round score", memo.patchedSkin, "38blue");
        Label value8 = new Label("" + opponent.getHighestThirdRoundScore(), memo.patchedSkin, "36beige");
        description8.setPosition(32, 29);
        value8.setPosition(422, 31);
        highestThirdRoundScore.addActor(description8);
        highestThirdRoundScore.addActor(value8);
        table.add(highestThirdRoundScore).padBottom(-13).row();

        /*Button bestThirdRoundTime = new Button(memo.patchedSkin, "constantWhiteCell");
        Label description9 = new Label("Best third round time", memo.patchedSkin, "38blue");
        Label value9 = new Label("" + opponent.getBestThirdRoundTime() + " s", memo.patchedSkin, "32beige");
        description9.setPosition(35, 35);
        value9.setPosition(425, 35);
        bestThirdRoundTime.addActor(description9);
        bestThirdRoundTime.addActor(value9);
        table.add(bestThirdRoundTime).padBottom(-13).row();*/


        //Costruisco lo ScrollPane
        ScrollPane infoScrollPane = new ScrollPane(table,
                memo.patchedSkin, "scrollPane");
        // achievementScrollPane.setScrollBarPositions(true, false);
        // achievementScrollPane.setFadeScrollBars(false);
        //achievementScrollPane.setScrollbarsOnTop(false);
        //achievementScrollPane.setupOverscroll(0, 0, 0);

        Table infoScrollPaneContainer = new Table();
        infoScrollPaneContainer.setBounds(0, 19, 540, 536);
        infoScrollPaneContainer.add(infoScrollPane).expand().fill().colspan(9);

        return infoScrollPaneContainer;

    }


    private void onBlockPlayerClicked() {

        block = new Button(memo.patchedSkin, "StopSignal");
        buttonsLayer.addActor(block);
        block.setScale(0, 0);
        block.setTransform(true);
        block.setOrigin(block.getWidth() / 2, block.getHeight() / 2);
        block.setPosition(407, 639);
        block.addAction(scaleTo(1, 1, 0.1f));

        block.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                block.addAction(sequence(scaleTo(0, 0, 0.1f), run(new Runnable() {
                    public void run() {
                        onUnblockPlayerClicked();
                    }
                })));


                for (int i = 0; i < User.data.blockedUsersList.size; i++) {
                    if (User.data.blockedUsersList.get(i).equals(opponent.getUserName())) {
                        System.out.println("Confronto:" + User.data.blockedUsersList.get(i));
                        System.out.println("Confronto:" + opponent.getUserName());
                        System.out.println(User.data.blockedUsersList);
                        System.out.println(i);
                        User.data.blockedUsersList.removeIndex(i);
                        System.out.println(User.data.blockedUsersList);
                    }

                }
                DataManager.instance.saveData();
            }
        });

    }

    private void onUnblockPlayerClicked() {

        block = new Button(memo.patchedSkin, "RedCircleButton");
        block.setTransform(true);
        block.setOrigin(block.getWidth() / 2, block.getHeight() / 2);
        block.add(new Label("Block", memo.patchedSkin, "32white"));
        buttonsLayer.addActor(block);
        block.setScale(0, 0);
        block.setPosition(404, 632);
        block.addAction(scaleTo(1, 1, 0.1f));

        block.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
                block.addAction(sequence(scaleTo(0, 0, 0.1f), run(new Runnable() {
                    public void run() {
                        onBlockPlayerClicked();
                    }
                })));

                User.data.blockedUsersList.add(opponent.getUserName());
                DataManager.instance.saveData();
                System.out.println(User.data.blockedUsersList);
            }
        });

    }


    @Override
    public void pause() {

    }


    @Override
    public void resume() {

    }
}
