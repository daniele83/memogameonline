package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamehelpers.TaskCallback;
import com.memogameonline.gameobjects.SettingCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class Settings extends AbstractScreen {

    private SettingCell accountSettings;
    private Image heroAvatar;
    private ScrollPane scrollPane;

    Settings(final MemoGameOnline memo, final MOCDataCarrier mocdc) {

        super(memo, mocdc);
        TAG = "Settings";

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    User.data.comingFromFacebookLogIn = false;
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new Home(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        checkServerMessage();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

        checkFacebookLogIn();


    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {

        rebuildStage();

    }

    public void checkFacebookLogIn() {

        if (User.data.comingFromFacebookLogIn) {
            User.data.comingFromFacebookLogIn = false;
            String[] strings = {"0603", User.data.facebookID, User.data.facebookPictureURL};
            System.out.println("Invio al Server le stringhe FacebookID: " + User.data.facebookID);
            System.out.println("Invio al Server le stringhe FacebookPictureURL : " + User.data.facebookPictureURL);
            mocdc.sendToServer(strings);
        }

    }

    public void rebuildStage() {


        stage.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("PowerUpBackground");
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.addActor(buildScrollPane());
        screen.addActor(buildImagesLayer());
        stage.addActor(screen);

        //onAccountSettingsClicked();
        // stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        //topBar = new Image(memo.patchedSkin, "topBar");
        // topBar.setPosition(-9, 783);
        Label title = new Label("Settings and Help", memo.patchedSkin, "40black");
        title.setPosition((540 - title.getWidth()) / 2, 805);

        //layer.addActor(topBar);
        layer.addActor(title);

        return layer;
    }

    public Table buildSettings() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        header.setSize(508, 56);
        table.center().top();
        table.add(header).padTop(50);
        table.row();
        Label title = new Label("Settings", memo.patchedSkin, "40white");
        table.add(title).padTop(-70);
        table.row();


        String name1 = "Account settings";
        String legend1 = "Logged as ".concat(User.data.userName);
        accountSettings = new SettingCell(memo, name1, legend1);
        heroAvatar = null;
        if (User.data.imageId != 48) {
            heroAvatar = AvatarManager.instance.getBigAvatar(User.data.imageId);
            heroAvatar.setBounds(19, 18, 80, 70);
            accountSettings.getWhiteCell().addActor(heroAvatar);
        } else {
            if (User.data.facebookPicture != null) {
                heroAvatar = User.data.facebookPicture;
                heroAvatar.setBounds(19, 22, 60, 60);
                accountSettings.getWhiteCell().addActor(heroAvatar);
                Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                pictureFrame.setBounds(17, 20, 64, 64);
                accountSettings.getWhiteCell().addActor(pictureFrame);
            } else {
                downloadFacebookIcon();
            }
        }

        accountSettings.setNameX(120);
        accountSettings.setLegendX(120);
        accountSettings.setNamePressedX(120);
        accountSettings.setLegendPressedX(120);
        table.add(accountSettings.getWhiteCell()).padTop(-8);

        accountSettings.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onAccountSettingsClicked();
            }

        });
        table.row();

        Button audio = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(audio).padTop(-14);
        Label name2 = new Label("Audio", memo.patchedSkin, "32black");
        audio.addActor(name2);
        name2.setPosition(30, 45);
        Label legend2 = new Label("Enable/Disable game sounds", memo.patchedSkin,
                "26beige");
        audio.addActor(legend2);
        legend2.setPosition(30, 20);
        Button activeButton2 = new Button(memo.patchedSkin, "SwitchButton");
        activeButton2.setPosition(380, 39);
        audio.addActor(activeButton2);
        if (!GamePreferences.instance.audio) {
            activeButton2.toggle();
            System.out.println("Audio disattivato");
        }
        activeButton2.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onAudioClicked();
            }

        });
        table.row();

        Button notifications = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(notifications).padTop(-14);
        Label name3 = new Label("Notifications", memo.patchedSkin, "32black");
        notifications.addActor(name3);
        name3.setPosition(30, 45);
        Label legend3 = new Label("Enable/Disable notifications", memo.patchedSkin,
                "26beige");
        notifications.addActor(legend3);
        legend3.setPosition(30, 20);
        Button activeButton3 = new Button(memo.patchedSkin, "SwitchButton");
        activeButton3.setPosition(380, 39);
        notifications.addActor(activeButton3);
        if (!GamePreferences.instance.enableNotifications) {
            activeButton3.toggle();
            System.out.println("Notifiche disattivate");
        }
        activeButton3.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onNotificationsClicked();
            }

        });
        table.row();

        Button soundNotification = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(soundNotification).padTop(-14);
        Label name4 = new Label("Audio Notifications", memo.patchedSkin, "32black");
        soundNotification.addActor(name4);
        name4.setPosition(30, 45);
        Label legend4 = new Label("Enable/Disable sound notifications",
                memo.patchedSkin, "26beige");
        soundNotification.addActor(legend4);
        legend4.setPosition(30, 20);
        Button activeButton4 = new Button(memo.patchedSkin, "SwitchButton");
        activeButton4.setPosition(380, 39);
        soundNotification.addActor(activeButton4);
        if (!GamePreferences.instance.enableSoundNotifications) {
            activeButton4.toggle();
            System.out.println("Audio delle Notifiche disattivato");
        }
        activeButton4.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSoundNotificationsClicked();
            }

        });
        table.row();

        Button vibrationNotification = new Button(memo.patchedSkin,
                "constantWhiteCell");
        table.add(vibrationNotification).padTop(-14);
        Label name5 = new Label("Vibration Notifications", memo.patchedSkin, "32black");
        vibrationNotification.addActor(name5);
        name5.setPosition(30, 45);
        Label legend5 = new Label("Enable/Disable vibration notifications",
                memo.patchedSkin, "26beige");
        vibrationNotification.addActor(legend5);
        legend5.setPosition(30, 20);
        Button activeButton5 = new Button(memo.patchedSkin, "SwitchButton");
        activeButton5.setPosition(380, 39);
        vibrationNotification.addActor(activeButton5);
        if (!GamePreferences.instance.enableVibrationNotifications) {
            activeButton5.toggle();
            System.out.println("Vibrazione delle Notifiche disattivata");
        }
        activeButton5.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onVibrationNotificationsClicked();
            }

        });
        table.row();

        return table;

    }

    public Table buildSocial() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        table.center().top();
        table.add(header).padTop(30);
        Label title = new Label("Social", memo.patchedSkin, "40white");
        table.row();
        table.add(title).padTop(-70);
        table.row();

        Button fbConnection = new Button(memo.patchedSkin, "constantWhiteCell");
        table.add(fbConnection).padTop(-8);
        Label name1 = new Label("Connect with Facebook", memo.patchedSkin, "32black");
        fbConnection.addActor(name1);
        name1.setPosition(30, 45);
        Label legend1 = new Label("Play with your friends!", memo.patchedSkin,
                "26beige");
        fbConnection.addActor(legend1);
        legend1.setPosition(30, 20);
        Button activeButton = new Button(memo.patchedSkin, "SwitchButton");
        activeButton.setPosition(380, 39);
        fbConnection.addActor(activeButton);
        switch (Gdx.app.getType()) {
            case Android:
                if (!memo.getFacebookService().isLoggedIn()) {
                    activeButton.toggle();
                }
        }
        activeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onFacebookConnectionClicked();
            }

        });
        table.row();

        String name2 = "Like MemoGameOnline on Facebook";
        String legend2 = "Stay connected clicking I Like on Facebook";
        SettingCell fbLike = new SettingCell(memo, name2, legend2);
        table.add(fbLike.getWhiteCell()).padTop(-14);

        fbLike.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onLikeFacebookClicked();
            }

        });
        table.row();

        String name3 = "Follow us  on twitter";
        String legend3 = "Stay connected following us on Twitter";
        SettingCell twitterFollower = new SettingCell(memo, name3, legend3);
        table.add(twitterFollower.getWhiteCell()).padTop(-14);

        twitterFollower.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onTwitterClicked();
            }

        });
        table.row();

        String name4 = "Send MemoGameOnline to your friends";
        String legend4 = "";
        SettingCell sendMemoToFriends = new SettingCell(memo, name4, legend4);
        sendMemoToFriends.getCellName().setPosition(30, 30);
        sendMemoToFriends.getCellNamePressed().setPosition(30, 30);
        table.add(sendMemoToFriends.getWhiteCell()).padTop(-14);

        sendMemoToFriends.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSendMemoToFriendsClicked();
            }

        });
        table.row();

        return table;

    }

    public Table buildHelp() {
        Table table = new Table();

        Image header = new Image(memo.patchedSkin, "GreenHeader");
        table.center().top();
        table.add(header).padTop(30);
        Label title = new Label("Help", memo.patchedSkin, "40white");
        table.row();
        table.add(title).padTop(-70);
        table.row();

        String name1 = "Support";
        String legend1 = "";
        SettingCell support = new SettingCell(memo, name1, legend1);
        support.getCellName().setPosition(30, 30);
        support.getCellNamePressed().setPosition(30, 30);
        table.add(support.getWhiteCell()).padTop(-8);
        table.row();

        String name2 = "Credits";
        String legend2 = "";
        SettingCell credits = new SettingCell(memo, name2, legend2);
        credits.getCellName().setPosition(30, 30);
        credits.getCellNamePressed().setPosition(30, 30);
        table.add(credits.getWhiteCell()).padTop(-14);
        table.row();

        String name3 = "Rate MemoGameOnline";
        String legend3 = "leave a feedback on your App Store";
        SettingCell review = new SettingCell(memo, name3, legend3);
        table.add(review.getWhiteCell()).padTop(-14);
        review.getWhiteCell().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onReviewClicked();
            }

        });
        table.row();

        String name4 = "Tutorial";
        String legend4 = "Levels, Exp, Coins, Power Up, Tournaments";
        SettingCell tutorial = new SettingCell(memo, name4, legend4);
        table.add(tutorial.getWhiteCell()).padTop(-14);
        table.row();

        return table;

    }

    private Table buildScrollPane() {

        Table scrollContainer = new Table(memo.patchedSkin);
        Table table = new Table();

        table.add(buildSettings()).row();
        table.top();
        table.add(buildSocial()).row();
        table.top();
        table.add(buildHelp());
        table.top();

        scrollPane = new ScrollPane(table, memo.patchedSkin, "scrollPane");

        scrollPane.setScrollBarPositions(true, false);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollbarsOnTop(false);
        scrollPane.setOverscroll(false, false);
        // scrollPane.setScrollPercentY(10);
        // scrollPane.setScrollY(100);

        scrollContainer.setBounds(0, 10, 540, 726);

        Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        background.setBounds(4, -2, 530, 730);
        scrollContainer.addActor(background);
        scrollContainer.add(scrollPane).expand().fill().colspan(4);

        return scrollContainer;
    }

    public void onAccountSettingsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);
        memo.setScreen(new AccountManagementView(this.memo, this.mocdc),
                transition);
    }

    public void onAudioClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (GamePreferences.instance.audio) {
            GamePreferences.instance.audio = false;
            System.out.println("AUDIO VIA!!!");
        } else {
            GamePreferences.instance.audio = true;
            System.out.println("AUDIO DENTRO!!!");
        }
        GamePreferences.instance.save();
    }

    public void onNotificationsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (GamePreferences.instance.enableNotifications)
            GamePreferences.instance.enableNotifications = false;
        else
            GamePreferences.instance.enableNotifications = true;
        GamePreferences.instance.save();
    }

    public void onSoundNotificationsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (GamePreferences.instance.enableSoundNotifications)
            GamePreferences.instance.enableSoundNotifications = false;
        else
            GamePreferences.instance.enableSoundNotifications = true;
        GamePreferences.instance.save();
    }

    public void onVibrationNotificationsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (GamePreferences.instance.enableVibrationNotifications)
            GamePreferences.instance.enableVibrationNotifications = false;
        else
            GamePreferences.instance.enableVibrationNotifications = true;
        GamePreferences.instance.save();
    }

    public void onFacebookConnectionClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        if (memo.getFacebookService().isLoggedIn()) {
            memo.getFacebookService().logOut();
        } else {
            TaskCallback taskCallback = new TaskCallback(memo, mocdc, this);
            taskCallback.setCalledToLogIn(true);
            try {
                memo.getFacebookService().logIn(taskCallback);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void onSendMemoToFriendsClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        ScreenTransition transition = ScreenTransitionSlide.init(duration,
                ScreenTransitionSlide.LEFT, false, interpolation);
        memo.setScreen(new SendToFriendsView(this.memo, this.mocdc), transition);

    }

    public void onLikeFacebookClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        //Gdx.net.openURI("https://www.facebook.com/pages/Alfonso-Signorini/34872216337");
    }

    public void onTwitterClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        //Gdx.net.openURI("https://twitter.com/alfosignorini");

    }

    public void onReviewClicked() {
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        //Gdx.net.openURI("https://play.google.com/store/apps/details?id=com.gamedalf.memo.android");
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }


    private void downloadFacebookIcon() {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, User.data.facebookPictureURL);
                //int numBytes = download(bytes, User.data.facebookPictureURL);
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            User.data.facebookPicture = new Image(image);
                            heroAvatar = User.data.facebookPicture;
                            heroAvatar.setBounds(19, 22, 60, 60);
                            accountSettings.getWhiteCell().addActor(heroAvatar);
                            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
                            pictureFrame.setBounds(17, 20, 64, 64);
                            accountSettings.getWhiteCell().addActor(pictureFrame);

                        }
                    });
                }
            }
        }).start();
    }

}
