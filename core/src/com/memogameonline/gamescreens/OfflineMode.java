package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

public class OfflineMode extends AbstractScreen {
    public static final String TAG = OfflineMode.class.getName();

    private Skin skin;
    private TextureAtlas atlas;

    private Button classic;
    private Button memobile;
    private Button memoFiends;

    public OfflineMode(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new GameSelection(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, false);

    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));
    }

    @Override
    public void show() {

        rebuildStage();
    }

    @Override
    public void pause() {

    }

    public void rebuildStage() {
        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("OfflineModeBackground");
        screen.addActor(buildImagesLayer());
        screen.addActor(buildButtonsLayer());
        screen.addActor(buildsStats());
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        if (randomOpponentChallengePopUp != null)
            stage.addActor(buildRandomOpponentChallengePopUp());

        stage.clear();
        stage.addActor(screen);

        //stage.setDebugAll(true);

    }

    private Table buildImagesLayer() {

        Table layer = new Table();

        Label playWithFriendsText = new Label("Training   Mode", memo.patchedSkin,
                "45blue");
        playWithFriendsText.setPosition((540 - playWithFriendsText.getWidth()) / 2, 805);
        layer.addActor(playWithFriendsText);


        return layer;
    }

    private Table buildButtonsLayer() {

        Table layer = new Table();

        classic = new Button(memo.patchedSkin, "GreenButton");
        classic.add(new Label("Round", memo.patchedSkin, "60white")).row();
        classic.add(new Label("1", memo.patchedSkin, "60white")).padTop(-50);
        layer.addActor(classic);
        classic.setBounds(20, 640, 200, 100);
        classic.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onClassicClicked();
            }
        });
        layer.row();

        memobile = new Button(memo.patchedSkin, "GreenButton");
        memobile.setBounds(20, 387, 200, 100);
        memobile.add(new Label("Round", memo.patchedSkin, "60white")).row();
        memobile.add(new Label("2", memo.patchedSkin, "60white")).padTop(-50);
        memobile.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onMemobileClicked();

            }
        });
        layer.addActor(memobile);


        memoFiends = new Button(memo.patchedSkin, "GreenButton");
        memoFiends.setBounds(20, 137, 200, 100);
        memoFiends.add(new Label("Round", memo.patchedSkin, "60white")).row();
        memoFiends.add(new Label("3", memo.patchedSkin, "60white")).padTop(-50);
        memoFiends.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onFiendsClicked(5);

            }
        });
        layer.addActor(memoFiends);


        return layer;
    }

    private Table buildsStats() {
        Table layer = new Table();

        Label firstRoundDescription = new Label("Classic  memory  cards  game, tap  the  cards, \nmatch  the  couple  and  complete  the  grid  before\nthe  time  expires!", memo.patchedSkin, "36blue");
        firstRoundDescription.setPosition(classic.getX() + 5, classic.getY() - 108);

        Label secondRoundDescription = new Label("Match  the  couple  but  be  carefull! \nAfter  4  mistakes  two  cards  swap  their  \nposition!", memo.patchedSkin, "36blue");
        secondRoundDescription.setPosition(memobile.getX() + 5, memobile.getY() - 108);

        Label thirdRoundDescription = new Label("Swipe  your  finger  on  the  cards  grid\nand  make  chain  with  same  cards.\nLonger  is  the  chain  more  points  you  get!", memo.patchedSkin, "36blue");
        thirdRoundDescription.setPosition(memoFiends.getX() + 5, memoFiends.getY() - 108);


        Label highestFirstRoundScoreLabel = new Label("Highest Score:        " + User.data.highestFirstRoundScoreOFFLINE, memo.patchedSkin, "40blue");
        highestFirstRoundScoreLabel.setPosition(classic.getX() + classic.getWidth() + 20, classic.getY() + 60);
        Label bestFirstRoundTime = new Label("Best Time:              " + User.data.bestFirstRoundTimeOFFLINE + "s", memo.patchedSkin, "40blue");
        bestFirstRoundTime.setPosition(classic.getX() + classic.getWidth() + 20, classic.getY() + 10);

        Label highestSecondRoundScore = new Label("Highest Score:        " + User.data.highestSecondRoundScoreOFFLINE, memo.patchedSkin, "40blue");
        highestSecondRoundScore.setPosition(memobile.getX() + memobile.getWidth() + 20, memobile.getY() + 60);
        Label bestSecondRoundTime = new Label("Best Time:              " + User.data.bestSecondRoundTimeOFFLINE + "s", memo.patchedSkin, "40blue");
        bestSecondRoundTime.setPosition(memobile.getX() + memobile.getWidth() + 20, memobile.getY() + 10);

        Label highestThirdRoundScore = new Label("Highest Score:        " + User.data.highestThirdRoundScoreOFFLINE, memo.patchedSkin, "40blue");
        highestThirdRoundScore.setPosition(memoFiends.getX() + memoFiends.getWidth() + 20, memoFiends.getY() + 50);

        layer.addActor(highestFirstRoundScoreLabel);
        layer.addActor(bestFirstRoundTime);
        layer.addActor(highestSecondRoundScore);
        layer.addActor(bestSecondRoundTime);
        layer.addActor(highestThirdRoundScore);

        layer.addActor(firstRoundDescription);
        layer.addActor(secondRoundDescription);
        layer.addActor(thirdRoundDescription);

        return layer;
    }

    private void onClassicClicked() {

        boolean[] powerUp = {false, false, false};
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        // AudioManager.instance.play(CardsAsset.instance.music.gameLoop);

        memo.setScreen(new GameScreen(this.memo, mocdc, 999, new Opponent(), 1, powerUp, true));

    }

    private void onMemobileClicked() {
        boolean[] powerUp = {false, false, false};
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        // AudioManager.instance.play(CardsAsset.instance.music.gameLoop);

        memo.setScreen(new GameScreen(this.memo, mocdc, 999, new Opponent(), 2, powerUp, true));

    }

    private void onFiendsClicked(int memoFiendsMode) {
        boolean[] powerUp = {false, false, false};
        AudioManager.instance.play(CardsAsset.instance.sounds.pressedButton);
        // AudioManager.instance.play(CardsAsset.instance.music.gameLoop);
        System.out.println("MEMOFRIENDS!!!!: " + mocdc);
        memo.setScreen(new GameScreen(this.memo, mocdc, 999, new Opponent(), memoFiendsMode,
                powerUp, true));

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

}
