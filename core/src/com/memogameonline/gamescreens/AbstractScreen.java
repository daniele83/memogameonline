package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.FPSLimiter;
import com.memogameonline.gamehelpers.LevelManagement;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.ExperienceBar;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public abstract class AbstractScreen implements Screen {

    public static String TAG;

    protected Table screen;
    protected final MemoGameOnline memo;
    protected long matchID;
    protected Opponent opponent;
    protected Stage stage;
    protected OrthographicCamera cam;
    protected SpriteBatch batcher = new SpriteBatch(30);
    protected MOCDataCarrier mocdc;

    protected Image experienceBarContainer;
    protected Image experienceBarSeparetor;
    protected ExperienceBar experienceBar;
    protected Image levelIcon;
    protected Image pillsCan;

    protected Window randomOpponentChallengePopUp;
    protected Label playerChallengingLabel;
    protected TextButton accept;
    protected Image randomOpponentIcon;
    protected Image randomOpponentIconFrame;
    protected boolean popUpShowed;

    protected Opponent challengingOpponent;
    protected long challengeMatchID;
    protected String challengeOpponentID;
    protected int challengeOpponentLevel;
    protected int challengeOpponentImageId;
    protected String challengeOpponentFacebookPictureRUL;
    protected int challengeOpponentMatchesPlayed;
    protected int challengeOpponentMatchesWon;

    protected int challengeOpponentConsecutiveMatchWon;
    protected int challengeOpponentHighestmatchScore;
    protected int challengeOpponentHighestFirstRoundScore;
    protected float challengeOpponentBestFirstRoundTime;
    protected int challengeOpponentHighestSecondRoundScore;
    protected float challengeOpponentBestSecondRoundTime;
    protected int challengeOpponentHighestThirdRoundScore;
    protected float challengeOpponentBestThirdRoundTime;

    protected boolean downloadingRandomOpponentFacebookPicture;
    protected boolean downloadingRandomOpponentFacebookPictureHasFinished;
    protected boolean opponentForChallengePopUpHasBeenBuilded;

    protected InputMultiplexer multiplexer;
    protected InputAdapter backManager;
    protected final float duration = 0.2f;
    protected final Interpolation interpolation = Interpolation.pow5Out;

    protected final FPSLimiter fpsLimiter = new FPSLimiter(100);
    protected BitmapFont zighia30 = CardsAsset.instance.fonts.zighia30;
    protected BitmapFont zighia32 = CardsAsset.instance.fonts.zighia32;
    protected BitmapFont zighia36 = CardsAsset.instance.fonts.zighia36;
    protected BitmapFont zighia37 = CardsAsset.instance.fonts.zighia37;
    protected BitmapFont zighia44 = CardsAsset.instance.fonts.zighia44;
    protected BitmapFont zighia45 = CardsAsset.instance.fonts.zighia45;
    protected BitmapFont zighia48 = CardsAsset.instance.fonts.zighia48;

    private final static float popInTimeDuration = 0.1f;

    public AbstractScreen(MemoGameOnline memo, MOCDataCarrier mocdc) {

        this.memo = memo;

        cam = new OrthographicCamera();
        cam.setToOrtho(false, Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        // batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);

        experienceBarContainer = new Image(memo.patchedSkin, "experienceBarContainer");
        experienceBarContainer.setPosition(8.3f, 790);

        experienceBar = new ExperienceBar();

        experienceBarSeparetor = new Image(memo.patchedSkin, "experienceBarSeparetor");
        experienceBarSeparetor.setPosition(8.3f, 790);

        // levelIcon = new Image(memo.patchedSkin, "levelIcon");
        //levelIcon.setPosition(13, 796);

        //pillsCan = new Image(memo.patchedSkin, "pillsCan");
        // pillsCan.setPosition(455, 790);

        screen = new Table(memo.patchedSkin);
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        stage = new Stage(new StretchViewport(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT));
        multiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(multiplexer);

        this.mocdc = mocdc;

    }

    public abstract void render(float delta);

    public abstract void resize(int width, int height);

    public abstract void show();

    public void hide() {
        dispose();
    }

    public abstract void pause();

    public abstract void resume();

    public void dispose() {
        screen.clear();
        stage.dispose();
        batcher.dispose();

    }

    public InputProcessor getInputProcessor() {
        return this.multiplexer;
    }

    public String getTAG() {

        return TAG;
    }

    public void checkServerMessage() {


        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80 || mocdc.cmdToExecute.first() == 98 || mocdc.cmdToExecute.first() == 99) {
                mocdc.removeMessage();
            } else if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH
                // SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 3) { // OPPONENT REFUSE A CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 2) { // OPPONENT FOUND, CREATE A MATCH!!!

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);


                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA compsota di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////

                User.data.searchingForRandomOpponent = false;
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 6) { // Match CLOSED BY OTHER PLAYER
                Long matchID = Long.valueOf(mocdc.dataToExecute.first()[0]);
                if (User.data.getMatch(matchID) != null) {
                    User.data.closeMatch(matchID);
                    User.data.getMatch(matchID).setMatchWon(false);
                    User.data.getMatch(matchID).setFinished(true);
                    User.data.getMatch(matchID).setFinishedForGiveUp(true);
                    DataManager.instance.saveData();
                }
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        User.data.challengeFromFriendReceived = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27
                    || mocdc.cmdToExecute.first() >= 80 && mocdc.cmdToExecute.first() <= 99) { // DISCARD MESSAGE!!!
                System.out.println("ABStract!!!!");
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }


        }

        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }

    public void drawExperienceBar() {

        experienceBarContainer.draw(batcher, 1);
        experienceBar.draw(batcher, 8.3f, 790,
                360 - 360 * LevelManagement.instance
                        .getPercentageIncrease(User.data.experience));
        experienceBarSeparetor.draw(batcher, 1);
        // levelIcon.draw(batcher, 1);
        // pillsCan.draw(batcher, 1);


        if (User.data.level > 9)
            zighia37.draw(batcher, "" + User.data.level, 36.5f, 842);
        else
            zighia48.draw(batcher, "" + User.data.level, 42, 845);//42, 845

        if (User.data.pills <= 9)
            zighia48.draw(batcher, "" + User.data.pills, 485, 846);//485,846
        if (User.data.pills > 9 && User.data.pills < 100)
            zighia48.draw(batcher, "" + User.data.pills, 477, 846);//477,846
        if (User.data.pills > 99)
            zighia44.draw(batcher, "" + User.data.pills, 470, 844);


    }

    public void renderDebugInfo() {

        int fps = Gdx.graphics.getFramesPerSecond();

        if (fps >= 45) {

            zighia36.setColor(0, 1, 0, 1);
        } else if (fps >= 30) {

            zighia36.setColor(1, 1, 0, 1);
        } else {

            zighia36.setColor(1, 0, 0, 1);
        }
        zighia36.draw(batcher, "FPS: " + fps, 100, 881);
        zighia36.setColor(Color.BLACK);
        zighia36.draw(batcher, "Heap: " + Gdx.app.getJavaHeap() / 1000000, 220, 881);
        zighia36.draw(batcher, "" + User.data.userName, 100, 850);
        zighia36.draw(batcher, "Native: " + Gdx.app.getNativeHeap() / 1000000, 220, 850);

    }

    public void renderDebugInfoSETTINGS() {

        int fps = Gdx.graphics.getFramesPerSecond();

        if (fps >= 45) {

            zighia36.setColor(0, 1, 0, 1);
        } else if (fps >= 30) {

            zighia36.setColor(1, 1, 0, 1);
        } else {

            zighia36.setColor(1, 0, 0, 1);
        }
        zighia36.draw(batcher, "FPS: " + fps, 10, 881);
        zighia36.setColor(1, 1, 1, 1); // white
        zighia36.draw(batcher, "Heap: " + Gdx.app.getJavaHeap() / 1000000, 400, 881);
        zighia36.draw(batcher, "" + User.data.userName, 10, 850);
        zighia36.draw(batcher, "Native: " + Gdx.app.getNativeHeap() / 1000000, 400, 850);

    }

    public Table buildRandomOpponentChallengePopUp() {

        randomOpponentChallengePopUp = new Window("", memo.patchedSkin);
        randomOpponentChallengePopUp.setBounds(70, -210, 400, 280);
        randomOpponentChallengePopUp.setBackground("BlueBackground");
        randomOpponentChallengePopUp.setTransform(true);
        randomOpponentChallengePopUp.setOrigin(randomOpponentChallengePopUp.getWidth() / 2, randomOpponentChallengePopUp.getHeight() / 2);
        playerChallengingLabel = new Label("" + challengeOpponentID
                + " vuole \n giocare con te!", memo.patchedSkin, "40white");
        randomOpponentChallengePopUp.addActor(playerChallengingLabel);
        playerChallengingLabel.setPosition(202, 138);

        accept = new TextButton("Play (" + (int) User.data.timerEvent + ")", memo.patchedSkin);
        accept.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onAcceptRandomChallengeClicked();
            }
        });
        accept.setBounds(173, 14, 193, 95);

        Button refuse = new Button(memo.patchedSkin, "RedButton");
        refuse.add(new Label("Refuse", memo.patchedSkin, "36white"));
        refuse.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onRefuseRandomChallengeClicked();
            }
        });
        refuse.setBounds(28, 14, 140, 68);

        randomOpponentChallengePopUp.addActor(accept);
        randomOpponentChallengePopUp.addActor(refuse);
        //randomOpponentChallengePopUp.setScale(0, 0);
        randomOpponentChallengePopUp.setVisible(false);
        randomOpponentChallengePopUp.setModal(true);
        return randomOpponentChallengePopUp;
    }

    public void onAcceptRandomChallengeClicked() {

        String[] sendMsg = {"0208", String.valueOf(challengeMatchID)};
        mocdc.sendToServer(sendMsg);

        User.data.addActiveMatch(challengeMatchID, challengingOpponent);

        randomOpponentChallengePopUp.addAction(sequence(moveTo(randomOpponentChallengePopUp.getX(), -210,
                popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                randomOpponentChallengePopUp.setVisible(false);
                popUpShowed = false;
            }
        })));

        User.data.resetAndStopTimerEvent();
        mocdc.removeMessage();
        opponentForChallengePopUpHasBeenBuilded = false;
        downloadingRandomOpponentFacebookPictureHasFinished = false;

    }

    public void onRefuseRandomChallengeClicked() {

        if (User.data.challengeFromFriendReceived) {
            String[] sendMsg = {"0303", challengeOpponentID};
            User.data.challengeFromFriendReceived = false;
            mocdc.sendToServer(sendMsg);
            System.out.println("RIFIUTO INVITO PERSONALIZZATO!!!!!!!!!!!!!!!!!!!!!");
        } else {
            String[] sendMsg = {"0207", String.valueOf(challengeMatchID)};
            mocdc.sendToServer(sendMsg);
        }
        randomOpponentChallengePopUp.addAction(sequence(moveTo(randomOpponentChallengePopUp.getX(), -210,
                popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                randomOpponentChallengePopUp.setVisible(false);
                popUpShowed = false;
            }
        })));
        User.data.resetAndStopTimerEvent();
        mocdc.removeMessage();
        opponentForChallengePopUpHasBeenBuilded = false;
        downloadingRandomOpponentFacebookPictureHasFinished = false;
    }

    public void downloadOpponentFacebookIcon(final Long matchID, final Opponent opponent) {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                System.out.println("FOTO URL:  " + opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            System.out.print("FOTO SCARICATA!!!");
                            User.data.addActiveMatch(challengeMatchID, opponent);
                        }
                    });
                }
            }
        }).start();
    }

    public void downloadOpponentFacebookIconForPopUpChallenge(final Opponent opponent) {
        System.out.println("ENTRO NEL DOWNLOAD!!!");
        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                System.out.println("FOTO URL:  " + opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            downloadingRandomOpponentFacebookPicture = false;
                            downloadingRandomOpponentFacebookPictureHasFinished = true;
                            System.out.println("FINITO IL DOWNLOAD!!!");
                            System.out.println("DOWNLOAD - Ottenuta foto di: " + challengingOpponent.getUserName());
                            System.out.println("DOWNLOAD - Foto???: " + challengingOpponent.getFacebookPicture());

                        }
                    });
                }
            }
        }).start();
    }

    public void buildRandomOpponentProfile() {

        challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
        challengeOpponentID = mocdc.dataToExecute.first()[1];
        challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute.first()[2]);
        challengeOpponentImageId = Integer.parseInt(mocdc.dataToExecute.first()[3]);
        challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
        challengeOpponentMatchesPlayed = Integer.parseInt(mocdc.dataToExecute.first()[5]);
        challengeOpponentMatchesWon = Integer.parseInt(mocdc.dataToExecute.first()[6]);
        challengeOpponentConsecutiveMatchWon = Integer.parseInt(mocdc.dataToExecute.first()[7]);
        challengeOpponentHighestmatchScore = Integer.parseInt(mocdc.dataToExecute.first()[8]);
        challengeOpponentHighestFirstRoundScore = Integer.parseInt(mocdc.dataToExecute.first()[9]);
        challengeOpponentBestFirstRoundTime = Float.parseFloat(mocdc.dataToExecute.first()[10]);
        challengeOpponentHighestSecondRoundScore = Integer.parseInt(mocdc.dataToExecute.first()[11]);
        challengeOpponentBestSecondRoundTime = Float.parseFloat(mocdc.dataToExecute.first()[12]);
        challengeOpponentHighestThirdRoundScore = Integer.parseInt(mocdc.dataToExecute.first()[13]);
        challengeOpponentBestThirdRoundTime = Float.parseFloat(mocdc.dataToExecute.first()[14]);

        challengingOpponent = new Opponent();
        challengingOpponent.setUserName(challengeOpponentID);
        challengingOpponent.setLevel(challengeOpponentLevel);
        challengingOpponent.setImageID(challengeOpponentImageId);
        challengingOpponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
        challengingOpponent.setMatchPlayed(challengeOpponentMatchesPlayed);
        challengingOpponent.setMatchesWon(challengeOpponentMatchesWon);
        challengingOpponent.setConsecutiveMatchWon(challengeOpponentConsecutiveMatchWon);
        challengingOpponent.setHighestmatchScore(challengeOpponentHighestmatchScore);
        challengingOpponent.setHighestFirstRoundScore(challengeOpponentHighestFirstRoundScore);
        challengingOpponent.setBestFirstRoundTime(challengeOpponentBestFirstRoundTime);
        challengingOpponent.setHighestSecondRoundScore(challengeOpponentHighestSecondRoundScore);
        challengingOpponent.setBestSecondRoundTime(challengeOpponentBestSecondRoundTime);
        challengingOpponent.setHighestThirdRoundScore(challengeOpponentHighestThirdRoundScore);
        challengingOpponent.setBestThirdRoundTime(challengeOpponentBestThirdRoundTime);

        System.out.println("Costruito nuovo Opponent. Nome: " + challengingOpponent.getUserName());
        System.out.println("ImageID: " + challengingOpponent.getImageID());
        System.out.println("FacebookPicture: " + challengingOpponent.getFacebookPicture());
    }


}
