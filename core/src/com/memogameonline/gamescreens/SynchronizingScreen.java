package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Match;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by danie_000 on 19/11/2015.
 */

public class SynchronizingScreen extends AbstractScreen {

    private boolean isServerSynchFinished;
    private boolean isFacebookPictureUserDownloaded;
    private boolean isFacebookPicturesOpponentDownloaded;
    private boolean isSynchFinished;
    private int numberOfOpponentFacebookPicturesToDonwload = 0;

    public SynchronizingScreen(MemoGameOnline memo, MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "LOADINGSYINCHRONIZING";
        String[] strings = {"0800"};
        mocdc.sendToServer(strings);
        //TODO TOGLIERE

    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

        checkSynchCompletion();
        checkOpponentFacebookPicturesDownloading();


    }

    private void checkSynchCompletion() {
        //System.out.println("server:  " + isServerSynchFinished);
        //System.out.println("user picture:  " + isFacebookPictureUserDownloaded);
        //System.out.println("opponent pictuers:  " + isFacebookPicturesOpponentDownloaded);
        //System.out.println("TOTAL SYNCHs:  " + isSynchFinished);
        //System.out.println("_____________________________________________________________");
        if (isServerSynchFinished &&
                isFacebookPictureUserDownloaded &&
                isFacebookPicturesOpponentDownloaded
                && !isSynchFinished) {
            memo.setScreen(new Home(memo, mocdc));
            isSynchFinished = true;
        }

    }

    private void checkOpponentFacebookPicturesDownloading() {
        if (isServerSynchFinished) {
            if (numberOfOpponentFacebookPicturesToDonwload == 0) {
                isFacebookPicturesOpponentDownloaded = true;
            }
        }

    }

    @Override
    public void show() {
        screen.setBackground("SignInBackground");
        stage.addActor(screen);
        if (User.data.imageId == 48)
            downloadUserFacebookPicture();
        else {
            isFacebookPictureUserDownloaded = true;
            System.out.println("NESSUNA FOTO UTENTE DA SCARICARE!!!");
        }
    }

    @Override
    public void pause() {

    }

    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }


    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    public void downloadUserFacebookPicture() {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, "https://scontent.xx.fbcdn.net/hprofile-xpa1/v/t1.0-1/" +
                        "p50x50/12115705_1650716381878140_2621404005692089800_n.jpg?oh=393ae634f765a231c4382a29aed353eb&oe=5740545F");
                //int numBytes = download(bytes, User.data.facebookPictureURL);
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            User.data.facebookPicture = new Image(image);
                            isFacebookPictureUserDownloaded = true;
                            System.out.println("FINITO DI SCARICARE LA FOTO UTENTE!!!");
                        }
                    });
                }
            }
        }).start();
    }

    public void downloadOpponentFacebookPicture(final Opponent opponent) {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                System.out.println("SCARICO LA FOTO di " + opponent.getUserName() + " da " + opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            numberOfOpponentFacebookPicturesToDonwload--;
                            System.out.println("Scaricata la foto facebook di:  " + opponent.getUserName());
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void checkServerMessage() {
        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 80) { // SYNC A MATCH

                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                String challengeOpponentID = mocdc.dataToExecute.first()[1];
                int challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                int challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                String opponentFaceookPictureURL = mocdc.dataToExecute.first()[4];
                int challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                int challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);

                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(opponentFaceookPictureURL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                if (opponent.getImageID() == 48) {
                    downloadOpponentFacebookPicture(opponent);
                    numberOfOpponentFacebookPicturesToDonwload++;
                }

                //NEW MACH
                Match match = new Match(matchID, opponent);
                match.setFirstRoundHeroScore(Integer.parseInt(mocdc.dataToExecute.first()[15]));
                match.setSecondRoundHeroScore(Integer.parseInt(mocdc.dataToExecute.first()[16]));
                match.setThirdRoundHeroScore(Integer.parseInt(mocdc.dataToExecute.first()[17]));
                match.setFirstRoundOpponentScore(Integer.parseInt(mocdc.dataToExecute.first()[18]));
                match.setSecondRoundOpponentScore(Integer.parseInt(mocdc.dataToExecute.first()[19]));
                match.setThirdRoundOpponentScore(Integer.parseInt(mocdc.dataToExecute.first()[20]));

                if (!mocdc.dataToExecute.first()[21].equals(null)) {
                    if (mocdc.dataToExecute.first()[21].equals(User.data.userName)) {
                        match.setFinishedForGiveUp(true);
                        match.setMatchWon(false);
                    } else if (mocdc.dataToExecute.first()[21].equals(match.getOpponent().getUserName())) {
                        match.setFinishedForGiveUp(true);
                        match.setMatchWon(true);
                    }
                }
                String chatMessages = mocdc.dataToExecute.first()[23];
                User.data.elaborateChatMessages(match, chatMessages);

                User.data.synchronizeMatch(match, Boolean.valueOf(mocdc.dataToExecute.first()[22]));


                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 98) { // RESET SYNC
                User.data.activeMatches.clear();
                User.data.finishedMatches.clear();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 99) { // SYNC COMPLETED
                mocdc.removeMessage();
                isServerSynchFinished = true;

            }
        }
    }

}
