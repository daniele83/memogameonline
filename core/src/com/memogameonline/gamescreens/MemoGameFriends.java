package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.StreamUtils;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.MemoFriendCell;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class MemoGameFriends extends AbstractScreen {

    private Table friendsCellPanel;


    private Table friendsListGrid = new Table();
    private ScrollPane scrollPane;
    private Table scrollContainer;

    private Table playerFoundPopUp;
    private Table playerNotFoundPopUp;
    private Table playerAlreadyFriendPopUp;

    private Button searchButton;
    private Button searchPanel;
    private Image searchPanelLeftPart;
    private Opponent opponentWasSearchingFor;
    private String friendUserNameSearchedFor;
    private int friendImageIDSearchedFor;
    private TextField searchField;

    private Label playerNotFoundlabel;
    private Label playerFoundlabel;
    private Label playerAlreadyFriendabel;
    private final static float popInTimeDuration = 0.1f;

    public MemoGameFriends(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "FacebookFriendsScreen";

        multiplexer = new InputMultiplexer();
        backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new GameSelection(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);

    }

    @Override
    public void render(float runTime) {

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(runTime);
        stage.draw();

        batcher.begin();
        drawExperienceBar();
        batcher.end();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));


    }

    @Override
    public void show() {
        rebuildStage();
    }

    @Override
    public void pause() {

    }

    public void rebuildStage() {

        stage.clear();
        if (friendsListGrid != null)
            friendsListGrid.clearChildren();
        friendsCellPanel = buildButtonsLayer();

        screen = new Table(memo.patchedSkin);
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.setBackground("GameSelectionBackground");
        screen.addActor(friendsCellPanel);
        screen.addActor(buildSearchPanel());
        stage.addActor(screen);

        if (playerFoundPopUp != null)
            stage.addActor(playerFoundPopUp);
        if (playerAlreadyFriendPopUp != null)
            stage.addActor(playerAlreadyFriendPopUp);
        if (playerNotFoundPopUp != null)
            stage.addActor(playerNotFoundPopUp);
        if (randomOpponentChallengePopUp != null) {
            stage.addActor(buildRandomOpponentChallengePopUp());
        }


        // stage.setDebugAll(true);

    }

    private void reRreshStageAfterFriendCellWasAdded() {

        // scrollContainer.clearChildren();
        final MemoFriendCell cell = new MemoFriendCell(memo, memo.patchedSkin,
                opponentWasSearchingFor);
        if (!User.data.challengedFriendList.contains(friendUserNameSearchedFor, false)
                && !User.data.matchAlreadyExist(friendUserNameSearchedFor)) {
            cell.getPlayButton().addListener(new ChangeListener() {
                public void changed(ChangeEvent event, Actor actor) {
                    onPlayClicked(cell);
                }
            });
        }

        if (friendsListGrid.getChildren().size == 0)
            friendsListGrid.add(cell.getCell()).padTop(10).row();
        else
            friendsListGrid.add(cell.getCell()).padTop(-11).row();

        scrollContainer.top();

        //Passo da semplice griglia a  uno Scroll Pane!!!
        if (friendsListGrid.getChildren().size == 7) {

            scrollContainer.removeActor(friendsListGrid);
            scrollPane = new ScrollPane(friendsListGrid,
                    memo.patchedSkin, "scrollPane");

            scrollPane.setScrollBarPositions(true, false);
            scrollPane.setFadeScrollBars(false);
            scrollPane.setScrollbarsOnTop(false);
            scrollContainer.add(scrollPane).expand().fill().colspan(4);
        }


    }

    private Table buildButtonsLayer() {


        //  friendsListGrid.setSize(520, 471);
        friendsListGrid.defaults().expandX();
        boolean firstCellAdded = false;
        int numberOfCells = User.data.memoFriendsList.size;

        for (int i = 0; i < User.data.memoFriendsList.size; i++) {

            if (User.data.memoFriendsList.get(i).getImageID() != 48 ||
                    User.data.memoFriendsList.get(i).getImageID() == 48 && User.data.memoFriendsList.get(i).getFacebookPicture() != null) {
                final MemoFriendCell cell = new MemoFriendCell(memo, memo.patchedSkin,
                        User.data.memoFriendsList.get(i));
                if (!User.data.challengedFriendList.contains(User.data.memoFriendsList.get(i).getUserName(), false)
                        && !User.data.matchAlreadyExist(User.data.memoFriendsList.get(i).getUserName())) {
                    cell.getPlayButton().addListener(new ChangeListener() {
                        public void changed(ChangeEvent event, Actor actor) {
                            onPlayClicked(cell);
                        }
                    });
                }

                if (!firstCellAdded) {
                    friendsListGrid.add(cell.getCell()).padTop(0).row();
                    firstCellAdded = true;
                } else
                    friendsListGrid.add(cell.getCell()).padTop(-11).row();
            } else {
                downloadOpponentFacebookIcon(User.data.memoFriendsList.get(i));
            }
        }

        scrollContainer = new Table(memo.patchedSkin);
        scrollContainer.setBounds(10, 24, 520, 527);
        // Image background = new Image(memo.patchedSkin, "TrasparentFrame");
        // background.setBounds(-6, -3, 530, 528);
        //scrollContainer.addActor(background);
        Image header = new Image(memo.patchedSkin, "RedHeader");
        Label headerText = new Label("MemoGame  Friends", memo.patchedSkin, "48white");
        header.setPosition(33, 527);
        headerText.setPosition(header.getX() + (header.getWidth() - headerText.getWidth()) / 2, 526);


        scrollContainer.addActor(header);
        scrollContainer.addActor(headerText);

        if (numberOfCells <= 6) {
            scrollContainer.add(friendsListGrid);
            scrollContainer.top();
            System.out.println("NIENTE SCROLL!!!");
        } else {
            scrollPane = new ScrollPane(friendsListGrid,
                    memo.patchedSkin, "scrollPane");

            scrollPane.setScrollBarPositions(true, false);
            scrollPane.setFadeScrollBars(false);
            scrollPane.setScrollbarsOnTop(false);
            scrollContainer.add(scrollPane).expand().fill().colspan(4);
            System.out.println("COSTRUISCO LO SCROLL!!!!");
        }

        // System.out.println(scrollPane);
        return scrollContainer;

    }

    public Table buildSearchPanel() {

        Table table = new Table();
        Button cell = new Button(memo.patchedSkin, "constantWhiteCell");
        table.addActor(cell);

        searchPanel = new Button(memo.patchedSkin, "FacebookFriendsSearchField");
        searchButton = new Button(memo.patchedSkin, "FacebookFriendsSearchButton");
        searchButton.addListener(new InputListener() {
                                     @Override
                                     public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                                         onSearchClicked();
                                         return true;
                                     }
                                 }

        );
        searchButton.setPosition(410, 25);

        searchPanelLeftPart = new Image(memo.patchedSkin,
                "FacebookFriendsSearchFieldLeftPart");
        searchPanelLeftPart.setBounds(116, 18, searchPanelLeftPart.getWidth(), 70);
        searchPanel.setBounds(130, 18, searchPanel.getWidth(), 70);

        searchField = new TextField("", memo.patchedSkin);
        searchField.setMessageText("       Search for a friend");
        searchField.setBounds(130, 20, 258, 50);
        searchField.addListener(new InputListener() {
            @Override
            public boolean keyTyped(InputEvent event, char character) {
                if (event.getKeyCode() == Input.Keys.ENTER) {
                    onSearchClicked();
                    Gdx.input.setOnscreenKeyboardVisible(false);
                }
                return true;
            }
        });

        cell.addActor(searchButton);
        cell.addActor(searchPanelLeftPart);
        cell.addActor(searchPanel);
        cell.addActor(searchField);

        Image header = new Image(memo.patchedSkin, "RedHeader");
        Label headerText = new Label("Add  a  friend", memo.patchedSkin, "48white");
        header.setPosition(39, searchPanel.getY() + 61);
        headerText.setPosition(header.getX() + (header.getWidth() - headerText.getWidth()) / 2, searchPanel.getY() + 59);
        cell.setPosition(13, searchPanel.getY() - 25);
        table.addActor(header);
        table.addActor(headerText);
        table.setY(630);

        return table;

    }


    private void onPlayClicked(MemoFriendCell cell) {

        String[] strings = {"0300", cell.getFriendName()};
        mocdc.sendToServer(strings);
        System.out.println("SFIDO " + cell.getFriendName() + " !!!");
        User.data.challengedFriendList.add(cell.getFriendName());
        cell.friendHasBeenChallenged();

    }

    private void onSearchClicked() {

        Gdx.input.setOnscreenKeyboardVisible(false);

        if (!popUpShowed) {
            friendUserNameSearchedFor = searchField.getText();
            if (User.data.isAlreadyFriend(friendUserNameSearchedFor)) {
                if (playerAlreadyFriendPopUp == null)
                    stage.addActor(buildPlayerAlreadyFriendPopUp());
                popUpShowed = true;
                playerAlreadyFriendPopUp.setVisible(true);
                playerAlreadyFriendPopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                playerAlreadyFriendabel.setText("" + friendUserNameSearchedFor + " is already your friend!");

            } else if (friendUserNameSearchedFor.equals("")) {
                System.out.println("NON CERCO!! CAMPO VUOTO!!!");

            } else {
                String[] strings = {"0900", friendUserNameSearchedFor};
                mocdc.sendToServer(strings);
                System.out.println("Metodo Search: " + friendUserNameSearchedFor);
            }
        }
    }

    public Table buildPlayerFoundPopUp() {

        playerFoundPopUp = new Table(memo.patchedSkin);
        playerFoundPopUp.setBackground("BlackBackground");
        playerFoundPopUp.setTransform(true);
        playerFoundPopUp.setBounds(81, 300, 400, 280);
        playerFoundPopUp.setOrigin(playerFoundPopUp.getWidth() / 2, playerFoundPopUp.getHeight() / 2);
        playerFoundlabel = new Label("" + friendUserNameSearchedFor
                + "\nhas been added to your friend list!", memo.patchedSkin, "50white");
        playerFoundPopUp.addActor(playerFoundlabel);
        playerFoundlabel.setPosition((playerFoundPopUp.getWidth()-playerFoundlabel.getWidth())/2+playerFoundPopUp.getX() , 120);
        Button continueToPlay = new Button(memo.patchedSkin, "RedButton");
        continueToPlay.add(new Label("Continue", memo.patchedSkin, "48white"));
        continueToPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                continueToPlayAfterPlayerWasFoundClicked();
            }
        });
        continueToPlay.setSize(200, 90);
        continueToPlay.setPosition(100, 5);
        // Image icon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
        // icon.setBounds(30, 120, 99, 92);

        playerFoundPopUp.addActor(continueToPlay);
        // playerFoundPopUp.addActor(icon);
        playerFoundPopUp.setScale(0, 0);
        playerFoundPopUp.setVisible(false);
        // playerFoundPopUp.addAction(scaleTo(0, 0, popInTimeDuration));
        // playerFoundPopUp.setModal(true);
        return playerFoundPopUp;
    }

    public Table buildPlayerNotFoundPopUp() {

        playerNotFoundPopUp = new Table(memo.patchedSkin);
        playerNotFoundPopUp.setBackground("BlackBackground");
        playerNotFoundPopUp.setBounds(81, 350, 400, 220);
        playerNotFoundPopUp.setOrigin(playerNotFoundPopUp.getWidth() / 2, playerNotFoundPopUp.getHeight() / 2);
        playerNotFoundPopUp.setTransform(true);
        playerNotFoundlabel = new Label("Player not found!" + friendUserNameSearchedFor, memo.patchedSkin, "50white");
        playerNotFoundPopUp.addActor(playerNotFoundlabel);
        System.out.println((playerNotFoundPopUp.getWidth() - playerNotFoundlabel.getWidth()) / 2);
        playerNotFoundlabel.setPosition(75, 120);

        Button continueToPlay = new Button(memo.patchedSkin, "RedButton");
        continueToPlay.add(new Label("Continue", memo.patchedSkin, "48white"));
        continueToPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                continueToPlayAfterPlayerNotFoundClicked();
            }
        });

        continueToPlay.setSize(200, 90);
        continueToPlay.setPosition(100, 5);

        playerNotFoundPopUp.addActor(continueToPlay);
        playerNotFoundPopUp.setScale(0, 0);
        playerNotFoundPopUp.setVisible(false);
        // playerNotFoundPopUp.setModal(true);
        //playerNotFoundPopUp.setMovable(false);
        // playerNotFoundPopUp.addAction(scaleTo(0, 0, popInTimeDuration));
        return playerNotFoundPopUp;
    }

    private void continueToPlayAfterPlayerNotFoundClicked() {
        System.out.println("RENDO INVISIBILE E RIMUOVO LABEL!!!!");
        playerNotFoundPopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                playerNotFoundPopUp.setVisible(false);
                popUpShowed = false;
            }
        })));
        // playerNotFoundPopUp.setModal(false);
        mocdc.removeMessage();


    }

    public Table buildPlayerAlreadyFriendPopUp() {

        playerAlreadyFriendPopUp = new Table(memo.patchedSkin);
        playerAlreadyFriendPopUp.setBackground("BlackBackground");
        playerAlreadyFriendPopUp.setTransform(true);
        playerAlreadyFriendPopUp.setBounds(81, 300, 410, 230);
        playerAlreadyFriendPopUp.setOrigin(playerAlreadyFriendPopUp.getWidth() / 2, playerAlreadyFriendPopUp.getHeight() / 2);
        playerAlreadyFriendabel = new Label("" + friendUserNameSearchedFor + " is already your friend!", memo.patchedSkin, "50white");
        playerAlreadyFriendPopUp.addActor(playerAlreadyFriendabel);
        playerAlreadyFriendabel.setPosition((playerAlreadyFriendPopUp.getWidth() - playerAlreadyFriendabel.getWidth()) / 2, 120);
        Button continueToPlay = new Button(memo.patchedSkin, "RedButton");
        continueToPlay.add(new Label("Continue", memo.patchedSkin, "48white"));
        continueToPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                continueToPlayAfterPlayerAlreadyFriendClicked();
            }
        });

        continueToPlay.setBounds((playerAlreadyFriendPopUp.getWidth() - continueToPlay.getWidth()) / 2, 3, 194, 98);

        playerAlreadyFriendPopUp.addActor(continueToPlay);
        playerAlreadyFriendPopUp.setScale(0, 0);
        playerAlreadyFriendPopUp.setVisible(false);
        // playerAlreadyFriendPopUp.setModal(true);
        // playerAlreadyFriendPopUp.addAction(scaleTo(0, 0, popInTimeDuration));
        return playerAlreadyFriendPopUp;
    }

    private void continueToPlayAfterPlayerWasFoundClicked() {

        User.data.addFriend(opponentWasSearchingFor);
        playerFoundPopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                playerFoundPopUp.setVisible(false);
                popUpShowed = false;
            }
        })));
        System.out.println(friendImageIDSearchedFor != 48);
        System.out.println(friendImageIDSearchedFor == 48);
        System.out.println(opponentWasSearchingFor.getFacebookPicture() != null);
        DataManager.instance.saveData();
        if (opponentWasSearchingFor.getImageID() != 48 ||
                opponentWasSearchingFor.getImageID() == 48 && opponentWasSearchingFor.getFacebookPicture() != null) {
            reRreshStageAfterFriendCellWasAdded();
        } else {
            downloadOpponentFacebookIcon(opponentWasSearchingFor);
        }
        mocdc.removeMessage();

    }


    private void continueToPlayAfterPlayerAlreadyFriendClicked() {

        System.out.println("RENDO INVISIBILE E RIMUOVO LABEL!!!!");
        playerAlreadyFriendPopUp.addAction(sequence(scaleTo(0, 0, popInTimeDuration), run(new Runnable() {
            public void run() {
                System.out.println("Action complete! Lo rendo invisibile per non chiamar e il metodo draw()");
                playerAlreadyFriendPopUp.setVisible(false);
                popUpShowed = false;
            }
        })));
        // playerAlreadyFriendPopUp.setModal(false);

    }

    private void downloadOpponentFacebookIcon(final Opponent opponent) {

        new Thread(new Runnable() {
            /** Downloads the content of the specified url to the array. The array has to be big enough. */
            private int download(byte[] out, String url) {
                InputStream in = null;
                try {
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(false);
                    conn.setUseCaches(true);
                    conn.connect();
                    in = conn.getInputStream();
                    int readBytes = 0;
                    while (true) {
                        int length = in.read(out, readBytes, out.length - readBytes);
                        if (length == -1) break;
                        readBytes += length;
                    }
                    return readBytes;
                } catch (Exception ex) {
                    return 0;
                } finally {
                    StreamUtils.closeQuietly(in);
                }
            }

            @Override
            public void run() {
                byte[] bytes = new byte[200 * 1024]; // assuming the content is not bigger than 200kb.
                //TODO TOGLIERE COMMENTO!!!
                int numBytes = download(bytes, opponent.getFacebookPictureURL());
                if (numBytes != 0) {
                    // load the pixmap, make it a power of two if necessary (not needed for GL ES 2.0!)
                    Pixmap pixmap = new Pixmap(bytes, 0, numBytes);
                    final int originalWidth = pixmap.getWidth();
                    final int originalHeight = pixmap.getHeight();
                    int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
                    int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
                    final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
                    potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    pixmap.dispose();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Texture tex = new Texture(potPixmap);
                            tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                            TextureRegion image = new TextureRegion(tex, 0, 0, originalWidth, originalHeight);
                            opponent.setFacebookPicture(new Image(image));
                            rebuildStage();
                            System.out.println("HO DOVUTO SCARICARE LA FOTO!!!!");

                        }
                    });
                }
            }
        }).start();
    }


    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void checkServerMessage() {

        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 4) { // UPDATE OPPONENT MATCH
                // SCORE

                int matchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                int score = Integer.parseInt(mocdc.dataToExecute.first()[1]);
                User.data.updateOpponentScore(matchID, score);
                User.data.updateMatchStatus(matchID);
                DataManager.instance.saveData();
                System.out.println("" + User.data.userName
                        + " ha aggiornato la partita " + matchID
                        + " con il punteggio: " + score);
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.get(0) == 3) { // OPPONENT REFUSE A CHALLENGE

                String opponentID = mocdc.dataToExecute.first()[0];
                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(opponentID)) ;
                    User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }
                System.out.println("" + User.data.userName
                        + " ha ricevuto il rifiutato da parte di " + opponentID);
                mocdc.removeMessage();
                DataManager.instance.saveData();
            } else if (mocdc.cmdToExecute.get(0) == 2) { // OPPONENT FOUND, CREATE A MATCH!!!
                //Inizializzo un nuovo OPPONENT e quindi un nuovo MATCH
                challengeMatchID = Integer
                        .parseInt(mocdc.dataToExecute.first()[0]);
                challengeOpponentID = mocdc.dataToExecute.first()[1];
                challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                        .first()[2]);
                challengeOpponentImageId = Integer
                        .parseInt(mocdc.dataToExecute.first()[3]);
                challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[4];
                challengeOpponentMatchesPlayed = Integer
                        .parseInt(mocdc.dataToExecute.first()[5]);
                challengeOpponentMatchesWon = Integer
                        .parseInt(mocdc.dataToExecute.first()[6]);

                int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[9]);
                float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[10]);
                int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[11]);
                float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[12]);
                int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[13]);
                float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[14]);


                //NEW OPPONENT
                Opponent opponent = new Opponent();
                opponent.setUserName(challengeOpponentID);
                opponent.setLevel(challengeOpponentLevel);
                opponent.setImageID(challengeOpponentImageId);
                opponent.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                opponent.setMatchPlayed(challengeOpponentMatchesPlayed);
                opponent.setMatchesWon(challengeOpponentMatchesWon);
                opponent.setConsecutiveMatchWon(consecutiveMatchWon);
                opponent.setHighestmatchScore(highestmatchScorer);
                opponent.setHighestFirstRoundScore(highestFirstRoundScorer);
                opponent.setBestFirstRoundTime(bestFirstRoundTime);
                opponent.setHighestSecondRoundScore(highestSecondRoundScore);
                opponent.setBestSecondRoundTime(bestSecondRoundTime);
                opponent.setHighestThirdRoundScore(highestThirdRoundScore);
                opponent.setBestThirdRoundTime(bestThirdRoundTime);

                User.data.addActiveMatch(challengeMatchID, opponent);

                //CONTROLLO SULLA LISTA challengedFriendList ///////////////////////////////////////////////////////////////////
                System.out.println("-------------------------------------------------");
                System.out.println("Lista CORRENTE composta di " + User.data.challengedFriendList.size + " amici: ");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);

                for (int i = 0; i < User.data.challengedFriendList.size; i++) {
                    if (User.data.challengedFriendList.get(i).equals(challengeOpponentID))
                        User.data.challengedFriendList.removeIndex(i);
                    System.out.println("TOLGO IL NOME DAGLI AMICI SFIDATI!!!");
                }

                System.out.println("Lista AGGIORNATA composta di " + User.data.challengedFriendList.size + " amici:");
                for (String s : User.data.challengedFriendList)
                    System.out.println("Utente già sfidato: " + s);
                System.out.println("-------------------------------------------------");
                // FINE CONTROLLO ///////////////////////////////////////////////////////////////////
                this.rebuildStage();

                User.data.searchingForRandomOpponent = false;
                DataManager.instance.saveData();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 6) { // Match CLOSED BY OTHER PLAYER
                Long matchID = Long.valueOf(mocdc.dataToExecute.first()[0]);
                if (User.data.getMatch(matchID) != null) {
                    User.data.closeMatch(matchID);
                    User.data.getMatch(matchID).setMatchWon(false);
                    User.data.getMatch(matchID).setFinished(true);
                    User.data.getMatch(matchID).setFinishedForGiveUp(true);
                    DataManager.instance.saveData();
                }
                rebuildStage();
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 11) { // NEW CHALLENGE FROM RANDOM OPPONENT RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }


            } else if (mocdc.cmdToExecute.first() == 12) { // NEW CHALLENGE FROM A FRIEND RECEIVED

                if (!opponentForChallengePopUpHasBeenBuilded) {
                    buildRandomOpponentProfile();
                    opponentForChallengePopUpHasBeenBuilded = true;
                }
                if (challengeOpponentImageId == 48 && !downloadingRandomOpponentFacebookPicture && !downloadingRandomOpponentFacebookPictureHasFinished) {
                    downloadingRandomOpponentFacebookPicture = true;
                    downloadOpponentFacebookIconForPopUpChallenge(challengingOpponent);

                } else if (opponentForChallengePopUpHasBeenBuilded && !downloadingRandomOpponentFacebookPicture) {
                    System.out.println("ORA POSSO COSTRUIRE POP UP!!!");
                    if (randomOpponentChallengePopUp == null) {
                        stage.addActor(buildRandomOpponentChallengePopUp());
                        System.out.println("PopUp non creato! inizializzo!");
                    }
                    if (!popUpShowed) {
                        User.data.challengeFromFriendReceived = true;
                        popUpShowed = true;
                        if (challengingOpponent.getImageID() != 48) {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIconFrame);
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = AvatarManager.instance.getBigAvatar(challengeOpponentImageId);
                            randomOpponentIcon.setPosition(30, 115);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                        } else {
                            randomOpponentChallengePopUp.removeActor(randomOpponentIcon);
                            randomOpponentIcon = challengingOpponent.getFacebookPicture();
                            randomOpponentIcon.setBounds(30, 115, 120, 120);
                            randomOpponentIconFrame = new Image(memo.patchedSkin, "pictureFrame");
                            randomOpponentIconFrame.setBounds(28, 113, 124, 124);
                            randomOpponentChallengePopUp.addActor(randomOpponentIcon);
                            randomOpponentChallengePopUp.addActor(randomOpponentIconFrame);
                        }

                        randomOpponentChallengePopUp.setVisible(true);
                        final Interpolation interpolation = Interpolation.elasticOut;
                        randomOpponentChallengePopUp.addAction(moveTo(randomOpponentChallengePopUp.getX(), 0, 1, interpolation));
                        User.data.startTimerEvent();
                        playerChallengingLabel.setText("" + challengeOpponentID
                                + " wants to\nplay with you!");
                        System.out.println("MOSTRO POP UP !!!");
                    }
                }

            } else if (mocdc.cmdToExecute.first() == 41) { // PLAYER FOUND (SEARCH PLAYER FUNCTION)
                if (playerFoundPopUp == null)
                    stage.addActor(buildPlayerFoundPopUp());
                if (!popUpShowed) {
                    popUpShowed = true;

                    //Inizializzo un nuovo AMICO
                    challengeOpponentID = mocdc.dataToExecute.first()[0];
                    challengeOpponentLevel = Integer.parseInt(mocdc.dataToExecute
                            .first()[1]);
                    challengeOpponentImageId = Integer
                            .parseInt(mocdc.dataToExecute.first()[2]);
                    challengeOpponentFacebookPictureRUL = mocdc.dataToExecute.first()[3];
                    challengeOpponentMatchesPlayed = Integer
                            .parseInt(mocdc.dataToExecute.first()[4]);
                    challengeOpponentMatchesWon = Integer
                            .parseInt(mocdc.dataToExecute.first()[5]);

                    int consecutiveMatchWon = Integer.valueOf(mocdc.dataToExecute.first()[6]);
                    int highestmatchScorer = Integer.valueOf(mocdc.dataToExecute.first()[7]);
                    int highestFirstRoundScorer = Integer.valueOf(mocdc.dataToExecute.first()[8]);
                    float bestFirstRoundTime = Float.valueOf(mocdc.dataToExecute.first()[9]);
                    int highestSecondRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[10]);
                    float bestSecondRoundTime = Float.valueOf(mocdc.dataToExecute.first()[11]);
                    int highestThirdRoundScore = Integer.valueOf(mocdc.dataToExecute.first()[12]);
                    float bestThirdRoundTime = Float.valueOf(mocdc.dataToExecute.first()[13]);


                    //NEW OPPONENT
                    opponentWasSearchingFor = new Opponent();
                    opponentWasSearchingFor.setUserName(challengeOpponentID);
                    opponentWasSearchingFor.setLevel(challengeOpponentLevel);
                    opponentWasSearchingFor.setImageID(challengeOpponentImageId);
                    opponentWasSearchingFor.setFacebookPictureURL(challengeOpponentFacebookPictureRUL);
                    opponentWasSearchingFor.setMatchPlayed(challengeOpponentMatchesPlayed);
                    opponentWasSearchingFor.setMatchesWon(challengeOpponentMatchesWon);
                    opponentWasSearchingFor.setConsecutiveMatchWon(consecutiveMatchWon);
                    opponentWasSearchingFor.setHighestmatchScore(highestmatchScorer);
                    opponentWasSearchingFor.setHighestFirstRoundScore(highestFirstRoundScorer);
                    opponentWasSearchingFor.setBestFirstRoundTime(bestFirstRoundTime);
                    opponentWasSearchingFor.setHighestSecondRoundScore(highestSecondRoundScore);
                    opponentWasSearchingFor.setBestSecondRoundTime(bestSecondRoundTime);
                    opponentWasSearchingFor.setHighestThirdRoundScore(highestThirdRoundScore);
                    opponentWasSearchingFor.setBestThirdRoundTime(bestThirdRoundTime);


                    playerFoundPopUp.setVisible(true);
                    playerFoundPopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                    playerFoundlabel.setText("" + friendUserNameSearchedFor
                            + "\nhas been added to your\nfriend list!");
                }

            } else if (mocdc.cmdToExecute.first() == 42) { // PLAYER NOT FOUND (SEARCH PLAYER FNCTION)
                if (playerNotFoundPopUp == null)
                    stage.addActor(buildPlayerNotFoundPopUp());
                if (!popUpShowed) {
                    popUpShowed = true;
                    playerNotFoundPopUp.setVisible(true);
                    playerNotFoundPopUp.addAction(scaleTo(1, 1, popInTimeDuration));
                    playerNotFoundlabel.setText("Player not found!");
                }

            } else if (mocdc.cmdToExecute.first() >= 21 && mocdc.cmdToExecute.first() <= 27) { // DISCARD MESSAGE!!!
                mocdc.removeMessage();

            } else if (mocdc.cmdToExecute.first() == 101) {// NEW CHAT MESSAGE FROM OPPONENT RECEIVED
                challengeMatchID = Long.parseLong(mocdc.dataToExecute.first()[0]);
                String chatMessage = mocdc.dataToExecute.first()[1];
                if (User.data.getMatch(challengeMatchID) != null)
                    User.data.getMatch(challengeMatchID).addOpponentChatMessages(chatMessage);
                mocdc.removeMessage();

            }
        }

        if (User.data.startedTimerEvent) {
            User.data.timerEvent = User.data.timerEvent - Gdx.graphics.getDeltaTime();
            if (accept != null)
                accept.setText("Play (" + (int) User.data.timerEvent + ")");
            if (User.data.timerEvent < 0) {
                onRefuseRandomChallengeClicked();
            }
        }
    }
}




