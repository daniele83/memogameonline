package com.memogameonline.gamescreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by danie_000 on 10/11/2015.
 */

public class UsernameChooseFacebookSignUp extends AbstractScreen {

    private String facebookId;
    private String tempUsername;
    private String tempFacebookPictureURL;
    private int tempImageId;


    private Table signUpFacebookPanel;
    private Button usernameSignUpFacebookPanel;
    private TextField usernameFieldSignUpFacebookPanel;
    private Button chooseUserNameButtonSignUpFacebookPanel;

    private boolean alreadyExistError;

    private float signUpFacebookPanelHeight = 270;

    public UsernameChooseFacebookSignUp(final MemoGameOnline memo, final MOCDataCarrier mocdc) {
        super(memo, mocdc);
        TAG = "SignIn";
        //memo.getAdsController().showBannerAd();

        multiplexer = new InputMultiplexer();
        InputAdapter backManager = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
                    ScreenTransition transition = ScreenTransitionSlide.init(duration,
                            ScreenTransitionSlide.RIGHT, false, interpolation);
                    memo.setScreen(new SignIn(memo, mocdc), transition);
                }
                return true; // return true to indicate the event was handled
            }
        };

        multiplexer.addProcessor(backManager);
        multiplexer.addProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
        // use true here to center the camera
        // that's what you probably want in case of a UI
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        checkServerMessage();

        if (!memo.socket.isConnected())
            memo.setScreen(new ReconnectionScreen(memo));

    }

    @Override
    public void show() {

        screen.clear();
        screen = new Table(memo.patchedSkin);
        screen.setBackground("SignInBackground");
        screen.setSize(Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        screen.addActor(buildButtonAndFields());
        stage.clear();
        stage.addActor(screen);


    }


    @Override
    public void pause() {


    }

    @Override
    public void resume() {

    }

    public Table buildButtonAndFields() {


        // PANNELLO SING UP CON FACEBOOK
        signUpFacebookPanel = new Table();
        usernameSignUpFacebookPanel = new Button(memo.patchedSkin, "SignInTextFields");
        usernameSignUpFacebookPanel.setPosition(58, 110);
        usernameFieldSignUpFacebookPanel = new TextField("", memo.patchedSkin);
        usernameFieldSignUpFacebookPanel.setBounds(15, 8, 300, 45);
        usernameFieldSignUpFacebookPanel.setMessageText("User name");
        // usernameFieldSignUpFacebookPanel.setText("");

        usernameSignUpFacebookPanel.addActor(usernameFieldSignUpFacebookPanel);

        chooseUserNameButtonSignUpFacebookPanel = new Button(memo.patchedSkin, "GreenButton");
        chooseUserNameButtonSignUpFacebookPanel.add(new Label(
                "Choose a User Name", memo.patchedSkin, "40white"));
        chooseUserNameButtonSignUpFacebookPanel.setBounds(55, 10, 408, 88);
        chooseUserNameButtonSignUpFacebookPanel.addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        if (!usernameFieldSignUpFacebookPanel.getText().equals(""))
                            onChooseUsernameFacebookPanelClicked();
                    }
                });

        signUpFacebookPanel.addActor(usernameSignUpFacebookPanel);
        signUpFacebookPanel.addActor(chooseUserNameButtonSignUpFacebookPanel);

        signUpFacebookPanel.setPosition(10, signUpFacebookPanelHeight);

        return signUpFacebookPanel;

    }


    public void onChooseUsernameFacebookPanelClicked() {

        facebookId = User.data.facebookID;
        tempUsername = usernameFieldSignUpFacebookPanel.getText();
        tempImageId = 48;
        tempFacebookPictureURL = User.data.facebookPictureURL;
        String[] strings = {"0113", tempUsername, facebookId, tempFacebookPictureURL, User.data.email, String.valueOf(tempImageId)};
        //System.out.println("MANDO AL SEREVER:   "+User.data.facebookPictureURL);
        mocdc.sendToServer(strings);

    }


    private void displayError() {

        if (this.alreadyExistError) {
            Label errorMessage = new Label("User Name already exists",
                    memo.patchedSkin, "28red");
            errorMessage.setPosition(255, 125);
            errorMessage.setColor(Color.RED);
            errorMessage.addAction(fadeOut(4));
            signUpFacebookPanel.addActor(errorMessage);
            // System.out.println("GESTIONE CORRETTA DELL'ERRORE SIGN UP!!!!");
            alreadyExistError = false;

        }
    }


    public void checkServerMessage() {
        if (mocdc.cmdToExecute.size > 0) {
            if (mocdc.cmdToExecute.first() == 21) { // SIGN UP SUCCESS
                System.out.println("Account creato con successo!!!");
                User.data.resetData();
                User.data.userName = tempUsername;
                User.data.facebookID = facebookId;
                User.data.facebookPictureURL = tempFacebookPictureURL;
                User.data.imageId = tempImageId;

                DataManager.instance.saveData();
                GamePreferences.instance.lastLoggedUser = User.data.userName;
                GamePreferences.instance.autoLogIn = false;
                GamePreferences.instance.autoLogInFacebook = true;
                GamePreferences.instance.save();
                mocdc.removeMessage();
                memo.setScreen(new SynchronizingScreen(memo, mocdc));

            } else if (mocdc.cmdToExecute.first() == 22) { // SIGN UP FAILED, NAME ALREADY EXISTS
                this.alreadyExistError = true;
                this.displayError();
                mocdc.removeMessage();
            }
        }
    }
}

