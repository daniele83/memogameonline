package com.memogameonline.externalAPI;

import com.badlogic.gdx.utils.Timer;
import com.memogameonline.gamehelpers.TaskCallback;

import java.io.IOException;

public abstract class FacebookService {

	abstract public void logIn(TaskCallback callback) throws IOException;
	
	abstract public void getFriendList(TaskCallback callback);

	abstract public void downloadProfilePicture(String userID, TaskCallback callback);

	abstract public byte[] serializePicture(Object obj)throws  IOException;

	abstract public boolean isLoggedIn();

	abstract public void logOut();


}
