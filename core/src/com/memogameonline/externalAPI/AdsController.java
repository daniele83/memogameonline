package com.memogameonline.externalAPI;

/**
 * Created by danie_000 on 24/10/2015.
 */
public interface AdsController {

    public void showBannerAd();
    public void hideBannerAd();

    public void showOrLoadInterstital();
    public void loadInterstital();

}
