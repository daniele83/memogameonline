package com.memogameonline.players;

import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gametable.Classic;
import com.memogameonline.gametable.MemoFiends;

public class MemofiendsPlayer {
	public static final String TAG = ClassicPlayer.class.getName();
	private int cardIndex;

	private MemoFiends table;

	public MemofiendsPlayer(MemoFiends table) {

		cardIndex = -1;

		this.table = table;
	}

	public int getCardIndex() {
		return cardIndex;
	}

	public void setFirstSlot(int firstSlot) {
		this.cardIndex = firstSlot;
	}

	public boolean selectFirstCard(int x, int y) {

		for (int i = 0; i < table.getCardsNumber(); i++) {

			if (x > table.getGrid().get(i).getPosition().x
					&& x < (table.getGrid().get(i).getPosition().x + 116)
					&& 885 - y > table.getGrid().get(i).getPosition().y
					&& 885 - y < (table.getGrid().get(i).getPosition().y + 116)) {

				if (table.isDeckFaceUP()) {
					System.out.println("Non Puoi Selezionare Le carte ORA!!!");
					return false;
				} else if (!table.getGrid().get(i).getCard().isSelectable()) {
					System.out.println("Carta  non selezionabile!!!");
					return false;
				} else if (table.isBlockInputHandler()) {
					System.out.println("Input inibiti!");
					return false;
				} else if (table.isInputInihibited()) {
					System.out.println("Input inibiti!");
					return false;
				} else {
					cardIndex = i;
					return true;
				}

			}
		}

		return false;
	}

	public boolean selectCard(int x, int y) {

		for (int i = 0; i < table.getCardsNumber(); i++) {

			if (x > table.getGrid().get(i).getPosition().x + 18
					&& x < (table.getGrid().get(i).getPosition().x + 96)
					&& 885 - y > table.getGrid().get(i).getPosition().y + 18
					&& 885 - y < (table.getGrid().get(i).getPosition().y + 96)) {

				if (table.isDeckFaceUP()) {
					System.out.println("Non Puoi Selezionare Le carte ORA!!!");
					return false;
				} else if (!table.getGrid().get(i).getCard().isSelectable()) {
					System.out.println("Carta  non selezionabile!!!");
					return false;
				} else if (table.isBlockInputHandler()) {
					System.out.println("Input inibiti!");
					return false;
				} else if (table.getChain().size == 0) {
					System.out
							.println("Prima carta  non ancora selezionata!!!!");
					return false;
				} else if (table.isInputInihibited()) {
					System.out.println("INPUT INIBITI!!!");
					return false;
				} else {
					cardIndex = i;
					return true;
				}
			}
		}

		return false;
	}

}