package com.memogameonline.players;

import com.badlogic.gdx.Gdx;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gametable.Classic;

// TEST 1000

public class ClassicPlayer {
    public static final String TAG = "CLASSIC PLAYER: ";
    private int firstCardIndex;
    private int secondCardIndex;

    private Classic table;

    public ClassicPlayer(Classic table) {

        firstCardIndex = -1;
        secondCardIndex = -1;
        this.table = table;
    }

    public int getFirstCardIndex() {
        return firstCardIndex;
    }

    public void setFirstCardIndex(int firstSlot) {
        this.firstCardIndex = firstSlot;
    }

    public int getSecondCardIndex() {
        return secondCardIndex;
    }

    public void setSecondCardIndex(int secondSlot) {
        this.secondCardIndex = secondSlot;
    }

    public boolean selectKidCard(int x, int y, boolean fastSelectionInvocation) {

        for (int i = 0; i < 24; i++) {

            if (x > table.getDeck().getCards().get(i).getPosition().x
                    && x < (table.getDeck().getCards().get(i).getPosition().x + 116)
                    && 885 - y > table.getDeck().getCards().get(i)
                    .getPosition().y
                    && 885 - y < (table.getDeck().getCards().get(i)
                    .getPosition().y + 116)) {

                if (!this.table.getDeck().getCards().get(i).isSelectable()) {
                    System.out.println("Slot  non selezionabile!!!");
                    return false;

                } else if (secondCardIndex != -1 && !table.isFastSelection()) {
                    System.out
                            .println("slot gia selezionati! Attendere la fine del confronto!!!");
                    return false;

                } else if (secondCardIndex != -1 && firstCardIndex != -1 && table.isFastSelection() && !table.isInputInihibited()) {
                    Gdx.app.log(TAG, "due carte già in movimento, effettuo una FAST_SELECTION (coordinate fisiche: " + x + " e " + y + ")");
                    table.doFastSelection(x, y);
                    return false;

                } else if (table.isInitialCountdown()) {
                    System.out.println("Partita  non ancora iniziata!!");
                    return false;

                } else if (table.isInputInihibited()) {
                    System.out.println("PARTITA TERMINATA!!!!!!!!!!!");
                    return false;

                } else {
                    if (firstCardIndex == -1) {
                        firstCardIndex = i;
                        if (!fastSelectionInvocation) {
                            Gdx.app.log(TAG, "-------------------------------------------------------------------CARD SELECTED " + "(" + x + " e " + y + ")");
                            Gdx.app.log(TAG, "settato SLOT_ONE  " + i + " (" +
                                    CardsAsset.instance.cards[table.getDeck().getCards().get(firstCardIndex).getID()] + ")");
                        } else {
                            Gdx.app.log(TAG, "-------------------------------------------------------------------CARD SELECTED FROM FAST SELECTION " + "(" + x + " e " + y + ")");
                            Gdx.app.log(TAG, "settato SLOT_ONE  " + i + " (" +
                                    CardsAsset.instance.cards[table.getDeck().getCards().get(firstCardIndex).getID()] + ")");
                        }
                    } else {
                        secondCardIndex = i;
                        if (!fastSelectionInvocation) {
                            Gdx.app.log(TAG, "-------------------------------------------------------------------CARD SELECTED " + "(" + x + " e " + y + ")");
                            Gdx.app.log(TAG, "settato SLOT_TWO  " + i + " (" +
                                    CardsAsset.instance.cards[table.getDeck().getCards().get(secondCardIndex).getID()] + ")");
                        } else {
                            Gdx.app.log(TAG, "-------------------------------------------------------------------CARD SELECTED FROM FAST SELECTION " + "(" + x + " e " + y + ")");
                            Gdx.app.log(TAG, "settato SLOT_ONE  " + i + " (" +
                                    CardsAsset.instance.cards[table.getDeck().getCards().get(firstCardIndex).getID()] + ")");
                        }
                    }
                    // System.out.println(CardsAsset.instance.kidDeck[i]);
                    return true;
                }
            }
        }

        Gdx.app.log(TAG, "nessuna carta trovata!!! (coordinate fisiche: " + x + " e " + y + ")");
        return false;
    }
}