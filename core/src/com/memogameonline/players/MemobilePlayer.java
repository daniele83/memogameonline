package com.memogameonline.players;

import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gametable.Classic;
import com.memogameonline.gametable.Memobile;

// TEST 1000

public class MemobilePlayer {
	public static final String TAG = ClassicPlayer.class.getName();
	private int firstCardIndex;
	private int secondCardIndex;

	private Memobile table;

	public MemobilePlayer(Memobile table) {

		firstCardIndex = -1;
		secondCardIndex = -1;
		this.table = table;
	}

	public int getFirstCardIndex() {
		return firstCardIndex;
	}

	public void setFirstCardIndex(int firstSlot) {
		this.firstCardIndex = firstSlot;
	}

	public int getSecondCardIndex() {
		return secondCardIndex;
	}

	public void setSecondCardIndex(int secondSlot) {
		this.secondCardIndex = secondSlot;
	}

	public boolean selectKidCard(int x, int y) {

		for (int i = 0; i < 24; i++) {

			if (x > table.getDeck().getCards().get(i).getPosition().x
					&& x < (table.getDeck().getCards().get(i).getPosition().x + 116)
					&& 885 - y > table.getDeck().getCards().get(i)
							.getPosition().y
					&& 885 - y < (table.getDeck().getCards().get(i)
							.getPosition().y + 116)) {

				if (!this.table.getDeck().getCards().get(i).isSelectable()) {
					System.out.println("Slot  non selezionabile!!!");
					return false;

				} else if (secondCardIndex != -1 && !table.isFastSelection()) {
					System.out
							.println("slot gi� selezionati! Attendere la fine del confronto!!!");
					return false;

				} else if (secondCardIndex != -1 && table.isFastSelection()) {
					table.doFastSelection(x, y);
					return false;

				} else if (table.isInitialCountdown()) {
					System.out.println("Partita  non ancora iniziata!!");
					return false;

				} else if (table.isInputInihibited()) {
					System.out.println("Input INIBITI!!!");
					return false;

				} else {
					if (firstCardIndex == -1)
						firstCardIndex = i;
					else
						secondCardIndex = i;
					// System.out.println(CardsAsset.instance.kidDeck[i]);
					return true;
				}
			}
		}

		return false;
	}
}