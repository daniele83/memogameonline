package com.memogameonline.main;

import com.badlogic.gdx.utils.Json;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamescreens.LoadingScreen;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by danie_000 on 08/11/2015.
 */
public class ServerSynchronizer implements Runnable {

    private MemoGameOnline memo;
    private MOCDataCarrier mocdc;

    private boolean threadIsAlive = true;
    private final static String[] synchronizingMessage = {"9990"};
    private final static int sleepingTime = 20000;//ms
    private boolean waitingForPong;

    public ServerSynchronizer(MemoGameOnline memo, MOCDataCarrier mocdc) {
        this.memo = memo;
        this.mocdc = mocdc;
    }

    @Override
    public void run() {

        {
            while (threadIsAlive) {

                if (waitingForPong) {
                    //System.out.println("Synchronizer Thred --->  Risposta non arrivata :( Connessione interrotta!!!");
                    threadIsAlive = false;
                    memo.reader.stop();
                } else {
                    mocdc.sendToServer(synchronizingMessage);
                    waitingForPong = true;
                    //System.out.println("Synchronizer Thred --->  Invio messaggio di sincronizzazione!");
                    try {
                        Thread.sleep(sleepingTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }


    public void synchronizinMessageReceived() {
        waitingForPong = false;
        //System.out.println("Synchronizer Thred --->  Sincronizzazione avvenuta!");
    }
}
