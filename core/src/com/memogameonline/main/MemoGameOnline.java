package com.memogameonline.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.memogameonline.externalAPI.AdsController;
import com.memogameonline.externalAPI.Email;
import com.memogameonline.externalAPI.FacebookService;
import com.memogameonline.externalAPI.InternetService;
import com.memogameonline.externalAPI.SMS;
import com.memogameonline.externalAPI.Twitter;
import com.memogameonline.externalAPI.WhatsApp;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gamescreens.DirectedGame;
import com.memogameonline.gamescreens.LoadingScreen;
import com.memogameonline.gamescreens.ReconnectionScreen;
import com.memogameonline.gamescreens.SignIn;
import com.memogameonline.user.User;


public class MemoGameOnline extends DirectedGame {

    //COMMENTO DI PROVA!!!!!!!


    //String hostIpAddress = "54.68.246.95";// EC2 EMMA
    public int versionCode;
    public String versionName;
    public Skin patchedSkin;
    public TextureAtlas patchedAtlas;
    public TextureAtlas bigIconsAtlas;
    public TextureAtlas smallIconsAtlas;
    public Skin bigIconsSkin;
    public Skin smallIconsSkin;
    public MOCDataCarrier mocdc;

    public Socket socket;
    private BufferedReader br;

    // Protocol protocol;

    private String hostIpAddress = "52.34.132.175";//EC2 NOSTRO
    //private String hostIpAddress = "127.0.0.1";//IP DI PROVA PER IL LOCALE
    private int port = 64000;
    private SocketHints hints;
    public ReaderServerMessage reader;
    public ServerSynchronizer synchronizer;

    private InternetService internetService;
    private FacebookService facebookService = null;
    private AdsController adsController;
    private WhatsApp whatsApp;
    private SMS sms;
    private Twitter twitter;
    private Email email;


    public final static String TAG = MemoGameOnline.class.getName();

    public MemoGameOnline(AdsController adsController) {
        this.adsController = adsController;
    }

    @Override
    public void create() {

        //Caricamento Assets
        CardsAsset.instance.load(new AssetManager());
        patchedAtlas = new TextureAtlas(
                Gdx.files.internal(Constants.GENERAL_ASSETS_PATCHED_ATLAS));
        for (Texture t : patchedAtlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        patchedSkin = new Skin(Gdx.files.internal(Constants.GENERAL_ASSETS_PATCHED_JSON),
                patchedAtlas);

        bigIconsAtlas = new TextureAtlas(
                Gdx.files.internal(Constants.BIG_ICONS_ATLAS));
        for (Texture t : bigIconsAtlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bigIconsSkin = new Skin(Gdx.files.internal(Constants.BIG_ICONS_SKIN),
                bigIconsAtlas);

        smallIconsAtlas = new TextureAtlas(
                Gdx.files.internal(Constants.SMALL_ICONS_ATLAS));
        for (Texture t : smallIconsAtlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        smallIconsSkin = new Skin(Gdx.files.internal(Constants.SMALL_ICONS_SKIN),
                smallIconsAtlas);

        AvatarManager.instance.load(bigIconsSkin, smallIconsSkin);


        if (internetService != null) {
            System.out.println("Rilevato Applicativo Android !!!");
            if (internetService.isNetworkConnected()) {
                try {
                    socket = Gdx.net.newClientSocket(Protocol.TCP, hostIpAddress,
                            port, null);
                    mocdc = new MOCDataCarrier(this);
                    br = new BufferedReader(new InputStreamReader(mocdc.socket
                            .getInputStream()));
                    reader = new ReaderServerMessage(this, mocdc, br);
                    new Thread(reader).start();
                    synchronizer = new ServerSynchronizer(this, mocdc);
                    new Thread(synchronizer).start();

                    setScreen(new SignIn(this, mocdc));
                } catch (GdxRuntimeException gdxEx) {
                    System.out.println("Eccezione Gestita!!!");
                    gdxEx.printStackTrace();
                    setScreen(new ReconnectionScreen(this));
                }

            } else {
                setScreen(new ReconnectionScreen(this));
            }
        } else {
            System.out.println("Rilevato Applicativo Desktop !!!");
            try {
                socket = Gdx.net.newClientSocket(Protocol.TCP, hostIpAddress,
                        port, null);
                mocdc = new MOCDataCarrier(this);
                br = new BufferedReader(new InputStreamReader(mocdc.socket
                        .getInputStream()));
            } catch (GdxRuntimeException gdxEx) {
                System.out.println("Eccezione Gestita!!!");
                gdxEx.printStackTrace();
            }

            reader = new ReaderServerMessage(this, mocdc, br);
            new Thread(reader).start();
            synchronizer = new ServerSynchronizer(this, mocdc);
            new Thread(synchronizer).start();

            setScreen(new SignIn(this, mocdc));
        }


    }

    public void resetConnection() {

        if (internetService.isNetworkConnected()) {
            System.out.println("Memo---> tentativo di riconnessione...");
            try {
                socket = Gdx.net.newClientSocket(Protocol.TCP, hostIpAddress,
                        port, null);
                mocdc = new MOCDataCarrier(this);
                br = new BufferedReader(new InputStreamReader(mocdc.socket
                        .getInputStream()));
                System.out.println("Memo---> riconnessione riuscita!");
            } catch (GdxRuntimeException gdxEx) {
                System.out.println("Eccezione Gestita!!!");
                gdxEx.printStackTrace();
            }

            reader = new ReaderServerMessage(this, mocdc, br);
            new Thread(reader).start();
            synchronizer = new ServerSynchronizer(this, mocdc);
            new Thread(synchronizer).start();

            setScreen(new SignIn(this, mocdc));

            User.data.activeMatches.clear();
            User.data.finishedMatches.clear();
        } else {
            setScreen(new ReconnectionScreen(this));
        }

    }

    public static void debug(String s) {

        Gdx.app.log("Facebook", s);
    }

    public FacebookService getFacebookService() {
        return facebookService;
    }

    public void setFacebookService(FacebookService facebookService) {
        this.facebookService = facebookService;
    }

    public WhatsApp getWhatsApp() {
        return whatsApp;
    }

    public void setWhatsApp(WhatsApp whatsApp) {
        this.whatsApp = whatsApp;
    }

    public SMS getSms() {
        return sms;
    }

    public void setSms(SMS sms) {
        this.sms = sms;
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public AdsController getAdsController() {
        return adsController;
    }

    public void setAdsController(AdsController adsController) {
        this.adsController = adsController;
    }

    public void setVersionCode(int i) {
        this.versionCode = i;
    }

    public void setVersionName(String s) {
        this.versionName = s;
    }

    public InternetService getInternetService() {
        return internetService;
    }

    public void setInternetService(InternetService internetService) {
        this.internetService = internetService;
    }
}
