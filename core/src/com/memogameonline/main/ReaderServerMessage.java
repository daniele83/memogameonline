package com.memogameonline.main;

import com.badlogic.gdx.utils.Json;
import com.memogameonline.gamehelpers.MOCDataCarrier;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by danie_000 on 08/11/2015.
 */
public class ReaderServerMessage implements Runnable {

    private MemoGameOnline memo;
    private MOCDataCarrier mocdc;
    private BufferedReader br;

    private String messageReceived;
    private String[] inputArray;
    private Json json = new Json();

    private boolean threadIsAlive = true;

    public ReaderServerMessage(MemoGameOnline memo, MOCDataCarrier mocdc, BufferedReader br) {
        this.memo = memo;
        this.mocdc = mocdc;
        this.br = br;
    }

    @Override
    public void run() {

        {

            // TODO move to SpalshScreen
            System.out.println("Reader Server Message Thread: established Connectio: " + mocdc.socket.isConnected()); // LOG

            try {
                messageReceived = br.readLine();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            while (threadIsAlive && memo.socket.isConnected()) {
                try {
                    messageReceived = br.readLine();
                    if (messageReceived != null) {
                        inputArray = json.fromJson(String[].class,
                                messageReceived);
                        mocdc.readInput(inputArray);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void stop() {
        threadIsAlive = false;
        memo.socket.dispose();

    }
}
