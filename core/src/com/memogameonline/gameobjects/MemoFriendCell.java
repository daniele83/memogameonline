package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

public class MemoFriendCell {

    private MemoGameOnline memo;
    private Button cell;
    private String friendName;
    private String friendFacebookID;
    private Image opponentAvatar;
    private Button playButton;
    private Label challengAlreadySent;
    private Label matchLareadyExistLabel;

    public MemoFriendCell(MemoGameOnline memo, Skin skin, Opponent opponent) {
        this.memo = memo;

        cell = new Button(skin, "friendsCell");
        //cell.setWidth(456);
        //cell.setHeight(77);
        friendName = opponent.getUserName();
        this.friendFacebookID = friendFacebookID;
        if (opponent.getImageID() != 48) {
            opponentAvatar = AvatarManager.instance.getBigAvatar(opponent.getImageID());
            opponentAvatar.setBounds(12, 12, 80, 70);
            cell.addActor(opponentAvatar);
        } else {
            opponentAvatar = opponent.getFacebookPicture();
            opponentAvatar.setBounds(24, 19, 55, 55);
            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
            pictureFrame.setBounds(22, 17, 59, 59);
            cell.addActor(opponentAvatar);
            cell.addActor(pictureFrame);

        }

        Label friendNameLabel;
        if (friendName.length() > 18)
            friendNameLabel = new Label(friendName.substring(0, 18), memo.patchedSkin, "40blue");
        else
            friendNameLabel = new Label(friendName, memo.patchedSkin, "40blue");

        if (User.data.matchAlreadyExist(friendName)) {
            /*matchLareadyExistLabel = new Label("There's  already  a  running\nmatch  against  " + friendName, memo.patchedSkin, "28red");
            matchLareadyExistLabel.setPosition(213, 16);
            cell.addActor(matchLareadyExistLabel);*/
            playButton = new Button(memo.patchedSkin, "emptyButton");
            playButton.setBounds(330, 11, 0, 0);
            cell.addActor(playButton);
        } else if (!User.data.challengedFriendList.contains(friendName, false)) {
            playButton = new Button(memo.patchedSkin, "GreenButton");
            playButton.add(new Label("Play", memo.patchedSkin, "45white"));
            playButton.setBounds(330, 11, 117, 66);
            cell.addActor(playButton);
        } else {
            /*challengAlreadySent = new Label("Waiting for an answer...", memo.patchedSkin, "28red");
            challengAlreadySent.setBounds(213, 20, 105, 62);
            cell.addActor(challengAlreadySent);*/
            playButton = new Button(memo.patchedSkin, "GreenButton");
            playButton.add(new Label("Wait...", memo.patchedSkin, "42white"));
            playButton.setBounds(330, 11, 117, 66);
            cell.addActor(playButton);
            playButton.setDisabled(true);
        }


        friendNameLabel.setPosition(96, 22);

        cell.addActor(friendNameLabel);


    }

    public Button getCell() {
        return cell;
    }


    public Table getPlayButton() {
        return playButton;
    }

    public String getFriendFacebookID() {
        return friendFacebookID;
    }

    public void setFriendFacebookID(String friendFacebookID) {
        this.friendFacebookID = friendFacebookID;
    }

    public String getFriendName() {
        return friendName;
    }

    public void friendHasBeenChallenged() {
        cell.removeActor(playButton);
        /*challengAlreadySent = new Label("Waiting for an answer...", memo.patchedSkin, "28red");
        challengAlreadySent.setBounds(213, 20, 98, 58);
        cell.addActor(challengAlreadySent);*/

        playButton = new Button(memo.patchedSkin, "GreenButton");
        playButton.add(new Label("Wait...", memo.patchedSkin, "40white"));
        playButton.setBounds(330, 11, 117, 66);
        cell.addActor(playButton);
        playButton.setDisabled(true);

    }

    public void matchHasStarted(String opponentName) {
        cell.removeActor(challengAlreadySent);
        matchLareadyExistLabel = new Label("There's  already  a  running\nmatch  against  " + opponentName, memo.patchedSkin, "28red");
        matchLareadyExistLabel.setPosition(213, 16);
        cell.addActor(matchLareadyExistLabel);

    }


}
