package com.memogameonline.gameobjects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.GamePreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
	public static final String TAG = Deck.class.getName();
	private ArrayList<Card> cards = new ArrayList<Card>();

	public Deck(int deck) {

		for (int a = 0; a < 2; a++) {
			for (int i = 0; i < 12; i++) {
				cards.add(new Card(deck*12 + i));
			}
		}

		// printDeck();
	}

	public void shuffle() {
		Collections.shuffle(this.cards);
		// printDeck();
	}

	public ArrayList<Card> getCards() {
		return cards;
	}

	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}

	public Card[] getCardByID(int ID) {

		Card[] pair = new Card[2];
		int index = 0;
		for (Card card : cards)
			if (card.getID() == ID) {
				pair[index] = card;
				index += 1;
			}
		return pair;
	}

	public void printDeck() {
		System.out.println("Elenco degli ID della carte presenti nel mazzo");
		for (int i = 0; i < 24; i++) {
			System.out.println(i + "  " + cards.get(i).getID());
		}
	}

}
