package com.memogameonline.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Card implements Poolable {
	public static final String TAG = Card.class.getName();
	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleration;
	private Vector2 start;
	private Vector2 end;
	private float distance;

	private boolean faceUP;
	private boolean faceDown;
	private boolean matched;
	private boolean movingFaceUp;
	private boolean movingFaceDown;
	private boolean moving;
	private boolean accelerationSet;
	private boolean shaking;
	private boolean selectable;
	private boolean selected;
	private boolean bonusActived;
	private boolean downFalling;
	private boolean chainElement;
	private boolean availableForTris;
	private boolean disappearing;
	private boolean faceingUpBecauseOfDoubleCouple;
	private boolean exploded;
	private boolean swapping;
	private boolean visible;

	private boolean powerUpped;
	private boolean powerUpped_bomb;
	private boolean powerUpped_time;
	private boolean powerUpped_chain;
	private boolean powerUpped_bonus;
	private boolean powerUpped_faceUp;
	private boolean powerUpped_stop;

	private boolean collision;

	private int ID;

	private float[] shakingArray;
	private int indexShaking;
	private float[] animationArray;
	private int indexAnimation;
	private float[] disappearingArray;
	private int indexDisappearing;
	private static final int NUMBER_FRAMES = 100;
	private static final int NUMBER_BACK_FRAMES = 7;
	private static final int NUMBER_FACE_FRAMES = 7;
	private static final int NUMBER_FACEUP_REMAINING_TIME_FRAMES = 15;
	private static final int NUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES = 69;
	private static final int NUMBER_DISAPPEARING_FRAMES = 10;

	public Card() {
		matched = false;
		faceUP = false;
		faceDown = true;
		movingFaceUp = false;
		movingFaceDown = false;
		moving = false;
		accelerationSet = false;
		shaking = false;
		selectable = true;
		selected = false;
		bonusActived = false;
		downFalling = true;
		chainElement = false;
		disappearing = false;
		availableForTris = false;
		faceingUpBecauseOfDoubleCouple = false;
		exploded = false;
		swapping = false;
		visible = true;

		createShakingArray();
		indexShaking = 0;
		createAnimationArray();
		indexAnimation = 0;
		createDisappearingArray();
		indexDisappearing = 0;

		position = new Vector2(700, 5);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;

		collision = false;

		powerUpped = false;
		powerUpped_bomb = false;
		powerUpped_time = false;
		powerUpped_chain = false;
		powerUpped_bonus = false;
		powerUpped_faceUp = false;
		powerUpped_stop = false;
	}

	public Card(int ID) {
		this.ID = ID;
		matched = false;
		faceUP = false;
		faceDown = true;
		movingFaceUp = false;
		movingFaceDown = false;
		moving = false;
		accelerationSet = false;
		shaking = false;
		selectable = true;
		selected = false;
		bonusActived = false;
		downFalling = true;
		chainElement = false;
		disappearing = false;
		availableForTris = false;
		faceingUpBecauseOfDoubleCouple = false;
		exploded = false;
		swapping = false;
		visible = true;

		createShakingArray();
		indexShaking = 0;
		createAnimationArray();
		indexAnimation = 0;
		createDisappearingArray();
		indexDisappearing = 0;

		position = new Vector2(700, 5);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;

		collision = false;

		powerUpped = false;
		powerUpped_bomb = false;
		powerUpped_time = false;
		powerUpped_chain = false;
		powerUpped_bonus = false;
		powerUpped_faceUp = false;
		powerUpped_stop = false;

	}

	public Card(Card card) {
		this.ID = card.getID();

		matched = card.isMatched();
		faceUP = card.isFaceUP();
		faceDown = card.isFaceDown();
		movingFaceUp = card.isMovingFaceUp();
		movingFaceDown = card.isMovingFaceDown();
		moving = card.isMoving();
		accelerationSet = card.isAccelerationSet();
		shaking = card.isShaking();
		selectable = card.isSelectable();
		selected = card.isSelected();
		bonusActived = card.isBonusActived();
		downFalling = card.isDownFalling();
		chainElement = card.isChainElement();
		disappearing = card.isDisappearing();
		availableForTris = card.availableForTris;
		faceingUpBecauseOfDoubleCouple = card.isFaceingUpBecauseOfDoubleCouple();
		exploded = card.isExploded();
		swapping = card.isSwapping();
		visible = card.isVisible();

		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;

		createShakingArray();
		indexShaking = 0;
		createAnimationArray();
		indexAnimation = 0;
		createDisappearingArray();
		indexDisappearing = 0;

		position = new Vector2(700, 5);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;

		collision = false;

		powerUpped = card.isPowerUpped();
		powerUpped_bomb = card.isPowerUpped_bomb();
		powerUpped_time = card.isPowerUpped_time();
		powerUpped_chain = card.isPowerUpped_chain();
		powerUpped_bonus = card.isPowerUpped_bonus();
		powerUpped_faceUp = card.isPowerUpped_faceUp();
		powerUpped_stop = card.isPowerUpped_stop();

	}

	@Override
	public void reset() {
		matched = false;
		faceUP = false;
		faceDown = true;
		movingFaceUp = false;
		movingFaceDown = false;
		moving = false;
		accelerationSet = false;
		shaking = false;
		selectable = true;
		selected = false;
		bonusActived = false;
		downFalling = true;
		chainElement = false;
		disappearing = false;
		availableForTris = false;
		faceingUpBecauseOfDoubleCouple = false;
		exploded = false;
		swapping = false;
		visible = true;

		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;
		position = new Vector2(700, 5);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		collision = false;

		powerUpped = false;
		powerUpped_bomb = false;
		powerUpped_time = false;
		powerUpped_chain = false;
		powerUpped_bonus = false;
		powerUpped_faceUp = false;
		powerUpped_stop = false;

	}

	public void update(float delta) {

		position.add(velocity.cpy().scl(delta));
		velocity.add(acceleration.cpy().scl(delta));

		/*
		 * if (moving && (start.dst(position) >= distance)) { velocity.set(0,
		 * 0); acceleration.set(0, 0); position.set(end); moving = false; }
		 */
	}

	public boolean isCollision() {
		return collision;
	}

	public void setCollision(boolean collision) {
		this.collision = collision;
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isBonusActived() {
		return bonusActived;
	}

	public boolean isPowerUpped() {
		return powerUpped;
	}

	public void setPowerUpped(boolean powerUpped) {
		this.powerUpped = powerUpped;
	}

	public boolean isSwapping() {
		return swapping;
	}

	public void setSwapping(boolean swapping) {
		this.swapping = swapping;
	}

	public boolean isChainElement() {
		return chainElement;
	}

	public void setChainElement(boolean chainElement) {
		this.chainElement = chainElement;
	}

	public int getNUMBER_FACEUP_REMAINING_TIME_FRAMES() {
		return NUMBER_FACEUP_REMAINING_TIME_FRAMES;
	}

	public int getNUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES() {
		return NUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES;
	}

	public void setBonusActived(boolean bonusActived) {
		this.bonusActived = bonusActived;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public boolean isPowerUpped_bomb() {
		return powerUpped_bomb;
	}

	public void setPowerUpped_bomb(boolean powerUpped_bomb) {
		this.powerUpped_bomb = powerUpped_bomb;
	}

	public boolean isPowerUpped_time() {
		return powerUpped_time;
	}

	public void setPowerUpped_time(boolean powerUpped_time) {
		this.powerUpped_time = powerUpped_time;
	}

	public boolean isPowerUpped_chain() {
		return powerUpped_chain;
	}

	public void setPowerUpped_chain(boolean powerUpped_chain) {
		this.powerUpped_chain = powerUpped_chain;
	}

	public boolean isPowerUpped_bonus() {
		return powerUpped_bonus;
	}

	public void setPowerUpped_bonus(boolean powerUpped_bonus) {
		this.powerUpped_bonus = powerUpped_bonus;
	}

	public boolean isPowerUpped_faceUp() {
		return powerUpped_faceUp;
	}

	public void setPowerUpped_faceUp(boolean powerUpped_faceUp) {
		this.powerUpped_faceUp = powerUpped_faceUp;
	}

	public boolean isPowerUpped_stop() {
		return powerUpped_stop;
	}

	public void setPowerUpped_stop(boolean powerUpped_stop) {
		this.powerUpped_stop = powerUpped_stop;
	}

	public static String getTag() {
		return TAG;
	}

	public int getNUMBER_DISAPPEARING_FRAMES() {
		return NUMBER_DISAPPEARING_FRAMES;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public void setVelocity(float x, float y) {
		this.velocity.x = x;
		this.velocity.y = y;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}

	public boolean isFaceingUpBecauseOfDoubleCouple() {
		return faceingUpBecauseOfDoubleCouple;
	}

	public void setFaceingUpBecauseOfDoubleCouple(boolean faceingUpBecauseOfDoubleCouple) {
		this.faceingUpBecauseOfDoubleCouple = faceingUpBecauseOfDoubleCouple;
	}

	public void setAcceleration(Vector2 acceleration) {
		this.acceleration = acceleration;
	}

	public void setAcceleration(float x, float y) {
		this.acceleration.x = x;
		this.acceleration.y = y;
	}

	public Vector2 getStart() {
		return start;
	}

	public void setStart(Vector2 start) {
		this.start = start;
	}

	public int getNUMBER_FRAMES() {
		return NUMBER_FRAMES;
	}

	public int getNUMBER_BACK_FRAMES() {
		return NUMBER_BACK_FRAMES;
	}

	public boolean isAccelerationSet() {
		return accelerationSet;
	}

	public void setAccelerationSet(boolean accelerationSet) {
		this.accelerationSet = accelerationSet;
	}

	public boolean isCollided() {
		return availableForTris;
	}

	public boolean isExploded() {
		return exploded;
	}

	public void setExploded(boolean exploded) {
		this.exploded = exploded;
	}

	public void setCollided(boolean collided) {
		this.availableForTris = collided;
	}

	public int getNUMBER_FACE_FRAMES() {
		return NUMBER_FACE_FRAMES;
	}

	public float[] getDisappearingArray() {
		return disappearingArray;
	}

	public void setDisappearingArray(float[] disappearingArray) {
		this.disappearingArray = disappearingArray;
	}

	public int getDisappearingIndex() {
		return indexDisappearing;
	}

	public void setDisappearingIndex(int disappearingIndex) {
		this.indexDisappearing = disappearingIndex;
	}

	public boolean isDisappearing() {
		return disappearing;
	}

	public void setDisappearing(boolean disappearing) {
		this.disappearing = disappearing;
	}

	public int getIndexDisappearing() {
		return indexDisappearing;
	}

	public void setIndexDisappearing(int indexDisappearing) {
		this.indexDisappearing = indexDisappearing;
	}

	public boolean isAvailableForTris() {
		return availableForTris;
	}

	public void setAvailableForTris(boolean availableForTris) {
		this.availableForTris = availableForTris;
	}

	public Vector2 getEnd() {
		return end;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public boolean isShaking() {
		return shaking;
	}

	public void setShaking(boolean shaking) {
		this.shaking = shaking;
	}

	public void setEnd(Vector2 end) {
		this.end = end;
	}

	public boolean isDownFalling() {
		return downFalling;
	}

	public void setDownFalling(boolean downFalling) {
		this.downFalling = downFalling;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public boolean isMovingFaceUp() {
		return movingFaceUp;
	}

	public void setMovingFaceUp(boolean movingFaceUp) {
		this.movingFaceUp = movingFaceUp;
	}

	public boolean isMovingFaceDown() {
		return movingFaceDown;
	}

	public void setMovingFaceDown(boolean movingFaceDown) {
		this.movingFaceDown = movingFaceDown;
	}

	public float[] getShakingArray() {
		return shakingArray;
	}

	public float[] getAnimationArray() {
		return animationArray;
	}

	public int getIndexAnimation() {
		return indexAnimation;
	}

	public void setAnimationArray(float[] animationArray) {
		this.animationArray = animationArray;
	}

	public void setIndexAnimation(int indexAnimation) {
		this.indexAnimation = indexAnimation;
	}

	public void setShakingArray(float[] shakingArray) {
		this.shakingArray = shakingArray;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isFaceUP() {
		return faceUP;
	}

	public void setFaceUP(boolean faceUP) {
		this.faceUP = faceUP;
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	public boolean isFaceDown() {
		return faceDown;
	}

	public void setFaceDown(boolean faceDown) {
		this.faceDown = faceDown;
	}

	public int getIndexShaking() {
		return indexShaking;
	}

	public void setIndexShaking(int indexShaking) {
		this.indexShaking = indexShaking;
	}

	private void createShakingArray() {
		shakingArray = new float[30];
		for (int i = 0; i < 30; i += 2)
			shakingArray[i] = 5;
		for (int i = 0; i < 30; i += 2)
			shakingArray[i + 1] = -5;

	}

	private void createDisappearingArray() {
		disappearingArray = new float[NUMBER_DISAPPEARING_FRAMES];
		for (int i = 0; i < NUMBER_DISAPPEARING_FRAMES; i++) {
			disappearingArray[i] = 1f - (1f / NUMBER_DISAPPEARING_FRAMES)
					* i;
		}

	}

	public void resetIndexShaking() {

		indexShaking = 0;
	}

	public void incrementIndexShaking() {
		indexShaking++;
	}

	public void incrementIndexDisappearing() {
		indexDisappearing++;
	}

	public void resetIndexDisappearing() {
		indexDisappearing = 0;
	}

	public void createAnimationArray() {

		animationArray = new float[NUMBER_FRAMES];

		for (int i = 0; i < NUMBER_BACK_FRAMES; i++)
			animationArray[i] = 1f - (1f / NUMBER_BACK_FRAMES) * i;

		for (int i = NUMBER_BACK_FRAMES; i < (NUMBER_BACK_FRAMES + NUMBER_FACE_FRAMES); i++)
			animationArray[i] = (1f / NUMBER_FACE_FRAMES)
					* (i - NUMBER_BACK_FRAMES);

		for (int i = NUMBER_BACK_FRAMES + NUMBER_FACE_FRAMES; i < NUMBER_FRAMES; i++)
			animationArray[i] = 1;

	}

	public void resetIndexAnimation() {

		indexAnimation = 0;
	}

	public void incrementIndexAnimation() {
		indexAnimation++;
	}

	public float getPositionX() {

		return Math.round(position.x);
	}

	public float getPositionY() {

		return Math.round(position.y);
	}

	public void setPositionY(float pos) {

		position.y = pos;
	}

}
