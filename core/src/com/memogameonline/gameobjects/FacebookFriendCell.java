package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

public class FacebookFriendCell {

    private MemoGameOnline memo;
    private Button cell;
    private String friendName;
    private String friendFacebookID;
    private Image friendFacebookImage;
    private Button playButton;
    private Label challengAlreadySent;
    private Label matchLareadyExistLabel;

    public FacebookFriendCell(MemoGameOnline memo, Skin skin, Opponent opponent) {
        this.memo = memo;

        cell = new Button(skin, "constantWhiteCell");
        cell.setWidth(456);
        cell.setHeight(77);
        friendName = opponent.getFacebookName();
        this.friendFacebookID = opponent.getFacebookID();
        friendFacebookImage = opponent.getFacebookPicture();
        friendFacebookImage.setBounds(24, 23, 55, 55);
        Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
        pictureFrame.setBounds(22, 21, 59, 59);


        Label friendNameLabel;
        if (friendName.length() > 21)
            friendNameLabel = new Label(friendName.substring(0, 21), memo.patchedSkin, "40blue");
        else
            friendNameLabel = new Label(friendName, memo.patchedSkin, "40blue");

        if (User.data.matchAlreadyExist(friendName)) {
            /*matchLareadyExistLabel = new Label("There's  already  a  running\nmatch  against  " + friendName, memo.patchedSkin, "28red");
            matchLareadyExistLabel.setPosition(213, 16);
            cell.addActor(matchLareadyExistLabel);*/
            playButton = new Button(memo.patchedSkin, "emptyButton");
            playButton.setBounds(330, 11, 0, 0);
            cell.addActor(playButton);
        } else if (!User.data.challengedFriendList.contains(friendName, false)) {
            playButton = new Button(memo.patchedSkin, "GreenButton");
            playButton.add(new Label("Play", memo.patchedSkin, "45white"));
            playButton.setBounds(360, 13, 135, 72);
            cell.addActor(playButton);
        } else {
            /*challengAlreadySent = new Label("Waiting for an answer...", memo.patchedSkin, "28red");
            challengAlreadySent.setBounds(213, 20, 105, 62);
            cell.addActor(challengAlreadySent);*/
            playButton = new Button(memo.patchedSkin, "GreenButton");
            playButton.add(new Label("Wait...", memo.patchedSkin, "42white"));
            playButton.setBounds(360, 13, 135, 72);
            cell.addActor(playButton);
            playButton.setDisabled(true);
        }


        friendNameLabel.setPosition(100, 27);

        cell.addActor(friendNameLabel);
        cell.addActor(friendFacebookImage);
        cell.addActor(pictureFrame);


    }

    public Button getCell() {
        return cell;
    }


    public Table getPlayButton() {
        return playButton;
    }

    public String getFriendFacebookID() {
        return friendFacebookID;
    }

    public void setFriendFacebookID(String friendFacebookID) {
        this.friendFacebookID = friendFacebookID;
    }

    public String getFriendName() {
        return friendName;
    }

    public void friendHasBeenChallenged() {
        cell.removeActor(playButton);
        playButton = new Button(memo.patchedSkin, "GreenButton");
        playButton.add(new Label("Wait...", memo.patchedSkin, "40white"));
        playButton.setBounds(330, 11, 117, 66);
        cell.addActor(playButton);
        playButton.setDisabled(true);

    }

    public void matchHasStarted(String opponentName) {
        cell.removeActor(challengAlreadySent);
        matchLareadyExistLabel = new Label("There's already a running match against " + opponentName, memo.patchedSkin, "24red");
        matchLareadyExistLabel.setPosition(200, 25);
        cell.addActor(matchLareadyExistLabel);

    }


}
