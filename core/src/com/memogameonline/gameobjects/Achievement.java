package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.memogameonline.main.MemoGameOnline;

public class Achievement {

    private Button cell;
    private Label achievementName;
    private Image imageSlot;
    private ProgressBar progressBar;
    private Image flag;
    private int barValue;
    private boolean completed;

    public Achievement(MemoGameOnline memo, Skin skin, String name,
                       int referenceValue, int max) {
        cell = new Button(skin, "achievementCell");
        imageSlot = new Image(skin, "imageSlot");
        achievementName = new Label(name, memo.patchedSkin, "36black");

        imageSlot.setPosition(20, 20);
        achievementName.setPosition(25, 25);

        cell.addActor(achievementName);
        //cell.addActor(imageSlot);

        //barValue = (float) referenceValue / (float)max;
        barValue = referenceValue;
        System.out.println("REF VALUE: " + referenceValue);
        System.out.println("BAR VALUE: " + referenceValue);
        System.out.println("MAX: " + max);
        System.out.println("______");

        if (barValue < max) {
            progressBar = new ProgressBar(0, max, 1, false, skin);
            progressBar.setBounds(320, (cell.getHeight() - progressBar.getHeight()) / 2 + 17, 115, 1);//(X,Width,Y)
            //progressBar.setPosition(280, 40);
            //progressBar.setAnimateDuration(2);
            //progressBar.setValue(max+20);

            Image leftEdge = new Image(memo.patchedSkin, "ProgressBarLeftEdge");
            leftEdge.setPosition(progressBar.getX(), progressBar.getY() - 8);
            //cell.addActor(leftEdge);
            cell.addActor(progressBar);
        } else {
            completed = true;
            flag = new Image(memo.patchedSkin, "PowerUpAchievementFlag");
            flag.setPosition(370, 40);
            cell.addActor(flag);
        }

    }

    public Button getCell() {
        return cell;
    }

    public void setCell(Button cell) {
        this.cell = cell;
    }

    public Label getAchievementName() {
        return achievementName;
    }

    public void setAchievementName(Label achievementName) {
        this.achievementName = achievementName;
    }

    public Image getImageSlot() {
        return imageSlot;
    }

    public void setImageSlot(Image imageSlot) {
        this.imageSlot = imageSlot;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public void startBarAnimation() {

        if (!completed) {
            progressBar.setAnimateDuration(1);
            progressBar.setValue(barValue);
            //progressBar.setValue(50);
        }

    }

    public void resetBar() {
        if (!completed)
            progressBar.setValue(0);

    }

}
