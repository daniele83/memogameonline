package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

public class ChatCell {

    private MemoGameOnline memo;
    private TextButton cell;
    private String message;
    private boolean messageFromOpponent;


    public ChatCell(MemoGameOnline memo, Skin skin, String msg, boolean messageFromOpponent) {
        this.memo = memo;

        /*if (msg.length() > 30 && msg.length() <= 60) {
            int i = msg.substring(25, 35).indexOf(" ") + 25;
            if (i == -1) {
                msg = msg.substring(0, 30) + "\n" + msg.substring(30);
            } else
                msg = msg.substring(0, i) + "\n" + msg.substring(i);
        } else if (msg.length() > 60 && msg.length() <= 90) {
            int firstSpaceIndex = msg.indexOf(" ", 25);
            int secondSpaceIndex = msg.substring(55, 65).indexOf(" ") + 55;
            //System.out.println("DEBUG FIRST: " + firstSpaceIndex);
            //System.out.println("DEBUG SECOND: " + secondSpaceIndex);
            if (secondSpaceIndex == -1) {
                if (firstSpaceIndex == -1) {
                    msg = msg.substring(0, 30) + "\n" + msg.substring(30, 60) + "\n" + msg.substring(60);
                } else {
                    msg = msg.substring(0, firstSpaceIndex) + "\n" + msg.substring(firstSpaceIndex, 60) + "\n" + msg.substring(60);
                }
            } else if (firstSpaceIndex == -1) {
                msg = msg.substring(0, 30) + "\n" + msg.substring(30, secondSpaceIndex) + msg.substring(secondSpaceIndex);
            } else {
                msg = msg.substring(0, firstSpaceIndex) + "\n" + msg.substring(firstSpaceIndex, secondSpaceIndex) + "\n" + msg.substring(secondSpaceIndex);
                //System.out.println("DEBUG MSG: " + msg);
            }*/


        int numberOfLines = msg.length() / 30;
        Array<String> msgLines = new Array<String>();
        System.out.println("Numero di caratteri:  " + msg.length());
        System.out.println("Numero di righe:  " + (numberOfLines + 1));
        for (int i = 0; i < numberOfLines; i++) {
            int spaceIndex = msg.substring(25, 35).indexOf(" ") + 25;
            System.out.println("Spazio indice:  " + spaceIndex);
            if (spaceIndex == -1) {
                msgLines.add(msg.substring(0, 30));
                msg = msg.substring(30);
            } else {
                msgLines.add(msg.substring(0, spaceIndex));
                msg = msg.substring(spaceIndex);
            }
            System.out.println("Stringa sottratta: " + msgLines.get(i));
            System.out.println("___________________________");
        }
        msgLines.add(msg);
        msg = "";

        for (int j = 1; j < msgLines.size ; j++) {
            if (msgLines.get(j).startsWith(" ")) {
                System.out.println("Stringa in entrata : " + msgLines.get(j));
                msgLines.set(j, "\n" + msgLines.get(j).substring(1));
                System.out.println("Stringa  in uscita : " + msgLines.get(j));
            } else {
                System.out.println("Stringa in entrata : " + msgLines.get(j));
                msgLines.set(j, "\n" + msgLines.get(j));
                System.out.println("Stringa  in uscita : " + msgLines.get(j));
            }
        }


        for (String s : msgLines)
            System.out.print(s);

        for (String s : msgLines)
            msg = msg.concat(s);

        if (messageFromOpponent) {
            cell = new TextButton(msg, skin, "chatButtonRight");
        } else {
            cell = new TextButton(msg, skin, "chatButtonLeft");
        }
        message = msg;
        this.messageFromOpponent = messageFromOpponent;
        // cell.setWidth(500);

    }

    public Button getCell() {
        return cell;
    }

    public void setCell(TextButton cell) {
        this.cell = cell;
    }

    public MemoGameOnline getMemo() {
        return memo;
    }

    public void setMemo(MemoGameOnline memo) {
        this.memo = memo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMessageFromOpponent() {
        return messageFromOpponent;
    }

    public void setMessageFromOpponent(boolean messageFromOpponent) {
        this.messageFromOpponent = messageFromOpponent;
    }
}
