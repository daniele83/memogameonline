package com.memogameonline.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gametable.Classic;

public class ScrollingBackground {

	private Vector2 position;
	private Vector2 velocity;
	private String difficulty;
	GamePreferences prefs;
	private final int VERTICAL_SPEED_SCROLLING = 10;
	private final int HORIZONTAL_SPEED_SCROLLING = 15;

	public ScrollingBackground() {

		position = new Vector2(0, 0);
		prefs = GamePreferences.instance;
		/*if (difficulty.equals("Kid")) {
			velocity = new Vector2(-HORIZONTAL_SPEED_SCROLLING, 0);
		} else
			velocity = new Vector2(0, -VERTICAL_SPEED_SCROLLING);*/

	}

	public void update(float delta) {
		

	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

}
