package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;

/**
 * Created by danie_000 on 03/02/2016.
 */
public class ProfilePictureCell {

    private Button whiteCell;
    private Image image;
    private Label label;

    public ProfilePictureCell(MemoGameOnline memo, int i) {
        whiteCell = new Button(memo.patchedSkin, "SquareWhiteCell");
        image = AvatarManager.instance.getSmallAvatar(i);
        whiteCell.add(image);

    }

    public Button getWhiteCell() {
        return whiteCell;
    }

    public void setWhiteCell(Button whiteCell) {
        this.whiteCell = whiteCell;
    }

    public Label getLabel() {

        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void removeImage(){

        whiteCell.removeActor(image);
    }
}
