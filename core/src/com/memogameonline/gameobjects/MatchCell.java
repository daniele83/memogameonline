package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

public class MatchCell extends Actor {

    private Long matchID;
    private Opponent opponent;
    private Button button;
    private Label playerInfo;
    private Label matchStatus;
    private Label playerInfoPressedButton;
    private Label matchStatusPressedButton;
    private Image opponentAvatar;
    private Label opponentLevel;
    private MemoGameOnline memo;

    public MatchCell(final MemoGameOnline memo, long matchID, Opponent opponent, Skin skin,
                     String styleName, String playerInfo, String matchStatus) {
        this.memo = memo;
        this.matchID = matchID;
        this.opponent = opponent;
        button = new Button(skin, styleName);
        if (opponent.getImageID() != 48) {
            opponentAvatar = AvatarManager.instance.getSmallAvatar(opponent.getImageID());
            opponentAvatar.setPosition(15, 10);
            button.addActor(opponentAvatar);
        } else {
            opponentAvatar = opponent.getFacebookPicture();
            opponentAvatar.setBounds(24, 17, 55, 55);
            Image pictureFrame = new Image(memo.patchedSkin, "pictureFrame");
            pictureFrame.setBounds(22, 15, 59, 59);
            button.addActor(opponentAvatar);
            button.addActor(pictureFrame);
        }

        if (opponent.getLevel() < 10) {
            opponentLevel = new Label("" + opponent.getLevel(), skin,
                    "30white");
            opponentLevel.setPosition(113, 25);
        } else {
            opponentLevel = new Label(""
                    + opponent.getLevel(), skin,
                    "24white");
            opponentLevel.setPosition(109, 26);
        }


        button.addActor(opponentLevel);

        button.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                switchLabelToPressedButton();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                switchLabelToUnpressedButton();
            }
        });


        if (User.data.getMatch(matchID).isReadyToDiscoverResult()) {

            this.playerInfo = new Label(playerInfo,
                    memo.patchedSkin, "45blue");

            this.playerInfoPressedButton = new Label(playerInfo,
                    memo.patchedSkin, "45white");


            this.matchStatus = new Label(matchStatus,
                    memo.patchedSkin, "36beige");

            this.matchStatusPressedButton = new Label(
                    matchStatus, memo.patchedSkin, "36white");

            this.playerInfo.setPosition(150, 16);

            this.playerInfoPressedButton.setPosition(155, 16);
            playerInfoPressedButton.setVisible(false);

        } else {
            this.playerInfo = new Label(playerInfo,
                    memo.patchedSkin, "36blue");

            this.matchStatus = new Label(matchStatus,
                    memo.patchedSkin, "36beige");


            this.playerInfoPressedButton = new Label(playerInfo,
                    memo.patchedSkin, "36white");
            this.matchStatusPressedButton = new Label(
                    matchStatus, memo.patchedSkin, "36white");

            this.playerInfo.setPosition(150, 40);
            this.matchStatus.setPosition(150, 7);

            this.playerInfoPressedButton.setPosition(150, 40);
            this.matchStatusPressedButton.setPosition(150, 7);
            playerInfoPressedButton.setVisible(false);
            matchStatusPressedButton.setVisible(false);

        }

        button.addActor(this.playerInfo);
        button.addActor(this.matchStatus);
        button.addActor(this.playerInfoPressedButton);
        button.addActor(this.matchStatusPressedButton);
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button cell) {
        this.button = cell;
    }

    public Label getPlayerInfo() {
        return playerInfo;
    }

    public void setPlayerInfo(Label playerInfo) {
        this.playerInfo = playerInfo;
        button.addActor(playerInfo);

    }

    public Label getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(Label matchStatus) {
        this.matchStatus = matchStatus;
        button.addActor(matchStatus);
    }

    public Label getPlayerInfoPressedButton() {
        return playerInfoPressedButton;
    }

    public void setPlayerInfoPressedButton(Label playerInfoPressedButton) {
        this.playerInfoPressedButton = playerInfoPressedButton;
        this.playerInfoPressedButton.setVisible(false);
        button.addActor(playerInfoPressedButton);
    }

    public Label getMatchStatusPressedButton() {
        return matchStatusPressedButton;
    }

    public void setMatchStatusPressedButton(Label matchStatusPressedButton) {
        this.matchStatusPressedButton = matchStatusPressedButton;
        this.matchStatusPressedButton.setVisible(false);
        button.addActor(matchStatusPressedButton);
    }

    public void switchLabelToPressedButton() {
        this.playerInfo.setVisible(false);
        this.matchStatus.setVisible(false);
        this.playerInfoPressedButton.setVisible(true);
        this.matchStatusPressedButton.setVisible(true);
    }

    public void switchLabelToUnpressedButton() {
        this.playerInfo.setVisible(true);
        this.matchStatus.setVisible(true);
        this.playerInfoPressedButton.setVisible(false);
        this.matchStatusPressedButton.setVisible(false);
    }

    public Opponent getOpponent() {
        return opponent;
    }
}
