package com.memogameonline.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.utils.Array;

public class ParticleEffect_MemoFiendsCardSelection {

	private ParticleEffect starsEffect;
	private ParticleEffectPool starsPool;

	public ParticleEffect_MemoFiendsCardSelection() {

		starsEffect = new ParticleEffect();
		starsEffect.load(Gdx.files.internal("particle/MemoFiendsCardSelection.pfx"),
				Gdx.files.internal("particle"));
		starsPool=new ParticleEffectPool(starsEffect,1,5);
	}

	public PooledEffect getEffect() {
		return starsPool.obtain();
	}

	public ParticleEffect getStarsEffect() {
		return starsEffect;
	}

	public void setStarsEffect(ParticleEffect starsEffect) {
		this.starsEffect = starsEffect;
	}

	public ParticleEffectPool getStarsPool() {
		return starsPool;
	}

	public void setStarsPool(ParticleEffectPool starsPool) {
		this.starsPool = starsPool;
	}

	public void dispose(){

		starsEffect.dispose();


	}


}
