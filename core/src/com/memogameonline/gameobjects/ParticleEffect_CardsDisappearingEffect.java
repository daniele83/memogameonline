package com.memogameonline.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.utils.Array;

public class ParticleEffect_CardsDisappearingEffect {

	private ParticleEffect starsEffect;
	private ParticleEffectPool starsPool;

	public ParticleEffect_CardsDisappearingEffect() {

		starsEffect = new ParticleEffect();
		starsEffect.load(Gdx.files.internal("particle/disappearingEffect.pfx"),
				Gdx.files.internal("particle"));
		starsPool=new ParticleEffectPool(starsEffect,2,4);
	}

	public PooledEffect getEffect() {
		return starsPool.obtain();
	}

	public void setStarsPool(ParticleEffectPool starsPool) {
		this.starsPool = starsPool;
	}

	public void dispose(){

		starsEffect.dispose();


	}

}
