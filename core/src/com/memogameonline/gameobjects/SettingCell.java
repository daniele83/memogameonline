package com.memogameonline.gameobjects;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import com.memogameonline.gamehelpers.AvatarManager;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

public class SettingCell extends Actor {
    private Button whiteCell;
    private Label name;
    private Label legend;
    private Label namePressedButton;
    private Label legendPressedButton;
    private boolean draggedActived;

    public SettingCell(MemoGameOnline memo, String name, String legend) {
        whiteCell = new Button(memo.patchedSkin, "settingsBigCell");

        this.name = new Label(name, memo.patchedSkin, "32black");
        whiteCell.addActor(this.name);
        this.name.setPosition(30, 45);
        this.legend = new Label(legend, memo.patchedSkin, "26beige");
        whiteCell.addActor(this.legend);
        this.legend.setPosition(30, 20);

        this.namePressedButton = new Label(name, memo.patchedSkin, "32white");
        this.namePressedButton.setVisible(false);
        whiteCell.addActor(this.namePressedButton);
        this.namePressedButton.setPosition(30, 45);
        this.legendPressedButton = new Label(legend, memo.patchedSkin, "26white");
        this.legendPressedButton.setVisible(false);
        whiteCell.addActor(this.legendPressedButton);
        this.legendPressedButton.setPosition(30, 20);

        whiteCell.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                switchLabelToPressedButton();

                draggedActived = false;
                return true;
            }

            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                switchLabelToUnpressedButton();
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (x > 11 && x < 508 && y > 11 && y < 90)
                    switchLabelToPressedButton();
                else switchLabelToUnpressedButton();

            }
        });
    }

    public Button getWhiteCell() {
        return whiteCell;
    }

    public Label getCellName() {
        return name;
    }

    public Label getCellNamePressed() {
        return this.namePressedButton;
    }

    public void setNameX(float x) {

        name.setX(x);
    }

    public void setLegendX(float x) {

        legend.setX(x);
    }

    public void setNamePressedX(float x) {

        namePressedButton.setX(x);
    }

    public void setLegendPressedX(float x) {

        legendPressedButton.setX(x);
    }

    public void switchLabelToPressedButton() {
        this.name.setVisible(false);
        this.legend.setVisible(false);
        this.namePressedButton.setVisible(true);
        this.legendPressedButton.setVisible(true);
    }

    public void switchLabelToUnpressedButton() {
        this.name.setVisible(true);
        this.legend.setVisible(true);
        this.namePressedButton.setVisible(false);
        this.legendPressedButton.setVisible(false);
    }
}
