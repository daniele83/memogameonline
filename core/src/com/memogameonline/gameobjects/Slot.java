package com.memogameonline.gameobjects;

import com.badlogic.gdx.math.Vector2;

public class Slot {

	private int gridPosition;
	private Vector2 velocity;
	private Vector2 acceleration;
	private Vector2 start;
	private Vector2 end;
	private float distance;

	private Card card;
	private Slot[] neighbors;
	private Vector2 position;

	private boolean limitedOnTop;
	private boolean limitedOnTopRight;
	private boolean limitedOnTopLeft;
	private boolean limitedOnRight;
	private boolean limitedOnLeft;
	private boolean limitedOnBottom;
	private boolean limitedOnBottomRight;
	private boolean limitedOnBottomLeft;

	private boolean checkedAsFreeSlot;

	private boolean empty;

	private boolean moving;
	private boolean accelerationSet;

	public Slot(int gridPosition, Vector2 pos) {
		neighbors = new Slot[8];
		this.gridPosition = gridPosition;

		position = pos;
		start = new Vector2(0, 0);
		end = new Vector2(0, 0);
		distance = 0;
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		limitedOnBottomLeft = false;
		limitedOnBottom = false;
		limitedOnBottomRight = false;
		limitedOnRight = false;
		limitedOnTopRight = false;
		limitedOnTop = false;
		limitedOnTopLeft = false;
		limitedOnLeft = false;

		checkedAsFreeSlot = false;

		empty = true;

		moving = false;
		accelerationSet = false;
	}

	public void update(float delta) {

		position.add(velocity.cpy().scl(delta));
		velocity.add(acceleration.cpy().scl(delta));

	}

	public void addCard(Card card) {

		this.card = card;
		empty = false;
	}

	public Card getCard() {
		return card;
	}

	public Vector2 getPosition() {
		return position;
	}

	public boolean isLimitedOnTop() {
		return limitedOnTop;
	}

	public boolean isLimitedOnRight() {
		return limitedOnRight;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public boolean isMoving() {
		return moving;
	}

	public void setVelocity(float x, float y) {
		this.velocity.x = x;
		this.velocity.y = y;
	}

	public void setAcceleration(float x, float y) {
		this.acceleration.x = x;
		this.acceleration.y = y;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public Vector2 getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(Vector2 acceleration) {
		this.acceleration = acceleration;
	}

	public Vector2 getStart() {
		return start;
	}

	public void setStart(Vector2 start) {
		this.start = start;
	}

	public Vector2 getEnd() {
		return end;
	}

	public boolean isAccelerationSet() {
		return accelerationSet;
	}

	public void setAccelerationSet(boolean accelerationSet) {
		this.accelerationSet = accelerationSet;
	}

	public void setEnd(Vector2 end) {
		this.end = end;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public Vector2 getMiddlePoint() {

		return new Vector2(position.x + 58, position.y + 58);
	}

	public int getGridPosition() {
		return gridPosition;
	}

	public void setGridPosition(int gridPosition) {
		this.gridPosition = gridPosition;
	}

	public void setNeighbors(Slot[] neighbors) {
		this.neighbors = neighbors;
	}

	public Slot[] getNeighbors() {
		return neighbors;
	}

	public boolean isLimitedOnTopRight() {
		return limitedOnTopRight;
	}

	public boolean isLimitedOnTopLeft() {
		return limitedOnTopLeft;
	}

	public boolean isLimitedOnBottomRight() {
		return limitedOnBottomRight;
	}

	public boolean isLimitedOnBottomLeft() {
		return limitedOnBottomLeft;
	}

	public void setLimitedOnTopRight(boolean limitedOnTopRight) {
		this.limitedOnTopRight = limitedOnTopRight;
	}

	public void setLimitedOnTopLeft(boolean limitedOnTopLeft) {
		this.limitedOnTopLeft = limitedOnTopLeft;
	}

	public void setLimitedOnBottomRight(boolean limitedOnBottomRight) {
		this.limitedOnBottomRight = limitedOnBottomRight;
	}

	public void setLimitedOnBottomLeft(boolean limitedOnBottomLeft) {
		this.limitedOnBottomLeft = limitedOnBottomLeft;
	}

	public boolean isCheckedAsFreeSlot() {
		return checkedAsFreeSlot;
	}

	public void setCheckedAsFreeSlot(boolean checkedAsFreeSlot) {
		this.checkedAsFreeSlot = checkedAsFreeSlot;
	}

	public boolean isLimitedOnLeft() {
		return limitedOnLeft;
	}

	public boolean isLimitedOnBottom() {
		return limitedOnBottom;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public void deleteCard() {
		card = null;
		empty = true;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public void setLimitedOnTop(boolean limitedOnTop) {
		this.limitedOnTop = limitedOnTop;
	}

	public void setLimitedOnRight(boolean limitedOnRight) {
		this.limitedOnRight = limitedOnRight;
	}

	public void setLimitedOnLeft(boolean limitedOnLeft) {
		this.limitedOnLeft = limitedOnLeft;
	}

	public void setLimitedOnBottom(boolean limitedOnBottom) {
		this.limitedOnBottom = limitedOnBottom;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public float getPositionX() {

		return position.x;
	}

	public float getPositionY() {

		return position.y;
	}

}
