package com.memogameonline.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.utils.Array;

public class ParticleEffect_StarsSquareEffect {

	private ParticleEffect starsEffect;
	private ParticleEffectPool starsPool;

	public ParticleEffect_StarsSquareEffect() {

		starsEffect = new ParticleEffect();
		starsEffect.load(Gdx.files.internal("particle/starsEffect.pfx"),
				Gdx.files.internal("particle"));
		starsPool=new ParticleEffectPool(starsEffect,1,5);
	}

	public ParticleEffectPool getStarsPool() {
		return starsPool;
	}

	public void setStarsPool(ParticleEffectPool starsPool) {
		this.starsPool = starsPool;
	}

	public void dispose(){

		starsEffect.dispose();


	}


}
