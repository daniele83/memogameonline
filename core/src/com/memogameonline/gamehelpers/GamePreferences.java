package com.memogameonline.gamehelpers;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.memogameonline.user.Match;

public class GamePreferences {
	public static final String TAG = GamePreferences.class.getName();
	public static final GamePreferences instance = new GamePreferences();

	public String lastLoggedUser;
	public boolean autoLogIn;
	public boolean autoLogInFacebook;
	public boolean audio;
	public boolean enableNotifications;
	public boolean enableSoundNotifications;
	public boolean enableVibrationNotifications;
	public boolean facebookConnection;

	private Preferences prefs;

	// singleton
	private GamePreferences() {
		prefs = Gdx.app.getPreferences(Constants.PREFERENCES);
	}

	public void load() {

		lastLoggedUser = prefs.getString("lastLoggedUser", "");
		autoLogIn = prefs.getBoolean("autoLogIn", false);
		autoLogInFacebook = prefs.getBoolean("autoLogInFacebook", false);
		audio = prefs.getBoolean("audio", true);
		enableNotifications=prefs.getBoolean("enableNotifications",true);
		enableSoundNotifications=prefs.getBoolean("enableSoundNotifications",true);
		enableVibrationNotifications=prefs.getBoolean("enableVibrationNotifications",true);
		facebookConnection=prefs.getBoolean("facebookConnection", true);

		prefs.flush();

	}

	public void save() {
		prefs.putString("lastLoggedUser", lastLoggedUser);
		prefs.putBoolean("autoLogIn", autoLogIn);
		prefs.putBoolean("autoLogInFacebook", autoLogInFacebook);
		prefs.putBoolean("audio", audio);
		prefs.putBoolean("enableNotifications", enableNotifications);
		prefs.putBoolean("enableSoundNotifications", enableSoundNotifications);
		prefs.putBoolean("enableVibrationNotifications", enableVibrationNotifications);
		prefs.putBoolean("facebookConnection", facebookConnection);

		prefs.flush();
	}

	public void reset() {

		prefs.clear();
		prefs.flush();
		prefs.clear();
		this.load();
		
	}

}
