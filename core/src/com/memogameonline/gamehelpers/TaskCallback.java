package com.memogameonline.gamehelpers;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;

import java.io.IOException;


public class TaskCallback {

    private MemoGameOnline memo;
    private Screen screen;
    private MOCDataCarrier mocdc;

    private boolean calledToLogIn;
    private boolean calledToChallengeFriend;
    private boolean calledToSuggestAppToFriends;

    protected final float duration = 0.4f;
    protected final Interpolation interpolation = Interpolation.pow5Out;

    public TaskCallback(MemoGameOnline memo, MOCDataCarrier mocdc, Screen screen) {
        this.memo = memo;
        this.screen = screen;
        this.mocdc = mocdc;
    }

    public void onSuccess(String id, String name, String[] friendsList, String url, final Object obj) {
        if (calledToLogIn) {
            System.out.println("CALLBACK: LOG  IN SUCCESS!!!!!");
            System.out.println("name (from JSONObject): " + name);
            System.out.println("userID (from JSONObject): " + id);
            System.out.println("pictureURL (from JSONObject): " + url);
            User.data.facebookID = id;

            User.data.comingFromFacebookLogIn = true;
            System.out
                    .println("TASK CALL BACK: Setto il client COMING FROM LOG IN!!!!");

            User.data.facebookPictureURL=url;

            System.out
                    .println("Salvato l'url della foto!!!!");


        } else if (calledToChallengeFriend) {
            System.out.println("CALLBACK: FRIENDS LIST REQUEST SUCCESS!!!!!");
            System.out.println("Dimensione della lsita: " + friendsList.length);
            for (int i = 0; i < friendsList.length; i += 3) {
                System.out.println("name (from JSONObject): " + friendsList[i]
                        + " (i =" + i);
                System.out.println("userID (from JSONObject): "
                        + friendsList[i + 1] + " (i =" + (i + 1));
                System.out.println("iamgeURLID (from JSONObject): "
                        + friendsList[i + 2] + " (i =" + (i + 2));
                System.out.println("-");
                Opponent oppo= new Opponent();
                oppo.setFacebookName(friendsList[i]);
                oppo.setFacebookID(friendsList[i + 1]);
                oppo.setFacebookPictureURL(friendsList[i + 2]);

                User.data.facebookFriendsList.add(oppo);

            }
            User.data.comingFromFacebookFriendsListRequest = true;
            System.out
                    .println("TASK CALL BACK: Setto il client COMING FROM FRIENDS REQUEST!!!!");
            //screen.resume();
        }
    }

    public void onCancel() {
        System.out.println("CALLBACK: LOG  IN CANCEL!!!!!");
    }

    public void onError() {
        System.out.println("CALLBACK: LOG  IN ERROR!!!!!");
    }

    public boolean isCalledToLogIn() {
        return calledToLogIn;
    }

    public void setCalledToLogIn(boolean calledToLogIn) {
        this.calledToLogIn = calledToLogIn;
    }

    public boolean isCalledToChallengeFriend() {
        return calledToChallengeFriend;
    }

    public void setCalledToChallengeFriend(boolean calledToChallengeFriend) {
        this.calledToChallengeFriend = calledToChallengeFriend;
    }

    public boolean isCalledToSuggestAppToFriends() {
        return calledToSuggestAppToFriends;
    }

    public void setCalledToSuggestAppToFriends(
            boolean calledToSuggestAppToFriends) {
        this.calledToSuggestAppToFriends = calledToSuggestAppToFriends;
    }

}
