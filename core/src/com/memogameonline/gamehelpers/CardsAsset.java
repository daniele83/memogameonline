package com.memogameonline.gamehelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.Disposable;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class CardsAsset implements Disposable, AssetErrorListener {
    public static final String TAG = CardsAsset.class.getName();
    public static final CardsAsset instance = new CardsAsset();
    private AssetManager assetManager;

    private TextureAtlas deckAtlas;
    private TextureAtlas gameScreenAtlas;

    public AtlasRegion[] cards;
    public AtlasRegion background;
    public AtlasRegion topBar;
    public AtlasRegion scoreContainer;
    public AtlasRegion experienceBarContainer;
    public AtlasRegion experienceBarSeparetor;
    public AtlasRegion levelIcon;
    public AtlasRegion timeContainer;
    public AtlasRegion cardBackBlue;
    public AtlasRegion cardBackGreen;
    public AtlasRegion cardBackRed;
    public AtlasRegion cardBackRedDouble;
    public AtlasRegion cardBackRedTime;
    public AtlasRegion cardBackRedFaceUp;
    public AtlasRegion cardBackRedStop;
    public AtlasRegion cardBackRedChain;
    public AtlasRegion cardBackRedBonus;

    public AtlasRegion banner;
    public AtlasRegion bonusTime;
    public AtlasRegion[] initialCountdown;

    public AssetFonts fonts;
    public AssetSounds sounds;

    private boolean completedLoading;

    public class AssetFonts {

        public final BitmapFont zighia24;
        public final BitmapFont zighia28;
        public final BitmapFont zighia30;
        public final BitmapFont zighia32;
        public final BitmapFont zighia36;
        public final BitmapFont zighia37;
        public final BitmapFont zighia44;
        public final BitmapFont zighia45;
        public final BitmapFont zighia48;

        public AssetFonts() {

            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(
                    Gdx.files.internal(Constants.ZIGHIA));

            FreeTypeFontParameter parameter24 = new FreeTypeFontParameter();
            parameter24.size = 24;
            zighia24 = generator.generateFont(parameter24);

            FreeTypeFontParameter parameter28 = new FreeTypeFontParameter();
            parameter28.size = 28;
            zighia28 = generator.generateFont(parameter28);

            FreeTypeFontParameter parameter30 = new FreeTypeFontParameter();
            parameter30.size = 30;
            zighia30 = generator.generateFont(parameter30);

            FreeTypeFontParameter parameter32 = new FreeTypeFontParameter();
            parameter32.size = 32;
            zighia32 = generator.generateFont(parameter32);

            FreeTypeFontParameter parameter36 = new FreeTypeFontParameter();
            parameter36.size = 36;
            zighia36 = generator.generateFont(parameter36);

            FreeTypeFontParameter parameter37 = new FreeTypeFontParameter();
            parameter37.size = 37;
            zighia37 = generator.generateFont(parameter37);

            FreeTypeFontParameter parameter44 = new FreeTypeFontParameter();
            parameter44.size = 44;
            zighia44 = generator.generateFont(parameter44);

            FreeTypeFontParameter parameter45 = new FreeTypeFontParameter();
            parameter45.size = 45;
            zighia45 = generator.generateFont(parameter45);

            FreeTypeFontParameter parameter48 = new FreeTypeFontParameter();
            parameter48.size = 48;
            zighia48 = generator.generateFont(parameter48);

            generator.dispose();

        }
    }

    private CardsAsset() {
    }

    public class AssetSounds {
        public final Sound flipCard;
        public final Sound pressedButton;
        public final Sound matchingCards;
        public final Sound gameWin;
        public final Sound gameOver;
        public final Sound initialCountdown;
        public final Sound finalCountdown;
        public final Sound scoreCount;
        public final Sound chainCard;
        public final Sound errorChaining;
        public final Sound pill;

        public AssetSounds(AssetManager am) {

            pressedButton = am.get("sound/button_pressed.mp3", Sound.class);
            flipCard = am.get("sound/card_flip.mp3", Sound.class);
            matchingCards = am.get("sound/success_matching.mp3", Sound.class);
            gameWin = am.get("sound/game_win.mp3", Sound.class);
            gameOver = am.get("sound/game_over.mp3", Sound.class);
            initialCountdown = am.get("sound/initial_countdown.mp3",
                    Sound.class);
            finalCountdown = am.get("sound/final_countdown.mp3", Sound.class);
            scoreCount = am.get("sound/score_count.mp3", Sound.class);
            chainCard = am.get("sound/chain-card.mp3", Sound.class);
            errorChaining = am.get("sound/error-sound.mp3", Sound.class);
            pill = am.get("sound/pill.mp3", Sound.class);

        }
    }

    public void load(AssetManager assetManager) {

        this.assetManager = assetManager;
        completedLoading = false;
        assetManager.setErrorListener(this);
        assetManager.load(Constants.CARDS, TextureAtlas.class);
        assetManager.load(Constants.GAME_SCREEN_ATLAS, TextureAtlas.class);
        assetManager.finishLoading();
        System.out.println("# of assets loaded: "
                + assetManager.getAssetNames().size);
        for (String a : assetManager.getAssetNames())
            System.out.println("asset: " + a);

        deckAtlas = assetManager.get(Constants.CARDS);
        gameScreenAtlas = assetManager.get(Constants.GAME_SCREEN_ATLAS);
        for (Texture t : deckAtlas.getTextures())
            t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        for (Texture t : gameScreenAtlas.getTextures())
            t.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        loadAnimalsDeck();
        loadSportsDeck();
        loadFoodDeck();
        loadTransportDeck();
        loadWhiteCard();
        loadBackground();
        loadBar();
        loadCardBack();
        loadCardBackPowerUp();
        loadExperienceBarStuff();
        loadLevelIcon();
        loadInitialCountdown();
        loadSounds();
        loadMusic();

        fonts = new AssetFonts();
        sounds = new AssetSounds(this.assetManager);

        completedLoading = true;

    }

    private void loadAnimalsDeck() {

        cards = new AtlasRegion[1000];

        cards[0] = deckAtlas.findRegion("cane");
        cards[1] = deckAtlas.findRegion("coniglio");
        cards[2] = deckAtlas.findRegion("delfino");
        cards[3] = deckAtlas.findRegion("elefante");
        cards[4] = deckAtlas.findRegion("gallo");
        cards[5] = deckAtlas.findRegion("gatto");
        cards[6] = deckAtlas.findRegion("giraffa");
        cards[7] = deckAtlas.findRegion("gufo");
        cards[8] = deckAtlas.findRegion("pipistrello");
        cards[9] = deckAtlas.findRegion("riccio");
        cards[10] = deckAtlas.findRegion("scimmia");
        cards[11] = deckAtlas.findRegion("serpente");

    }

    private void loadSportsDeck() {

        cards[12] = deckAtlas.findRegion("arco");
        cards[13] = deckAtlas.findRegion("baseball");
        cards[14] = deckAtlas.findRegion("basket");
        cards[15] = deckAtlas.findRegion("bowling");
        cards[16] = deckAtlas.findRegion("box");
        cards[17] = deckAtlas.findRegion("calcio");
        cards[18] = deckAtlas.findRegion("golf");
        cards[19] = deckAtlas.findRegion("pingpong");
        cards[20] = deckAtlas.findRegion("rugby");
        cards[21] = deckAtlas.findRegion("sci");
        cards[22] = deckAtlas.findRegion("tennis");
        cards[23] = deckAtlas.findRegion("volley");

    }

    private void loadFoodDeck() {

        cards[24] = deckAtlas.findRegion("bibita");
        cards[25] = deckAtlas.findRegion("caffe");
        cards[26] = deckAtlas.findRegion("cioccolata");
        cards[27] = deckAtlas.findRegion("cupcake");
        cards[28] = deckAtlas.findRegion("frutta");
        cards[29] = deckAtlas.findRegion("gelato");
        cards[30] = deckAtlas.findRegion("hamburger");
        cards[31] = deckAtlas.findRegion("hot-dog");
        cards[32] = deckAtlas.findRegion("latte");
        cards[33] = deckAtlas.findRegion("patatine");
        cards[34] = deckAtlas.findRegion("pizza");
        cards[35] = deckAtlas.findRegion("vino");

    }

    private void loadTransportDeck() {

        cards[36] = deckAtlas.findRegion("aereo");
        cards[37] = deckAtlas.findRegion("auto");
        cards[38] = deckAtlas.findRegion("bicicletta");
        cards[39] = deckAtlas.findRegion("bus");
        cards[40] = deckAtlas.findRegion("canoa");
        cards[41] = deckAtlas.findRegion("mongolfiera");
        cards[42] = deckAtlas.findRegion("moto");
        cards[43] = deckAtlas.findRegion("nave");
        cards[44] = deckAtlas.findRegion("razzo");
        cards[45] = deckAtlas.findRegion("slitta");
        cards[46] = deckAtlas.findRegion("sottomarino");
        cards[47] = deckAtlas.findRegion("treno");

    }

    private void loadWhiteCard() {

        cards[999] = deckAtlas.findRegion("whiteCard");
    }

    private void loadBar() {

        topBar = gameScreenAtlas.findRegion("topBar");
        scoreContainer = gameScreenAtlas.findRegion("scoreContainer");
        experienceBarContainer = gameScreenAtlas
                .findRegion("	experienceBarContainer");
        experienceBarSeparetor = gameScreenAtlas
                .findRegion("experienceBarSeparetor");
        timeContainer = gameScreenAtlas.findRegion("timeContainer");

    }

    private void loadBackground() {

        background = gameScreenAtlas.findRegion("background");

    }

    private void loadExperienceBarStuff() {
        experienceBarContainer = gameScreenAtlas
                .findRegion("experienceBarContainer");
        experienceBarSeparetor = gameScreenAtlas
                .findRegion("experienceBarSeparetor");

    }

    private void loadLevelIcon() {

        levelIcon = gameScreenAtlas.findRegion("levelIcon");
    }

    private void loadCardBack() {

        cardBackBlue = gameScreenAtlas.findRegion("cardBackBlue");
        cardBackGreen = gameScreenAtlas.findRegion("cardBackGreen");
        cardBackRed = gameScreenAtlas.findRegion("cardBackRed");

    }

    private void loadCardBackPowerUp() {

        cardBackRedTime = gameScreenAtlas.findRegion("cardBackRedTime");
        cardBackRedDouble = gameScreenAtlas.findRegion("cardBackRedDouble");
        cardBackRedFaceUp = gameScreenAtlas.findRegion("cardBackRedFaceUp");
        cardBackRedStop = gameScreenAtlas.findRegion("cardBackRedStop");
        cardBackRedBonus = gameScreenAtlas.findRegion("cardBackRedBonus");
        cardBackRedChain = gameScreenAtlas.findRegion("cardBackRedChain");
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        fonts.zighia24.dispose();
        fonts.zighia28.dispose();
        fonts.zighia36.dispose();

    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        // TODO Auto-generated method stub

    }

    public void loadBanner() {
        banner = gameScreenAtlas.findRegion("banner");
    }

    public void loadInitialCountdown() {

        initialCountdown = new AtlasRegion[3];
        initialCountdown[0] = gameScreenAtlas.findRegion("uno");
        initialCountdown[1] = gameScreenAtlas.findRegion("due");
        initialCountdown[2] = gameScreenAtlas.findRegion("tre");

    }

    public void loadSounds() {

        assetManager.load("sound/card_flip.mp3", Sound.class);
        assetManager.load("sound/button_pressed.mp3", Sound.class);
        assetManager.load("sound/success_matching.mp3", Sound.class);
        assetManager.load("sound/game_win.mp3", Sound.class);
        assetManager.load("sound/game_over.mp3", Sound.class);
        assetManager.load("sound/initial_countdown.mp3", Sound.class);
        assetManager.load("sound/final_countdown.mp3", Sound.class);
        assetManager.load("sound/score_count.mp3", Sound.class);
        assetManager.load("sound/chain-card.mp3", Sound.class);
        assetManager.load("sound/error-sound.mp3", Sound.class);
        assetManager.load("sound/pill.mp3", Sound.class);

        assetManager.finishLoading();

    }

    public void loadMusic() {

        assetManager.load("music/menu-music.mp3", Music.class);
        assetManager.finishLoading();
    }

    public boolean isCompletedLoading() {
        return completedLoading;
    }

    public void setCompletedLoading(boolean completedLoading) {
        this.completedLoading = completedLoading;
    }

}
