package com.memogameonline.gamehelpers;

import java.io.PrintWriter;

import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.memogameonline.main.MemoGameOnline;

//CMD 2: RandomOpponentFound
//CMD 3: OpponentRefusedChallenge
//CMD 4: InfoRoundReceived
//CMD 5: CloseMatchReceived
//CMD 6: GiveUpMatchFromOtherPlayer
//CMD 7: GiveUpMatchFromHeroServerConfirmation
//CMD 8: ByuPowerUpServerConfirmation
//CMD 11: NewChallengeReceived
//CMD 12: NewChallengeFromFriend
//CMD 21: SignUpSuccess
//CMD 22: SignUpFailed
//CMD 23: SignInSuccess
//CMD 24: SignInFailed (Wrong Credentials)
//CMD 25: SignInFailed (User Already Signed In)
//CMD 26: SignUp with Facebook
//CMD 27: signIn with Facebook
//CMD 41: PlayerFound (search player  in MemoFriends View)
//CMD 42: PlayerNotFound (search player  in MemoFriends View)

//Da80 a 99 ----->Sync
//CMD 80: Sync Match
//CMD 98: Restart Sync
//CMD 99: Sync Completed

//CMD 101: receive chat Message

/**
 * <<<<<<< HEAD cmdToExecute 0 not set 1 random opponent not found 2 random
 * opponent found ======= cmdToExecute 0 not set 1 random opponent not found 2
 * random opponent found 3 clint send round end >>>>>>>
 * refs/remotes/origin/master
 */
public class MOCDataCarrier {

    private MemoGameOnline memo;
    private Json json;
    private PrintWriter pw;

    private Array<Long> ackedMessageID = new Array<Long>();
    public String inputRcv;
    public Array<Integer> cmdToExecute = new Array<Integer>();
    public Array<String[]> dataToExecute = new Array<String[]>();
    public String[] data = new String[30];
    public String[] ackMessage = {"9993", null};
    public String[] voidMessage = {""};
    public Socket socket;

    public int randomOpponentNotFound = 0; // 0 not set ; 1 found ; 2 not found
    public String[] randomOpponentFound;

    public MOCDataCarrier(MemoGameOnline memo) {
        this.memo = memo;
        this.socket = memo.socket;
        json = new Json();
        // br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        pw = new PrintWriter(socket.getOutputStream(), true);

    }

    public String[] readInput(String[] inputArray) {

        if (inputArray[0].equals("9991")) {
            //Synch Msg
            inputRcv = inputArray[0];
            memo.synchronizer.synchronizinMessageReceived();
        } else {
            ackMessage[1] = inputArray[0];
            this.sendToServer(ackMessage);
            if (!ackedMessageID.contains(Long.valueOf(inputArray[0]), true)) {
                ackedMessageID.add(Long.valueOf(inputArray[0]));
                inputRcv = inputArray[1];
                //Evito di stampare  il messaggio di sincronizzazione!!!!
                if (!inputRcv.equals("9991")) {
                    System.out.println(" ____");
                    System.out.println("|MSG_ID: " + Long.valueOf(inputArray[0]));
                    System.out.print("|INPUT: " + inputRcv + " ----> CMD To Execute: ");
                }

                switch (inputRcv) {
                    //Sign UP FAILED (Name Already Exists)
                    case "0101":
                        signUpFailedNameAlreadyExists();
                        break;
                    // SIGN UP SUCCESS
                    case "0103":
                        signUpSuccess();
                        break;
                    // SIGN IN SUCCESS
                    case "0106":
                        singInSuccess(inputArray);
                        break;
                    // SIGN IN FAILED (Wrong Credentials)
                    case "0107":
                        signInFailedWrongCredentials();
                        break;
                    // SIGN IN FAILED (ALREADY SIGNED IN)
                    case "0108":
                        signUpFailedAlreadySignedIn();
                        break;
                    // SIGN UP with Facebook
                    case "0112":
                        signUpFacebook();
                        break;
                    // SIGN IN with Facebook
                    case "0111":
                        signInFacebookSuccess(inputArray);
                        break;
                    // INVIA NUOVa sfida
                    case "0206":
                        newRandomChallenge(inputArray);
                        break;
                    // RANDOM OPPONENT NOT FOUND
                    case "0211":
                        randomOpponentNotFound();
                        break;
                    // AVVERSARIO CASUALE TROVATO
                    case "0210":
                        opponentFound(inputArray);
                        break;
                    // L'amico ha rifiutato la sfida
                    case "0304":
                        friendRefusedChallenge(inputArray);
                        break;
                    // Un amico ti ha sfidato
                    case "0306":
                        newChallengeFromFriend(inputArray);
                        break;
                    // partita da sincronizzare
                    case "0801":
                        synchronizeMatch(inputArray);
                        break;
                    // restartSync
                    case "0887":
                        restartSync();
                        break;
                    // fineSync
                    case "0888":
                        syncCompleted(inputArray);
                        break;
                    // Il player cercato è stato trovato e aggiunto alla lsita
                    case "0901":
                        playerSearchedForHasBeenFound(inputArray);
                        break;
                    // il player cercato NON è stato trovato
                    case "0902":
                        playerSearchedForHasNotBeenFound();
                        break;
                    // GIVE UP MATCH SERVER CONFIRMATION
                    case "1001":
                        giveUpMatchServerConfirmation(inputArray);
                        break;
                    case "1003":
                        buyPowerUpServerConfirmation(inputArray);
                        break;
                    // GIVE UP MATCH
                    case "1002":
                        opponentGaveUpTheMatch(inputArray);
                        break;
                    // INFO ROUND END RECEIVED
                    case "1006":
                        roundEndRcv(inputArray);
                        break;
                    // CHECK ON COINS ON CLOSED MATCH
                    case "1008":
                        closeMatchRcv(inputArray);
                        break;
                    // CHECK ON COINS ON CLOSED MATCH
                    case "2001":
                        receiveMessageFormOpponent(inputArray);
                        break;

                }
            } else {
                System.out.println("Messaggio già processato! scarto!");
            }
        }

        //è
       /* if (inputRcv.equals("0101")) {
            signUpFailedNameAlreadyExists();
        } else if (inputRcv.equals("0103")) {
            signUpSuccess();
        } else if (inputRcv.equals("0106")) {
            singInSuccess();
        } else if (inputRcv.equals("0107")) {
            signInFailedWrongCredentials();
        } else if (inputRcv.equals("0108")) {
            signUpFailedAlreadySignedIn();
        } else if (inputRcv.equals("0112")) {
            signUpFacebook();
        } else if (inputRcv.equals("0111")) {
            signInFacebookSuccess(inputArray);
        } else if (inputRcv.equals("0211")) {
            randomOpponentNotFound();
        } else if (inputRcv.equals("0210")) {
            opponentFound(inputArray);
        } else if (inputRcv.equals("0206")) {
            newRandomChallenge(inputArray);
        } else if (inputRcv.equals("0304")) {
            friendRefusedChallenge(inputArray);
        } else if (inputRcv.equals("0306")) {
            newChallengeFromFriend(inputArray);
        } else if (inputRcv.equals("0901")) {
            playerSearchedForHasBeenFound(inputArray);
        } else if (inputRcv.equals("0902")) {
            playerSearchedForHasNotBeenFound();
        } else if (inputRcv.equals("1001")) {
            giveUpMatchServerConfirmation(inputArray);
        } else if (inputRcv.equals("1002")) {
            opponentGaveUpTheMatch(inputArray);
        } else if (inputRcv.equals("1006")) {
            roundEndRcv(inputArray);
        } else if (inputRcv.equals("1008")) {
            closeMatchRcv(inputArray);
        }*/

        return null;
    }

    private void signUpSuccess() {

        System.out.println(21 + " -----> SingUp SUCCESS");
        System.out.println("|____");
        this.cmdToExecute.add(21);
        this.dataToExecute.add(this.voidMessage);
    }

    private void signUpFailedNameAlreadyExists() {
        System.out.println(22 + " -----> SingUp Failed, user name ALREADY exists");
        System.out.println("|____");
        this.cmdToExecute.add(22);
        this.dataToExecute.add(this.voidMessage);

    }

    private void singInSuccess(String[] inputArray) {
        System.out.println(23 + " -----> SingIn SUCCESS");
        this.data[0] = inputArray[2]; // facebookPictureURL
        this.data[1] = inputArray[3]; // level
        this.data[2] = inputArray[4]; // experience
        this.data[3] = inputArray[5]; // image
        this.data[4] = inputArray[6]; // matchesPlayed
        this.data[5] = inputArray[7]; // matchesWon
        this.data[6] = inputArray[8]; // consecutiveMatchesWon
        this.data[7] = inputArray[9]; // highestMatchScore
        this.data[8] = inputArray[10]; // firstRoundHighestScore
        this.data[9] = inputArray[11]; // firstRoundbestTime
        this.data[10] = inputArray[12]; // secondRoundHighestScore
        this.data[11] = inputArray[13]; // secondRoundbestTime
        this.data[12] = inputArray[14]; // thirdRoundHighestScore
        this.data[13] = inputArray[15]; // thirdRoundbestTime
        this.data[14] = inputArray[16]; // Time
        this.data[15] = inputArray[17]; // DoubleCouple
        this.data[16] = inputArray[18]; // FaceUp
        this.data[17] = inputArray[19]; // Stop
        this.data[18] = inputArray[20]; // Chain
        this.data[19] = inputArray[21]; // Bonus
        this.data[20] = inputArray[22]; // Pills

        String[] description = {"FacebookPicturURL", "Level", "Experience", "ImageID", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime", "PA_Time", "PA_DoubleCouple", "PA_FaceUp",
                "PA_Stop", "PA_Chain", "PA_Bonus","Pills"};
        String[] strToExecute = {this.data[0], this.data[1], this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13], this.data[14], this.data[15], this.data[16], this.data[17], this.data[18], this.data[19],this.data[20]};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(23);

    }

    private void signInFailedWrongCredentials() {
        System.out.println(24 + " -----> SingIn Failed, WRONG user name/password");
        System.out.println("|____");
        this.cmdToExecute.add(24);
        this.dataToExecute.add(this.voidMessage);

    }

    private void signUpFailedAlreadySignedIn() {

        System.out.println(25 + " -----> SingIn Failed, user ALREADY signed in");
        System.out.println("|____");
        this.cmdToExecute.add(25);
        this.dataToExecute.add(this.voidMessage);

    }

    private void signUpFacebook() {
        System.out.println(26 + " -----> Id Facebook isn't in DB, choose a user name and SingUp");
        System.out.println("|____");
        this.cmdToExecute.add(26);
        this.dataToExecute.add(this.voidMessage);

    }

    private void signInFacebookSuccess(String[] inputArray) {
        System.out.println(27 + " -----> SingIn SUCCESS (FACEBOOK)");
        this.data[0] = inputArray[2]; // username
        this.data[1] = inputArray[3]; // facebookPictureURL
        this.data[2] = inputArray[4]; // level
        this.data[3] = inputArray[5]; // experience
        this.data[4] = inputArray[6]; // image
        this.data[5] = inputArray[7]; // matchesPlayed
        this.data[6] = inputArray[8]; // matchesWon
        this.data[7] = inputArray[9]; // consecutiveMatchesWon
        this.data[8] = inputArray[10]; // highestMatchScore
        this.data[9] = inputArray[11]; // firstRoundHighestScore
        this.data[10] = inputArray[12]; // firstRoundbestTime
        this.data[11] = inputArray[13]; // secondRoundHighestScore
        this.data[12] = inputArray[14]; // secondRoundbestTime
        this.data[13] = inputArray[15]; // thirdRoundHighestScore
        this.data[14] = inputArray[16]; // thirdRoundbestTime
        this.data[15] = inputArray[17]; // Time
        this.data[16] = inputArray[18]; // DoubleCouple
        this.data[17] = inputArray[19]; // FaceUp
        this.data[18] = inputArray[20]; // Stop
        this.data[19] = inputArray[21]; // Chain
        this.data[20] = inputArray[22]; // Bonus
        this.data[21] = inputArray[23]; // Pills

        String[] description = {"Username", "FacebookPictureURL", "Level", "Experience", "ImageID", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime", "PA_Time", "PA_DoubleCouple", "PA_FaceUp",
                "PA_Stop", "PA_Chain", "PA_Bonus","Pills"};
        String[] strToExecute = {this.data[0], this.data[1], this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13], this.data[14],this.data[15],this.data[16],this.data[17],this.data[18],this.data[19],this.data[20],
                this.data[21]};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(27);

    }

    private void randomOpponentNotFound() {
        this.cmdToExecute.add(1);
        System.out.println("|____");
        this.dataToExecute.add(this.voidMessage);
    }

    private void opponentFound(String[] inputArray) {
        System.out.println(2 + " -----> OPPONENT Found, match created");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // username
        this.data[2] = inputArray[4]; // level
        this.data[3] = inputArray[5]; // image
        this.data[4] = inputArray[6]; // facebookPictureURL
        this.data[5] = inputArray[7]; // matchesPlayed
        this.data[6] = inputArray[8]; // matchesWon
        this.data[7] = inputArray[9]; // consecutiveMatchesWon
        this.data[8] = inputArray[10]; // highestMatchScore
        this.data[9] = inputArray[11]; // firstRoundHighestScore
        this.data[10] = inputArray[12]; // firstRoundbestTime
        this.data[11] = inputArray[13]; // secondRoundHighestScore
        this.data[12] = inputArray[14]; // secondRoundbestTime
        this.data[13] = inputArray[15]; // thirdRoundHighestScore
        this.data[14] = inputArray[16]; // thirdRoundbestTime
        String[] strToExecute = {this.data[0], this.data[1],
                this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13], this.data[14]};
        String[] description = {"MatchID", "UserName", "Level", "ImageID", "FacebookPictureURL", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime",};

        printCmdStringToExecuteStack(strToExecute, description);

        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(2);
    }

    private void roundEndRcv(String[] inputArray) {
        System.out.println(4 + " -----> Round end SCORE received");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // exp
        String[] strToExecute = {this.data[0], this.data[1]};
        String[] description = {"MatchID", "Score"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(4);

    }

    private void closeMatchRcv(String[] inputArray) {
        System.out.println(5 + " -----> COINS Check for a COMPLETED match");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // coins
        String[] strToExecute = {this.data[0], this.data[1]};
        String[] description = {"MatchID", "Coins"};
        printCmdStringToExecuteStack(strToExecute, description);
        //TODO CHECK!!!
        //this.dataToExecute.add(strToExecute);
        //this.cmdToExecute.add(5);
    }

    private void opponentGaveUpTheMatch(String[] inputArray) {
        System.out.println(6 + " -----> Opponent GAVE UP the match, match will be closed");
        this.data[0] = inputArray[2]; // idGame
        String[] strToExecute = {this.data[0]};
        String[] description = {"MatchID"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(6);

    }

    private void giveUpMatchServerConfirmation(String[] inputArray) {
        System.out.println(7 + " -----> Server CONFIRM match has been closed ( because Hero GAVE UP)");
        this.data[0] = inputArray[2]; // idGame
        String[] strToExecute = {this.data[0]};
        String[] description = {"MatchID"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(7);
    }

    private void buyPowerUpServerConfirmation(String[] inputArray) {
        System.out.println(8 + " -----> Server CONFIRM power up has been buyed");
        this.data[0] = inputArray[2]; // pills cost
        String[] strToExecute = {this.data[0]};
        String[] description = {"PillsCost"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(8);
    }

    private void newRandomChallenge(String[] inputArray) {
        System.out.println(11 + " -----> Received request for a RANDOM CHALLENGE");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // username
        this.data[2] = inputArray[4]; // level
        this.data[3] = inputArray[5]; // image
        this.data[4] = inputArray[6]; // facebookPictureURL
        this.data[5] = inputArray[7]; // matchesPlayed
        this.data[6] = inputArray[8]; // matchesWon
        this.data[7] = inputArray[9]; // consecutiveMatchesWon
        this.data[8] = inputArray[10]; // highestMatchScore
        this.data[9] = inputArray[11]; // firstRoundHighestScore
        this.data[10] = inputArray[12]; // firstRoundbestTime
        this.data[11] = inputArray[13]; // secondRoundHighestScore
        this.data[12] = inputArray[14]; // secondRoundbestTime
        this.data[13] = inputArray[15]; // thirdRoundHighestScore
        this.data[14] = inputArray[16]; // thirdRoundbestTime
        String[] strToExecute = {this.data[0], this.data[1],
                this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13], this.data[14]};
        String[] description = {"MatchID", "UserName", "Level", "ImageID", "FacebookPictureURL", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime",};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(11);
    }

    private void newChallengeFromFriend(String[] inputArray) {
        System.out.println(12 + " -----> Received request from a FRIEND for a CHALLENGE");
        for (int i = 0; i < inputArray.length; i++)
            System.out.println(i + " --> " + inputArray[i]);
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // username
        this.data[2] = inputArray[4]; // level
        this.data[3] = inputArray[5]; // image
        this.data[4] = inputArray[6]; // facebookPictureURL
        this.data[5] = inputArray[7]; // matchesPlayed
        this.data[6] = inputArray[8]; // matchesWon
        this.data[7] = inputArray[9]; // consecutiveMatchesWon
        this.data[8] = inputArray[10]; // highestMatchScore
        this.data[9] = inputArray[11]; // firstRoundHighestScore
        this.data[10] = inputArray[12]; // firstRoundbestTime
        this.data[11] = inputArray[13]; // secondRoundHighestScore
        this.data[12] = inputArray[14]; // secondRoundbestTime
        this.data[13] = inputArray[15]; // thirdRoundHighestScore
        this.data[14] = inputArray[16]; // thirdRoundbestTime
        String[] strToExecute = {this.data[0], this.data[1],
                this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13], this.data[14]};
        String[] description = {"MatchID", "UserName", "Level", "ImageID", "FacebookPictureURL", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime",};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(12);
    }

    private void friendRefusedChallenge(String[] inputArray) {
        System.out.println(3 + " -----> A Friend REFUSED your challenge");
        this.data[0] = inputArray[2]; // opponentID
        String[] strToExecute = {this.data[0]};
        String[] description = {"UserName"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(3);
    }

    private void playerSearchedForHasBeenFound(String[] inputArray) {
        System.out.println(41 + " -----> PLAYER SEARCHED for has been found, Hero can add him to Friend List");
        this.data[0] = inputArray[2]; // username
        this.data[1] = inputArray[3]; // level
        this.data[2] = inputArray[4]; // image
        this.data[3] = inputArray[5]; // facebookPictureURL
        this.data[4] = inputArray[6]; // matchesPlayed
        this.data[5] = inputArray[7]; // matchesWon
        this.data[6] = inputArray[8]; // consecutiveMatchesWon
        this.data[7] = inputArray[9]; // highestMatchScore
        this.data[8] = inputArray[10]; // firstRoundHighestScore
        this.data[9] = inputArray[11]; // firstRoundbestTime
        this.data[10] = inputArray[12]; // secondRoundHighestScore
        this.data[11] = inputArray[13]; // secondRoundbestTime
        this.data[12] = inputArray[14]; // thirdRoundHighestScore
        this.data[13] = inputArray[15]; // thirdRoundbestTime
        String[] strToExecute = {this.data[0], this.data[1],
                this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7], this.data[8], this.data[9],
                this.data[10], this.data[11], this.data[12], this.data[13]};
        String[] description = {"UserName", "Level", "ImageID", "FacebookPictureURL", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime",};
        System.out.println("DEBUG:   " + strToExecute.length);
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(41);
    }

    private void playerSearchedForHasNotBeenFound() {
        System.out.println(42 + " -----> PLAYER SEARCHED for has NOT been found");
        this.cmdToExecute.add(42);
        this.dataToExecute.add(this.voidMessage);
    }

    private void syncCompleted(String[] inputArray) {
        System.out.println(99 + " -----> Synch COMPLETED");
        this.cmdToExecute.add(99);
        this.dataToExecute.add(this.voidMessage);
    }

    private void restartSync() {
        System.out.println(98 + " -----> RESTART Sync");
        this.cmdToExecute.add(98);
        this.dataToExecute.add(this.voidMessage);
    }

    private void synchronizeMatch(String[] inputArray) {
        System.out.println(80 + " -----> Synch a MATCH");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // username
        this.data[2] = inputArray[4]; // level
        this.data[3] = inputArray[5]; // image
        this.data[4] = inputArray[6]; // facebookPictureURL
        this.data[5] = inputArray[7]; // matchesPlayed
        this.data[6] = inputArray[8]; // matchesWon
        this.data[7] = inputArray[9]; // consecutiveMatchesWon
        this.data[8] = inputArray[10]; // highestMatchScore
        this.data[9] = inputArray[11]; // firstRoundHighestScore
        this.data[10] = inputArray[12]; // firstRoundbestTime
        this.data[11] = inputArray[13]; // secondRoundHighestScore
        this.data[12] = inputArray[14]; // secondRoundbestTime
        this.data[13] = inputArray[15]; // thirdRoundHighestScore
        this.data[14] = inputArray[16]; // thirdRoundbestTime
        this.data[15] = inputArray[17]; // heroRound1Score
        this.data[16] = inputArray[18]; // heroRound2Score
        this.data[17] = inputArray[19]; // heroRound3Score
        this.data[18] = inputArray[20]; // oppoRound1Score
        this.data[19] = inputArray[21]; // oppoRound2Score
        this.data[20] = inputArray[22]; // oppoRound3Score
        this.data[21] = inputArray[23]; // playerWhoGaveUp
        this.data[22] = inputArray[24]; // playerReceivedPills
        this.data[23] = inputArray[25]; // chat
        String[] strToExecute = {this.data[0], this.data[1],
                this.data[2], this.data[3], this.data[4], this.data[5], this.data[6], this.data[7],
                this.data[8], this.data[9], this.data[10], this.data[11], this.data[12], this.data[13], this.data[14], this.data[15], this.data[16], this.data[17], this.data[18], this.data[19], this.data[20],
                this.data[21], this.data[22], this.data[23]};
        String[] description = {"MatchID", "UserName", "Level", "ImageID", "FacebookPictureURL", "Played", "Won", "ConsecutiveMatchesWon", "HighestScore", "firstRoundHighestScore",
                "firstRoundbestTime", "secondRoundHighestScore", "secondRoundbestTime", "thirdRoundHighestScore", "thirdRoundbestTime", "HeroRound1Score", "HeroRound2Score"
                , "HeroRound3Score", "OppoRound1Score", "OppoRound2Score", "OppoRound3Score", "playerWhoGaveUp", "playerReceivedPills", "Chat"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(80);
    }


    private void receiveMessageFormOpponent(String[] inputArray) {
        System.out.println(101 + " -----> Receive a Chat Messsage");
        this.data[0] = inputArray[2]; // idGame
        this.data[1] = inputArray[3]; // message
        String[] strToExecute = {this.data[0], this.data[1]};
        String[] description = {"MatchID", "Message"};
        printCmdStringToExecuteStack(strToExecute, description);
        this.dataToExecute.add(strToExecute);
        this.cmdToExecute.add(101);

    }


    private void printCmdStringToExecuteStack(String[] dataToExecute, String[] description) {

        System.out.println("|Dimensioni del DATO: "
                + dataToExecute.length);
        System.out.print("|");
        for (int i = 0; i < dataToExecute.length; i++) {
            System.out.print(description[i] + "         ");
        }
        System.out.print("\n|");
        for (int i = 0; i < dataToExecute.length; i++) {
            if (dataToExecute[i].length() < 20)
                System.out.print(dataToExecute[i]);
            else System.out.print(dataToExecute[i].substring(0, 18));
            for (int j = 0; j < description[i].length() - dataToExecute[i].length(); j++)
                System.out.print(" ");
            System.out.print("         ");
        }
        System.out.println("\n|____");

    }

    public void removeMessage() {
        // System.out.println("_____");
        // System.out.println("|Cancellazione CMD e DATA To Execute, dimensioni rispettive degli array: :"
        // + this.cmdToExecute.size + " (CMD To Execute) e " + this.dataToExecute.size + " (DATA To Execute)");

        // printCmdStringToExecuteStack();
        cmdToExecute.removeIndex(0);
        dataToExecute.removeIndex(0);
        // System.out.println("| -----> CMD Rimosso: " + CMDToDelete);
        // System.out.println("| -----> Nuove dimensioni: "
        // + this.cmdToExecute.size + "  e " + this.dataToExecute.size);
        // System.out.println("|____\n");
    }

    public void sendToServer(String[] msg) {
        pw.println(json.toJson(msg));
        pw.flush();
        //System.out.println(json.toJson(msg));
    }
}
