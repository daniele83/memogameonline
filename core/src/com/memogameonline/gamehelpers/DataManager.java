package com.memogameonline.gamehelpers;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.memogameonline.user.Match;
import com.memogameonline.user.Opponent;
import com.memogameonline.user.User;
import com.memogameonline.user.UserWrapper;

public class DataManager {

    public static final DataManager instance = new DataManager();
    private Json json;
    private FileHandle file;
    private String fileName;

    private DataManager() {

        json = new Json();
    }

    public void saveData() {

        UserWrapper userWrapper = new UserWrapper();
        userWrapper.setUserName(User.data.userName);
        userWrapper.setPassword(User.data.password);
        userWrapper.setEmail(User.data.email);
        userWrapper.setFacebookID(User.data.facebookID);

        userWrapper.setBestFirstRoundTimeOFFLINE(User.data.bestFirstRoundTimeOFFLINE);
        userWrapper.setBestSecondRoundTimeOFFLINE(User.data.bestSecondRoundTimeOFFLINE);
        userWrapper.setHighestFirstRoundScoreOFFLINE(User.data.highestFirstRoundScoreOFFLINE);
        userWrapper.setHighestSecondRoundScoreOFFLINE(User.data.highestSecondRoundScoreOFFLINE);
        userWrapper.setHighestThirdRoundScoreOFFLINE(User.data.highestThirdRoundScoreOFFLINE);

        userWrapper.setMemoFriendsList(User.data.memoFriendsList);
        for (Opponent oppo : userWrapper.getMemoFriendsList())
            oppo.setFacebookPicture(null);
        userWrapper.setChallengedFriendsList(User.data.challengedFriendList);
        userWrapper.setBlockedUsersList(User.data.blockedUsersList);

        fileName = User.data.userName.concat(".sav");
        file = Gdx.files.local(fileName);
        file.writeString(json.toJson(userWrapper), false);
        System.out
                .println("Salvataggio dei dati riuscita! Salvati in: " + file);
        // System.out.println("Ho salvato: " + json.prettyPrint(mw));
    }

    public boolean loadData() {
        fileName = User.data.userName.concat(".sav");
        try {
            file = Gdx.files.local(fileName);
            String s = file.readString();
            UserWrapper userWrapper = json.fromJson(UserWrapper.class, s);
            User.data.userName = userWrapper.getUserName();
            User.data.password = userWrapper.getPassword();
            User.data.email = userWrapper.getEmail();
            User.data.facebookID = userWrapper.getFacebookID();

            User.data.bestFirstRoundTimeOFFLINE = userWrapper.getBestFirstRoundTimeOFFLINE();
            User.data.bestSecondRoundTimeOFFLINE = userWrapper.getBestSecondRoundTimeOFFLINE();
            User.data.highestFirstRoundScoreOFFLINE = userWrapper.getHighestFirstRoundScoreOFFLINE();
            User.data.highestSecondRoundScoreOFFLINE = userWrapper.getHighestSecondRoundScoreOFFLINE();
            User.data.highestThirdRoundScoreOFFLINE = userWrapper.getHighestThirdRoundScoreOFFLINE();

            User.data.memoFriendsList = userWrapper.getMemoFriendsList();
            for (Opponent oppo : User.data.memoFriendsList)
                oppo.setFacebookPicture(null);
            User.data.challengedFriendList = userWrapper.getChallengedFriendsList();
            User.data.blockedUsersList = userWrapper.getBlockedUsersList();

            System.out.println("Caricamento dei dati riuscito! Caricati da: "
                    + file);
            //System.out.println("Ho caricato: " + json.prettyPrint(mw));
            return true;
        } catch (GdxRuntimeException e) {
            e.printStackTrace();
            return false;
        }

    }
}
