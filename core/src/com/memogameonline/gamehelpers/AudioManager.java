package com.memogameonline.gamehelpers;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioManager {

    public static final AudioManager instance = new AudioManager();
    private Music playingMusic;

    private AudioManager() {
    }

    public long play(Sound sound) {
        return play(sound, 1);
    }

    public long play(Sound sound, float volume) {

        return play(sound, volume, 1);
    }

    public long play(Sound sound, float volume, float pitch) {

        return play(sound, volume, pitch, 0);
    }

    public long play(Sound sound, float volume, float pitch, float pan) {
        if (GamePreferences.instance.audio)
            return sound.play(volume, pitch, pan);
        else return 0;
    }

    public void play(Music music) {
        stopMusic();
        playingMusic = music;
        if (true) {
            music.setLooping(true);
            music.play();
        }
    }

    public void stopMusic() {
        if (playingMusic != null) playingMusic.stop();
    }

    public void stopSound(Sound sound) {
        sound.stop();

    }
}
