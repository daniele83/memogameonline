package com.memogameonline.gamehelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.memogameonline.gamescreens.OfflineMode;
import com.memogameonline.gametable.Classic;
import com.memogameonline.gametable.ClassicRenderer;
import com.memogameonline.gametable.Memobile;
import com.badlogic.gdx.Input.Keys;
import com.memogameonline.main.MemoGameOnline;


public class MemobileInputHandler implements InputProcessor {
	public static final String TAG= MemobileInputHandler.class.getName();
	Memobile table;
	MemoGameOnline memo;
	MOCDataCarrier mocdc;

	public MemobileInputHandler (MemoGameOnline memo, MOCDataCarrier mocdc, Memobile table) {
		this.table = table;
		this.memo = memo;
		this.mocdc = mocdc;
	}

	@Override
	public boolean keyDown(int keycode) {        
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if ((keycode == Keys.ESCAPE || keycode == Keys.BACK) && table.isOfflineMode()) {
			AudioManager.instance.stopSound(CardsAsset.instance.sounds.initialCountdown);
			memo.setScreen(new OfflineMode(memo, mocdc));
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		table.selectCard(x,y,false);
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

}
