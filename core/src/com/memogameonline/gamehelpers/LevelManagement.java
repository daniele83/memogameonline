package com.memogameonline.gamehelpers;

import com.memogameonline.user.User;

public class LevelManagement {

	public static LevelManagement instance = new LevelManagement();
	private int levelMin;
	private int[] levelMax;

	private LevelManagement() {

		buildLevels();
	}

	private void buildLevels() {

		levelMin = 0;
		levelMax = new int[1000];
		for (int i = 0; i < 1000; i++) {
			levelMax[i] = 9000 * (i + 1);
		}
	}

	public float getPercentageIncrease(float exp) {
		return (exp / (float) (levelMax[User.data.level - 1] - levelMin));

	}

	public int getCurrentExpRange() {

		//System.out.println(levelMax[0]);
		//System.out.println(levelMax[User.data.level - 1]);
		return levelMax[User.data.level - 1];
	}
}
