package com.memogameonline.gamehelpers;

public class Constants {

	public static final String ZIGHIA="font/MemoGame.ttf";
	
	public static final String CARDS = "cards/cards.atlas";

	public static final String BIG_ICONS_ATLAS = "BigIcons/bigIcons.atlas";
	public static final String BIG_ICONS_SKIN = "BigIcons/BigIconsSkin.json";

	public static final String SMALL_ICONS_ATLAS = "SmallIcons/smallIcons.atlas";
	public static final String SMALL_ICONS_SKIN = "SmallIcons/SmallIconsSkin.json";

	public static final String GENERAL_ASSETS_PATCHED_ATLAS = "GeneralAssetsPatched/GeneralAssets.atlas";
	public static final String GENERAL_ASSETS_PATCHED_JSON = "GeneralAssetsPatched/GeneralAssetsSkin.json";

	public static final String GAME_SCREEN_ATLAS="game screen/game screen.atlas";

	public static final String TEXTURE_ATLAS_UI = "images/memo.atlas";
	public static final String SKIN_MEMO_UI = "images/memo.json";

	public static final String PREFERENCES = "settings";

	public static final int VIRTUAL_VIEWPORT_WIDTH = 540;
	public static final int VIRTUAL_VIEWPORT_HEIGHT = 885;
	
	

}
