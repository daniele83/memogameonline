package com.memogameonline.gamehelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.memogameonline.gamescreens.OfflineMode;
import com.memogameonline.gametable.Classic;
import com.memogameonline.gametable.ClassicRenderer;
import com.memogameonline.gametable.MemoFiends;
import com.badlogic.gdx.Input.Keys;
import com.memogameonline.main.MemoGameOnline;

public class MemoFiendsInputHandler implements InputProcessor {
	public static final String TAG = ClassicInputHandler.class.getName();
	MemoFiends table;
	MemoGameOnline memo;
	MOCDataCarrier mocdc;

	public MemoFiendsInputHandler(MemoGameOnline memo, MOCDataCarrier mocdc, MemoFiends mf) {
		this.table = mf;
		this.memo = memo;
		this.mocdc = mocdc;

	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if ((keycode == Keys.ESCAPE || keycode == Keys.BACK) && table.isOfflineMode()) {
			AudioManager.instance.stopSound(CardsAsset.instance.sounds.initialCountdown);
			memo.setScreen(new OfflineMode(memo, mocdc));
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		table.selectFirstCard(x, y);
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// table.compareChainElements();
		table.faceUpChain();
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		table.addCardToChain(x, y);
		return false;
	}

	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

}
