package com.memogameonline.gamehelpers;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.user.User;

public class AvatarManager {

	public static final String TAG = AvatarManager.class.getName();
	public static final AvatarManager instance = new AvatarManager();

	private Skin bigIconsSkin;
	private Skin smallIconsSkin;

	private AvatarManager() {

	}

	public void load(Skin bigIconsSkin, Skin smallIconsSkin) {
		this.bigIconsSkin=bigIconsSkin;
		this.smallIconsSkin=smallIconsSkin;

	}

	public Image getBigAvatar(int imageID) {

		if (imageID == 0)
			return new Image(bigIconsSkin, "aereo");
		else if (imageID == 1)
			return new Image(bigIconsSkin, "arco");
		else if (imageID == 2)
			return new Image(bigIconsSkin, "auto");
		else if (imageID == 3)
			return new Image(bigIconsSkin, "baseball");
		else if (imageID == 4)
			return new Image(bigIconsSkin, "basket");
		else if (imageID == 5)
			return new Image(bigIconsSkin, "bibita");
		else if (imageID == 6)
			return new Image(bigIconsSkin, "bicicletta");
		else if (imageID == 7)
			return new Image(bigIconsSkin, "bowling");
		else if (imageID == 8)
			return new Image(bigIconsSkin, "box");
		else if (imageID == 9)
			return new Image(bigIconsSkin, "bus");
		else if (imageID == 10)
			return new Image(bigIconsSkin, "caffe");
		else if (imageID == 11)
			return new Image(bigIconsSkin, "calcio");
		else if (imageID == 12)
			return new Image(bigIconsSkin, "cane");
		else if (imageID == 13)
			return new Image(bigIconsSkin, "canoa");
		else if (imageID == 14)
			return new Image(bigIconsSkin, "cioccolata");
		else if (imageID == 15)
			return new Image(bigIconsSkin, "coniglio");
		else if (imageID == 16)
			return new Image(bigIconsSkin, "cupcake");
		else if (imageID == 17)
			return new Image(bigIconsSkin, "delfino");
		else if (imageID == 18)
			return new Image(bigIconsSkin, "elefante");
		else if (imageID == 19)
			return new Image(bigIconsSkin, "frutta");
		else if (imageID == 20)
			return new Image(bigIconsSkin, "gallo");
		else if (imageID == 21)
			return new Image(bigIconsSkin, "gatto");
		else if (imageID == 22)
			return new Image(bigIconsSkin, "gelato");
		else if (imageID == 23)
			return new Image(bigIconsSkin, "giraffa");
		else if (imageID == 24)
			return new Image(bigIconsSkin, "golf");
		else if (imageID == 25)
			return new Image(bigIconsSkin, "gufo");
		else if (imageID == 26)
			return new Image(bigIconsSkin, "hotdog");
		else if (imageID == 27)
			return new Image(bigIconsSkin, "latte");
		else if (imageID == 28)
			return new Image(bigIconsSkin, "mongolfiera");
		else if (imageID == 29)
			return new Image(bigIconsSkin, "nave");
		else if (imageID == 30)
			return new Image(bigIconsSkin, "panino");
		else if (imageID == 31)
			return new Image(bigIconsSkin, "patatine");
		else if (imageID == 32)
			return new Image(bigIconsSkin, "pingpong");
		else if (imageID == 33)
			return new Image(bigIconsSkin, "pipistrello");
		else if (imageID == 34)
			return new Image(bigIconsSkin, "pizza");
		else if (imageID == 35)
			return new Image(bigIconsSkin, "razzo");
		else if (imageID == 36)
			return new Image(bigIconsSkin, "riccio");
		else if (imageID == 37)
			return new Image(bigIconsSkin, "rugby");
		else if (imageID == 38)
			return new Image(bigIconsSkin, "sci");
		else if (imageID == 39)
			return new Image(bigIconsSkin, "scimmia");
		else if (imageID == 40)
			return new Image(bigIconsSkin, "serpente");
		else if (imageID == 41)
			return new Image(bigIconsSkin, "slitta");
		else if (imageID == 42)
			return new Image(bigIconsSkin, "sottomarino");
		else if (imageID == 43)
			return new Image(bigIconsSkin, "tennis");
		else if (imageID == 44)
			return new Image(bigIconsSkin, "treno");
		else if (imageID == 45)
			return new Image(bigIconsSkin, "vespa");
		else if (imageID == 46)
			return new Image(bigIconsSkin, "vino");
		else if (imageID == 47)
			return new Image(bigIconsSkin, "volley");
		else
			return null;
	}

	public Image getSmallAvatar(int imageID) {

		if (imageID == 0)
			return new Image(smallIconsSkin, "aereo");
		else if (imageID == 1)
			return new Image(smallIconsSkin, "arco");
		else if (imageID == 2)
			return new Image(smallIconsSkin, "auto");
		else if (imageID == 3)
			return new Image(smallIconsSkin, "baseball");
		else if (imageID == 4)
			return new Image(smallIconsSkin, "basket");
		else if (imageID == 5)
			return new Image(smallIconsSkin, "bibita");
		else if (imageID == 6)
			return new Image(smallIconsSkin, "bicicletta");
		else if (imageID == 7)
			return new Image(smallIconsSkin, "bowling");
		else if (imageID == 8)
			return new Image(smallIconsSkin, "box");
		else if (imageID == 9)
			return new Image(smallIconsSkin, "bus");
		else if (imageID == 10)
			return new Image(smallIconsSkin, "caffe");
		else if (imageID == 11)
			return new Image(smallIconsSkin, "calcio");
		else if (imageID == 12)
			return new Image(smallIconsSkin, "cane");
		else if (imageID == 13)
			return new Image(smallIconsSkin, "canoa");
		else if (imageID == 14)
			return new Image(smallIconsSkin, "cioccolata");
		else if (imageID == 15)
			return new Image(smallIconsSkin, "coniglio");
		else if (imageID == 16)
			return new Image(smallIconsSkin, "cupcake");
		else if (imageID == 17)
			return new Image(smallIconsSkin, "delfino");
		else if (imageID == 18)
			return new Image(smallIconsSkin, "elefante");
		else if (imageID == 19)
			return new Image(smallIconsSkin, "frutta");
		else if (imageID == 20)
			return new Image(smallIconsSkin, "gallo");
		else if (imageID == 21)
			return new Image(smallIconsSkin, "gatto");
		else if (imageID == 22)
			return new Image(smallIconsSkin, "gelato");
		else if (imageID == 23)
			return new Image(smallIconsSkin, "giraffa");
		else if (imageID == 24)
			return new Image(smallIconsSkin, "golf");
		else if (imageID == 25)
			return new Image(smallIconsSkin, "gufo");
		else if (imageID == 26)
			return new Image(smallIconsSkin, "hotdog");
		else if (imageID == 27)
			return new Image(smallIconsSkin, "latte");
		else if (imageID == 28)
			return new Image(smallIconsSkin, "mongolfiera");
		else if (imageID == 29)
			return new Image(smallIconsSkin, "nave");
		else if (imageID == 30)
			return new Image(smallIconsSkin, "panino");
		else if (imageID == 31)
			return new Image(smallIconsSkin, "patatine");
		else if (imageID == 32)
			return new Image(smallIconsSkin, "pingpong");
		else if (imageID == 33)
			return new Image(smallIconsSkin, "pipistrello");
		else if (imageID == 34)
			return new Image(smallIconsSkin, "pizza");
		else if (imageID == 35)
			return new Image(smallIconsSkin, "razzo");
		else if (imageID == 36)
			return new Image(smallIconsSkin, "riccio");
		else if (imageID == 37)
			return new Image(smallIconsSkin, "rugby");
		else if (imageID == 38)
			return new Image(smallIconsSkin, "sci");
		else if (imageID == 39)
			return new Image(smallIconsSkin, "scimmia");
		else if (imageID == 40)
			return new Image(smallIconsSkin, "serpente");
		else if (imageID == 41)
			return new Image(smallIconsSkin, "slitta");
		else if (imageID == 42)
			return new Image(smallIconsSkin, "sottomarino");
		else if (imageID == 43)
			return new Image(smallIconsSkin, "tennis");
		else if (imageID == 44)
			return new Image(smallIconsSkin, "treno");
		else if (imageID == 45)
			return new Image(smallIconsSkin, "vespa");
		else if (imageID == 46)
			return new Image(smallIconsSkin, "vino");
		else if (imageID == 47)
			return new Image(smallIconsSkin, "volley");
		else
			return null;
	}

}
