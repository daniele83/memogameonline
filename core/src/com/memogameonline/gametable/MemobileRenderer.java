package com.memogameonline.gametable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.LevelManagement;
import com.memogameonline.gameobjects.ExperienceBar;
import com.memogameonline.gameobjects.TimeIcon;
import com.memogameonline.user.User;

public class MemobileRenderer {
    public static final String TAG = ClassicRenderer.class.getName();
    protected Memobile table;

    protected OrthographicCamera cam;

    protected StretchViewport viewport;
    protected SpriteBatch batcher;

    protected BitmapFont zighia24;
    protected BitmapFont zighia28;
    protected BitmapFont zighia36;
    protected BitmapFont zighia37;
    protected BitmapFont zighia48;

    protected final float PLAYER_TIME;

    protected final float COUNTDOWN;

    protected String difficulty;
    protected int cardsNumber;

    protected int framesNumber;
    protected int backFramesNumber;
    protected int faceUpFramesNumber;
    protected int faceUpAfterMatchingFramesNumber;
    protected int faceUpAfterSpecialFramesNumber;

    protected float experienceIncreasingSpeed;
    protected float scoreCounter;
    protected float scoreDecreaser;
    protected int scoreDecreasingSpeed;
    protected float timeCounter;
    protected float timeDecreasingSpeed;
    protected float timeDecreaser;
    protected float countTime;
    protected float runTime;
    protected TimeIcon timeIcon;
    protected float angle;
    protected float experienceBarAngle;
    protected ExperienceBar experienceBar;
    private boolean calculatedTimeBonusScore;
    private boolean levelUp;
    private boolean levelUpResetted;
    private int newScore;
    private int newTimeBonusScore;

    public MemobileRenderer(Memobile table, OrthographicCamera cam,
                            SpriteBatch batcher) {

        this.table = table;

        framesNumber = table.getDeck().getCards().get(1).getNUMBER_FRAMES();
        backFramesNumber = table.getDeck().getCards().get(1)
                .getNUMBER_BACK_FRAMES();
        faceUpFramesNumber = table.getDeck().getCards().get(1)
                .getNUMBER_FACE_FRAMES();
        faceUpAfterMatchingFramesNumber = table.getDeck().getCards().get(1)
                .getNUMBER_FACEUP_REMAINING_TIME_FRAMES();
        faceUpAfterSpecialFramesNumber = table.getDeck().getCards().get(1)
                .getNUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES();

        zighia24 = CardsAsset.instance.fonts.zighia24;
        zighia28 = CardsAsset.instance.fonts.zighia28;
        zighia36 = CardsAsset.instance.fonts.zighia36;
        zighia37 = CardsAsset.instance.fonts.zighia37;
        zighia48 = CardsAsset.instance.fonts.zighia48;

        zighia24.setColor(0f, 0f, 0f, 1f);
        zighia28.setColor(0f, 0f, 0f, 1f);
        zighia36.setColor(0f, 0f, 0f, 1f);

        this.cam = cam;
        this.batcher = batcher;
        this.cam.update();

        cardsNumber = 24;
        PLAYER_TIME = table.getPLAYER_TIME();
        COUNTDOWN = table.getCountdown();

        runTime = 0;
        timeIcon = new TimeIcon();
        angle = 0;
        experienceBar = new ExperienceBar();
        levelUp = false;

    }


    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        runTime += Gdx.graphics.getDeltaTime();

        if (table.isInitialCountdown()) {
            batcher.begin();
            renderBackground();
            renderTopBar();
            renderBack();
            renderScore();
            renderTime();
            renderExperienceBar();
            renderLevelIcon();
            renderInitialCountdown();
            batcher.end();

        } else {
            batcher.begin();
            renderBackground();
            renderTopBar();
            if (table.isGameOver() && table.isExperienceAnimationIsRunning() && !table.isOfflineMode()) {
                if (table.getRemainingTime() > 0
                        || !this.calculatedTimeBonusScore) {
                    renderExperienceAnimationTimeBased(delta);
                } else {
                    if (!levelUpResetted) {
                        levelUp = false;
                        levelUpResetted = true;
                    }
                    renderExperienceAnimationScoreBased(delta);
                }
            }
            renderScore();
            renderTime();
            renderExperienceBar();
            renderLevelIcon();
            renderBack();
            renderFace();
            renderFaceUpAnimation();
            renderFaceDownAnimation();
            renderBonusFaceUp();
            renderBonusDoubleCouple();
            renderDisappearing();
            renderSpecialEffect(delta);
            batcher.end();

            if (table.isGameOver() && table.isContinuePopUpIsShowing()) {
                renderContinuePopUp(delta);
            }

        }

    }

    private void renderBackground() {

        batcher.draw(CardsAsset.instance.background, 0, 0);

    }

    private void renderTopBar() {
        batcher.draw(CardsAsset.instance.topBar, 0, 766);
    }

    private void renderScore() {
       // System.out.println("Renderizzo lo score!");
        if (table.getRenderingScore() < 10)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 266f, 848);
        else if (table.getRenderingScore() > 9
                && table.getRenderingScore() < 99)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 257.5f, 848);
        else if (table.getRenderingScore() > 99
                && table.getRenderingScore() < 999)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 251f, 848);
        else if (table.getRenderingScore() > 999)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 245f, 848);
        else if (table.isGameOver() && table.getRemainingTime() < 0)
            zighia48.draw(batcher, "0", 266f, 848);

    }

    private void renderTime() {
        //System.out.println("Renderizzo il tempo!");
        if (table.getRemainingTime() > 0)
            angle = 360 * (1 - table.getRemainingTime()
                    / table.getPLAYER_TIME());

        if (table.isInitialCountdown()) {
            timeIcon.draw(batcher, 450, 790, 0);
            zighia48.draw(batcher, "" + PLAYER_TIME, 464, 844);
        } else {
            if (table.getRemainingTime() >= 0.1
                    && table.getRemainingTime() < 10) {
                timeIcon.draw(batcher, 450, 790, angle);
                zighia48.draw(batcher,
                        "" + Math.round(table.getRemainingTime() * 10.0f)
                                / 10.0f, 473, 844);
            } else if (table.getRemainingTime() >= 10) {
                timeIcon.draw(batcher, 450, 790, angle);
                zighia48.draw(batcher,
                        "" + Math.round(table.getRemainingTime() * 10.0f)
                                / 10.0f, 464, 844);
            } else {
                zighia48.draw(batcher, "0.0", 472, 844);

            }
            //zighia48.setColor(0, 0, 0, 1);
        }
    }

    private void renderExperienceBar() {
        //System.out.println("Renderizzo la barra degli xp!");
        if (!table.isExperienceAnimationIsRunning() || table.isOfflineMode()) {
            batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
            experienceBar.draw(batcher, 8.3f, 791f,
                    360 - 360 * LevelManagement.instance
                            .getPercentageIncrease(User.data.experience));
        }
        batcher.draw(CardsAsset.instance.experienceBarSeparetor, 8.3f, 790f);
    }

    private void renderLevelIcon() {
       // System.out.println("Renderizzo il livello!");
        if (User.data.level > 9)
            zighia37.draw(batcher, "" + User.data.level, 36.5f, 842);
        else
            zighia48.draw(batcher, "" + User.data.level, 42, 845);//42, 845

    }

    private void renderBack() {
        //System.out.println("Renderizzo il dorso!");
        for (int i = 0; i < cardsNumber; i++) {
            if (table.getDeck().getCards().get(i).isFaceDown())
                if (!table.getDeck().getCards().get(i).isPowerUpped())
                    batcher.draw(CardsAsset.instance.cardBackRed, table
                                    .getDeck().getCards().get(i).getPosition().x, table
                                    .getDeck().getCards().get(i).getPosition().y, 58,
                            0, 116, 116, 1, 1, 0);
                else if (table.getDeck().getCards().get(i).isPowerUpped_stop())
                    batcher.draw(CardsAsset.instance.cardBackRedStop, table
                                    .getDeck().getCards().get(i).getPosition().x, table
                                    .getDeck().getCards().get(i).getPosition().y, 58,
                            0, 116, 116, 1, 1, 0);
                else if (table.getDeck().getCards().get(i).isPowerUpped_time())
                    batcher.draw(CardsAsset.instance.cardBackRedTime, table
                                    .getDeck().getCards().get(i).getPosition().x, table
                                    .getDeck().getCards().get(i).getPosition().y, 58,
                            0, 116, 116, 1, 1, 0);
                else if (table.getDeck().getCards().get(i)
                        .isPowerUpped_faceUp())
                    batcher.draw(CardsAsset.instance.cardBackRedFaceUp, table
                                    .getDeck().getCards().get(i).getPosition().x, table
                                    .getDeck().getCards().get(i).getPosition().y, 58,
                            0, 116, 116, 1, 1, 0);
        }

    }

    private void renderFace() {
        //System.out.println("Renderizzo la faccia!");
        for (int i = 0; i < cardsNumber; i++) {
            if (table.getDeck().getCards().get(i).isFaceUP()) {

                batcher.draw(CardsAsset.instance.cards[table.getDeck()
                        .getCards().get(i).getID()], table.getDeck().getCards()
                        .get(i).getPosition().x, table.getDeck().getCards()
                        .get(i).getPosition().y, 58, 0, 116, 116, 1, 1, 0);
            }
        }

    }

    private void renderFaceUpAnimation() {
       // System.out.println("Renderizzo il face up!");
        for (int i = 0; i < cardsNumber; i++) {

            if (table.getDeck().getCards().get(i).isMovingFaceUp()) {
                if (table.getDeck().getCards().get(i).getIndexAnimation() < this.backFramesNumber) {
                    if (!table.getDeck().getCards().get(i).isPowerUpped())
                        batcher.draw(
                                CardsAsset.instance.cardBackRed,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_stop())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedStop,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_time())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedTime,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_faceUp())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedFaceUp,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);

                } else {

                    batcher.draw(CardsAsset.instance.cards[table.getDeck()
                                    .getCards().get(i).getID()], table.getDeck()
                                    .getCards().get(i).getPosition().x, table.getDeck()
                                    .getCards().get(i).getPosition().y, 58, 0, 116,
                            116, table.getDeck().getCards().get(i)
                                    .getAnimationArray()[table.getDeck()
                                    .getCards().get(i).getIndexAnimation()], 1,
                            0);
                }

                table.getDeck().getCards().get(i).incrementIndexAnimation();
                if ((table.getPlayers().getSecondCardIndex() != -1)) {
                    table.setFastSelection(true);
                    // System.out.println("Abilitato!!!");
                }

                if (table.getDeck().getCards().get(i).getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber
                        + this.faceUpAfterMatchingFramesNumber - 1) {

                    table.getDeck().getCards().get(i).setMovingFaceUp(false);
                    table.getDeck().getCards().get(i).setFaceUP(true);
                    table.getDeck().getCards().get(i).resetIndexAnimation();

                    if (table.getPlayers().getSecondCardIndex() != -1
                            && !table
                            .getDeck()
                            .getCards()
                            .get(table.getPlayers().getFirstCardIndex())
                            .isMovingFaceUp()
                            && !table
                            .getDeck()
                            .getCards()
                            .get(table.getPlayers()
                                    .getSecondCardIndex())
                            .isMovingFaceUp())
                        table.compareSlots();

                }
            }
        }

    }

    private void renderFaceDownAnimation() {
        //System.out.println("Renderizzo il face down!");
        for (int i = 0; i < cardsNumber; i++) {

            if (table.getDeck().getCards().get(i).isMovingFaceDown()) {

                if (table.getDeck().getCards().get(i).getIndexAnimation() < this.faceUpFramesNumber) {
                    batcher.draw(CardsAsset.instance.cards[table.getDeck()
                                    .getCards().get(i).getID()], table.getDeck()
                                    .getCards().get(i).getPosition().x, table.getDeck()
                                    .getCards().get(i).getPosition().y, 58, 0, 116,
                            116, table.getDeck().getCards().get(i)
                                    .getAnimationArray()[this.backFramesNumber
                                    + this.faceUpFramesNumber
                                    - 1
                                    - table.getDeck().getCards().get(i)
                                    .getIndexAnimation()], 1, 0);
                } else {
                    if (!table.getDeck().getCards().get(i).isPowerUpped())
                        batcher.draw(
                                CardsAsset.instance.cardBackRed,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getDeck().getCards().get(i)
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_stop())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedStop,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getDeck().getCards().get(i)
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_time())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedTime,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getDeck().getCards().get(i)
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_faceUp())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedFaceUp,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getDeck().getCards().get(i)
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                }

                table.getDeck().getCards().get(i).incrementIndexAnimation();

                if (table.getDeck().getCards().get(i).getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber - 1) {
                    table.getDeck().getCards().get(i).setMovingFaceDown(false);
                    table.getDeck().getCards().get(i).setFaceDown(true);
                    table.getDeck().getCards().get(i).setSelectable(true);
                    table.getDeck().getCards().get(i).resetIndexAnimation();

                }

            }
        }
    }


    public void resize(int width, int height) {

        // viewport.update(width, height, true);

    }

    public void renderSpecialEffect(float delta) {
        //System.out.println("Renderizzo le stelline speciali!");
        for (int i = table.getSpecialEffects().size - 1; i >= 0; i--) {
            PooledEffect effect = table.getSpecialEffects().get(i);
            effect.draw(batcher, delta);
            if (effect.isComplete()) {
                effect.free();
                table.getSpecialEffects().removeIndex(i);
            }

        }
    }

    public void renderBonusFaceUp() {
        //System.out.println("Renderizzo il bonus face up!");
        for (int i = 0; i < cardsNumber; i++) {

            if (table.getDeck().getCards().get(i).isBonusActived()
                    && !table.getDeck().getCards().get(i).isFaceUP()) {

                if (table.getDeck().getCards().get(i).getIndexAnimation() < this.backFramesNumber) {
                    if (!table.getDeck().getCards().get(i).isPowerUpped())
                        batcher.draw(
                                CardsAsset.instance.cardBackRed,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_stop())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedDouble,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_time())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedTime,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_faceUp())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedFaceUp,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                } else
                    batcher.draw(CardsAsset.instance.cards[table.getDeck()
                                    .getCards().get(i).getID()], table.getDeck()
                                    .getCards().get(i).getPosition().x, table.getDeck()
                                    .getCards().get(i).getPosition().y, 58, 0, 116,
                            116, table.getDeck().getCards().get(i)
                                    .getAnimationArray()[table.getDeck()
                                    .getCards().get(i).getIndexAnimation()], 1,
                            0);

                table.getDeck().getCards().get(i).incrementIndexAnimation();

                if (table.getDeck().getCards().get(i).getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber
                        + this.faceUpAfterSpecialFramesNumber - 1) {

                    table.getDeck().getCards().get(i).setBonusActived(false);
                    table.getDeck().getCards().get(i).setMovingFaceDown(true);
                    table.getDeck().getCards().get(i).setSelectable(true);
                    table.getDeck().getCards().get(i).resetIndexAnimation();

                }
            }

        }
    }

    public void renderBonusDoubleCouple() {
        //System.out.println("Renderizzo il bonus double couple!");
        for (int i = 0; i < cardsNumber; i++) {

            if (table.getDeck().getCards().get(i).isFaceingUpBecauseOfDoubleCouple()) {

                if (table.getDeck().getCards().get(i).getIndexAnimation() < this.backFramesNumber) {
                    if (!table.getDeck().getCards().get(i).isPowerUpped())
                        batcher.draw(
                                CardsAsset.instance.cardBackRed,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_stop())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedDouble,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_time())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedTime,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getDeck().getCards().get(i)
                            .isPowerUpped_faceUp())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedFaceUp,
                                table.getDeck().getCards().get(i).getPosition().x,
                                table.getDeck().getCards().get(i).getPosition().y,
                                58, 0, 116, 116, table.getDeck().getCards()
                                        .get(i).getAnimationArray()[table
                                        .getDeck().getCards().get(i)
                                        .getIndexAnimation()], 1, 0);
                } else
                    batcher.draw(CardsAsset.instance.cards[table.getDeck()
                                    .getCards().get(i).getID()], table.getDeck()
                                    .getCards().get(i).getPosition().x, table.getDeck()
                                    .getCards().get(i).getPosition().y, 58, 0, 116,
                            116, table.getDeck().getCards().get(i)
                                    .getAnimationArray()[table.getDeck()
                                    .getCards().get(i).getIndexAnimation()], 1,
                            0);

                table.getDeck().getCards().get(i).incrementIndexAnimation();

                if (table.getDeck().getCards().get(i).getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber
                        + this.faceUpAfterSpecialFramesNumber - 1) {

                    table.getDeck().getCards().get(i).setFaceingUpBecauseOfDoubleCouple(false);
                    table.getDeck().getCards().get(i).setSelectable(false);
                    table.getDeck().getCards().get(i).setExploded(true);
                    table.getDeck().getCards().get(i).setDisappearing(true);
                    table.getDeck().getCards().get(i).setMatched(true);

                }
            }
        }
    }

    private void renderDisappearing() {
        //System.out.println("Renderizzo l'effetto di sparizione");
        for (int i = 0; i < cardsNumber; i++) {
            if (table.getDeck().getCards().get(i).isDisappearing()) {
                batcher.draw(CardsAsset.instance.cards[table.getDeck()
                                .getCards().get(i).getID()], table.getDeck().getCards()
                                .get(i).getPosition().x, table.getDeck().getCards()
                                .get(i).getPosition().y, 50f, 70f, 116f, 116f,
                        table.getDeck().getCards().get(i)
                                .getDisappearingArray()[table.getDeck()
                                .getCards().get(i).getDisappearingIndex()],
                        table.getDeck().getCards().get(i)
                                .getDisappearingArray()[table.getDeck()
                                .getCards().get(i).getDisappearingIndex()], 0);

                table.getDeck().getCards().get(i).incrementIndexDisappearing();

                if (table.getDeck().getCards().get(i).getIndexDisappearing() == 9) {
                    table.getDeck().getCards().get(i).setDisappearing(false);
                    table.getDeck().getCards().get(i).setFaceUP(true);
                    table.getDeck().getCards().get(i).resetIndexDisappearing();

                    PooledEffect effect = table.getSpecialEffectPool()
                            .getEffect();
                    effect.setPosition(table.getDeck().getCards().get(i)
                            .getPositionX() + 58, table.getDeck().getCards()
                            .get(i).getPositionY() + 58);
                    table.getSpecialEffects().add(effect);

                    table.getDeck().getCards().get(i)
                            .setPosition(new Vector2(-200, 0));
                }
            }

        }

    }

    public void renderInitialCountdown() {
        //System.out.println("Renderizzo il countdown iniziale");
        if (table.getCountdown() > 2.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[2], 82, 350);
        } else if (table.getCountdown() > 1.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[1], 82, 350);
        } else if (table.getCountdown() > 0.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[0], 82, 350);
        }
        // zighia36.draw(batcher, "" + table.getCountdown(), 50, 650);
    }

    public void renderBonusTime() {
        zighia24.setColor(67 / 255.0f, 254 / 255.0f, 51 / 255.0f,
                table.getAlpha());
        zighia24.draw(batcher, "+5", table.getBonusTimePositionX(),
                table.getBonusTimePositionY());
        zighia24.setColor(1, 1, 1, 1f);
    }


    public void renderExperienceAnimationTimeBased(float delta) {
        System.out.println("Renderizzo l'animazione basata sul tempo");
        timeDecreasingSpeed = (float) table.getTimeBonusScore() / 10000;

        experienceIncreasingSpeed = (int) ((5.2f / 10f) * table
                .getTimeBonusScore());

        if (timeCounter * experienceIncreasingSpeed < table.getTimeBonusScore()
                && !levelUp) {
            timeCounter += Gdx.graphics.getDeltaTime();
            timeDecreaser += Gdx.graphics.getDeltaTime();
        } else if (timeCounter * experienceIncreasingSpeed < newTimeBonusScore
                && levelUp) {
            timeCounter += Gdx.graphics.getDeltaTime();
            timeDecreaser += Gdx.graphics.getDeltaTime();
        } else if (!levelUp && !calculatedTimeBonusScore) {
            User.data.experience += (int) (timeCounter * experienceIncreasingSpeed);
            this.calculatedTimeBonusScore = true;
            System.out.println("FINITA!!!!!");
            return;
        } else if (levelUp && !calculatedTimeBonusScore) {
            User.data.experience = (int) (timeCounter * experienceIncreasingSpeed);
            this.calculatedTimeBonusScore = true;
            System.out.println("FINITA!!!!!");
            return;
        }

        if (timeCounter * experienceIncreasingSpeed < table.getTimeBonusScore()
                && experienceBarAngle < 0) {
            levelUp = true;
            User.data.experience = 1;
            User.data.level += 1;
            newTimeBonusScore = table.getTimeBonusScore()
                    - (int) (timeCounter * experienceIncreasingSpeed);
            timeCounter = 0;
        }
        experienceBarAngle = 360 - 360 * (LevelManagement.instance
                .getPercentageIncrease(User.data.experience) + LevelManagement.instance
                .getPercentageIncrease(timeCounter * experienceIncreasingSpeed));

        batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
        experienceBar.draw(batcher, 8.3f, 791f, experienceBarAngle);

        if (table.getRemainingTime() > 0)
            table.setRunTimeRemainingTime(table.getRunTimeRemainingTime()
                    + timeDecreaser * timeDecreasingSpeed);

		/*
         * if (table.getRemainingTime() < 0) { if (!levelUp) {
		 * User.data.experience += table.getTimeBonusScore(); return; } else {
		 * User.data.experience = (int) (timeCounter *
		 * experienceIncreasingSpeed); return; }
		 *
		 * }
		 */

    }

    public void renderExperienceAnimationScoreBased(float delta) {
        System.out.println("Renderizzo l'animazione basata sullo score!");
        scoreDecreasingSpeed = (int) ((3.7f / 10f) * table.getTotalScore());
        if (scoreCounter * scoreDecreasingSpeed < table.getTotalScore()
                && !levelUp) {
            scoreCounter += Gdx.graphics.getDeltaTime();
            scoreDecreaser += Gdx.graphics.getDeltaTime();
        } else if (scoreCounter * scoreDecreasingSpeed < newScore && levelUp) {
            scoreCounter += Gdx.graphics.getDeltaTime();
            scoreDecreaser += Gdx.graphics.getDeltaTime();
        } else if (!levelUp) {
            User.data.experience += (int) (scoreCounter * scoreDecreasingSpeed);
            table.setExperienceAnimationIsRunning(false);
            table.setContinuePopUpShown(true);
            return;
        } else {
            User.data.experience = (int) (scoreCounter * scoreDecreasingSpeed);
            table.setExperienceAnimationIsRunning(false);
            table.setContinuePopUpShown(true);
            return;
        }

        if (scoreCounter * scoreDecreasingSpeed < table.getTotalScore()
                && experienceBarAngle < 0) {
            levelUp = true;
            User.data.experience = 1;
            User.data.level += 1;
            newScore = table.getTotalScore() - (int) scoreCounter
                    * scoreDecreasingSpeed;
            scoreCounter = 0;
        }
        experienceBarAngle = 360 - 360 * (LevelManagement.instance
                .getPercentageIncrease(User.data.experience) + LevelManagement.instance
                .getPercentageIncrease(scoreCounter * scoreDecreasingSpeed));

        batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
        experienceBar.draw(batcher, 8.3f, 791f, experienceBarAngle);

        if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) < 10
                && (table.getTotalScore() - scoreDecreaser
                * scoreDecreasingSpeed) > 0)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 9
                && (table.getTotalScore() - scoreDecreaser
                * scoreDecreasingSpeed) < 99)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 99)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 999)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) < 0)
            table.setRenderingScore(0);

    }

    public void renderContinuePopUp(float delta) {
        table.getContinueSharingPopUp().getStage().act(delta);
        table.getContinueSharingPopUp().getStage().draw();

    }
}