package com.memogameonline.gametable;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;


import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.Card;
import com.memogameonline.gameobjects.ParticleEffect_MemoFiendsCardSelection;
import com.memogameonline.gameobjects.ScrollingBackground;
import com.memogameonline.gameobjects.Slot;
import com.memogameonline.gameobjects.ParticleEffect_StarsSquareEffect;
import com.memogameonline.gamescreens.OfflineMode;
import com.memogameonline.gamescreens.RoundEndPopUp;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.players.ClassicPlayer;
import com.memogameonline.players.MemofiendsPlayer;
import com.memogameonline.user.Opponent;

public class MemoFiends {
    public static final String TAG = Classic.class.getName();

    protected MemoGameOnline memo;
    protected long matchID;
    protected Opponent opponent;

    protected final Pool<Card> cardPool;

    protected RoundEndPopUp continueSharingPopUp;
    protected boolean continuePopUpShown;
    protected boolean continuePopUpIsShowing;
    protected boolean experienceAnimationIsStarted;
    protected boolean experienceAnimationIsRunning;
    protected boolean inputInihibited;

    protected int startGameTime;
    protected int totalScore;
    protected int renderingScore;
    protected int timeBonusScore;
    protected boolean renderSpecial;

    protected OrthographicCamera cam;
    protected Vector3 virtualTouch = new Vector3();

    protected ScrollingBackground background;

    protected boolean enableToCompare;

    protected GamePreferences prefs;
    protected boolean enableSound;
    protected boolean enableMusic;
    protected boolean fastSelection;

    protected boolean gameOver;
    protected boolean gameWin;

    protected int point;

    protected int level;
    protected float PLAYER_TIME;
    protected float remainingTime;
    protected final float COUNTDOWN;
    protected float countdown;
    protected float runTimeCountdown;
    protected float runTimeRemainingTime;

    protected boolean initialCountdownStarted;
    protected boolean initialCountdown;
    protected boolean bonusTime;

    protected MOCDataCarrier mocdc;

    private MemofiendsPlayer player;

    private boolean blockInputHandler;
    private boolean deckFaceUP;
    private int cardsNumber;

    private ParticleEffect_StarsSquareEffect starsEffectPool;
    private Array<PooledEffect> stars;
    private ParticleEffect_MemoFiendsCardSelection cardSelectionEffectPool;
    private Array<PooledEffect> cardSelectorEffect;
    private int bonusPoint;

    private boolean vibrationEnabled;
    private int countMatchedCards;

    private float runTime;
    private float bonusTimer;
    private float alpha;
    private int bonusTimePositionX;
    private int bonusTimePositionY;

    private float distributionCardTime;
    private int indexGeneratedCard;
    private int currentGrid;

    private final int NUMBER_OF_DECK = 10;
    private final int TYPE_OF_CARDS_PER_DECK = 3;
    private final int CARD_SPEED_GOING_OUT = 1000;
    private final int CARD_SPEED_GOING_IN = 1000;
    private final float CARDS_FACING_UP_DURATION = 1.5f;
    private final float CARDS_FACING_UP_DELAY = -1.4f;

    private final int COLUMNS = 4;
    protected int ROWS = 6;

    private Array<Slot> chainSelected;
    private Array<Slot> chainToCompare;

    private Array<Slot> grid;
    private int typeOfCard;
    private int[] lastChain;
    private Random rand;
    private int madeChains;
    private int memoFiendsType;

    private int[] firstPotentialChain;
    private int[] secondPotentialChain;
    private int[] thirdPotentialChain;
    private int[] fourthPotentialChain;

    protected boolean[] powerUp;
    private boolean offlineMode;

    protected Random random = new Random();

    private boolean chainMustBeCompared;

    /*CHAIN_MULTIPLIER:
    3 ---> Full Chain = 63 points
    4 ---> Full Chain = 84 points
    5 ---> Full Chain = 105 points
    6 ---> Full Chain = 126 points

     */
    private final int CHAIN_MULTIPLIER = 4;
    private final int BONUS_TIME = 1;
    private final int BONUS_POINT = CHAIN_MULTIPLIER * 7;

    public MemoFiends(MemoGameOnline memo, MOCDataCarrier mocdc, long matchID, Opponent opponent,
                      int GameMode, float time, boolean[] powerUp, int memoFiendsType, boolean offlineMode) {

        this.memoFiendsType = memoFiendsType;

        this.memo = memo;
        this.mocdc = mocdc;
        this.matchID = matchID;
        this.opponent = opponent;

        cardPool = new Pool<Card>() {
            @Override
            protected Card newObject() {
                return new Card();
            }
        };
        this.powerUp = powerUp;
        this.offlineMode = offlineMode;

        PLAYER_TIME = time;
        remainingTime = time;
        COUNTDOWN = 3;
        runTimeCountdown = 0;
        runTimeRemainingTime = 0;
        cam = new OrthographicCamera();
        cam.setToOrtho(true, Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        virtualTouch = new Vector3();
        gameOver = false;
        gameWin = false;

        player = new MemofiendsPlayer(this);

        distributionCardTime = 0;
        indexGeneratedCard = 0;
        runTime = 0;

        initialCountdownStarted = false;
        initialCountdown = true;
        this.bonusTime = false;
        deckFaceUP = false;

        cardsNumber = 48;
        chainSelected = new Array<Slot>();
        chainToCompare = new Array<Slot>();
        grid = new Array<Slot>();
        rand = new Random();

        initGrid();
        setSlotsLimits();
        setNeighbors();
        fillGrids();
        currentGrid = 0;
        moveGridIntoTheScreen();

        setPowerUpCards();
        blockInputHandler = true;
        starsEffectPool = new ParticleEffect_StarsSquareEffect();
        stars = new Array<PooledEffect>();
        cardSelectionEffectPool = new ParticleEffect_MemoFiendsCardSelection();
        cardSelectorEffect = new Array<PooledEffect>();

    }

    public void update(float delta) {

        // System.out.println(runTime2);
        this.checkDeckRotation();

        for (int i = 0; i < cardsNumber; i++) {
            grid.get(i).update(delta);
            if (grid.get(i).isMoving()
                    && (grid.get(i).getStart().dst(grid.get(i).getPosition()) >= grid
                    .get(i).getDistance())) {
                grid.get(i).setVelocity(0, 0);
                grid.get(i).setAcceleration(0, 0);
                grid.get(i).setPosition(grid.get(i).getEnd());
                grid.get(i).setMoving(false);
            }

            if (grid.get(i).isMoving()
                    && !grid.get(i).isAccelerationSet()
                    && grid.get(i).getEnd().dst(grid.get(i).getPosition()) <= 35) {
                double scalAccel = -1;
                Vector2 startTemp = new Vector2(grid.get(i).getStart().x, grid
                        .get(i).getStart().y);
                Vector2 endTemp = new Vector2(grid.get(i).getEnd().x, grid.get(
                        i).getEnd().y);
                ;
                Vector2 direction = endTemp.sub(startTemp).nor().scl(100);
                Vector2 accel = new Vector2((float) (direction.x * (-1)),
                        (float) (direction.y * (-1)));
                grid.get(i).setVelocity(direction);
                grid.get(i).setAcceleration(accel);
                grid.get(i).setAccelerationSet(true);

            }

        }

        if (!initialCountdownStarted) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.initialCountdown);
            initialCountdownStarted = true;
        }

        if (initialCountdown) {
            refreshCountdown();
        }

        if (!initialCountdown && !gameOver) {
            refreshRemainingTime();
            runTime += Gdx.graphics.getDeltaTime();
        }

        if (bonusTime) {
            bonusTimer += Gdx.graphics.getDeltaTime();
            alpha = 1 - bonusTimer / 2;
            bonusTimePositionX = 390 + Math.round((bonusTimer / 2) * 50);
            bonusTimePositionY = 950 - Math.round((bonusTimer / 2) * 50);
            if (bonusTimer > 2) {
                bonusTime = false;
                bonusTimer = 0;
                bonusTimePositionX = 390;
                bonusTimePositionY = 950;
                alpha = 1;
            }
        }

        if (gameOver && !experienceAnimationIsRunning
                && !experienceAnimationIsStarted) {
            // if (remainingTime < 0) {
            experienceAnimationIsRunning = true;
            experienceAnimationIsStarted = true;
            AudioManager.instance.play(CardsAsset.instance.sounds.scoreCount);
            // }

        }

        if (gameOver && continuePopUpShown && !continuePopUpIsShowing) {
            System.out.println("POP UP!!!!!!!!!!!!!!!!!!!!!!!!!!");
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            continuePopUpShown = false;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
        }

        if (chainMustBeCompared) {
            boolean compareChain = true;
            for (Slot slot : chainSelected) {
                if (!slot.getCard().isFaceUP())
                    compareChain = false;
            }
            if (compareChain)
                this.compareChainElements();
        }

        if (gameOver && !continuePopUpShown && offlineMode) {
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            continuePopUpShown = true;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
            System.out.println("MOSTRO OFFLINE!!!");
        }
    }

    public void faceUpChain() {
        chainMustBeCompared = true;

        for (int i = cardSelectorEffect.size - 1; i >= 0; i--) {
            PooledEffect effect = cardSelectorEffect.get(i);
            effect.free();
            cardSelectorEffect.removeIndex(i);
        }

        chainToCompare = new Array<Slot>(chainSelected);
        chainSelected.clear();
        if (chainToCompare.size >= 3) {

            for (Slot slot : chainToCompare) {
                slot.getCard().setSelected(false);
                slot.getCard().setMovingFaceUp(true);
                slot.getCard().setFaceDown(false);
            }
        }

    }

    /**
     * Metodo invocato durante il setting di ClassicPlayer.slotSecond
     *
     * @throws InterruptedException
     */
    public boolean compareChainElements() {
        // System.out
        // .println("Entro in compare Elements!!!!!!!!!!!!!!!!!!!!!!!!!!");
        // System.out.println("Dimensioni della catena: " + chain.size + "\n");
        chainMustBeCompared = false;
        if (chainToCompare.size == 1) {
            chainToCompare.get(0).getCard().setSelected(false);
            chainToCompare.clear();
            return false;
        } else if (chainToCompare.size == 2) {
            chainToCompare.get(0).getCard().setSelected(false);
            chainToCompare.get(1).getCard().setSelected(false);
            chainToCompare.clear();
            return false;
        } else if (chainToCompare.size == 0) {
            return false;
        }

        int indexA = 0;
        int indexB = 1;
        int tempPoints = 0;

        tempPoints++;
        for (int j = 0; j < chainToCompare.size - 1; j++) {
            /*
             * System.out.println("Entro nel cilo FOR per la volta: " + (j +
			 * 1));
			 */
            if (chainToCompare.get(indexA).getCard().getID() == chainToCompare
                    .get(indexB).getCard().getID()
                    || chainToCompare.get(indexA).getCard()
                    .isPowerUpped_chain()
                    || chainToCompare.get(indexB).getCard()
                    .isPowerUpped_chain()) {
                // System.out.println("Ciclo numero: " + indexB);
                // System.out.println(indexA);
                // System.out.println(indexB + "\n");
                tempPoints++;
                chainToCompare.get(indexA).getCard().setChainElement(true);
                chainToCompare.get(indexB).getCard().setChainElement(true);
                indexA += 1;
                indexB += 1;

            } else if (indexA > 0 && chainToCompare.get(indexA).getCard().isPowerUpped_chain()
                    && chainToCompare.get(indexA).getCard().getID() == chainToCompare.get(indexA - 1).getCard().getID()
                    || indexA > 1
                    && indexA < chainToCompare.size - 2
                    && chainToCompare.get(indexA).getCard()
                    .isPowerUpped_chain() &&
                    chainToCompare.get(indexA).getCard().getID() == chainToCompare.get(indexA + 1).getCard().getID()) {
                tempPoints++;
                chainToCompare.get(indexA).getCard().setChainElement(true);
                chainToCompare.get(indexB).getCard().setChainElement(true);
                indexA += 1;
                indexB += 1;

            } else {
                // System.out.println(chain.get(indexA).getID());
                // System.out.println(chain.get(indexB).getID());
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.errorChaining);

                for (int i = chainToCompare.size - 1; i >= 0; i--) {
                    chainToCompare.get(i).getCard().setSelectable(false);
                    chainToCompare.get(i).getCard().setChainElement(false);

                }
                chainToCompare.clear();
                madeChains = 0;
                this.switchGrid();

                return false;
            }

        }

		/*
         * System.out.println("Chain corretta! Hai totalizzato " + score +
		 * " punti!!");
		 */
        AudioManager.instance.play(CardsAsset.instance.sounds.matchingCards);
        for (int i = cardSelectorEffect.size - 1; i >= 0; i--) {
            PooledEffect effect = cardSelectorEffect.get(i);
            effect.free();
            cardSelectorEffect.removeIndex(i);
        }
        for (int i = chainToCompare.size - 1; i >= 0; i--) {
            if (chainToCompare.get(i).getCard().isChainElement()) {
                PooledEffect effect = starsEffectPool.getStarsPool().obtain();
                effect.setPosition(chainToCompare.get(i).getPositionX(),
                        chainToCompare.get(i).getPositionY());
                stars.add(effect);
            }
            chainToCompare.get(i).getCard().setSelectable(false);
            chainToCompare.get(i).getCard().setChainElement(false);

            if (chainToCompare.get(i).getCard().isPowerUpped_time()) {
                this.addExtraTime(BONUS_TIME);
            }
            if (chainToCompare.get(i).getCard().isPowerUpped_bonus()) {

                totalScore += BONUS_POINT;
                renderingScore += BONUS_POINT;

            }
            chainToCompare.removeIndex(i);
        }

        for (int i = 0; i < tempPoints; i++) {
            totalScore += CHAIN_MULTIPLIER * (tempPoints - i);
            renderingScore += CHAIN_MULTIPLIER * (tempPoints - i);
        }

        if (memoFiendsType == 3 || memoFiendsType == 4) {
            madeChains += 1;
            if (madeChains == 3) {
                madeChains = 0;
                this.switchGrid();
            }
        } else if (memoFiendsType == 5 || memoFiendsType == 6) {
            madeChains += 1;
            if (madeChains == 4) {
                madeChains = 0;
                this.switchGrid();
            }
        }

        // System.out.println("Chain svuotata!!! Dimensioni: " + chain.size);
        return true;

    }

    public void checkDeckRotation() {
        // System.out.println(runTime);

        if (runTime > 0 && runTime < this.CARDS_FACING_UP_DURATION
                && !deckFaceUP) {
            deckFaceUP = true;

            for (int i = currentGrid * 24; i < (currentGrid + 1) * 24; i++) {

                grid.get(i).getCard().setMovingFaceUp(true);
                grid.get(i).getCard().setFaceDown(false);
            }
        } else if (runTime > this.CARDS_FACING_UP_DURATION && deckFaceUP) {

            for (int i = currentGrid * 24; i < (currentGrid + 1) * 24; i++) {

                grid.get(i).getCard().setFaceUP(false);
                grid.get(i).getCard().setMovingFaceDown(true);

            }

            this.resetOutOfBoundsGrid();
            deckFaceUP = false;
            blockInputHandler = false;
        }
    }

    public void switchGrid() {

        for (int i = stars.size - 1; i >= 0; i--) {
            PooledEffect effect = stars.get(i);
            effect.allowCompletion();
            effect.free();
            stars.removeIndex(i);
        }
        for (int i = cardSelectorEffect.size - 1; i >= 0; i--) {
            PooledEffect effect = cardSelectorEffect.get(i);
            effect.free();
            cardSelectorEffect.removeIndex(i);
        }

        // System.out.println("Dimensioni delle stelle: " + stars.size);
        for (int i = currentGrid * 24; i < (currentGrid + 1) * 24; i++) {
            moveSlotOutOfScreen(grid.get(i), new Vector2(grid.get(i)
                    .getPositionX(), grid.get(i).getPositionY() - 960));

        }

        currentGrid += 1;
        if (currentGrid == 2)
            currentGrid = 0;

        blockInputHandler = true;
        // System.out.println("Passo al mazzo numero: " + (currentDeck + 1));
        if (currentGrid == 100) {
            // System.out.println("MAzzi FINITI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            gameOver = true;

        } else {
            runTime = this.CARDS_FACING_UP_DELAY;
            this.moveGridIntoTheScreen();
        }

    }

    public void resetOutOfBoundsGrid() {

        int outOfBoundsGridIndex = 0;
        if (currentGrid == 0) {
            outOfBoundsGridIndex = 1;
        }

        if (grid.get(outOfBoundsGridIndex + 23).getPositionY() < 0) {
            boolean done = false;

            for (int i = outOfBoundsGridIndex * 24; i < (outOfBoundsGridIndex + 1) * 24; i++) {
                Card card = grid.get(i).getCard();
                grid.get(i).deleteCard();
                cardPool.free(card);
            }

            while (!done) {
                // System.out.println("SONO CAZZI NELLA CREAZIONE DELLA GRIGLIA!!!!!!!!!!!!!!!");
                firstPotentialChain = createPotentialChain(
                        outOfBoundsGridIndex, 6, 1);
                if (firstPotentialChain[0] == -1)
                    continue;
                else {
                    secondPotentialChain = createPotentialChain(
                            outOfBoundsGridIndex, 6, 2);
                    if (secondPotentialChain[0] == -1)
                        continue;
                    else {
                        thirdPotentialChain = createPotentialChain(
                                outOfBoundsGridIndex, 6, 3);
                        if (thirdPotentialChain[0] == -1)
                            continue;
                        else {
                            fourthPotentialChain = createPotentialChain(
                                    outOfBoundsGridIndex, 6, 4);
                            if (fourthPotentialChain[0] == -1)
                                continue;
                            else {
                                done = true;
                            }

                        }

                    }

                }
            }
            int deck = random.nextInt(4);
            int card1 = random.nextInt(12) + (deck * 12);
            int card2 = random.nextInt(12) + (deck * 12);
            int card3 = random.nextInt(12) + (deck * 12);
            int card4 = random.nextInt(12) + (deck * 12);
            while (card1 == card2) {
                card2 = random.nextInt(12) + (deck * 12);
            }
            while (card2 == card3 || card1 == card3) {
                card3 = random.nextInt(12) + (deck * 12);
            }
            while (card3 == card4 || card2 == card4 || card1 == card4) {
                card4 = random.nextInt(12) + (deck * 12);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card1);
                grid.get(firstPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card2);
                grid.get(secondPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card3);
                grid.get(thirdPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card4);
                grid.get(fourthPotentialChain[k]).addCard(card);
            }

        }

        if (powerUp[0]) {
            int index = outOfBoundsGridIndex * 24 + rand.nextInt(24);
            grid.get(index).getCard().setPowerUpped(true);
            grid.get(index).getCard().setPowerUpped_time(true);

        }

        boolean done;
        if (powerUp[1]) {
            done = false;
            while (!done) {
                int index = outOfBoundsGridIndex * 24 + rand.nextInt(24);
                if (checkChainAvailability(grid.get(index))
                        && !grid.get(index).getCard().isPowerUpped()) {
                    done = true;
                    grid.get(index).getCard().setPowerUpped(true);
                    grid.get(index).getCard().setPowerUpped_chain(true);
                }
            }

        }

        if (powerUp[2]) {
            done = false;
            while (!done) {
                int index = outOfBoundsGridIndex * 24 + rand.nextInt(24);
                if (!grid.get(index).getCard().isPowerUpped()) {
                    done = true;
                    grid.get(index).getCard().setPowerUpped(true);
                    grid.get(index).getCard().setPowerUpped_bonus(true);
                }
            }

        }


        // for (int i = outOfBoundsGridIndex * 24; i < (outOfBoundsGridIndex +
        // 1) * 24; i++) {
        // grid.get(i).getCard().setVisible(false);
        // }
    }

    public int getStartGameTime() {
        return startGameTime;
    }

    public void setStartGameTime(int startGameTime) {
        this.startGameTime = startGameTime;
    }

    public void refreshCountdown() {
        countdown = Math.round((COUNTDOWN - (float) (Math
                .round(runTimeCountdown * 10.0f) / 10.0f)) * 10.0f) / 10.0f;
        // System.out.println(countdown);
        if (countdown < 0)
            initialCountdown = false;
        runTimeCountdown += Gdx.graphics.getDeltaTime();
    }

    public float getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(float remainingTime) {
        this.remainingTime = remainingTime;
    }

    public float getCountdown() {
        return countdown;
    }

    public int getBonusTimePositionX() {
        return bonusTimePositionX;
    }

    public void setBonusTimePositionX(int bonusTimePositionX) {
        this.bonusTimePositionX = bonusTimePositionX;
    }

    public Array<Slot> getChain() {
        return chainSelected;
    }

    public void setChain(Array<Slot> chain) {
        this.chainSelected = chain;
    }

    public int getBonusTimePositionY() {
        return bonusTimePositionY;
    }

    public int getCurrentGrid() {
        return currentGrid;
    }

    public void setCurrentGrid(int currentGrid) {
        this.currentGrid = currentGrid;
    }

    public void setBonusTimePositionY(int bonusTimePositionY) {
        this.bonusTimePositionY = bonusTimePositionY;
    }

    public int getCountMatchedCards() {
        return countMatchedCards;
    }

    public void setCountMatchedCards(int countMatchedCards) {
        this.countMatchedCards = countMatchedCards;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public float getPLAYER_TIME() {
        return PLAYER_TIME;
    }

    public boolean isInitialCountdown() {
        return initialCountdown;
    }

    public boolean isVibrationEnabled() {
        return vibrationEnabled;
    }

    public void setVibrationEnabled(boolean vibrationEnabled) {
        this.vibrationEnabled = vibrationEnabled;

        prefs.save();
    }

    public void setInitialCountdown(boolean initialCountdown) {
        this.initialCountdown = initialCountdown;
    }

    public Array<PooledEffect> getStars() {
        return stars;
    }

    public boolean isBlockInputHandler() {
        return blockInputHandler;
    }

    public void setBlockInputHandler(boolean blockInputHandler) {
        this.blockInputHandler = blockInputHandler;
    }

    public RoundEndPopUp getContinueSharingPopUp() {
        return continueSharingPopUp;
    }

    public void setContinueSharingPopUp(RoundEndPopUp continueSharingPopUp) {
        this.continueSharingPopUp = continueSharingPopUp;
    }

    public boolean isContinuePopUpShown() {
        return continuePopUpShown;
    }

    public void setContinuePopUpShown(boolean continuePopUpShown) {
        this.continuePopUpShown = continuePopUpShown;
    }

    public boolean isContinuePopUpIsShowing() {
        return continuePopUpIsShowing;
    }

    public void setContinuePopUpIsShowing(boolean continuePopUpIsShowing) {
        this.continuePopUpIsShowing = continuePopUpIsShowing;
    }

    public boolean isExperienceAnimationIsStarted() {
        return experienceAnimationIsStarted;
    }

    public void setExperienceAnimationIsStarted(
            boolean experienceAnimationIsStarted) {
        this.experienceAnimationIsStarted = experienceAnimationIsStarted;
    }

    public boolean isExperienceAnimationIsRunning() {
        return experienceAnimationIsRunning;
    }

    public void setExperienceAnimationIsRunning(
            boolean experienceAnimationIsRunning) {
        this.experienceAnimationIsRunning = experienceAnimationIsRunning;
    }

    public boolean isInputInihibited() {
        return inputInihibited;
    }

    public void setInputInihibited(boolean inputInihibited) {
        this.inputInihibited = inputInihibited;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public Array<Slot> getGrid() {
        return grid;
    }

    public void setGrid(Array<Slot> grid) {
        this.grid = grid;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getRenderingScore() {
        return renderingScore;
    }

    public void setRenderingScore(int renderingScore) {
        this.renderingScore = renderingScore;
    }

    public int getTimeBonusScore() {
        return timeBonusScore;
    }

    public void setTimeBonusScore(int timeBonusScore) {
        this.timeBonusScore = timeBonusScore;
    }

    public int getPoint() {
        return point;
    }

    public boolean isOfflineMode() {
        return offlineMode;
    }

    public void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public float getRunTimeRemainingTime() {
        return runTimeRemainingTime;
    }

    public void setRunTimeRemainingTime(float runTimeRemainingTime) {
        this.runTimeRemainingTime = runTimeRemainingTime;
    }

    public float getBonusTimer() {
        return bonusTimer;
    }

    public void setBonusTimer(float bonusTimer) {
        this.bonusTimer = bonusTimer;
    }

    public void setCountdown(float countdown) {
        this.countdown = countdown;
    }

    public void setStars(Array<PooledEffect> stars) {
        this.stars = stars;
    }

    public ScrollingBackground getBackground() {
        return background;
    }

    public void setBackground(ScrollingBackground background) {
        this.background = background;
    }

    public void refreshRemainingTime() {

        remainingTime = Math
                .round((PLAYER_TIME - runTimeRemainingTime) * 100.0f) / 100.0f;
        runTimeRemainingTime += Gdx.graphics.getDeltaTime();

        if (remainingTime <= 0) {
            gameOver = true;
            inputInihibited = true;
        }
    }

    public void playFinalCountdown() {

        if (remainingTime < 10.1 && remainingTime > 3.5
                && remainingTime % 1 == 0) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }
        if (remainingTime < 3.1 && remainingTime > 2.1
                && remainingTime % 0.25 == 0) {
            // System.out.println(Math.round(remainingTime / 0.20*10));
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }

        if (remainingTime < 2.1 && Math.round(remainingTime * 50) % 10 == 0) {

            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

			/*
             * System.out.println("Ding di final countdown numero: " + contatore
			 * + " al secondo: " + remainingTime + ". Manipolato diventa: " +
			 * (Math.round(remainingTime * 50) + ". Condizione: ") +
			 * (Math.round(remainingTime * 50) % 10 == 0));
			 */

        }
        /*
         * if (remainingTime < 1.1 && Math.round(remainingTime / 0.20*10)%5==0)
		 * { //System.out.println(Math.round(remainingTime / 0.20*10));
		 * AudioManager.instance
		 * .play(CardsAsset.instance.sounds.finalCountdown);
		 * 
		 * }
		 */

    }

    public boolean isGameWin() {
        return gameWin;
    }

    public void setGameWin(boolean gameWin) {
        this.gameWin = gameWin;
    }

    public MemofiendsPlayer getPlayer() {
        return player;
    }

    public boolean isBonusTime() {
        return bonusTime;
    }

    public void setBonusTime(boolean bonusTime) {
        this.bonusTime = bonusTime;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public int getCardsNumber() {
        return cardsNumber;
    }

    public void setCardsNumber(int cardsNumber) {
        this.cardsNumber = cardsNumber;
    }

    public Array<PooledEffect> getCardSelectorEffect() {
        return cardSelectorEffect;
    }

    public void setCardSelectorEffect(Array<PooledEffect> cardSelectorEffect) {
        this.cardSelectorEffect = cardSelectorEffect;
    }

    public boolean isEnableSound() {
        return enableSound;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public void setEnableSound(boolean enableSound) {
        this.enableSound = enableSound;
    }

    public boolean isEnableMusic() {
        return enableMusic;
    }

    public void setEnableMusic(boolean enableMusic) {
        this.enableMusic = enableMusic;
    }

    public boolean isDeckFaceUP() {
        return deckFaceUP;
    }

    public void setDeckFaceUP(boolean deckFaceUP) {
        this.deckFaceUP = deckFaceUP;
    }

    public int getBonusPoint() {
        return bonusPoint;
    }

    public void setBonusPoint(int bonusPoint) {
        this.bonusPoint = bonusPoint;
    }

    public boolean isEnableToCompare() {
        return enableToCompare;
    }

    public void setEnableToCompare(boolean enableToCompare) {
        this.enableToCompare = enableToCompare;
    }

    public void backToMenu() {
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.initialCountdown);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

        memo.setScreen(new OfflineMode(memo, mocdc));
    }


    public void selectFirstCard(int x, int y) {

        cam.unproject(virtualTouch.set(x, y, 0));
        // Gdx.app.log(TAG,x + " " + y);
        // Gdx.app.log(TAG,virtualTouch.x + " " + virtualTouch.y);

        if (!player.selectFirstCard((int) Math.round(virtualTouch.x),
                (int) Math.round(virtualTouch.y)) || this.isInitialCountdown()) {
        } else {
            if (!grid.get(player.getCardIndex()).getCard().isSelected()) {
                grid.get(player.getCardIndex()).getCard().setSelected(true);
                chainSelected.add(grid.get(player.getCardIndex()));
                PooledEffect effect = cardSelectionEffectPool.getStarsPool()
                        .obtain();
                effect.setPosition(chainSelected.first().getPositionX() + 7,
                        chainSelected.first().getPositionY() + 3);
                cardSelectorEffect.add(effect);
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.chainCard);
            }

        }

    }

    public void addCardToChain(int x, int y) {

        cam.unproject(virtualTouch.set(x, y, 0));
        // Gdx.app.log(TAG,x + " " + y);
        // Gdx.app.log(TAG,virtualTouch.x + " " + virtualTouch.y);

        if (!player.selectCard((int) Math.round(virtualTouch.x),
                (int) Math.round(virtualTouch.y))
                || this.isInitialCountdown()) {
        } else {

            if (!grid.get(player.getCardIndex()).getCard().isSelected()
                    && (new Vector2(grid.get(player.getCardIndex())
                    .getPositionX() + 58, grid.get(
                    player.getCardIndex()).getPositionY() + 58).dst(
                    chainSelected.get(chainSelected.size - 1)
                            .getPositionX() + 58,
                    chainSelected.get(chainSelected.size - 1)
                            .getPositionY() + 58)) < 180) {
                /*
                 * System.out.println(new Vector2(memoFiendsDeck.getCards()
				 * .get(player.getCardIndex()).getPositionX() + 58,
				 * memoFiendsDeck.getCards().get(player.getCardIndex())
				 * .getPositionY() + 58).dst( chain.get(chain.size -
				 * 1).getPositionX() + 58, chain .get(chain.size -
				 * 1).getPositionY() + 58));
				 */
                grid.get(player.getCardIndex()).getCard().setSelected(true);
                chainSelected.add(grid.get(player.getCardIndex()));

                PooledEffect effect = cardSelectionEffectPool.getStarsPool()
                        .obtain();
                effect.setPosition(chainSelected.get(chainSelected.size - 1)
                                .getPositionX() + 7,
                        chainSelected.get(chainSelected.size - 1)
                                .getPositionY() + 3);
                cardSelectorEffect.add(effect);
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.chainCard);

            } else if (grid.get(player.getCardIndex()).getCard().isSelected()
                    && chainSelected.indexOf(grid.get(player.getCardIndex()),
                    true) == chainSelected.size - 2) {
                removeCardFromChain();
                AudioManager.instance
                        .play(CardsAsset.instance.sounds.chainCard);

            } else if (grid.get(player.getCardIndex()).getCard().isSelected()
                    && chainSelected.indexOf(grid.get(player.getCardIndex()),
                    true) == chainSelected.size - 1) {

            }

        }

    }

    public void removeCardFromChain() {
        grid.get(grid.indexOf(chainSelected.get(chainSelected.size - 1), true))
                .getCard().setSelected(false);
        cardSelectorEffect.removeIndex(cardSelectorEffect.size - 1);
        chainSelected.removeIndex(chainSelected.size - 1);
    }

    public void moveSlotOutOfScreen(Slot slot, Vector2 direction) {

        float speed = this.CARD_SPEED_GOING_OUT;
        slot.setStart(new Vector2(slot.getPosition().x, slot.getPosition().y));
        slot.setEnd(direction);
        Vector2 directionTemp = new Vector2(direction.x, direction.y);
        Vector2 velocity = directionTemp.sub(slot.getStart()).nor().scl(speed);
        // Vector2 direction = vett[2].sub(vett[0].nor()).scl(velocity);
        slot.setVelocity(velocity);
        slot.setDistance(slot.getStart().dst(slot.getEnd()));
        slot.setMoving(true);

    }

    public void moveSlotInScreen(Slot slot, Vector2 direction) {

        float speed = this.CARD_SPEED_GOING_IN;
        slot.setStart(new Vector2(slot.getPosition().x, slot.getPosition().y));
        slot.setEnd(direction);
        Vector2 directionTemp = new Vector2(direction.x, direction.y);
        Vector2 velocity = directionTemp.sub(slot.getStart()).nor().scl(speed);
        // Vector2 direction = vett[2].sub(vett[0].nor()).scl(velocity);
        slot.setVelocity(velocity);
        slot.setDistance(slot.getStart().dst(slot.getEnd()));
        slot.setMoving(true);

    }

    protected void showContinueSharingPopUp() {

        continueSharingPopUp = new RoundEndPopUp(memo, mocdc, matchID, opponent,
                totalScore, 0, 0, offlineMode, 3);
        Gdx.input.setInputProcessor(continueSharingPopUp.getStage());
        AudioManager.instance.stopMusic();
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

    }

    public void initGrid() {

        int count = 0;
        for (int i = 0; i < cardsNumber / 24; i++) {
            for (int k = 0; k < ROWS; k++) {
                for (int j = 0; j < COLUMNS; j++) {

                    Vector2 position = new Vector2(22 + 126 * j, 960 * i + 84
                            + 126 * k);
                    grid.add(new Slot(count, position));
                    // System.out.println(count);
                    count += 1;

                }
            }

        }

    }

    private void moveGridIntoTheScreen() {
        int count = 24 * currentGrid;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {

                Vector2 position = new Vector2(22 + 126 * j, 960 + 84 + 126 * i);
                this.grid.get(count).setPosition(position);
                this.grid.get(count).getCard().setVisible(true);
                this.moveSlotInScreen(grid.get(count), new Vector2(
                        22 + 126 * j, 14 + 126 * i));
                count += 1;

            }
        }

    }

    public void setSlotsLimits() {

        for (int i = 0; i < cardsNumber / 24; i++) {

            grid.get(0 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(0 + 24 * i).setLimitedOnBottom(true);
            grid.get(0 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(0 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(0 + 24 * i).setLimitedOnLeft(true);

            grid.get(1 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(1 + 24 * i).setLimitedOnBottom(true);
            grid.get(1 + 24 * i).setLimitedOnBottomRight(true);

            grid.get(2 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(2 + 24 * i).setLimitedOnBottom(true);
            grid.get(2 + 24 * i).setLimitedOnBottomRight(true);

            grid.get(3 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(3 + 24 * i).setLimitedOnBottom(true);
            grid.get(3 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(3 + 24 * i).setLimitedOnRight(true);
            grid.get(3 + 24 * i).setLimitedOnTopRight(true);

            grid.get(4 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(4 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(4 + 24 * i).setLimitedOnLeft(true);

            grid.get(7 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(7 + 24 * i).setLimitedOnRight(true);
            grid.get(7 + 24 * i).setLimitedOnTopRight(true);

            grid.get(8 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(8 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(8 + 24 * i).setLimitedOnLeft(true);

            grid.get(11 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(11 + 24 * i).setLimitedOnRight(true);
            grid.get(11 + 24 * i).setLimitedOnTopRight(true);

            grid.get(12 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(12 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(12 + 24 * i).setLimitedOnLeft(true);

            grid.get(15 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(15 + 24 * i).setLimitedOnRight(true);
            grid.get(15 + 24 * i).setLimitedOnTopRight(true);

            grid.get(16 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(16 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(16 + 24 * i).setLimitedOnLeft(true);

            grid.get(19 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(19 + 24 * i).setLimitedOnRight(true);
            grid.get(19 + 24 * i).setLimitedOnTopRight(true);

            grid.get(20 + 24 * i).setLimitedOnBottomLeft(true);
            grid.get(20 + 24 * i).setLimitedOnTopRight(true);
            grid.get(20 + 24 * i).setLimitedOnTop(true);
            grid.get(20 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(20 + 24 * i).setLimitedOnLeft(true);

            grid.get(21 + 24 * i).setLimitedOnTopRight(true);
            grid.get(21 + 24 * i).setLimitedOnTop(true);
            grid.get(21 + 24 * i).setLimitedOnTopLeft(true);

            grid.get(22 + 24 * i).setLimitedOnTopRight(true);
            grid.get(22 + 24 * i).setLimitedOnTop(true);
            grid.get(22 + 24 * i).setLimitedOnTopLeft(true);

            grid.get(23 + 24 * i).setLimitedOnBottomRight(true);
            grid.get(23 + 24 * i).setLimitedOnRight(true);
            grid.get(23 + 24 * i).setLimitedOnTopRight(true);
            grid.get(23 + 24 * i).setLimitedOnTop(true);
            grid.get(23 + 24 * i).setLimitedOnTopLeft(true);
            grid.get(23 + 24 * i).setLimitedOnLeft(true);
        }

    }

    public void setNeighbors() {

        Slot ghostSlot = new Slot(-1, new Vector2(-300, -300));
        ghostSlot.addCard(new Card(-1));
        ghostSlot.setCheckedAsFreeSlot(true);
        ghostSlot.setEmpty(false);
        for (int j = 0; j < 8; j++)
            ghostSlot.getNeighbors()[j] = ghostSlot;

        for (int i = 0; i < cardsNumber; i++) {

            if (grid.get(i).isLimitedOnBottomLeft())
                grid.get(i).getNeighbors()[0] = ghostSlot;
            else
                grid.get(i).getNeighbors()[0] = grid.get(grid.get(i)
                        .getGridPosition() - 5);

            if (grid.get(i).isLimitedOnBottom())
                grid.get(i).getNeighbors()[1] = ghostSlot;
            else
                grid.get(i).getNeighbors()[1] = grid.get(grid.get(i)
                        .getGridPosition() - 4);

            if (grid.get(i).isLimitedOnBottomRight())
                grid.get(i).getNeighbors()[2] = ghostSlot;
            else
                grid.get(i).getNeighbors()[2] = grid.get(grid.get(i)
                        .getGridPosition() - 3);

            if (grid.get(i).isLimitedOnRight())
                grid.get(i).getNeighbors()[3] = ghostSlot;
            else
                grid.get(i).getNeighbors()[3] = grid.get(grid.get(i)
                        .getGridPosition() + 1);

            if (grid.get(i).isLimitedOnTopRight())
                grid.get(i).getNeighbors()[4] = ghostSlot;
            else
                grid.get(i).getNeighbors()[4] = grid.get(grid.get(i)
                        .getGridPosition() + 5);

            if (grid.get(i).isLimitedOnTop())
                grid.get(i).getNeighbors()[5] = ghostSlot;
            else
                grid.get(i).getNeighbors()[5] = grid.get(grid.get(i)
                        .getGridPosition() + 4);

            if (grid.get(i).isLimitedOnTopLeft())
                grid.get(i).getNeighbors()[6] = ghostSlot;
            else
                grid.get(i).getNeighbors()[6] = grid.get(grid.get(i)
                        .getGridPosition() + 3);

            if (grid.get(i).isLimitedOnLeft())
                grid.get(i).getNeighbors()[7] = ghostSlot;
            else {
                grid.get(i).getNeighbors()[7] = grid.get(grid.get(i)
                        .getGridPosition() - 1);
            }
        }

    }

    public void fillNCells(int gridNumber, int cellsNumber, int cardID) {
        boolean chainCompleted = false;

        while (!chainCompleted) {
            boolean reset = false;
            int[] indeces = new int[cellsNumber];
            for (int i = 0; i < cellsNumber; i++) {
                boolean validCell = false;
                while (!validCell) {
                    int index = 24 * (gridNumber) + rand.nextInt(24);
                    if (i == 0 || cellsContigued(indeces[i - 1], index)) {
                        if (grid.get(index).isEmpty()
                                && (i == cellsNumber - 1 || !isTrapped(grid
                                .get(index)))) {
                            validCell = true;
                            indeces[i] = index;
                            grid.get(index).addCard(new Card(cardID));

                        }

                    }

                }
                if (this.isPontingTrappedSlotsOrLimitsOrFilledSlots(grid
                        .get(indeces[i])) && i < cellsNumber - 2) {
                    for (int k = 0; k <= i; k++)
                        grid.get(indeces[k]).deleteCard();
                    reset = true;
                    break;

                }
            }

            if (reset) {
                continue;
            }
            for (int i = 0; i < cellsNumber; i++)
                System.out.println("Carta " + (i + 1) + " sistemata. Slot: "
                        + indeces[i]);
            chainCompleted = true;
            lastChain = indeces;

        }

    }

    public boolean cellsContigued(int cellA, int cellB) {

        if (grid.get(cellA).getMiddlePoint()
                .dst(grid.get(cellB).getMiddlePoint()) < 180)
            return true;
        else {
            // System.out.println(grid.get(cellA).getMiddlePoint());
            // System.out.println(grid.get(cellB).getMiddlePoint());
            // System.out.println(grid.get(cellA).getMiddlePoint()
            // .dst(grid.get(cellB).getMiddlePoint()));
            return false;
        }

    }

    public void deleteLastChain() {

        for (int i = 0; i < lastChain.length; i++)
            grid.get(lastChain[i]).deleteCard();

    }

    public boolean checkSpaceForChain(int gridNumber, int chainLength) {
        boolean spaceChecked = false;
        int resetCounter = 0;

        while (!spaceChecked) {
            int[] indeces = new int[chainLength];
            boolean reset = false;
            for (int i = 0; i < chainLength; i++) {
                boolean validCell = false;
                // AGGIUNGO UNA CELLA
                while (!validCell) {
                    int index = 24 * (gridNumber) + rand.nextInt(24);
                    if (i == 0 || cellsContigued(indeces[i - 1], index)) {
                        /*
                         * System.out.println("Provo ad aggiungere una cella: "
						 * + index); System.out.println("Cella vuota?  " +
						 * grid.get(index).isEmpty());
						 * System.out.println("Cella disponibile per  il check? "
						 * + !grid.get(index).isCheckedAsFreeSlot());
						 * System.out.println("Cella  (space) trappata? " +
						 * isSpaceTrapped(grid.get(index)));
						 */
                        if (grid.get(index).isEmpty()
                                && !grid.get(index).isCheckedAsFreeSlot()
                                && (i == chainLength - 1 || !isSpaceTrapped(grid
                                .get(index)))) {
                            validCell = true;
                            indeces[i] = index;
                            grid.get(index).setCheckedAsFreeSlot(true);

                        }
                    }
                }

                if (this.isPontingSpaceTrappedSlotsOrLimitsOrFilledSlots(grid
                        .get(indeces[i])) && i < chainLength - 2) {

                    for (int k = 0; k < cardsNumber; k++) {
                        grid.get(k).setCheckedAsFreeSlot(false);
                    }
                    reset = true;
                    resetCounter += 1;
                    if (resetCounter == 200) {
                        return false;
                    }
                    break;
                }

            }

            if (reset) {
                continue;
            }

            spaceChecked = true;
            for (int k = 0; k < chainLength; k++) {
                grid.get(indeces[k]).setCheckedAsFreeSlot(false);
            }
        }
        return true;

    }

    public boolean isTrapped(Slot slot) {

        if ((slot.isLimitedOnBottomLeft() || !slot.getNeighbors()[0].isEmpty())
                && (slot.isLimitedOnBottom() || !slot.getNeighbors()[1]
                .isEmpty())
                && (slot.isLimitedOnBottomRight() || !slot.getNeighbors()[2]
                .isEmpty())
                && (slot.isLimitedOnRight() || !slot.getNeighbors()[3]
                .isEmpty())
                && (slot.isLimitedOnTopRight() || !slot.getNeighbors()[4]
                .isEmpty())
                && (slot.isLimitedOnTop() || !slot.getNeighbors()[5].isEmpty())
                && (slot.isLimitedOnTopLeft() || !slot.getNeighbors()[6]
                .isEmpty())
                && (slot.isLimitedOnLeft() || !slot.getNeighbors()[7].isEmpty())) {

            return true;
        }

        return false;
    }

    public boolean isPontingTrappedSlotsOrLimitsOrFilledSlots(Slot slot) {
        if ((slot.isLimitedOnBottomLeft() || isTrapped(slot.getNeighbors()[0]) || !slot
                .getNeighbors()[0].isEmpty())
                && (slot.isLimitedOnBottom()
                || isTrapped(slot.getNeighbors()[1]) || !slot
                .getNeighbors()[1].isEmpty())
                && (slot.isLimitedOnBottomRight()
                || isTrapped(slot.getNeighbors()[2]) || !slot
                .getNeighbors()[2].isEmpty())
                && (slot.isLimitedOnRight()
                || isTrapped(slot.getNeighbors()[3]) || !slot
                .getNeighbors()[3].isEmpty())
                && (slot.isLimitedOnTopRight()
                || isTrapped(slot.getNeighbors()[4]) || !slot
                .getNeighbors()[4].isEmpty())
                && (slot.isLimitedOnTop() || isTrapped(slot.getNeighbors()[5]) || !slot
                .getNeighbors()[5].isEmpty())
                && (slot.isLimitedOnTopLeft()
                || isTrapped(slot.getNeighbors()[6]) || !slot
                .getNeighbors()[6].isEmpty())
                && (slot.isLimitedOnLeft() || isTrapped(slot.getNeighbors()[7]) || !slot
                .getNeighbors()[7].isEmpty())) {

            return true;
        }

        return false;

    }

    public boolean isSpaceTrapped(Slot slot) {

        if ((slot.isLimitedOnBottomLeft() || !slot.getNeighbors()[0].isEmpty() || slot
                .getNeighbors()[0].isCheckedAsFreeSlot())
                && (slot.isLimitedOnBottom()
                || !slot.getNeighbors()[1].isEmpty() || slot
                .getNeighbors()[1].isCheckedAsFreeSlot())
                && (slot.isLimitedOnBottomRight()
                || !slot.getNeighbors()[2].isEmpty() || slot
                .getNeighbors()[2].isCheckedAsFreeSlot())
                && (slot.isLimitedOnRight()
                || !slot.getNeighbors()[3].isEmpty() || slot
                .getNeighbors()[3].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTopRight()
                || !slot.getNeighbors()[4].isEmpty() || slot
                .getNeighbors()[4].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTop() || !slot.getNeighbors()[5].isEmpty() || slot
                .getNeighbors()[5].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTopLeft()
                || !slot.getNeighbors()[6].isEmpty() || slot
                .getNeighbors()[6].isCheckedAsFreeSlot())
                && (slot.isLimitedOnLeft() || !slot.getNeighbors()[7].isEmpty() || slot
                .getNeighbors()[7].isCheckedAsFreeSlot())) {

            return true;
        }

        return false;
    }

    public boolean isPontingSpaceTrappedSlotsOrLimitsOrFilledSlots(Slot slot) {
        if ((slot.isLimitedOnBottomLeft()
                || isSpaceTrapped(slot.getNeighbors()[0])
                || !slot.getNeighbors()[0].isEmpty() || slot.getNeighbors()[0]
                .isCheckedAsFreeSlot())
                && (slot.isLimitedOnBottom()
                || isSpaceTrapped(slot.getNeighbors()[1])
                || !slot.getNeighbors()[1].isEmpty() || slot
                .getNeighbors()[1].isCheckedAsFreeSlot())
                && (slot.isLimitedOnBottomRight()
                || isSpaceTrapped(slot.getNeighbors()[2])
                || !slot.getNeighbors()[2].isEmpty() || slot
                .getNeighbors()[2].isCheckedAsFreeSlot())
                && (slot.isLimitedOnRight()
                || isSpaceTrapped(slot.getNeighbors()[3])
                || !slot.getNeighbors()[3].isEmpty() || slot
                .getNeighbors()[3].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTopRight()
                || isSpaceTrapped(slot.getNeighbors()[4])
                || !slot.getNeighbors()[4].isEmpty() || slot
                .getNeighbors()[4].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTop()
                || isSpaceTrapped(slot.getNeighbors()[5])
                || !slot.getNeighbors()[5].isEmpty() || slot
                .getNeighbors()[5].isCheckedAsFreeSlot())
                && (slot.isLimitedOnTopLeft()
                || isSpaceTrapped(slot.getNeighbors()[6])
                || !slot.getNeighbors()[6].isEmpty() || slot
                .getNeighbors()[6].isCheckedAsFreeSlot())
                && (slot.isLimitedOnLeft()
                || isSpaceTrapped(slot.getNeighbors()[7])
                || !slot.getNeighbors()[7].isEmpty() || slot
                .getNeighbors()[7].isCheckedAsFreeSlot())) {

            return true;
        }

        return false;

    }

    public void fillGrids() {

		/*
         * if (memoFiendsType == 3) { for (int i = 0; i < cardsNumber / 24; i++)
		 * {
		 * 
		 * System.out .println(
		 * "_______________________________________________RIEMPIO LA GRIGLIA:  "
		 * + i);
		 * 
		 * fillNCells(i, 6, 0); boolean done = false; while (!done) {
		 * fillNCells(i, 6, 1); if (this.checkSpaceForChain(i, 6)) { done =
		 * true; } else { deleteLastChain(); }
		 * 
		 * } fillNCells(i, 6, 2); }
		 * 
		 * fillEmptySlot(); }
		 * 
		 * else if (memoFiendsType == 4) { for (int i = 0; i < cardsNumber / 24;
		 * i++) {
		 * 
		 * System.out .println(
		 * "_______________________________________________RIEMPIO LA GRIGLIA:  "
		 * + i);
		 * 
		 * fillNCells(i, 7, 0); boolean done = false; while (!done) {
		 * fillNCells(i, 6, 1); if (this.checkSpaceForChain(i, 5)) { done =
		 * true; } else { deleteLastChain(); }
		 * 
		 * } fillNCells(i, 5, 2); }
		 * 
		 * fillEmptySlot();
		 * 
		 * }
		 */

        for (int i = 0; i < this.cardsNumber / 24; i++) {
            boolean done = false;

            while (!done) {
                firstPotentialChain = createPotentialChain(i, 6, 1);
                if (firstPotentialChain[0] == -1)
                    continue;
                else {
                    secondPotentialChain = createPotentialChain(i, 6, 2);
                    if (secondPotentialChain[0] == -1)
                        continue;
                    else {
                        thirdPotentialChain = createPotentialChain(i, 6, 3);
                        if (thirdPotentialChain[0] == -1)
                            continue;
                        else {
                            fourthPotentialChain = createPotentialChain(i, 6, 4);
                            if (fourthPotentialChain[0] == -1)
                                continue;
                            else {
                                done = true;
                            }

                        }

                    }

                }
            }

            int deck = random.nextInt(4);
            int card1 = random.nextInt(12) + (deck * 12);
            int card2 = random.nextInt(12) + (deck * 12);
            int card3 = random.nextInt(12) + (deck * 12);
            int card4 = random.nextInt(12) + (deck * 12);
            while (card1 == card2) {
                card2 = random.nextInt(12) + (deck * 12);
            }
            while (card2 == card3 || card1 == card3) {
                card3 = random.nextInt(12) + (deck * 12);
            }
            while (card3 == card4 || card2 == card4 || card1 == card4) {
                card4 = random.nextInt(12) + (deck * 12);
            }

            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card1);
                grid.get(firstPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card2);
                grid.get(secondPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card3);
                grid.get(thirdPotentialChain[k]).addCard(card);
            }
            for (int k = 0; k < 6; k++) {
                Card card = cardPool.obtain();
                card.setID(card4);
                grid.get(fourthPotentialChain[k]).addCard(card);
            }

        }

        for (int i = 0; i < cardsNumber; i++) {
            this.grid.get(i).getCard().setVisible(false);
        }

		/*
         * else if (memoFiendsType == 6) { int count = 0; for (int i = 0; i <
		 * cardsNumber / 24; i++) { boolean done = false;
		 * 
		 * while (!done) { count += 1; firstPotentialChain =
		 * createPotentialChain(i, 8, 1); if (firstPotentialChain[0] == -1)
		 * continue; else { secondPotentialChain = createPotentialChain(i, 7,
		 * 2); if (secondPotentialChain[0] == -1) continue; else {
		 * thirdPotentialChain = createPotentialChain(i, 5, 3); if
		 * (thirdPotentialChain[0] == -1) continue; else { fourthPotentialChain
		 * = createPotentialChain(i, 4, 4); if (fourthPotentialChain[0] == -1)
		 * continue; else { done = true; }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * } } for (int k = 0; k < 8; k++)
		 * grid.get(firstPotentialChain[k]).addCard(new Card(0)); for (int k =
		 * 0; k < 7; k++) grid.get(secondPotentialChain[k]).addCard(new
		 * Card(1)); for (int k = 0; k < 5; k++)
		 * grid.get(thirdPotentialChain[k]).addCard(new Card(2)); for (int k =
		 * 0; k < 4; k++) grid.get(fourthPotentialChain[k]).addCard(new
		 * Card(3)); count = 0; }
		 * 
		 * }
		 */
    }

    public void fillEmptySlot() {

        for (int i = 0; i < cardsNumber; i++) {
            if (grid.get(i).isEmpty()) {
                grid.get(i).addCard(new Card(999));

            }

        }
    }

    public int[] createPotentialChain(int gridNumber, int chainLength,
                                      int chainNumber) {

        int[] indeces = new int[chainLength];
        boolean spaceChecked = false;
        int resetCounter = 0;
        int addingPotentialCellAttempt = 0;

        while (!spaceChecked) {

            boolean reset = false;
            for (int i = 0; i < chainLength; i++) {
                boolean validCell = false;
                // AGGIUNGO UNA CELLA
                while (!validCell) {
                    int index = 24 * (gridNumber) + rand.nextInt(24);

                    if (i == 0 || cellsContigued(indeces[i - 1], index)) {
                        addingPotentialCellAttempt += 1;
                        if (addingPotentialCellAttempt == 200) {
                            for (int k = 0; k < cardsNumber; k++)
                                grid.get(k).setCheckedAsFreeSlot(false);
                            indeces[0] = -1;
                            return indeces;

                        }

                        if (!grid.get(index).isCheckedAsFreeSlot()
                                && (i == chainLength - 1 || !isSpaceTrapped(grid
                                .get(index)))) {
                            validCell = true;
                            indeces[i] = index;
                            grid.get(index).setCheckedAsFreeSlot(true);
                            addingPotentialCellAttempt = 0;

                        }
                    }
                }

                if (this.isPontingSpaceTrappedSlotsOrLimitsOrFilledSlots(grid
                        .get(indeces[i])) && i < chainLength - 2) {

                    reset = true;
                    resetCounter += 1;
                    if (resetCounter == 200) {
                        for (int k = 0; k < cardsNumber; k++)
                            grid.get(i).setCheckedAsFreeSlot(false);
                        indeces[0] = -1;
                        return indeces;
                    }
                    break;
                }

            }

            if (reset) {
                continue;
            }

            spaceChecked = true;

        }
        return indeces;

    }

    public void setPowerUpCards() {

        if (powerUp[0]) {
            int index = rand.nextInt(24);
            grid.get(index).getCard().setPowerUpped(true);
            grid.get(index).getCard().setPowerUpped_time(true);

        }

        boolean done;
        if (powerUp[1]) {
            done = false;
            while (!done) {
                int index = rand.nextInt(24);
                if (checkChainAvailability(grid.get(index))
                        && !grid.get(index).getCard().isPowerUpped()) {
                    done = true;
                    grid.get(index).getCard().setPowerUpped(true);
                    grid.get(index).getCard().setPowerUpped_chain(true);
                }
            }

        }

        if (powerUp[2]) {
            done = false;
            while (!done) {
                int index = rand.nextInt(24);
                if (!grid.get(index).getCard().isPowerUpped()) {
                    done = true;
                    grid.get(index).getCard().setPowerUpped(true);
                    grid.get(index).getCard().setPowerUpped_bonus(true);
                }
            }

        }

    }

    public void addExtraTime(int extratime) {
        bonusTime = true;
        // PLAYER_TIME += extratime;
        this.PLAYER_TIME += extratime;
        if (remainingTime > 10)
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.finalCountdown);
    }

    private boolean checkChainAvailability(Slot slot) {
        int ID = slot.getCard().getID();
        for (int i = 0; i < 8; i++) {
            if (slot.getNeighbors()[i].getCard().getID() != -1)
                if (slot.getNeighbors()[i].getCard().getID() != ID) {
                    return true;
                }
        }
        return false;
    }
}
