package com.memogameonline.gametable;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.Card;
import com.memogameonline.gameobjects.Deck;
import com.memogameonline.gameobjects.ParticleEffect_CardsDisappearingEffect;
import com.memogameonline.gameobjects.ScrollingBackground;
import com.memogameonline.gamescreens.OfflineMode;
import com.memogameonline.gamescreens.RoundEndPopUp;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.players.ClassicPlayer;
import com.memogameonline.user.Opponent;

public class Classic {
    public static final String TAG = "LOGIC: ";

    private MemoGameOnline memo;
    private long matchID;
    protected Opponent opponent;

    private RoundEndPopUp continueSharingPopUp;
    private boolean continuePopUpShown;
    private boolean continuePopUpIsShowing;
    private boolean experienceAnimationIsStarted;
    private boolean experienceAnimationIsRunning;
    private boolean inputInihibited;

    private ParticleEffect_CardsDisappearingEffect specialEffectPool;
    private Array<PooledEffect> specialEffects;

    private int startGameTime; // da convertire in tipo time
    private int totalScore;
    private int renderingScore;
    private int timeBonusScore;
    private boolean renderSpecial;

    private OrthographicCamera cam;
    private Vector3 virtualTouch = new Vector3();

    private ScrollingBackground background;

    private ClassicPlayer player;

    private boolean enableToCompare;

    private boolean enableSound;
    private boolean enableMusic;
    private boolean fastSelection;

    private boolean gameOver;
    private boolean gameWin;

    private int countMatchedCards;

    private float PLAYER_TIME;
    private float remainingTime;
    private final float COUNTDOWN;
    private float countdown;
    private float runTimeCountdown;
    private float runTimeRemainingTime;

    private float bonusTimer;
    private float alpha;
    private int bonusTimePositionX;
    private int bonusTimePositionY;

    private int framesNumber;
    private int backFramesNumber;
    private int faceUpFramesNumber;
    private int faceUpAfterMatchingFramesNumber;
    private int faceUpAfterSpecialFramesNumber;

    private float runTime;
    private float distributionCardTime;
    protected int indexGeneratedCard;

    private boolean initialCountdownStarted;
    private boolean initialCountdown;
    private boolean bonusTime;

    private Deck deck;
    private final int ROWS = 6;
    private final int COLUMNS = 4;

    private boolean[] powerUp;
    private boolean offlineMode;

    private Random random;

    private MOCDataCarrier mocdc;


    private final int BONUS_TIME = 1;


    public Classic(MemoGameOnline memo, MOCDataCarrier mocdc, long matchID, Opponent opponent,
                   int GameMode, float time, boolean[] powerUp, boolean offlineMode) {

        this.memo = memo;
        this.mocdc = mocdc;
        this.matchID = matchID;
        this.opponent = opponent;

        this.powerUp = powerUp;
        this.offlineMode = offlineMode;

        PLAYER_TIME = time;
        remainingTime = time;
        COUNTDOWN = 3;
        runTimeCountdown = 0;
        runTimeRemainingTime = 0;
        cam = new OrthographicCamera();
        cam.setToOrtho(true, Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        virtualTouch = new Vector3();
        gameOver = false;
        gameWin = false;


        background = new ScrollingBackground();

        random = new Random();
        int deckType = random.nextInt(4);
        deck = new Deck(deckType);
        assignPowerUp();
        deck.shuffle();

        framesNumber = deck.getCards().get(1).getNUMBER_FRAMES();
        backFramesNumber = deck.getCards().get(1).getNUMBER_BACK_FRAMES();
        faceUpFramesNumber = deck.getCards().get(1).getNUMBER_FACE_FRAMES();
        faceUpAfterMatchingFramesNumber = deck.getCards().get(1)
                .getNUMBER_FACEUP_REMAINING_TIME_FRAMES();
        faceUpAfterSpecialFramesNumber = deck.getCards().get(1)
                .getNUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES();


        player = new ClassicPlayer(this);
        totalScore = 0;
        renderingScore = 0;
        enableToCompare = false;

        setCardsPosition();
        bonusTimer = 0;
        alpha = 1;
        bonusTimePositionX = 414;
        bonusTimePositionY = 899;

        runTime = 0;
        distributionCardTime = 0;
        indexGeneratedCard = 0;

        initialCountdownStarted = false;
        initialCountdown = true;
        this.bonusTime = false;
        fastSelection = false;

        specialEffectPool = new ParticleEffect_CardsDisappearingEffect();
        specialEffects = new Array<PooledEffect>();


    }

    public void update(float delta) {
        for (int i = 0; i < 24; i++)
            deck.getCards().get(i).update(delta);
        background.update(delta);

        if (!initialCountdownStarted) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.initialCountdown);
            initialCountdownStarted = true;
        }
        if (initialCountdown) {
            refreshCountdown();

        }

        if ((!initialCountdown && !gameOver || gameOver && gameWin && !offlineMode)
                && remainingTime > 0) {
            refreshRemainingTime();
        }

        // playFinalCountdown();

        if (bonusTime) {
            bonusTimer += Gdx.graphics.getDeltaTime();
            alpha = 1 - bonusTimer / 2;
            bonusTimePositionX = 6 + Math.round((bonusTimer / 2) * 50);
            bonusTimePositionY = 950 - Math.round((bonusTimer / 2) * 50);
            if (bonusTimer > 2) {
                bonusTime = false;
                bonusTimer = 0;
                bonusTimePositionX = 390;
                bonusTimePositionY = 950;
                alpha = 1;
            }
        }

        if (gameOver && !experienceAnimationIsRunning
                && !experienceAnimationIsStarted) {
            System.out.println("Inizio l'animazione dell'esperienza!");
            experienceAnimationIsRunning = true;
            experienceAnimationIsStarted = true;
            AudioManager.instance.play(CardsAsset.instance.sounds.scoreCount);

        }

        if (gameOver && continuePopUpShown && !continuePopUpIsShowing) {
            // System.out.println("POP UP!!!!!!!!!!!!!!!!!!!!!!!!!!");
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            continuePopUpShown = false;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
            System.out.println("MOSTRO ONLINE!!!");
        }

        if (gameOver && !continuePopUpShown && offlineMode) {
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            continuePopUpShown = true;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
            System.out.println("MOSTRO OFFLINE!!!");
        }
    }

    /**
     * Metodo invocato durante il setting di ClassicPlayer.slotSecond
     *
     * @throws InterruptedException
     */
    public boolean compareSlots() {

        Gdx.app.log(TAG, "effettuo un confronto tra le carte negli slot  " + player.getFirstCardIndex() + " e " + player.getSecondCardIndex()
                + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getFirstCardIndex()).getID()]
                + " e " + CardsAsset.instance.cards[deck.getCards().get(player.getSecondCardIndex()).getID()] + ")");

        if (deck.getCards().get(player.getFirstCardIndex()).getID() == deck
                .getCards().get(player.getSecondCardIndex()).getID()) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.matchingCards);
            this.countMatchedCards++;

            deck.getCards().get(player.getFirstCardIndex()).setFaceUP(false);
            deck.getCards().get(player.getSecondCardIndex()).setFaceUP(false);
            deck.getCards().get(player.getFirstCardIndex())
                    .setDisappearing(true);
            deck.getCards().get(player.getSecondCardIndex())
                    .setDisappearing(true);
            deck.getCards().get(player.getFirstCardIndex()).setMatched(true);
            deck.getCards().get(player.getSecondCardIndex()).setMatched(true);

            if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_bomb()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_bomb()) {
                activeDoubleCouple();
                this.countMatchedCards++;
            } else if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_time()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_time())
                addExtraTime(BONUS_TIME);
            else if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_faceUp()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_faceUp())
                activeFaceUp();

            // activeDoubleCouple();
            // this.countMatchedCards++;

            Gdx.app.log(TAG, "le carte MATCHANO!! " + player.getFirstCardIndex() + " e " + player.getSecondCardIndex()
                    + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getFirstCardIndex()).getID()]
                    + " e " + CardsAsset.instance.cards[deck.getCards().get(player.getSecondCardIndex()).getID()] + ")");

            player.setFirstCardIndex(-1);
            player.setSecondCardIndex(-1);

            Gdx.app.log(TAG, "-------------------------------------------------------------------FINE CONFRONTO (POSITIVO)");

            totalScore += 80;
            renderingScore += 80;

            if (this.countMatchedCards == 12) {
                // if (this.countMatchedCards == 1) {
                gameOver = true;
                gameWin = true;
                inputInihibited = true;
                timeBonusScore = (int) (remainingTime * 20);
                System.out.println(timeBonusScore);
            }
            fastSelection = false;

            return true;

        } else {

            if (totalScore > 0) {
                totalScore = totalScore - 10;
                renderingScore = renderingScore - 10;
            }

            Gdx.app.log(TAG, "le carte NON MATCHANO!! " + player.getFirstCardIndex() + " e " + player.getSecondCardIndex()
                    + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getFirstCardIndex()).getID()]
                    + " e " + CardsAsset.instance.cards[deck.getCards().get(player.getSecondCardIndex()).getID()] + ")");

            Gdx.app.log(TAG, "-------------------------------------------------------------------FINE CONFRONTO (NEGATIVO)");

            this.resetSlot();
            fastSelection = false;
//TODO TOGLIERE!!!!
           /* gameOver = true;
            gameWin = true;
            inputInihibited = true;
            timeBonusScore = (int) (remainingTime * 200);
            totalScore =800;
            renderingScore =800;*/


            return false;
        }
    }

    public int getStartGameTime() {
        return startGameTime;
    }

    public void setStartGameTime(int startGameTime) {
        this.startGameTime = startGameTime;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int score) {
        this.totalScore = score;
    }

    public int getRenderingScore() {
        return renderingScore;
    }

    public void setRenderingScore(int renderingScore) {
        this.renderingScore = renderingScore;
    }

    public float getRemainingTime() {
        return remainingTime;
    }

    public Array<PooledEffect> getSpecialEffects() {
        return specialEffects;
    }

    public void setSpecialEffects(Array<PooledEffect> specialEffects) {
        this.specialEffects = specialEffects;
    }

    public void setRemainingTime(float remainingTime) {
        this.remainingTime = remainingTime;
    }

    public float getCountdown() {
        return countdown;
    }

    public int getBonusTimePositionX() {
        return bonusTimePositionX;
    }

    public void setBonusTimePositionX(int bonusTimePositionX) {
        this.bonusTimePositionX = bonusTimePositionX;
    }

    public int getBonusTimePositionY() {
        return bonusTimePositionY;
    }

    public boolean isFastSelection() {
        return fastSelection;
    }

    public void setFastSelection(boolean fastSelection) {
        this.fastSelection = fastSelection;
    }

    public void setBonusTimePositionY(int bonusTimePositionY) {
        this.bonusTimePositionY = bonusTimePositionY;
    }

    public int getCountMatchedCards() {
        return countMatchedCards;
    }

    public void setCountMatchedCards(int countMatchedCards) {
        this.countMatchedCards = countMatchedCards;
    }

    public boolean isContinuePopUpShown() {
        return continuePopUpShown;
    }

    public void setContinuePopUpShown(boolean continuePopUpShown) {
        this.continuePopUpShown = continuePopUpShown;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public float getPLAYER_TIME() {
        return PLAYER_TIME;
    }

    public boolean isInitialCountdown() {
        return initialCountdown;
    }

    public void setInitialCountdown(boolean initialCountdown) {
        this.initialCountdown = initialCountdown;
    }

    public int getTimeBonusScore() {
        return timeBonusScore;
    }

    public void setTimeBonusScore(int timeBonusScore) {
        this.timeBonusScore = timeBonusScore;
    }

    public boolean isExperienceAnimation() {
        return experienceAnimationIsRunning;
    }

    public void setExperienceAnimation(boolean experienceAnimation) {
        this.experienceAnimationIsRunning = experienceAnimation;
    }

    public Deck getDeck() {
        return deck;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isInputInihibited() {
        return inputInihibited;
    }

    public void setInputInihibited(boolean inputInihibited) {
        this.inputInihibited = inputInihibited;
    }

    public boolean isOfflineMode() {
        return offlineMode;
    }

    public void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public ParticleEffect_CardsDisappearingEffect getSpecialEffectPool() {
        return specialEffectPool;
    }

    public void setSpecialEffectPool(ParticleEffect_CardsDisappearingEffect specialEffectPool) {
        this.specialEffectPool = specialEffectPool;
    }

    public ScrollingBackground getBackground() {
        return background;
    }

    public boolean isGameWin() {
        return gameWin;
    }

    public void setGameWin(boolean gameWin) {
        this.gameWin = gameWin;
    }

    public void setBackground(ScrollingBackground background) {
        this.background = background;
    }

    public float getRunTimeRemainingTime() {
        return runTimeRemainingTime;
    }

    public void setRunTimeRemainingTime(float runTimeRemainingTime) {
        this.runTimeRemainingTime = runTimeRemainingTime;
    }

    public RoundEndPopUp getContinueSharingPopUp() {
        return continueSharingPopUp;
    }

    public void setContinueSharingPopUp(RoundEndPopUp roundRecap) {
        this.continueSharingPopUp = roundRecap;
    }

    public void refreshRemainingTime() {

        remainingTime = Math
                .round((PLAYER_TIME - runTimeRemainingTime) * 100.0f) / 100.0f;
        runTimeRemainingTime += Gdx.graphics.getDeltaTime();

        if (remainingTime <= 0) {
            gameOver = true;
            inputInihibited = true;
        }
    }

    public void playFinalCountdown() {

        if (remainingTime < 10.1 && remainingTime > 3.5
                && remainingTime % 1 == 0) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }
        if (remainingTime < 3.1 && remainingTime > 2.1
                && remainingTime % 0.25 == 0) {
            // System.out.println(Math.round(remainingTime / 0.20*10));
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }

        if (remainingTime < 2.1 && Math.round(remainingTime * 50) % 10 == 0) {

            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

			/*
             * System.out.println("Ding di final countdown numero: " + contatore
			 * + " al secondo: " + remainingTime + ". Manipolato diventa: " +
			 * (Math.round(remainingTime * 50) + ". Condizione: ") +
			 * (Math.round(remainingTime * 50) % 10 == 0));
			 */

        }
        /*
         * if (remainingTime < 1.1 && Math.round(remainingTime / 0.20*10)%5==0)
		 * { //System.out.println(Math.round(remainingTime / 0.20*10));
		 * AudioManager.instance
		 * .play(CardsAsset.instance.sounds.finalCountdown);
		 * 
		 * }
		 */

    }

    public void refreshCountdown() {
        countdown = Math.round((COUNTDOWN - (float) (Math
                .round(runTimeCountdown * 10.0f) / 10.0f)) * 10.0f) / 10.0f;
        // System.out.println(countdown);
        if (countdown < 0)
            initialCountdown = false;
        runTimeCountdown += Gdx.graphics.getDeltaTime();
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public ClassicPlayer getPlayers() {
        return player;
    }

    public boolean isBonusTime() {
        return bonusTime;
    }

    public boolean isRenderSpecial() {
        return renderSpecial;
    }

    public void setRenderSpecial(boolean renderSpecial) {
        this.renderSpecial = renderSpecial;
    }

    public void setBonusTime(boolean bonusTime) {
        this.bonusTime = bonusTime;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }


    public boolean isContinuePopUpIsShowing() {
        return continuePopUpIsShowing;
    }

    public void setContinuePopUpIsShowing(boolean continuePopUpIsShowing) {
        this.continuePopUpIsShowing = continuePopUpIsShowing;
    }

    public boolean isEnableSound() {
        return enableSound;
    }

    public void setEnableSound(boolean enableSound) {
        this.enableSound = enableSound;
    }

    public boolean isEnableMusic() {
        return enableMusic;
    }

    public void setEnableMusic(boolean enableMusic) {
        this.enableMusic = enableMusic;
    }

    public void setPLAYER_TIME(float pLAYER_TIME) {
        PLAYER_TIME = pLAYER_TIME;
    }

    public boolean isEnableToCompare() {
        return enableToCompare;
    }

    public void setEnableToCompare(boolean enableToCompare) {
        this.enableToCompare = enableToCompare;
    }

    public void backToMenu() {
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.initialCountdown);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

        memo.setScreen(new OfflineMode(memo, mocdc));
    }

    protected void showContinueSharingPopUp() {

        continueSharingPopUp = new RoundEndPopUp(memo, mocdc, matchID, opponent,
                totalScore, timeBonusScore, Math.round(remainingTime * 10.0f)
                / 10.0f, offlineMode, 1);
        Gdx.input.setInputProcessor(continueSharingPopUp.getStage());
        AudioManager.instance.stopMusic();
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

    }

    public void setCardsPosition() {
        int count = 0;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {

                Vector2 position = new Vector2(22 + 126 * j, 14 + 126 * i);
                deck.getCards().get(count).setPosition(position);
                /*
                 * System.out.println("Carta " + count + " " +
				 * CardsAsset.instance.cards[deck.getCards().get(count)
				 * .getID()]);
				 */
                count += 1;
            }
        }

    }

    public void selectCard(int x, int y, boolean fastSelectionInvocation) {

        if (!fastSelectionInvocation) {
            Gdx.app.log(TAG, "cliccato FISICAMENTE SU " + x + " e " + y);
            cam.unproject(virtualTouch.set(x, y, 0));
            Gdx.app.log(TAG, "cliccato VIRTUALMENTE SU " + Math.round(virtualTouch.x) + " e " + Math.round(virtualTouch.y));
        } else {
            Gdx.app.log(TAG, "ho già trasformato le coordinate!");
        }

        if (!player.selectKidCard(Math.round(virtualTouch.x),
                Math.round(virtualTouch.y), fastSelectionInvocation)
                || this.isInitialCountdown()) {
        } else {
            AudioManager.instance.play(CardsAsset.instance.sounds.flipCard);
            if (player.getSecondCardIndex() == -1) {

                if (deck.getCards().get(player.getFirstCardIndex())
                        .isMovingFaceDown())
                    deck.getCards()
                            .get(player.getFirstCardIndex())
                            .setIndexAnimation(
                                    this.backFramesNumber
                                            + this.faceUpFramesNumber
                                            - 1
                                            - deck.getCards()
                                            .get(player
                                                    .getFirstCardIndex())
                                            .getIndexAnimation());

                deck.getCards().get(player.getFirstCardIndex())
                        .setMovingFaceUp(true);
                deck.getCards().get(player.getFirstCardIndex())
                        .setFaceDown(false);
                deck.getCards().get(player.getFirstCardIndex())
                        .setSelectable(false);
                deck.getCards().get(player.getFirstCardIndex())
                        .setMovingFaceDown(false);

                Gdx.app.log(TAG, "Moving FACE_UP la carta nello slot(FIRST_SLOT)  " + player.getFirstCardIndex()
                        + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getFirstCardIndex()).getID()] + ")");

            } else {

                if (deck.getCards().get(player.getSecondCardIndex())
                        .isMovingFaceDown())
                    deck.getCards()
                            .get(player.getSecondCardIndex())
                            .setIndexAnimation(
                                    this.backFramesNumber
                                            + this.faceUpFramesNumber
                                            - 1
                                            - deck.getCards()
                                            .get(player
                                                    .getSecondCardIndex())
                                            .getIndexAnimation());

                deck.getCards().get(player.getSecondCardIndex())
                        .setMovingFaceUp(true);
                deck.getCards().get(player.getSecondCardIndex())
                        .setFaceDown(false);
                deck.getCards().get(player.getSecondCardIndex())
                        .setSelectable(false);
                deck.getCards().get(player.getSecondCardIndex())
                        .setMovingFaceDown(false);
                Gdx.app.log(TAG, "Moving FACE_UP la carta nello slot(SECOND_SLOT)  " + player.getSecondCardIndex()
                        + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getSecondCardIndex()).getID()] + ")");

            }
        }

    }

    public void doFastSelection(int x, int y) {

        deck.getCards().get(player.getFirstCardIndex()).setMovingFaceUp(false);
        deck.getCards().get(player.getFirstCardIndex()).setFaceUP(true);
        deck.getCards().get(player.getFirstCardIndex()).resetIndexAnimation();

        deck.getCards().get(player.getSecondCardIndex()).setMovingFaceUp(false);
        deck.getCards().get(player.getSecondCardIndex()).setFaceUP(true);
        deck.getCards().get(player.getSecondCardIndex()).resetIndexAnimation();

        Gdx.app.log(TAG, "setto FACE_UP le carte negli slot(SECOND_SLOT)  " + player.getFirstCardIndex() + " e " + player.getSecondCardIndex()
                + " (" + CardsAsset.instance.cards[deck.getCards().get(player.getFirstCardIndex()).getID()]
                + " e " + CardsAsset.instance.cards[deck.getCards().get(player.getSecondCardIndex()).getID()] + ") e invoco il compare dal FAST_SELECTION");

        compareSlots();
        fastSelection = false;
        Gdx.app.log(TAG, "invoco nuovamente il SELECT_CARD dal FAST_SELECTION(coordinate fisiche: " + x + " e " + y + ")");
        selectCard(x, y, true);

    }


    public void resetSlot() {

        deck.getCards().get(player.getFirstCardIndex()).setFaceUP(false);
        deck.getCards().get(player.getSecondCardIndex()).setFaceUP(false);
        // slots[players[0].getFirstSlot()].getCard().setFaceDown(true);
        // slots[players[0].getSecondSlot()].getCard().setFaceDown(true);

        deck.getCards().get(player.getFirstCardIndex()).setMovingFaceDown(true);
        deck.getCards().get(player.getSecondCardIndex())
                .setMovingFaceDown(true);

        deck.getCards().get(player.getFirstCardIndex()).setSelectable(true);
        deck.getCards().get(player.getSecondCardIndex()).setSelectable(true);

        player.setFirstCardIndex(-1);
        player.setSecondCardIndex(-1);
    }


    public void activeDoubleCouple() {
        // System.out.println("EXPLOSIONE");
        boolean bonusHasBeenActived = false;
        int ID = 0;
        int firstCrdIndex = 0;
        Card firstCard, secondCard = null;
        while (!bonusHasBeenActived) {
            firstCard = deck.getCards().get(firstCrdIndex);
            ID = firstCard.getID();
            for (int i = firstCrdIndex + 1; i < 24; i++) {
                if (deck.getCards().get(i).getID() == ID)
                    secondCard = deck.getCards().get(i);
            }
            if (!firstCard.isMatched() && !secondCard.isMatched()) {
                deck.getCardByID(ID)[0].setFaceingUpBecauseOfDoubleCouple(true);
                deck.getCardByID(ID)[0].setSelectable(false);
                deck.getCardByID(ID)[0].setFaceDown(false);
                deck.getCardByID(ID)[1].setFaceingUpBecauseOfDoubleCouple(true);
                deck.getCardByID(ID)[1].setSelectable(false);
                deck.getCardByID(ID)[1].setFaceDown(false);
                bonusHasBeenActived = true;

                if (deck.getCardByID(ID)[0]
                        .isPowerUpped_time()
                        || deck.getCardByID(ID)[1]
                        .isPowerUpped_time())
                    addExtraTime(BONUS_TIME);
                else if (deck.getCardByID(ID)[0]
                        .isPowerUpped_faceUp()
                        || deck.getCardByID(ID)[1]
                        .isPowerUpped_faceUp())
                    activeFaceUp();
            }
            firstCrdIndex++;
        }

    }

    public void addExtraTime(int extratime) {
        bonusTime = true;
        this.PLAYER_TIME += extratime;
        if (remainingTime > 10)
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.finalCountdown);
    }

    public void activeFaceUp() {
        for (int i = 0; i < 24; i++) {
            deck.getCards().get(i).setBonusActived(true);
            deck.getCards().get(i).setFaceDown(false);
            deck.getCards().get(i).setSelectable(false);
        }
        for (Card card : deck.getCards()) {
            if (card.isMovingFaceDown() || card.isMovingFaceUp()
                    || card.isFaceUP() || card.isExploded()) {

                card.setBonusActived(false);
            }

        }

    }

    private void assignPowerUp() {

        if (powerUp[0]) {
            int i = (int) (Math.random() * 11);
            deck.getCards().get(i).setPowerUpped(true);
            deck.getCards().get(i).setPowerUpped_time(true);
        }

        boolean secondAssignmentDone = false;
        if (powerUp[1]) {

            while (!secondAssignmentDone) {
                int i = (int) (Math.random() * 11);
                if (!deck.getCards().get(i).isPowerUpped()) {
                    secondAssignmentDone = true;
                    deck.getCards().get(i).setPowerUpped(true);
                    deck.getCards().get(i).setPowerUpped_faceUp(true);
                }
            }
        }

        boolean thirdAssignmentDone = false;
        if (powerUp[2]) {

            while (!thirdAssignmentDone) {
                int i = (int) (Math.random() * 11);
                if (!deck.getCards().get(i).isPowerUpped()) {
                    thirdAssignmentDone = true;
                    deck.getCards().get(i).setPowerUpped(true);
                    deck.getCards().get(i).setPowerUpped_bomb(true);
                }
            }
        }

    }
}
