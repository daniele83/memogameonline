package com.memogameonline.gametable;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.GamePreferences;
import com.memogameonline.gamehelpers.MOCDataCarrier;
import com.memogameonline.gameobjects.Card;
import com.memogameonline.gameobjects.Deck;
import com.memogameonline.gameobjects.ParticleEffect_CardsDisappearingEffect;
import com.memogameonline.gameobjects.ScrollingBackground;
import com.memogameonline.gamescreens.Home;
import com.memogameonline.gamescreens.RoundEndPopUp;
import com.memogameonline.main.MemoGameOnline;
import com.memogameonline.players.MemobilePlayer;
import com.memogameonline.user.Opponent;

public class Memobile {
    public static final String TAG = Classic.class.getName();
    protected MemoGameOnline memo;
    protected long matchID;
    protected Opponent opponent;

    protected RoundEndPopUp continueSharingPopUp;
    protected boolean continuePopUpShown;
    protected boolean continuePopUpIsShowing;
    protected boolean experienceAnimationIsStarted;
    protected boolean experienceAnimationIsRunning;
    protected boolean inputInihibited;

    private ParticleEffect_CardsDisappearingEffect specialEffectPool;
    private Array<PooledEffect> specialEffects;

    protected int startGameTime; // da convertire in tipo time
    protected int totalScore;
    protected int renderingScore;
    protected int timeBonusScore;
    protected boolean renderSpecial;

    protected OrthographicCamera cam;
    protected Vector3 virtualTouch = new Vector3();

    protected ScrollingBackground background;

    protected MemobilePlayer player;

    protected boolean enableToCompare;

    protected GamePreferences prefs;
    protected boolean enableSound;
    protected boolean enableMusic;
    protected boolean fastSelection;

    protected boolean gameOver;
    protected boolean gameWin;

    protected boolean bonusTimeActive;

    protected int countMatchedCards;

    protected int level;
    protected float PLAYER_TIME;
    protected float remainingTime;
    protected final float COUNTDOWN;
    protected float countdown;
    protected float runTimeCountdown;
    protected float runTimeRemainingTime;

    protected float bonusTimer;
    protected float alpha;
    protected int bonusTimePositionX;
    protected int bonusTimePositionY;

    protected int framesNumber;
    protected int backFramesNumber;
    protected int faceUpFramesNumber;
    protected int faceUpAfterMatchingFramesNumber;
    protected int faceUpAfterSpecialFramesNumber;

    protected float runTime;
    protected float distributionCardTime;
    protected int indexGeneratedCard;

    protected boolean initialCountdownStarted;
    protected boolean initialCountdown;
    protected boolean bonusTime;

    protected Deck deck;
    protected final int ROWS = 6;
    protected final int COLUMNS = 4;

    protected boolean[] powerUp;
    private boolean offlineMode;

    protected Random random;

    protected MOCDataCarrier mocdc;
    private int errors;

    private boolean stopMoving;


    private final int BONUS_TIME = 1;

    public Memobile(MemoGameOnline memo, MOCDataCarrier mocdc, long matchID, Opponent opponent,
                    int GameMode, float time, boolean[] powerUp, boolean offlineMode) {

        this.memo = memo;
        this.mocdc = mocdc;
        this.matchID = matchID;
        this.opponent = opponent;

        this.powerUp = powerUp;
        this.offlineMode = offlineMode;


        PLAYER_TIME = time;
        remainingTime = time;
        COUNTDOWN = 3;
        runTimeCountdown = 0;
        runTimeRemainingTime = 0;
        cam = new OrthographicCamera();
        cam.setToOrtho(true, Constants.VIRTUAL_VIEWPORT_WIDTH,
                Constants.VIRTUAL_VIEWPORT_HEIGHT);
        virtualTouch = new Vector3();
        gameOver = false;
        gameWin = false;

        prefs = GamePreferences.instance;

        background = new ScrollingBackground();

        random = new Random();
        int deckType = random.nextInt(4);
        deck = new Deck(deckType);
        assignPowerUp();
        deck.shuffle();

        framesNumber = deck.getCards().get(1).getNUMBER_FRAMES();
        backFramesNumber = deck.getCards().get(1).getNUMBER_BACK_FRAMES();
        faceUpFramesNumber = deck.getCards().get(1).getNUMBER_FACE_FRAMES();
        faceUpAfterMatchingFramesNumber = deck.getCards().get(1)
                .getNUMBER_FACEUP_REMAINING_TIME_FRAMES();
        faceUpAfterSpecialFramesNumber = deck.getCards().get(1)
                .getNUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES();

        player = new MemobilePlayer(this);
        totalScore = 0;
        renderingScore = 0;
        enableToCompare = false;

        setCardsPosition();
        bonusTimeActive = false;
        bonusTimer = 0;
        alpha = 1;
        bonusTimePositionX = 414;
        bonusTimePositionY = 899;

        runTime = 0;
        distributionCardTime = 0;
        indexGeneratedCard = 0;

        initialCountdownStarted = false;
        initialCountdown = true;
        this.bonusTime = false;
        fastSelection = false;

        specialEffectPool = new ParticleEffect_CardsDisappearingEffect();
        specialEffects = new Array<PooledEffect>();

    }

    public void update(float delta) {
        updateCardsPosition(delta);

        if (!initialCountdownStarted) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.initialCountdown);
            initialCountdownStarted = true;
        }
        if (initialCountdown) {
            refreshCountdown();

        }

        if ((!initialCountdown && !gameOver || gameOver && gameWin && !offlineMode)
                && remainingTime > 0) {
            refreshRemainingTime();
        }

        // playFinalCountdown();

        /*if (bonusTime) {
            bonusTimer += Gdx.graphics.getDeltaTime();
            alpha = 1 - bonusTimer / 2;
            bonusTimePositionX = 6 + Math.round((bonusTimer / 2) * 50);
            bonusTimePositionY = 950 - Math.round((bonusTimer / 2) * 50);
            if (bonusTimer > 2) {
                bonusTime = false;
                bonusTimer = 0;
                bonusTimePositionX = 390;
                bonusTimePositionY = 950;
                alpha = 1;
            }
        }*/


        if (gameOver && !experienceAnimationIsRunning
                && !experienceAnimationIsStarted) {
            // if (remainingTime < 0) {
            System.out.println("Inizio l'animazione dell'esperienza!");
            experienceAnimationIsRunning = true;
            experienceAnimationIsStarted = true;
            AudioManager.instance.play(CardsAsset.instance.sounds.scoreCount);
            // }

        }

        if (gameOver && continuePopUpShown && !continuePopUpIsShowing) {
            System.out.println("Mostro il pop Up di fine round!");
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            continuePopUpShown = false;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
        }

        if (gameOver && !continuePopUpShown && offlineMode) {
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.scoreCount);
            System.out.println("Mostro il pop Up di fine round!");
            continuePopUpShown = true;
            continuePopUpIsShowing = true;
            showContinueSharingPopUp();
        }
    }

    /**
     * Metodo invocato durante il setting di ClassicPlayer.slotSecond
     *
     * @throws InterruptedException
     */
    public boolean compareSlots() {
        System.out.println("Confronto le carte!");
        if (deck.getCards().get(player.getFirstCardIndex()).getID() == deck
                .getCards().get(player.getSecondCardIndex()).getID()) {
            System.out.println("Le carte matchano!");
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.matchingCards);
            this.countMatchedCards++;

            deck.getCards().get(player.getFirstCardIndex()).setFaceUP(false);
            deck.getCards().get(player.getSecondCardIndex()).setFaceUP(false);
            deck.getCards().get(player.getFirstCardIndex())
                    .setDisappearing(true);
            deck.getCards().get(player.getSecondCardIndex())
                    .setDisappearing(true);
            deck.getCards().get(player.getFirstCardIndex()).setMatched(true);
            deck.getCards().get(player.getSecondCardIndex()).setMatched(true);

            if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_stop()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_stop()) {
                stopMoving = true;
            } else if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_time()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_time())
                addExtraTime(BONUS_TIME);
            else if (deck.getCards().get(player.getFirstCardIndex())
                    .isPowerUpped_faceUp()
                    || deck.getCards().get(player.getSecondCardIndex())
                    .isPowerUpped_faceUp())
                activeFaceUp();

            player.setFirstCardIndex(-1);
            player.setSecondCardIndex(-1);

            totalScore += 80;
            renderingScore += 80;

            if (this.countMatchedCards == 12) {
                // if (this.countMatchedCards == 1) {
                gameOver = true;
                gameWin = true;
                inputInihibited = true;
                timeBonusScore = (int) (remainingTime * 20);
                System.out.println(timeBonusScore);
            }
            fastSelection = false;

            return true;

        } else {

            System.out.println("Le carte non matchano!");
            if (totalScore > 0) {
                totalScore = totalScore - 10;
                renderingScore = renderingScore - 10;
            }
            System.out.println("Provo a resettare gli slot!");
            this.resetSlot();
            System.out.println("Resete effettuato!");

            errors += 1;
            if (errors == 4) {
                errors = 0;
                if (!stopMoving) {
                    System.out.println("Provo a scambiare due carte!");
                    swapCards();
                    System.out.println("carte settate per lo swap!");
                }
            }

            fastSelection = false;
            return false;
        }
    }

    public int getStartGameTime() {
        return startGameTime;
    }

    public void setStartGameTime(int startGameTime) {
        this.startGameTime = startGameTime;
    }

    public float getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(float remainingTime) {
        this.remainingTime = remainingTime;
    }

    public float getCountdown() {
        return countdown;
    }

    public int getBonusTimePositionX() {
        return bonusTimePositionX;
    }

    public void setBonusTimePositionX(int bonusTimePositionX) {
        this.bonusTimePositionX = bonusTimePositionX;
    }

    public int getBonusTimePositionY() {
        return bonusTimePositionY;
    }

    public boolean isFastSelection() {
        return fastSelection;
    }

    public void setFastSelection(boolean fastSelection) {
        this.fastSelection = fastSelection;
    }

    public void setBonusTimePositionY(int bonusTimePositionY) {
        this.bonusTimePositionY = bonusTimePositionY;
    }

    public int getCountMatchedCards() {
        return countMatchedCards;
    }

    public void setCountMatchedCards(int countMatchedCards) {
        this.countMatchedCards = countMatchedCards;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public float getPLAYER_TIME() {
        return PLAYER_TIME;
    }

    public boolean isInitialCountdown() {
        return initialCountdown;
    }

    public void setInitialCountdown(boolean initialCountdown) {
        this.initialCountdown = initialCountdown;
    }

    public RoundEndPopUp getContinueSharingPopUp() {
        return continueSharingPopUp;
    }

    public void setContinueSharingPopUp(RoundEndPopUp continueSharingPopUp) {
        this.continueSharingPopUp = continueSharingPopUp;
    }

    public boolean isContinuePopUpShown() {
        return continuePopUpShown;
    }

    public void setContinuePopUpShown(boolean continuePopUpShown) {
        this.continuePopUpShown = continuePopUpShown;
    }

    public boolean isContinuePopUpIsShowing() {
        return continuePopUpIsShowing;
    }

    public void setContinuePopUpIsShowing(boolean continuePopUpIsShowing) {
        this.continuePopUpIsShowing = continuePopUpIsShowing;
    }

    public boolean isExperienceAnimationIsStarted() {
        return experienceAnimationIsStarted;
    }

    public void setExperienceAnimationIsStarted(
            boolean experienceAnimationIsStarted) {
        this.experienceAnimationIsStarted = experienceAnimationIsStarted;
    }

    public boolean isExperienceAnimationIsRunning() {
        return experienceAnimationIsRunning;
    }

    public void setExperienceAnimationIsRunning(
            boolean experienceAnimationIsRunning) {
        this.experienceAnimationIsRunning = experienceAnimationIsRunning;
    }

    public boolean isInputInihibited() {
        return inputInihibited;
    }

    public void setInputInihibited(boolean inputInihibited) {
        this.inputInihibited = inputInihibited;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getRenderingScore() {
        return renderingScore;
    }

    public void setRenderingScore(int renderingScore) {
        this.renderingScore = renderingScore;
    }

    public int getTimeBonusScore() {
        return timeBonusScore;
    }

    public void setTimeBonusScore(int timeBonusScore) {
        this.timeBonusScore = timeBonusScore;
    }

    public boolean isBonusTimeActive() {
        return bonusTimeActive;
    }

    public void setBonusTimeActive(boolean bonusTimeActive) {
        this.bonusTimeActive = bonusTimeActive;
    }

    public float getRunTimeCountdown() {
        return runTimeCountdown;
    }

    public void setRunTimeCountdown(float runTimeCountdown) {
        this.runTimeCountdown = runTimeCountdown;
    }

    public float getRunTimeRemainingTime() {
        return runTimeRemainingTime;
    }

    public void setRunTimeRemainingTime(float runTimeRemainingTime) {
        this.runTimeRemainingTime = runTimeRemainingTime;
    }

    public Array<PooledEffect> getSpecialEffects() {
        return specialEffects;
    }

    public void setSpecialEffects(Array<PooledEffect> specialEffects) {
        this.specialEffects = specialEffects;
    }

    public float getBonusTimer() {
        return bonusTimer;
    }

    public void setBonusTimer(float bonusTimer) {
        this.bonusTimer = bonusTimer;
    }

    public float getRunTime() {
        return runTime;
    }

    public void setRunTime(float runTime) {
        this.runTime = runTime;
    }

    public boolean isInitialCountdownStarted() {
        return initialCountdownStarted;
    }

    public void setInitialCountdownStarted(boolean initialCountdownStarted) {
        this.initialCountdownStarted = initialCountdownStarted;
    }

    public boolean[] getPowerUp() {
        return powerUp;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public void setCountdown(float countdown) {
        this.countdown = countdown;
    }

    public Deck getDeck() {
        return deck;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isOfflineMode() {
        return offlineMode;
    }

    public void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public ScrollingBackground getBackground() {
        return background;
    }

    public boolean isGameWin() {
        return gameWin;
    }

    public void setGameWin(boolean gameWin) {
        this.gameWin = gameWin;
    }

    public void setBackground(ScrollingBackground background) {
        this.background = background;
    }

    public void refreshRemainingTime() {

        remainingTime = Math
                .round((PLAYER_TIME - runTimeRemainingTime) * 100.0f) / 100.0f;
        runTimeRemainingTime += Gdx.graphics.getDeltaTime();

        if (remainingTime <= 0) {
            gameOver = true;
            inputInihibited = true;
        }
    }

    public void playFinalCountdown() {

        if (remainingTime < 10.1 && remainingTime > 3.5
                && remainingTime % 1 == 0) {
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }
        if (remainingTime < 3.1 && remainingTime > 2.1
                && remainingTime % 0.25 == 0) {
            // System.out.println(Math.round(remainingTime / 0.20*10));
            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

        }

        if (remainingTime < 2.1 && Math.round(remainingTime * 50) % 10 == 0) {

            AudioManager.instance
                    .play(CardsAsset.instance.sounds.finalCountdown);

			/*
             * System.out.println("Ding di final countdown numero: " + contatore
			 * + " al secondo: " + remainingTime + ". Manipolato diventa: " +
			 * (Math.round(remainingTime * 50) + ". Condizione: ") +
			 * (Math.round(remainingTime * 50) % 10 == 0));
			 */

        }
        /*
         * if (remainingTime < 1.1 && Math.round(remainingTime / 0.20*10)%5==0)
		 * { //System.out.println(Math.round(remainingTime / 0.20*10));
		 * AudioManager.instance
		 * .play(CardsAsset.instance.sounds.finalCountdown);
		 * 
		 * }
		 */

    }

    public void refreshCountdown() {
        countdown = Math.round((COUNTDOWN - (float) (Math
                .round(runTimeCountdown * 10.0f) / 10.0f)) * 10.0f) / 10.0f;
        // System.out.println(countdown);
        if (countdown < 0)
            initialCountdown = false;
        runTimeCountdown += Gdx.graphics.getDeltaTime();
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public MemobilePlayer getPlayers() {
        return player;
    }

    public boolean isBonusTime() {
        return bonusTime;
    }

    public boolean isRenderSpecial() {
        return renderSpecial;
    }

    public void setRenderSpecial(boolean renderSpecial) {
        this.renderSpecial = renderSpecial;
    }

    public ParticleEffect_CardsDisappearingEffect getSpecialEffectPool() {
        return specialEffectPool;
    }

    public void setSpecialEffectPool(ParticleEffect_CardsDisappearingEffect specialEffectPool) {
        this.specialEffectPool = specialEffectPool;
    }

    public void setBonusTime(boolean bonusTime) {
        this.bonusTime = bonusTime;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }


    public boolean isEnableSound() {
        return enableSound;
    }

    public void setEnableSound(boolean enableSound) {
        this.enableSound = enableSound;
    }

    public boolean isEnableMusic() {
        return enableMusic;
    }

    public void setEnableMusic(boolean enableMusic) {
        this.enableMusic = enableMusic;
    }

    public void setPLAYER_TIME(float pLAYER_TIME) {
        PLAYER_TIME = pLAYER_TIME;
    }

    public boolean isEnableToCompare() {
        return enableToCompare;
    }

    public void setEnableToCompare(boolean enableToCompare) {
        this.enableToCompare = enableToCompare;
    }

    public void backToMenu() {
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.initialCountdown);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.matchingCards);
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

        memo.setScreen(new Home(memo, mocdc));
    }

    public void setCardsPosition() {
        int count = 0;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {

                Vector2 position = new Vector2(22 + 126 * j, 14 + 126 * i);
                deck.getCards().get(count).setPosition(position);
                count += 1;
            }
        }

    }

    public void selectCard(int x, int y,boolean fastSelectionInvocation) {

        if (!fastSelectionInvocation) {
            Gdx.app.log(TAG, "cliccato FISICAMENTE SU " + x + " e " + y);
            cam.unproject(virtualTouch.set(x, y, 0));
            Gdx.app.log(TAG, "cliccato VIRTUALMENTE SU " + Math.round(virtualTouch.x) + " e " + Math.round(virtualTouch.y));
        } else {
            Gdx.app.log(TAG, "ho già trasformato le coordinate!");
        }

        if (!player.selectKidCard((int) Math.round(virtualTouch.x),
                (int) Math.round(virtualTouch.y))
                || this.isInitialCountdown()) {
        } else {
            AudioManager.instance.play(CardsAsset.instance.sounds.flipCard);
            if (player.getSecondCardIndex() == -1) {

                if (deck.getCards().get(player.getFirstCardIndex())
                        .isMovingFaceDown())
                    deck.getCards()
                            .get(player.getFirstCardIndex())
                            .setIndexAnimation(
                                    this.backFramesNumber
                                            + this.faceUpFramesNumber
                                            - 1
                                            - deck.getCards()
                                            .get(player
                                                    .getFirstCardIndex())
                                            .getIndexAnimation());

                deck.getCards().get(player.getFirstCardIndex())
                        .setMovingFaceUp(true);
                deck.getCards().get(player.getFirstCardIndex())
                        .setFaceDown(false);
                deck.getCards().get(player.getFirstCardIndex())
                        .setSelectable(false);
                deck.getCards().get(player.getFirstCardIndex())
                        .setMovingFaceDown(false);

            } else {

                if (deck.getCards().get(player.getSecondCardIndex())
                        .isMovingFaceDown())
                    deck.getCards()
                            .get(player.getSecondCardIndex())
                            .setIndexAnimation(
                                    this.backFramesNumber
                                            + this.faceUpFramesNumber
                                            - 1
                                            - deck.getCards()
                                            .get(player
                                                    .getSecondCardIndex())
                                            .getIndexAnimation());

                deck.getCards().get(player.getSecondCardIndex())
                        .setMovingFaceUp(true);
                deck.getCards().get(player.getSecondCardIndex())
                        .setFaceDown(false);
                deck.getCards().get(player.getSecondCardIndex())
                        .setSelectable(false);
                deck.getCards().get(player.getSecondCardIndex())
                        .setMovingFaceDown(false);

            }
        }

    }

    public void resetSlot() {

        deck.getCards().get(player.getFirstCardIndex()).setFaceUP(false);
        deck.getCards().get(player.getSecondCardIndex()).setFaceUP(false);
        // slots[players[0].getFirstSlot()].getCard().setFaceDown(true);
        // slots[players[0].getSecondSlot()].getCard().setFaceDown(true);

        deck.getCards().get(player.getFirstCardIndex()).setMovingFaceDown(true);
        deck.getCards().get(player.getSecondCardIndex())
                .setMovingFaceDown(true);

        deck.getCards().get(player.getFirstCardIndex()).setSelectable(true);
        deck.getCards().get(player.getSecondCardIndex()).setSelectable(true);

        player.setFirstCardIndex(-1);
        player.setSecondCardIndex(-1);
    }

    public void doFastSelection(int x, int y) {

        deck.getCards().get(player.getFirstCardIndex()).setMovingFaceUp(false);
        deck.getCards().get(player.getFirstCardIndex()).setFaceUP(true);
        deck.getCards().get(player.getFirstCardIndex()).resetIndexAnimation();

        deck.getCards().get(player.getSecondCardIndex()).setMovingFaceUp(false);
        deck.getCards().get(player.getSecondCardIndex()).setFaceUP(true);
        deck.getCards().get(player.getSecondCardIndex()).resetIndexAnimation();

        compareSlots();
        fastSelection = false;
        selectCard(x, y,true);

    }

    public void updateCardsPosition(float delta) {
        //System.out.println("Aggiorno la posizione delle carte!");
        for (int i = 0; i < 24; i++) {
            deck.getCards().get(i).update(delta);
            if (deck.getCards().get(i).isMoving()
                    && (deck.getCards().get(i).getStart()
                    .dst(deck.getCards().get(i).getPosition()) >= deck
                    .getCards().get(i).getDistance())) {
                deck.getCards().get(i).setVelocity(0, 0);
                deck.getCards().get(i).setAcceleration(0, 0);
                deck.getCards().get(i)
                        .setPosition(deck.getCards().get(i).getEnd());
                deck.getCards().get(i).setMoving(false);
                deck.getCards().get(i).setSwapping(false);
                deck.getCards().get(i).setSelectable(true);

            }
        }

    }

    public void moveCardTo(Card card, Vector2 direction) {

        float speed = 500;

        card.setStart(new Vector2(card.getPosition().x, card.getPosition().y));
        card.setEnd(direction);
        Vector2 directionTemp = new Vector2(direction.x, direction.y);
        Vector2 velocity = directionTemp.sub(card.getStart()).nor().scl(speed);
        card.setVelocity(velocity);
        card.setDistance(card.getStart().dst(card.getEnd()));
        card.setMoving(true);

    }

    public void swapCards() {

        boolean firstCardFound = false;
        int firstCardIndex = (int) (Math.random() * 24);
        while (!firstCardFound) {
            System.out.println("Determino la prima carta!");
            if (deck.getCards().get(firstCardIndex).isSelectable()
                    && !deck.getCards().get(firstCardIndex).isSwapping()) {
                firstCardFound = true;
                deck.getCards().get(firstCardIndex).setSwapping(true);
                deck.getCards().get(firstCardIndex).setSelectable(false);
            } else
                firstCardIndex = (int) (Math.random() * 24);
        }

        System.out.println("Prima carta determinata!");
        boolean secondCardFound = false;
        int secondCardIndex = (int) (Math.random() * 24);
        while (!secondCardFound) {
            System.out.println("Determino la seconda carta!");
            if (deck.getCards().get(secondCardIndex).isSelectable()
                    && !deck.getCards().get(secondCardIndex).isSwapping()) {
                secondCardFound = true;
                deck.getCards().get(secondCardIndex).setSwapping(true);
                deck.getCards().get(secondCardIndex).setSelectable(true);
            } else
                secondCardIndex = (int) (Math.random() * 24);
        }
        System.out.println("Seconda Carta determinata!");
        Vector2 firstCardPosition = new Vector2(deck.getCards()
                .get(firstCardIndex).getPosition().x, deck.getCards()
                .get(firstCardIndex).getPosition().y);
        Vector2 secondCardPosition = new Vector2(deck.getCards()
                .get(secondCardIndex).getPosition().x, deck.getCards()
                .get(secondCardIndex).getPosition().y);

        moveCardTo(deck.getCards().get(firstCardIndex), secondCardPosition);
        moveCardTo(deck.getCards().get(secondCardIndex), firstCardPosition);

    }

    protected void showContinueSharingPopUp() {

        continueSharingPopUp = new RoundEndPopUp(memo, mocdc, matchID, opponent,
                totalScore, timeBonusScore, Math.round(remainingTime * 10.0f)
                / 10.0f, offlineMode, 2);
        Gdx.input.setInputProcessor(continueSharingPopUp.getStage());
        AudioManager.instance.stopMusic();
        AudioManager.instance
                .stopSound(CardsAsset.instance.sounds.finalCountdown);

    }

    public void addExtraTime(int extratime) {
        bonusTime = true;
        this.PLAYER_TIME += extratime;
        if (remainingTime > 10)
            AudioManager.instance
                    .stopSound(CardsAsset.instance.sounds.finalCountdown);
    }

    public void activeFaceUp() {
        for (int i = 0; i < 24; i++) {
            deck.getCards().get(i).setBonusActived(true);
            deck.getCards().get(i).setFaceDown(false);
            deck.getCards().get(i).setSelectable(false);
        }
        for (Card card : deck.getCards()) {
            if (card.isMovingFaceDown() || card.isMovingFaceUp()
                    || card.isFaceUP() || card.isExploded()) {

                card.setBonusActived(false);
            }

        }

    }

    private void assignPowerUp() {

        if (powerUp[0]) {
            int i = (int) (Math.random() * 11);
            deck.getCards().get(i).setPowerUpped(true);
            deck.getCards().get(i).setPowerUpped_time(true);
        }

        boolean secondAssignmentDone = false;
        if (powerUp[1]) {

            while (!secondAssignmentDone) {
                int i = (int) (Math.random() * 11);
                if (!deck.getCards().get(i).isPowerUpped()) {
                    secondAssignmentDone = true;
                    deck.getCards().get(i).setPowerUpped(true);
                    deck.getCards().get(i).setPowerUpped_faceUp(true);
                }
            }
        }

        boolean thirdAssignmentDone = false;
        if (powerUp[2]) {

            while (!thirdAssignmentDone) {
                int i = (int) (Math.random() * 11);
                if (!deck.getCards().get(i).isPowerUpped()) {
                    thirdAssignmentDone = true;
                    deck.getCards().get(i).setPowerUpped(true);
                    deck.getCards().get(i).setPowerUpped_stop(true);
                }
            }
        }

    }
}
