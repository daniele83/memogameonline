package com.memogameonline.gametable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.memogameonline.gamehelpers.AudioManager;
import com.memogameonline.gamehelpers.CardsAsset;
import com.memogameonline.gamehelpers.Constants;
import com.memogameonline.gamehelpers.LevelManagement;
import com.memogameonline.gameobjects.ExperienceBar;
import com.memogameonline.gameobjects.TimeIcon;
import com.memogameonline.user.User;

public class MemoFiendsRenderer {
    public static final String TAG = ClassicRenderer.class.getName();
    private MemoFiends table;

    private OrthographicCamera cam;

    private StretchViewport viewport;
    private SpriteBatch batcher;

    private BitmapFont zighia24;
    private BitmapFont zighia28;
    private BitmapFont zighia36;
    protected BitmapFont zighia37;
    protected BitmapFont zighia48;

    private final float PLAYER_TIME;

    public final float COUNTDOWN;

    private String difficulty;
    private int cardsNumber;

    private int framesNumber;
    private int backFramesNumber;
    private int faceUpFramesNumber;
    private int faceUpAfterMatchingFramesNumber;
    private int faceUpAfterSpecialFramesNumber;

    protected float countScore;
    protected float popUpAngle;

    protected float experienceIncreasingSpeed;
    protected float scoreCounter;
    protected float scoreDecreaser;
    protected int scoreDecreasingSpeed;
    protected float timeCounter;
    protected float timeDecreasingSpeed;
    protected float timeDecreaser;
    protected float countTime;
    protected float runTime;
    protected TimeIcon timeIcon;
    protected float angle;
    protected float experienceBarAngle;
    protected ExperienceBar experienceBar;
    private boolean calculatedTimeBonusScore;
    private boolean levelUp;
    private boolean levelUpResetted;
    private int newScore;
    private int newTimeBonusScore;
    private ShapeRenderer shaper;

    public MemoFiendsRenderer(MemoFiends table, OrthographicCamera cam,
                              SpriteBatch batcher) {

        this.table = table;

        framesNumber = table.getGrid().get(1).getCard().getNUMBER_FRAMES();
        backFramesNumber = table.getGrid().get(1).getCard()
                .getNUMBER_BACK_FRAMES();
        faceUpFramesNumber = table.getGrid().get(1).getCard()
                .getNUMBER_FACE_FRAMES();
        faceUpAfterMatchingFramesNumber = table.getGrid().get(1).getCard()
                .getNUMBER_FACEUP_REMAINING_TIME_FRAMES();
        faceUpAfterSpecialFramesNumber = table.getGrid().get(1).getCard()
                .getNUMBER_FACEUP_SPECIAL_REMAINING_TIME_FRAMES();

        zighia24 = CardsAsset.instance.fonts.zighia24;
        zighia28 = CardsAsset.instance.fonts.zighia28;
        zighia36 = CardsAsset.instance.fonts.zighia36;
        zighia37 = CardsAsset.instance.fonts.zighia37;
        zighia48 = CardsAsset.instance.fonts.zighia48;

        zighia24.setColor(0f, 0f, 0f, 1f);
        zighia28.setColor(0f, 0f, 0f, 1f);
        zighia36.setColor(0f, 0f, 0f, 1f);

        this.cam = cam;
        this.batcher = batcher;
        this.cam.update();

        cardsNumber = table.getCardsNumber();
        PLAYER_TIME = table.getPLAYER_TIME();
        COUNTDOWN = table.getCountdown();

        runTime = 0;
        timeIcon = new TimeIcon();
        angle = 0;
        experienceBar = new ExperienceBar();

        shaper = new ShapeRenderer();
        shaper.setAutoShapeType(true);

    }


    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (table.isInitialCountdown()) {

            batcher.begin();
            renderBackground();
            renderBack();
            renderInitialCountdown();
            renderTopBar();
            renderExperienceBar();
            renderLevelIcon();
            renderScore();
            renderTime();
            // renderGrid();
            // renderCardsLimits();
            batcher.end();

        } else {

            batcher.begin();
            renderBackground();
            renderBack();
            renderFace();
            renderFaceUpAnimation();
            renderFaceDownAnimation();

            // renderSelectedCards();
            // renderLoader();
            renderTopBar();
            if (table.isGameOver() && table.isExperienceAnimationIsRunning() && !table.isOfflineMode()) {
                if (table.getRemainingTime() > 0
                        || !this.calculatedTimeBonusScore) {
                    renderExperienceAnimationTimeBased(delta);
                } else {
                    if (!levelUpResetted) {
                        levelUp = false;
                        levelUpResetted = true;
                    }
                    renderExperienceAnimationScoreBased(delta);
                }
            }
            renderExperienceBar();
            renderLevelIcon();
            renderScore();
            renderTime();

            renderSpecialEffect(delta);
            // renderGrid();
            // renderCardsLimits();
            batcher.end();

            if (table.isGameOver() && table.isContinuePopUpIsShowing()) {
                renderContinuePopUp(delta);
            }

        }

    }

    private void renderBackground() {

        batcher.draw(CardsAsset.instance.background, 0, 0);

    }

    private void renderTopBar() {
        batcher.draw(CardsAsset.instance.topBar, 0, 766);
    }

    private void renderScore() {

        if (table.getRenderingScore() < 10)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 266f, 848);
        else if (table.getRenderingScore() > 9
                && table.getRenderingScore() < 99)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 257.5f, 848);
        else if (table.getRenderingScore() > 99
                && table.getRenderingScore() < 999)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 251f, 848);
        else if (table.getRenderingScore() > 999)
            zighia48.draw(batcher, "" + table.getRenderingScore(), 245f, 848);
        else if (table.isGameOver() && table.getRemainingTime() < 0)
            zighia48.draw(batcher, "0", 266f, 848);

    }

    private void renderTime() {

        if (table.getRemainingTime() > 0)
            angle = 360 * (1 - table.getRemainingTime()
                    / table.getPLAYER_TIME());

        if (table.isInitialCountdown()) {
            timeIcon.draw(batcher, 450, 790, 0);
            zighia48.draw(batcher, "" + PLAYER_TIME, 464, 844);
        } else {
            if (table.getRemainingTime() >= 0.1
                    && table.getRemainingTime() < 10) {
                timeIcon.draw(batcher, 450, 790, angle);
                zighia48.draw(batcher,
                        "" + Math.round(table.getRemainingTime() * 10.0f)
                                / 10.0f, 473, 844);
            } else if (table.getRemainingTime() >= 10) {
                timeIcon.draw(batcher, 450, 790, angle);
                zighia48.draw(batcher,
                        "" + Math.round(table.getRemainingTime() * 10.0f)
                                / 10.0f, 464, 844);
            } else {
                zighia48.draw(batcher, "0.0", 472, 844);

            }
            //zighia48.setColor(0, 0, 0, 1);
        }
    }

    private void renderExperienceBar() {
        if (!table.isExperienceAnimationIsRunning()|| table.isOfflineMode()) {
            batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
            experienceBar.draw(batcher, 8.3f, 791f,
                    360 - 360 * LevelManagement.instance
                            .getPercentageIncrease(User.data.experience));
        }
        batcher.draw(CardsAsset.instance.experienceBarSeparetor, 8.3f, 790f);
    }

    private void renderLevelIcon() {

        if (User.data.level > 9)
            zighia37.draw(batcher, "" + User.data.level, 36.5f, 842);
        else
            zighia48.draw(batcher, "" + User.data.level, 42, 845);//42, 845

    }


    private void renderBack() {


        for (int i = 0; i < table.getCardsNumber(); i++) {
            if (table.getGrid().get(i).getCard().isFaceDown()
                    && table.getGrid().get(i).getCard().isVisible()) {

                if (!table.getGrid().get(i).getCard().isPowerUpped())
                    batcher.draw(CardsAsset.instance.cardBackRed, table
                            .getGrid().get(i).getPosition().x, table.getGrid()
                            .get(i).getPosition().y, 58, 0, 116, 116, 1, 1, 0);
                else if (table.getGrid().get(i).getCard().isPowerUpped_chain())
                    batcher.draw(CardsAsset.instance.cardBackRedChain, table
                            .getGrid().get(i).getPosition().x, table.getGrid()
                            .get(i).getPosition().y, 58, 0, 116, 116, 1, 1, 0);
                else if (table.getGrid().get(i).getCard().isPowerUpped_time())
                    batcher.draw(CardsAsset.instance.cardBackRedTime, table
                            .getGrid().get(i).getPosition().x, table.getGrid()
                            .get(i).getPosition().y, 58, 0, 116, 116, 1, 1, 0);
                else if (table.getGrid().get(i).getCard().isPowerUpped_bonus())
                    batcher.draw(CardsAsset.instance.cardBackRedBonus, table
                            .getGrid().get(i).getPosition().x, table.getGrid()
                            .get(i).getPosition().y, 58, 0, 116, 116, 1, 1, 0);
            }


        }


    }

    private void renderFace() {

        for (int i = 0; i < table.getCardsNumber(); i++) {
            if (table.getGrid().get(i).getCard().isFaceUP()
                    && table.getGrid().get(i).getCard().isVisible()) {

                batcher.draw(CardsAsset.instance.cards[table.getGrid().get(i)
                                .getCard().getID()], table.getGrid().get(i)
                                .getPosition().x,
                        table.getGrid().get(i).getPosition().y, 58, 0, 116,
                        116, 1, 1, 0);
            }
        }

    }

    private void renderFaceUpAnimation() {

        for (int i = 0; i < table.getCardsNumber(); i++) {

            if (table.getGrid().get(i).getCard().isMovingFaceUp()
                    && table.getGrid().get(i).getCard().isVisible()) {

                if (table.getGrid().get(i).getCard().getIndexAnimation() < this.backFramesNumber) {
                    if (!table.getGrid().get(i).getCard().isPowerUpped())
                        batcher.draw(CardsAsset.instance.cardBackRed, table
                                        .getGrid().get(i).getPosition().x, table
                                        .getGrid().get(i).getPosition().y, 58, 0, 116,
                                116, table.getGrid().get(i).getCard()
                                        .getAnimationArray()[table.getGrid()
                                        .get(i).getCard().getIndexAnimation()],
                                1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_chain())
                        batcher.draw(CardsAsset.instance.cardBackRedChain,
                                table.getGrid().get(i).getPosition().x, table
                                        .getGrid().get(i).getPosition().y, 58,
                                0, 116, 116, table.getGrid().get(i).getCard()
                                        .getAnimationArray()[table.getGrid()
                                        .get(i).getCard().getIndexAnimation()],
                                1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_time())
                        batcher.draw(CardsAsset.instance.cardBackRedTime, table
                                        .getGrid().get(i).getPosition().x, table
                                        .getGrid().get(i).getPosition().y, 58, 0, 116,
                                116, table.getGrid().get(i).getCard()
                                        .getAnimationArray()[table.getGrid()
                                        .get(i).getCard().getIndexAnimation()],
                                1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_bonus())
                        batcher.draw(CardsAsset.instance.cardBackRedBonus,
                                table.getGrid().get(i).getPosition().x, table
                                        .getGrid().get(i).getPosition().y, 58,
                                0, 116, 116, table.getGrid().get(i).getCard()
                                        .getAnimationArray()[table.getGrid()
                                        .get(i).getCard().getIndexAnimation()],
                                1, 0);
                } else {
                    batcher.draw(
                            CardsAsset.instance.cards[table.getGrid().get(i)
                                    .getCard().getID()], table.getGrid().get(i)
                                    .getPosition().x, table.getGrid().get(i)
                                    .getPosition().y, 58, 0, 116, 116, table
                                    .getGrid().get(i).getCard()
                                    .getAnimationArray()[table.getGrid().get(i)
                                    .getCard().getIndexAnimation()], 1, 0);
                }

                table.getGrid().get(i).getCard().incrementIndexAnimation();

                if (table.getGrid().get(i).getCard().getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber
                        + this.faceUpAfterMatchingFramesNumber - 1) {

                    table.getGrid().get(i).getCard().setMovingFaceUp(false);
                    table.getGrid().get(i).getCard().setFaceUP(true);
                    table.getGrid().get(i).getCard().resetIndexAnimation();

                }
            }
        }

    }

    private void renderFaceDownAnimation() {

        for (int i = 0; i < table.getCardsNumber(); i++) {

            if (table.getGrid().get(i).getCard().isMovingFaceDown()
                    && table.getGrid().get(i).getCard().isVisible()) {

                if (table.getGrid().get(i).getCard().getIndexAnimation() < this.faceUpFramesNumber)
                    batcher.draw(
                            CardsAsset.instance.cards[table.getGrid().get(i)
                                    .getCard().getID()], table.getGrid().get(i)
                                    .getPosition().x, table.getGrid().get(i)
                                    .getPosition().y, 58, 0, 116, 116, table
                                    .getGrid().get(i).getCard()
                                    .getAnimationArray()[this.backFramesNumber
                                    + this.faceUpFramesNumber
                                    - 1
                                    - table.getGrid().get(i).getCard()
                                    .getIndexAnimation()], 1, 0);
                else {
                    if (!table.getGrid().get(i).getCard().isPowerUpped())
                        batcher.draw(
                                CardsAsset.instance.cardBackRed,
                                table.getGrid().get(i).getPosition().x,
                                table.getGrid().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getGrid().get(i).getCard()
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getGrid().get(i).getCard()
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_chain())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedChain,
                                table.getGrid().get(i).getPosition().x,
                                table.getGrid().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getGrid().get(i).getCard()
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getGrid().get(i).getCard()
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_time())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedTime,
                                table.getGrid().get(i).getPosition().x,
                                table.getGrid().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getGrid().get(i).getCard()
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getGrid().get(i).getCard()
                                        .getIndexAnimation()], 1, 0);
                    else if (table.getGrid().get(i).getCard()
                            .isPowerUpped_bonus())
                        batcher.draw(
                                CardsAsset.instance.cardBackRedBonus,
                                table.getGrid().get(i).getPosition().x,
                                table.getGrid().get(i).getPosition().y,
                                58,
                                0,
                                116,
                                116,
                                table.getGrid().get(i).getCard()
                                        .getAnimationArray()[this.backFramesNumber
                                        + this.faceUpFramesNumber
                                        - 1
                                        - table.getGrid().get(i).getCard()
                                        .getIndexAnimation()], 1, 0);
                }

                table.getGrid().get(i).getCard().incrementIndexAnimation();

                if (table.getGrid().get(i).getCard().getIndexAnimation() == this.backFramesNumber
                        + this.faceUpFramesNumber - 1) {
                    table.getGrid().get(i).getCard().setMovingFaceDown(false);
                    table.getGrid().get(i).getCard().setFaceDown(true);
                    table.getGrid().get(i).getCard().setSelectable(true);
                    table.getGrid().get(i).getCard().resetIndexAnimation();

                }

            }
        }
    }


    private void renderSelectedCards() {

        for (int i = 0; i < cardsNumber; i++) {
            if (table.getGrid().get(i).getCard().isSelected()) {
                if (!table.getGrid().get(i).getCard().isPowerUpped())
                    batcher.draw(CardsAsset.instance.cardBackRed, table
                                    .getGrid().get(i).getPosition().x, table.getGrid()
                                    .get(i).getPosition().y, 50f, 70f, 116f, 116f, 1,
                            1, table.getGrid().get(i).getCard()
                                    .getShakingArray()[table.getGrid().get(i)
                                    .getCard().getIndexShaking()]);
                else if (table.getGrid().get(i).getCard().isPowerUpped_chain())
                    batcher.draw(CardsAsset.instance.cardBackRedChain, table
                                    .getGrid().get(i).getPosition().x, table.getGrid()
                                    .get(i).getPosition().y, 50f, 70f, 116f, 116f, 1,
                            1, table.getGrid().get(i).getCard()
                                    .getShakingArray()[table.getGrid().get(i)
                                    .getCard().getIndexShaking()]);
                else if (table.getGrid().get(i).getCard().isPowerUpped_time())
                    batcher.draw(CardsAsset.instance.cardBackRedTime, table
                                    .getGrid().get(i).getPosition().x, table.getGrid()
                                    .get(i).getPosition().y, 50f, 70f, 116f, 116f, 1,
                            1, table.getGrid().get(i).getCard()
                                    .getShakingArray()[table.getGrid().get(i)
                                    .getCard().getIndexShaking()]);
                else if (table.getGrid().get(i).getCard().isPowerUpped_bonus())
                    batcher.draw(CardsAsset.instance.cardBackRedBonus, table
                                    .getGrid().get(i).getPosition().x, table.getGrid()
                                    .get(i).getPosition().y, 50f, 70f, 116f, 116f, 1,
                            1, table.getGrid().get(i).getCard()
                                    .getShakingArray()[table.getGrid().get(i)
                                    .getCard().getIndexShaking()]);

                table.getGrid().get(i).getCard().incrementIndexShaking();

                if (table.getGrid().get(i).getCard().getIndexShaking() == 29) {
                    table.getGrid().get(i).getCard().resetIndexShaking();
                }
            }

        }

    }

    public void resize(int width, int height) {

        // viewport.update(width, height, true);

    }

    public void renderInitialCountdown() {

        if (table.getCountdown() > 2.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[2], 82, 350);
        } else if (table.getCountdown() > 1.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[1], 82, 350);
        } else if (table.getCountdown() > 0.1) {

            batcher.draw(CardsAsset.instance.initialCountdown[0], 82, 350);
        }
        // fontBig.draw(batcher, "" + table.getCountdown(), 50, 650);
    }

    public void renderBanner() {

        batcher.draw(CardsAsset.instance.banner, 0, 0);
    }

    public void renderSpecialEffect(float delta) {

        for (int i = table.getStars().size - 1; i >= 0; i--) {
            PooledEffect effect = table.getStars().get(i);
            effect.draw(batcher, delta);
            if (effect.isComplete()) {
                effect.free();
                table.getStars().removeIndex(i);
            }

        }

        for (int i = table.getCardSelectorEffect().size - 1; i >= 0; i--) {
            PooledEffect effect = table.getCardSelectorEffect().get(i);
            effect.draw(batcher, delta);
            if (effect.isComplete()) {
                effect.free();
                table.getCardSelectorEffect().removeIndex(i);
            }

        }
    }

    public void renderExperienceAnimationTimeBased(float delta) {

        timeDecreasingSpeed = (float) table.getTimeBonusScore() / 10000;

        experienceIncreasingSpeed = (int) ((5.2f / 10f) * table
                .getTimeBonusScore());

        if (timeCounter * experienceIncreasingSpeed < table.getTimeBonusScore()
                && !levelUp) {
            timeCounter += Gdx.graphics.getDeltaTime();
            timeDecreaser += Gdx.graphics.getDeltaTime();
        } else if (timeCounter * experienceIncreasingSpeed < newTimeBonusScore
                && levelUp) {
            timeCounter += Gdx.graphics.getDeltaTime();
            timeDecreaser += Gdx.graphics.getDeltaTime();
        } else if (!levelUp && !calculatedTimeBonusScore) {
            System.out
                    .println("Esperienza aumentata (in base al tempo residuo) !!!");
            System.out.println("Vecchia Esperienza: " + User.data.experience);
            System.out.println("Esperienza acquisita: "
                    + (int) (timeCounter * experienceIncreasingSpeed));
            User.data.experience += (int) (timeCounter * experienceIncreasingSpeed);
            System.out.println("Esperienza Complessiva: "
                    + User.data.experience);
            this.calculatedTimeBonusScore = true;
            return;
        } else if (levelUp && !calculatedTimeBonusScore) {
            System.out
                    .println("Esperienza aumentata (in base al tempo residuo) !!!");
            User.data.experience = (int) (timeCounter * experienceIncreasingSpeed);
            this.calculatedTimeBonusScore = true;
            return;
        }

        if (timeCounter * experienceIncreasingSpeed < table.getTimeBonusScore()
                && experienceBarAngle < 0) {
            levelUp = true;
            User.data.experience = 1;
            User.data.level += 1;
            System.out.println("LevelUP grazie al TIME!!!! Nuovo Livello: "
                    + User.data.level);
            newTimeBonusScore = table.getTimeBonusScore()
                    - (int) (timeCounter * experienceIncreasingSpeed);
            timeCounter = 0;
        }
        experienceBarAngle = 360 - 360 * (LevelManagement.instance
                .getPercentageIncrease(User.data.experience) + LevelManagement.instance
                .getPercentageIncrease(timeCounter * experienceIncreasingSpeed));

        batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
        experienceBar.draw(batcher, 8.3f, 791f, experienceBarAngle);

        if (table.getRemainingTime() > 0)
            table.setRunTimeRemainingTime(table.getRunTimeRemainingTime()
                    + timeDecreaser * timeDecreasingSpeed);

		/*
         * if (table.getRemainingTime() < 0) { if (!levelUp) {
		 * User.data.experience += table.getTimeBonusScore(); return; } else {
		 * User.data.experience = (int) (timeCounter *
		 * experienceIncreasingSpeed); return; }
		 * 
		 * }
		 */

    }

    public void renderExperienceAnimationScoreBased(float delta) {

        scoreDecreasingSpeed = (int) ((3.7f / 10f) * table.getTotalScore());
        if (scoreCounter * scoreDecreasingSpeed < table.getTotalScore()
                && !levelUp) {
            scoreCounter += Gdx.graphics.getDeltaTime();
            scoreDecreaser += Gdx.graphics.getDeltaTime();
        } else if (scoreCounter * scoreDecreasingSpeed < newScore && levelUp) {
            scoreCounter += Gdx.graphics.getDeltaTime();
            scoreDecreaser += Gdx.graphics.getDeltaTime();
        } else if (!levelUp) {
            User.data.experience += (int) (scoreCounter * scoreDecreasingSpeed);
            table.setExperienceAnimationIsRunning(false);
            table.setContinuePopUpShown(true);
            return;
        } else {
            User.data.experience = (int) (scoreCounter * scoreDecreasingSpeed);
            System.out
                    .println("Esperienza aumentata con level up!!! Esperienza corrente: "
                            + User.data.experience);
            System.out.println("ScoreCoutner: " + scoreCounter);
            System.out
                    .println("ScoreDecreasing Speed: " + scoreDecreasingSpeed);

            table.setExperienceAnimationIsRunning(false);
            table.setContinuePopUpShown(true);
            return;
        }

        if (scoreCounter * scoreDecreasingSpeed < table.getTotalScore()
                && experienceBarAngle < 0) {
            levelUp = true;
            User.data.experience = 1;
            User.data.level += 1;
            System.out.println("LevelUP Grazie allo Score!! Nuovo Livello: "
                    + User.data.level);
            newScore = table.getTotalScore() - (int) scoreCounter
                    * scoreDecreasingSpeed;
            scoreCounter = 0;
        }
        experienceBarAngle = 360 - 360 * (LevelManagement.instance
                .getPercentageIncrease(User.data.experience) + LevelManagement.instance
                .getPercentageIncrease(scoreCounter * scoreDecreasingSpeed));

        batcher.draw(CardsAsset.instance.experienceBarContainer, 8.3f, 790);
        experienceBar.draw(batcher, 8.3f, 791f, experienceBarAngle);

        if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) < 10
                && (table.getTotalScore() - scoreDecreaser
                * scoreDecreasingSpeed) > 0)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 9
                && (table.getTotalScore() - scoreDecreaser
                * scoreDecreasingSpeed) < 99)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 99)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) > 999)
            table.setRenderingScore((int) (table.getTotalScore() - scoreDecreaser
                    * scoreDecreasingSpeed));
        else if ((table.getTotalScore() - scoreDecreaser * scoreDecreasingSpeed) < 0)
            table.setRenderingScore(0);

    }

    public void renderContinuePopUp(float delta) {

        table.getContinueSharingPopUp().getStage().act(delta);
        table.getContinueSharingPopUp().getStage().draw();

    }

    private void renderGrid() {

        shaper.begin();
        shaper.setColor(0, 0, 0, 1);

        for (int i = 0; i < table.getCardsNumber(); i++)

            shaper.rect(table.getGrid().get(i).getPosition().x, table.getGrid()
                    .get(i).getPosition().y, 116, 116);

        shaper.end();

    }

    private void renderCardsLimits() {

        shaper.begin();
        shaper.setColor(Color.RED);
        for (int i = 0; i < 24; i++) {
            if (table.getGrid().get(i).isLimitedOnBottomLeft()
                    || !table.getGrid().get(i).getNeighbors()[0].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 39, table
                                .getGrid().get(i).getPosition().y,
                        table.getGrid().get(i).getPosition().x, table.getGrid()
                                .get(i).getPosition().y + 39);
            if (table.getGrid().get(i).isLimitedOnBottom()
                    || !table.getGrid().get(i).getNeighbors()[1].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 39, table
                                .getGrid().get(i).getPosition().y,
                        table.getGrid().get(i).getPosition().x + 78, table
                                .getGrid().get(i).getPosition().y);
            if (table.getGrid().get(i).isLimitedOnBottomRight()
                    || !table.getGrid().get(i).getNeighbors()[2].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 77, table
                                .getGrid().get(i).getPosition().y,
                        table.getGrid().get(i).getPosition().x + 116, table
                                .getGrid().get(i).getPosition().y + 39);
            if (table.getGrid().get(i).isLimitedOnRight()
                    || !table.getGrid().get(i).getNeighbors()[3].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 116, table
                        .getGrid().get(i).getPosition().y + 38, table.getGrid()
                        .get(i).getPosition().x + 116, table.getGrid().get(i)
                        .getPosition().y + 77);
            if (table.getGrid().get(i).isLimitedOnTopRight()
                    || !table.getGrid().get(i).getNeighbors()[4].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 116, table
                        .getGrid().get(i).getPosition().y + 77, table.getGrid()
                        .get(i).getPosition().x + 77, table.getGrid().get(i)
                        .getPosition().y + 116);
            if (table.getGrid().get(i).isLimitedOnTop()
                    || !table.getGrid().get(i).getNeighbors()[5].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x + 39, table
                        .getGrid().get(i).getPosition().y + 116, table
                        .getGrid().get(i).getPosition().x + 77, table.getGrid()
                        .get(i).getPosition().y + 116);
            if (table.getGrid().get(i).isLimitedOnTopLeft()
                    || !table.getGrid().get(i).getNeighbors()[6].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x, table
                        .getGrid().get(i).getPosition().y + 77, table.getGrid()
                        .get(i).getPosition().x + 38, table.getGrid().get(i)
                        .getPosition().y + 116);
            if (table.getGrid().get(i).isLimitedOnLeft()
                    || !table.getGrid().get(i).getNeighbors()[7].isEmpty())
                shaper.line(table.getGrid().get(i).getPosition().x, table
                        .getGrid().get(i).getPosition().y + 38, table.getGrid()
                        .get(i).getPosition().x, table.getGrid().get(i)
                        .getPosition().y + 77);
        }
        shaper.end();

    }

}
