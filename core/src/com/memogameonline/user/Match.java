package com.memogameonline.user;

import com.badlogic.gdx.utils.Array;
import com.memogameonline.gameobjects.ChatMessage;

//prima modifica 2222
public class Match {

    private long matchID;
    private Opponent opponent;

    private int currentRound;
    private int tiedPoints;

    private int FirstRoundHeroScore;
    private int SecondRoundHeroScore;
    private int ThirdRoundHeroScore;
    private int FirstRoundOpponentScore;
    private int SecondRoundOpponentScore;
    private int ThirdRoundOpponentScore;
    private int totalHeroScore;
    private int totalOpponentScore;

    private int heroCoins;
    private int opponentCoins;

    private boolean yourTurn;
    private boolean waitingForOpponentTurn;
    private boolean finished;
    private boolean finishedForGiveUp;
    private boolean matchWon;

    private boolean readyToDiscoverResult;
    private boolean coinsAlreadyAssigned;

    private Array<ChatMessage> chatMessages;

    public Match() {
    }

    public Match(long matchID, Opponent opponent) {

        this.matchID = matchID;
        this.opponent = opponent;

        yourTurn = true;
        waitingForOpponentTurn = false;
        finished = false;

        currentRound = 1;

        FirstRoundHeroScore = -1;
        SecondRoundHeroScore = -1;
        ThirdRoundHeroScore = -1;
        FirstRoundOpponentScore = -1;
        SecondRoundOpponentScore = -1;
        ThirdRoundOpponentScore = -1;

        totalHeroScore = 0;
        totalOpponentScore = 0;

        heroCoins = -1;
        opponentCoins = -1;

        tiedPoints = 0;

        readyToDiscoverResult = false;

        chatMessages = new Array<ChatMessage>();

    }

    public Match(Match match) {

        this.matchID = match.getMatchID();
        this.opponent = match.getOpponent();

        this.currentRound = match.getCurrentRound();

        this.FirstRoundHeroScore = match.getFirstRoundHeroScore();
        this.SecondRoundHeroScore = match.getSecondRoundHeroScore();
        this.ThirdRoundHeroScore = match.getThirdRoundHeroScore();

        this.FirstRoundOpponentScore = match.getFirstRoundOpponentScore();
        this.SecondRoundOpponentScore = match.getSecondRoundOpponentScore();
        this.ThirdRoundOpponentScore = match.getThirdRoundOpponentScore();

        this.heroCoins = match.getHeroCoins();
        this.opponentCoins = match.getOpponentCoins();
        this.finished = match.isFinished();
        this.finishedForGiveUp = match.isFinishedForGiveUp();
        this.matchWon = match.isMatchWon();
        this.readyToDiscoverResult = false;
        this.coinsAlreadyAssigned = true;
        this.chatMessages = match.getChatMessages();

    }

    public long getMatchID() {
        return matchID;
    }

    public void setMatchID(int matchID) {
        this.matchID = matchID;
    }


    public int getCurrentRound() {
        return currentRound;
    }

    public boolean isYourTurn() {
        return yourTurn;
    }

    public void setYourTurn(boolean yourTurn) {
        this.yourTurn = yourTurn;
    }

    public boolean isWaitingForOpponentTurn() {
        return waitingForOpponentTurn;
    }

    public boolean isMatchWon() {
        return matchWon;
    }

    public void setMatchWon(boolean matchWon) {
        this.matchWon = matchWon;
    }

    public void setWaitingForOpponentTurn(boolean waitingForOpponentTurn) {
        this.waitingForOpponentTurn = waitingForOpponentTurn;
    }

    public Array<ChatMessage> getChatMessages() {

        return chatMessages;
    }

    public void addHeroChatMessages(String msg) {
        chatMessages.add(new ChatMessage(msg, false));
    }

    public void addOpponentChatMessages(String msg) {
        chatMessages.add(new ChatMessage(msg, true));
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public int getFirstRoundHeroScore() {
        return FirstRoundHeroScore;
    }

    public int getTiedPoints() {
        return tiedPoints;
    }

    public void setTiedPoints(int tiedPoint) {
        this.tiedPoints = tiedPoint;
    }

    public void setFirstRoundHeroScore(int firstRoundHeroScore) {
        FirstRoundHeroScore = firstRoundHeroScore;
    }

    public int getSecondRoundHeroScore() {
        return SecondRoundHeroScore;
    }

    public void setSecondRoundHeroScore(int secondRoundHeroScore) {
        SecondRoundHeroScore = secondRoundHeroScore;
    }

    public int getThirdRoundHeroScore() {
        return ThirdRoundHeroScore;
    }

    public boolean isCoinsAlreadyAssigned() {
        return coinsAlreadyAssigned;
    }

    public void setCoinsAlreadyAssigned(boolean coinsAlreadyAssigned) {
        this.coinsAlreadyAssigned = coinsAlreadyAssigned;
    }

    public void setMatchID(long matchID) {
        this.matchID = matchID;
    }

    public void setThirdRoundHeroScore(int thirdRoundHeroScore) {
        ThirdRoundHeroScore = thirdRoundHeroScore;
    }

    public int getFirstRoundOpponentScore() {
        return FirstRoundOpponentScore;
    }

    public int getTotalHeroScore() {
        return totalHeroScore;
    }

    public boolean isFinishedForGiveUp() {
        return finishedForGiveUp;
    }

    public void setFinishedForGiveUp(boolean finishedForGiveUp) {
        this.finishedForGiveUp = finishedForGiveUp;
    }


    public void setTotalHeroScore(int totalHeroScore) {
        this.totalHeroScore = totalHeroScore;
    }

    public int getTotalOpponentScore() {
        return totalOpponentScore;
    }

    public void setTotalOpponentScore(int totalOpponentScore) {
        this.totalOpponentScore = totalOpponentScore;
    }

    public void setFirstRoundOpponentScore(int firstRoundOpponentScore) {
        FirstRoundOpponentScore = firstRoundOpponentScore;
    }

    public int getSecondRoundOpponentScore() {
        return SecondRoundOpponentScore;
    }

    public void setSecondRoundOpponentScore(int secondRoundOpponentScore) {
        SecondRoundOpponentScore = secondRoundOpponentScore;
    }

    public int getThirdRoundOpponentScore() {
        return ThirdRoundOpponentScore;
    }

    public int getHeroCoins() {
        return heroCoins;
    }

    public void setHeroCoins(int heroCoins) {
        this.heroCoins = heroCoins;
    }

    public int getOpponentCoins() {
        return opponentCoins;
    }

    public void setOpponentCoins(int opponentCoins) {
        this.opponentCoins = opponentCoins;
    }

    public void setThirdRoundOpponentScore(int thirdRoundOpponentScore) {
        ThirdRoundOpponentScore = thirdRoundOpponentScore;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Opponent getOpponent() {
        return opponent;
    }

    public void setOpponent(Opponent opponent) {
        this.opponent = opponent;
    }

    public boolean isReadyToDiscoverResult() {
        return readyToDiscoverResult;
    }

    public void setReadyToDiscoverResult(boolean readyToDiscoverResult) {
        this.readyToDiscoverResult = readyToDiscoverResult;
    }
}
