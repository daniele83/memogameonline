package com.memogameonline.user;

import com.badlogic.gdx.utils.Array;

public class UserWrapper {

	public String userName;
	public String password;
	public String email;
	public String facebookID;

	public Array<Opponent> memoFriendsList;
	public Array<String> challengedFriendsList;
	public Array<String> blockedUsersList;

	public int highestFirstRoundScoreOFFLINE;
	public float bestFirstRoundTimeOFFLINE;
	public int highestSecondRoundScoreOFFLINE;
	public float bestSecondRoundTimeOFFLINE;
	public int highestThirdRoundScoreOFFLINE;

	public UserWrapper() {

	}


	public String getUserName() {
		return userName;
	}

	public String getFacebookID() {
		return facebookID;
	}

	public void setFacebookID(String facebookID) {
		this.facebookID = facebookID;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}

	public Array<Opponent> getMemoFriendsList() {
		return memoFriendsList;
	}

	public void setMemoFriendsList(Array<Opponent> memoFriendsList) {
		this.memoFriendsList = memoFriendsList;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public Array<String> getChallengedFriendsList() {
		return challengedFriendsList;
	}

	public void setChallengedFriendsList(Array<String> challengedFriendsList) {
		this.challengedFriendsList = challengedFriendsList;
	}

	public Array<String> getBlockedUsersList() {
		return blockedUsersList;
	}

	public void setBlockedUsersList(Array<String> blockedUsersList) {
		this.blockedUsersList = blockedUsersList;
	}

	public int getHighestThirdRoundScoreOFFLINE() {
		return highestThirdRoundScoreOFFLINE;
	}

	public void setHighestThirdRoundScoreOFFLINE(int highestThirdRoundScoreOFFLINE) {
		this.highestThirdRoundScoreOFFLINE = highestThirdRoundScoreOFFLINE;
	}

	public float getBestFirstRoundTimeOFFLINE() {
		return bestFirstRoundTimeOFFLINE;
	}

	public void setBestFirstRoundTimeOFFLINE(float bestFirstRoundTimeOFFLINE) {
		this.bestFirstRoundTimeOFFLINE = bestFirstRoundTimeOFFLINE;
	}

	public float getBestSecondRoundTimeOFFLINE() {
		return bestSecondRoundTimeOFFLINE;
	}

	public void setBestSecondRoundTimeOFFLINE(float bestSecondRoundTimeOFFLINE) {
		this.bestSecondRoundTimeOFFLINE = bestSecondRoundTimeOFFLINE;
	}

	public int getHighestFirstRoundScoreOFFLINE() {
		return highestFirstRoundScoreOFFLINE;
	}

	public void setHighestFirstRoundScoreOFFLINE(int highestFirstRoundScoreOFFLINE) {
		this.highestFirstRoundScoreOFFLINE = highestFirstRoundScoreOFFLINE;
	}

	public int getHighestSecondRoundScoreOFFLINE() {
		return highestSecondRoundScoreOFFLINE;
	}

	public void setHighestSecondRoundScoreOFFLINE(int highestSecondRoundScoreOFFLINE) {
		this.highestSecondRoundScoreOFFLINE = highestSecondRoundScoreOFFLINE;
	}
}
