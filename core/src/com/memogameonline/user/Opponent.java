package com.memogameonline.user;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by danie_000 on 28/10/2015.
 */
public class Opponent {

    private String userName;
    private String facebookName;
    private String facebookID;
    private String facebookPictureURL;
    private Image facebookPicture;
    private int level;
    private int imageID;
    private int matchPlayed;
    private int matchesWon;
    private float winningPercentage;

    private int consecutiveMatchWon;
    private int highestmatchScore;
    private int highestFirstRoundScore;
    private float bestFirstRoundTime;
    private int highestSecondRoundScore;
    private float bestSecondRoundTime;
    private int highestThirdRoundScore;
    private float bestThirdRoundTime;

    public Opponent(String userName, int imageID) {

        this.userName = userName;
        this.imageID = imageID;
    }

    public Opponent() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFacebookID() {
        return facebookID;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public int getMatchesPlayed() {
        return matchPlayed;
    }

    public void setMatchPlayed(int matchPlayed) {
        this.matchPlayed = matchPlayed;
    }

    public int getMatchesWon() {
        return matchesWon;
    }

    public void setMatchesWon(int matchesWon) {
        this.matchesWon = matchesWon;
    }

    public float getWinningPercentage() {
        return winningPercentage;
    }

    public void setWinningPercentage(float winningPercentage) {
        this.winningPercentage = winningPercentage;
    }

    public float getBestFirstRoundTime() {
        return bestFirstRoundTime;
    }

    public void setBestFirstRoundTime(float bestFirstRoundTime) {
        this.bestFirstRoundTime = bestFirstRoundTime;
    }

    public float getBestSecondRoundTime() {
        return bestSecondRoundTime;
    }

    public void setBestSecondRoundTime(float bestSecondRoundTime) {
        this.bestSecondRoundTime = bestSecondRoundTime;
    }

    public float getBestThirdRoundTime() {
        return bestThirdRoundTime;
    }

    public void setBestThirdRoundTime(float bestThirdRoundTime) {
        this.bestThirdRoundTime = bestThirdRoundTime;
    }

    public String getFacebookName() {
        return facebookName;
    }

    public void setFacebookName(String facebookName) {
        this.facebookName = facebookName;
    }

    public int getConsecutiveMatchWon() {
        return consecutiveMatchWon;
    }

    public void setConsecutiveMatchWon(int consecutiveMatchWon) {
        this.consecutiveMatchWon = consecutiveMatchWon;
    }

    public int getHighestFirstRoundScore() {
        return highestFirstRoundScore;
    }

    public void setHighestFirstRoundScore(int highestFirstRoundScore) {
        this.highestFirstRoundScore = highestFirstRoundScore;
    }

    public int getHighestmatchScore() {
        return highestmatchScore;
    }

    public void setHighestmatchScore(int highestmatchScore) {
        this.highestmatchScore = highestmatchScore;
    }

    public int getHighestSecondRoundScore() {
        return highestSecondRoundScore;
    }

    public void setHighestSecondRoundScore(int highestSecondRoundScore) {
        this.highestSecondRoundScore = highestSecondRoundScore;
    }

    public int getHighestThirdRoundScore() {
        return highestThirdRoundScore;
    }

    public void setHighestThirdRoundScore(int highestThirdRoundScore) {
        this.highestThirdRoundScore = highestThirdRoundScore;
    }

    public int getMatchPlayed() {
        return matchPlayed;
    }

    public Image getFacebookPicture() {
        return facebookPicture;
    }

    public void setFacebookPicture(Image facebookPicture) {
        this.facebookPicture = facebookPicture;
    }

    public String getFacebookPictureURL() {
        return facebookPictureURL;
    }

    public void setFacebookPictureURL(String facebookPictureURL) {
        this.facebookPictureURL = facebookPictureURL;
    }
}
