package com.memogameonline.user;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.memogameonline.gamehelpers.DataManager;
import com.memogameonline.gamehelpers.GamePreferences;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class User {

    public static final User data = new User();

    public String userName;
    public String password;
    public String email;
    public String facebookID;
    public String facebookPictureURL;
    public Image facebookPicture;
    public int imageId;
    public int level;
    public int experience;
    public int pills;

    public int powerUp_Time;
    public int powerUp_DoubleCouple;
    public int powerUp_FaceUp;
    public int powerUp_Stop;
    public int powerUp_Chain;
    public int powerUp_Bonus;

    public Array<Match> activeMatches;
    public Array<Match> finishedMatches;

    public Array<Opponent> facebookFriendsList;
    public Array<Opponent> memoFriendsList;
    public Array<String> challengedFriendList;
    public Array<String> blockedUsersList;

    public int playedMatches;
    public int wonMatches;
    public int givenUpMatches;
    public int bestMatchWinningStreak;
    public int currentMatchWinningStreak;
    public int highestMatchScore;
    public int highestFirstRoundScore;
    public float bestFirstRoundTime;
    public int highestSecondRoundScore;
    public float bestSecondRoundTime;
    public int highestThirdRoundScore;
    public float bestThirdRoundTime;

    public int highestFirstRoundScoreOFFLINE;
    public float bestFirstRoundTimeOFFLINE;
    public int highestSecondRoundScoreOFFLINE;
    public float bestSecondRoundTimeOFFLINE;
    public int highestThirdRoundScoreOFFLINE;

    public boolean searchingForRandomOpponent;
    public boolean challengeFromFriendReceived;
    public boolean comingFromFacebookLogIn;
    public boolean comingFromFacebookFriendsListRequest;

    public float timerEvent = 15;
    public boolean startedTimerEvent;

    private User() {

        userName = "";
        password = "";
        email = "";
        facebookID = "";
        facebookPictureURL = "";
        facebookPicture = null;
        level = 1;
        experience = 1;
        pills = 0;

        powerUp_Time = 0;
        powerUp_DoubleCouple = 0;
        powerUp_FaceUp = 0;
        powerUp_Stop = 0;
        powerUp_Chain = 0;
        powerUp_Bonus = 0;

        activeMatches = new Array<Match>();
        finishedMatches = new Array<Match>();


        /*activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        activeMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));
        finishedMatches.add(new Match(1, new Opponent("peppino",3)));*/

        facebookFriendsList = new Array<Opponent>();
        memoFriendsList = new Array<Opponent>();
        challengedFriendList = new Array<String>();
        blockedUsersList = new Array<String>();

    }

    public void resetData() {
        userName = "";
        password = "";
        email = "";
        facebookID = "";
        facebookPictureURL = "";
        facebookPicture = null;
        level = 1;
        experience = 1;
        pills = 0;

        powerUp_Time = 0;
        powerUp_DoubleCouple = 0;
        powerUp_FaceUp = 0;
        powerUp_Stop = 0;
        powerUp_Chain = 0;
        powerUp_Bonus = 0;

        activeMatches.clear();
        finishedMatches.clear();

        facebookFriendsList.clear();
        memoFriendsList.clear();
        challengedFriendList.clear();
        blockedUsersList.clear();

        playedMatches = 0;
        wonMatches = 0;
        givenUpMatches = 0;
        currentMatchWinningStreak = 0;
        bestMatchWinningStreak = 0;

        searchingForRandomOpponent = false;
        challengeFromFriendReceived = false;
        comingFromFacebookLogIn = false;
        comingFromFacebookFriendsListRequest = false;

    }

    public Match getMatch(long matchID) {

        for (Match match : activeMatches) {
            if (match.getMatchID() == matchID)
                return match;
        }

        for (Match match : finishedMatches) {
            if (match.getMatchID() == matchID)
                return match;
        }
        System.out.println("ENTRO IN GET MATCH!!!!");
        System.out.println("MATCH NON TROVATO!!!! con ID: " + matchID);
        return null;
    }

    public void addActiveMatch(long matchID, Opponent opponent) {
        activeMatches.add(new Match(matchID, opponent));
    }

    public void addActiveMatch(Match match) {
        activeMatches.add(match);
    }

    public void updateMatchStatus(long matchID) {
        if (!getMatch(matchID).isFinished()) {
            System.out.println("-_-_-_-_-_RICHIAMO IL MATCH UPDATE-_---_-_-_-");
            if (getMatch(matchID).getCurrentRound() == 1
                    && getMatch(matchID).getFirstRoundHeroScore() != -1
                    && getMatch(matchID).getFirstRoundOpponentScore() != -1
                    || getMatch(matchID).getCurrentRound() == 2
                    && getMatch(matchID).getSecondRoundHeroScore() != -1
                    && getMatch(matchID).getSecondRoundOpponentScore() != -1) {
                getMatch(matchID).setCurrentRound(
                        getMatch(matchID).getCurrentRound() + 1);
            } else if (getMatch(matchID).getCurrentRound() == 3
                    && getMatch(matchID).getThirdRoundHeroScore() != -1
                    && getMatch(matchID).getThirdRoundOpponentScore() != -1
                    && getMatch(matchID).getHeroCoins() != -1
                    && getMatch(matchID).getOpponentCoins() != -1) {

                System.out.println("Partita Completata, attendo che l'utente scopra  il risultato!!!");
                playedMatches++;
                getMatch(matchID).setReadyToDiscoverResult(true);


                refreshTotalScore(matchID);
                if (getMatch(matchID).getTotalHeroScore() >= getMatch(matchID)
                        .getTotalOpponentScore()) {
                    System.out.println("Partita VINTA!!!!!!!!!!");
                    getMatch(matchID).setMatchWon(true);
                    wonMatches++;
                    currentMatchWinningStreak++;
                    if (currentMatchWinningStreak > bestMatchWinningStreak)
                        bestMatchWinningStreak = currentMatchWinningStreak;
                } else {
                    System.out.println("Partita Persa  :(");
                    currentMatchWinningStreak = 0;
                    getMatch(matchID).setMatchWon(false);
                }
            }


            if (getMatch(matchID).getCurrentRound() == 1
                    && getMatch(matchID).getFirstRoundHeroScore() == -1
                    || getMatch(matchID).getCurrentRound() == 2
                    && getMatch(matchID).getSecondRoundHeroScore() == -1
                    || getMatch(matchID).getCurrentRound() == 3
                    && getMatch(matchID).getThirdRoundHeroScore() == -1) {
                getMatch(matchID).setYourTurn(true);
                getMatch(matchID).setWaitingForOpponentTurn(false);
                System.out.println("SETTO VERO IN UPDATE!!!!!");
            } else {
                getMatch(matchID).setYourTurn(false);
            }
        }
    }

    public void synchronizeMatch(Match match, boolean hereHasReceivedPills) {
        System.out.println("SINCRONIZZO IL MATCH!!!!!!!SINCRONIZZO IL MATCH!!!!!!!SINCRONIZZO IL MATCH!!!!!!!");
        if (match.isFinishedForGiveUp()) {
            match.setFinished(true);
            finishedMatches.add(match);
        } else if (match.getThirdRoundHeroScore() != -1
                && match.getThirdRoundOpponentScore() != -1) {
            int heroScore = match.getFirstRoundHeroScore() + match.getSecondRoundHeroScore() + match.getThirdRoundHeroScore();
            int oppoScore = match.getFirstRoundOpponentScore() + match.getSecondRoundOpponentScore() + match.getThirdRoundOpponentScore();
            match.setCurrentRound(3);

            if (heroScore > oppoScore)
                match.setMatchWon(true);

            if (hereHasReceivedPills) {
                match.setFinished(true);
                finishedMatches.add(match);
            } else {
                System.out.println("ARRIVO QUI!!!!");
                addActiveMatch(match);
                match.setReadyToDiscoverResult(true);
            }

        } else if (match.getThirdRoundHeroScore() != -1
                && match.getThirdRoundOpponentScore() == -1 ||
                match.getThirdRoundHeroScore() == -1
                        && match.getThirdRoundOpponentScore() != -1 ||
                (match.getThirdRoundHeroScore() == -1
                        && match.getThirdRoundOpponentScore() == -1 && match.getSecondRoundHeroScore() != -1
                        && match.getSecondRoundOpponentScore() != -1)) {
            match.setCurrentRound(3);
            addActiveMatch(match);
        } else if (match.getSecondRoundHeroScore() != -1
                && match.getSecondRoundOpponentScore() == -1 ||
                match.getSecondRoundHeroScore() == -1
                        && match.getSecondRoundOpponentScore() != -1 ||
                (match.getSecondRoundHeroScore() == -1
                        && match.getSecondRoundOpponentScore() == -1 && match.getFirstRoundHeroScore() != -1
                        && match.getFirstRoundOpponentScore() != -1)) {
            match.setCurrentRound(2);
            addActiveMatch(match);
        } else {
            match.setCurrentRound(1);
            addActiveMatch(match);
        }

    }

    public void updateHeroScore(long matchID, int score, float time) {
        if (getMatch(matchID).getCurrentRound() == 1) {
            getMatch(matchID).setFirstRoundHeroScore(score);
            getMatch(matchID).setTotalHeroScore(score);
            System.out
                    .println("Aggiornato punteggio primo round del match con ID: "
                            + matchID + "  . Score: " + score);
            if (score > highestFirstRoundScore)
                highestFirstRoundScore = score;
            if (time < bestFirstRoundTime)
                bestFirstRoundTime = time;
        } else if (getMatch(matchID).getCurrentRound() == 2) {
            getMatch(matchID).setSecondRoundHeroScore(score);
            getMatch(matchID).setTotalHeroScore(
                    getMatch(matchID).getTotalHeroScore() + score);
            System.out
                    .println("Aggiornato punteggio secondo round del match con ID: "
                            + matchID + "  . Score: " + score);
            if (score > highestSecondRoundScore)
                highestSecondRoundScore = score;
            if (time < bestSecondRoundTime)
                bestSecondRoundTime = time;
        } else if (getMatch(matchID).getCurrentRound() == 3) {
            getMatch(matchID).setThirdRoundHeroScore(score);
            getMatch(matchID).setTotalHeroScore(
                    getMatch(matchID).getTotalHeroScore() + score);
            System.out
                    .println("Aggiornato punteggio terzoo round del match con ID: "
                            + matchID + "  . Score: " + score);
            if (score > highestThirdRoundScore)
                highestThirdRoundScore = score;
            if (time < bestThirdRoundTime)
                bestThirdRoundTime = time;
            if (getMatch(matchID).getThirdRoundOpponentScore() != -1) {
                this.calculateMatchPills(matchID);

            }
        }
    }

    public void updateOpponentScore(long matchID, int score) {
        if (getMatch(matchID).getCurrentRound() == 1) {
            getMatch(matchID).setFirstRoundOpponentScore(score);
            getMatch(matchID).setTotalOpponentScore(score);
        } else if (getMatch(matchID).getCurrentRound() == 2) {
            getMatch(matchID).setSecondRoundOpponentScore(score);
            getMatch(matchID).setTotalOpponentScore(
                    getMatch(matchID).getTotalOpponentScore() + score);
        } else if (getMatch(matchID).getCurrentRound() == 3) {
            getMatch(matchID).setThirdRoundOpponentScore(score);
            getMatch(matchID).setTotalOpponentScore(
                    getMatch(matchID).getTotalOpponentScore() + score);

            if (getMatch(matchID).getThirdRoundHeroScore() != -1) {
                this.calculateMatchPills(matchID);

            }
        }
    }

    public void refreshTotalScore(long matchID) {
        getMatch(matchID).setTotalHeroScore(getMatch(matchID).getFirstRoundHeroScore() + getMatch(matchID).getSecondRoundHeroScore() + getMatch(matchID).getThirdRoundHeroScore());
        getMatch(matchID).setTotalOpponentScore(getMatch(matchID).getFirstRoundOpponentScore() + getMatch(matchID).getSecondRoundOpponentScore() + getMatch(matchID).getThirdRoundOpponentScore());


    }

    public void calculateMatchPills(long matchID) {
        System.out.println("Calcolo le monete relative alla partita finita!!!");
        int heroCoins = 2;
        if (getMatch(matchID).getFirstRoundHeroScore() >= getMatch(matchID)
                .getFirstRoundOpponentScore()) {
            heroCoins += 2;
            System.out.println("Primo round vinto da HERO! HeroCoins= " + heroCoins);
            if (getMatch(matchID).getFirstRoundHeroScore() == getMatch(matchID)
                    .getFirstRoundOpponentScore()) {
                getMatch(matchID).setTiedPoints(
                        getMatch(matchID).getTiedPoints() + 2);
                System.out
                        .println("Primo round PARI! i coins totali da assegnare sono: "
                                + (14 + getMatch(matchID).getTiedPoints()));
            }
        }
        if (getMatch(matchID).getSecondRoundHeroScore() >= getMatch(matchID)
                .getSecondRoundOpponentScore()) {
            heroCoins += 2;
            System.out.println("Second round vinto da HERO! HeroCoins= " + heroCoins);
            if (getMatch(matchID).getSecondRoundHeroScore() == getMatch(matchID)
                    .getSecondRoundOpponentScore()) {
                getMatch(matchID).setTiedPoints(
                        getMatch(matchID).getTiedPoints() + 2);
                System.out
                        .println("Second round PARI! i coins totali da assegnare sono: "
                                + (14 + getMatch(matchID).getTiedPoints()));
            }
        }
        if (getMatch(matchID).getThirdRoundHeroScore() >= getMatch(matchID)
                .getThirdRoundOpponentScore()) {
            heroCoins += 2;
            System.out.println("terzo round vinto da HERO! HeroCoins= " + heroCoins);
            if (getMatch(matchID).getThirdRoundHeroScore() == getMatch(matchID)
                    .getThirdRoundOpponentScore()) {
                getMatch(matchID).setTiedPoints(
                        getMatch(matchID).getTiedPoints() + 2);
                System.out
                        .println("terzo round PARI! i coins totali da assegnare sono: "
                                + (14 + getMatch(matchID).getTiedPoints()));
            }
        }
        if (getMatch(matchID).getTotalHeroScore() >= getMatch(matchID)
                .getTotalOpponentScore()) {
            heroCoins += 4;
            if (getMatch(matchID).getTotalHeroScore() == getMatch(matchID)
                    .getTotalOpponentScore()) {
                getMatch(matchID).setTiedPoints(
                        getMatch(matchID).getTiedPoints() + 4);
                System.out
                        .println("TOTAL SCORE PARI!!! i coins totali da assegnare sono: "
                                + (14 + getMatch(matchID).getTiedPoints()));
            }
        }
        getMatch(matchID).setHeroCoins(heroCoins);
        getMatch(matchID).setOpponentCoins(
                14 + getMatch(matchID).getTiedPoints() - heroCoins);

    }


    public void closeMatch(long matchID) {
        if (activeMatches.contains(getMatch(matchID), true)) {
            finishedMatches.add(new Match(getMatch(matchID)));
            activeMatches.removeValue(getMatch(matchID), true);

            getMatch(matchID).setFinished(true);
            getMatch(matchID).setCoinsAlreadyAssigned(true);
            getMatch(matchID).setReadyToDiscoverResult(false);

            System.out.println("Aggiunto un match tra  i finiti con ID: " + matchID);
        }

    }

    public void refreshAllMatchesStatus() {
        for (Match match : activeMatches) {
            if (match.getCurrentRound() == 1
                    && match.getFirstRoundHeroScore() == -1) {
                match.setYourTurn(true);
            } else if (match.getCurrentRound() == 1
                    && match.getFirstRoundHeroScore() != -1 && match.getFirstRoundOpponentScore() == -1) {
                match.setYourTurn(false);
                match.setWaitingForOpponentTurn(true);
                System.out.println("SETTO VERO IN REFRESH ALL!!!!!!!!!!!");
            } else if (match.getCurrentRound() == 2
                    && match.getSecondRoundHeroScore() == -1) {
                match.setYourTurn(true);
            } else if (match.getCurrentRound() == 2
                    && match.getSecondRoundHeroScore() != -1 && match.getSecondRoundOpponentScore() == -1) {
                match.setYourTurn(false);
                match.setWaitingForOpponentTurn(true);
            } else if (match.getCurrentRound() == 3
                    && match.getThirdRoundHeroScore() == -1) {
                match.setYourTurn(true);
            } else if (match.getCurrentRound() == 3
                    && match.getThirdRoundHeroScore() != -1 && match.getThirdRoundOpponentScore() == -1) {
                match.setYourTurn(false);
                match.setWaitingForOpponentTurn(true);
            } else if (match.getCurrentRound() == 3
                    && match.getThirdRoundHeroScore() != -1
                    && match.getThirdRoundOpponentScore() != -1) {
                match.setYourTurn(false);
                match.setWaitingForOpponentTurn(false);
                //match.setFinished(true);
            }

        }
    }

    public boolean matchAlreadyExist(String opponentName) {

        for (Match match : activeMatches)
            if (match.getOpponent().getUserName().equals(opponentName))
                return true;
        return false;
    }

    public Opponent getFriend(String userName) {

        for (Opponent opponent : this.memoFriendsList)
            if (opponent.getUserName().equals(userName))
                return opponent;
        return null;
    }

    public boolean isAlreadyFriend(String userName) {

        for (Opponent opponent : this.memoFriendsList)
            if (opponent.getUserName().equals(userName))
                return true;
        return false;
    }

    public boolean isBlockedPlayer(String username) {

        System.out.println("ASDASDASDSAD            SDASDASDASD       " + blockedUsersList);
        System.out.println("ASDASDASDSAD            SDASDASDASD       " + challengedFriendList);
        for (String name : this.blockedUsersList)
            if (name.equals(username))
                return true;
        return false;
    }

    public void startTimerEvent() {
        startedTimerEvent = true;
    }

    public void resetAndStopTimerEvent() {
        startedTimerEvent = false;
        timerEvent = 10;
    }

    public void addFriend(Opponent oppo) {

        if (!memoFriendsList.contains(oppo, true))
            memoFriendsList.add(oppo);
    }

    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ObjectOutputStream o = new ObjectOutputStream(b);
        o.writeObject(obj);
        System.out.println("Serializzazione avvenuta con successo!!!");
        return b.toByteArray();
    }


    public void elaborateChatMessages(Match match, String chat) {

        String msg;
        while (chat.length() > 0) {
            int i = chat.indexOf("-_666_-");
            // System.out.println("DDDDDDDDDDDDDDDDDD: " + i);
            if (i != -1) {
                msg = chat.substring(0, i);
                //System.out.println("Sottratta stringa: " + msg);
                chat = chat.substring(i + 7, chat.length());
                // System.out.println("Nuova stringa da rielaborare: " + chat);
            } else {
                msg = chat;
                chat = "";
                //System.out.println("Sottratta stringa: " + msg);
            }
            if (msg.contains("-_" + User.data.userName + "_-"))
                match.addHeroChatMessages(msg.substring((("-_" + User.data.userName + "_-").length())));
            else {
                if (!isBlockedPlayer(match.getOpponent().getUserName()))
                    match.addOpponentChatMessages(msg.substring((("-_" + match.getOpponent().getUserName() + "_-").length())));
            }
        }
    }


    public void refreshOfflineRecords(int round, int score, float time) {

        System.out.println("Round" + round);
        System.out.println("Score" + score);
        System.out.println("time" + time);

        if (round == 1) {
            if (highestFirstRoundScoreOFFLINE < score)
                highestFirstRoundScoreOFFLINE = score;
            if (bestFirstRoundTimeOFFLINE < time && time > 0)
                bestFirstRoundTimeOFFLINE = time;
        } else if (round == 2) {
            if (highestSecondRoundScoreOFFLINE < score)
                highestSecondRoundScoreOFFLINE = score;
            if (bestSecondRoundTimeOFFLINE < time && time > 0)
                bestSecondRoundTimeOFFLINE = time;
        } else if (round == 3) {
            if (highestThirdRoundScoreOFFLINE < score)
                highestThirdRoundScoreOFFLINE = score;
        }

        GamePreferences.instance.save();
    }

}
