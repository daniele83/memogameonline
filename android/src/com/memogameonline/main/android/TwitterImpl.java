package com.memogameonline.main.android;

import com.memogameonline.externalAPI.Twitter;

import android.app.Activity;
import android.content.Intent;

public class TwitterImpl implements Twitter{
	Activity activity;

	public TwitterImpl(Activity activity) {
		this.activity = activity;
	}

	public void openTwitter() {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
		sendIntent.setType("text/plain");
		activity.startActivity(sendIntent);
	}
}
