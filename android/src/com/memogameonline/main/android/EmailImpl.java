package com.memogameonline.main.android;

import com.memogameonline.externalAPI.Email;

import android.app.Activity;
import android.content.Intent;

public class EmailImpl implements  Email{
	Activity activity;

	public EmailImpl(Activity activity) {
		this.activity = activity;
	}

	public void openEmail() {
		Intent emailIntent = new Intent();
		emailIntent.setAction(Intent.ACTION_SEND);
		emailIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
		emailIntent.setType("vnd.android-dir/mms-sms");
		activity.startActivity(emailIntent);
	}
}
