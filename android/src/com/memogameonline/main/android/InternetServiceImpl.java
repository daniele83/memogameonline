package com.memogameonline.main.android;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.memogameonline.externalAPI.InternetService;

/**
 * Created by danie_000 on 09/11/2015.
 */
public class InternetServiceImpl implements InternetService {

    private Activity activity;
    public InternetServiceImpl(Activity activity){
        this.activity=activity;

    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null || !ni.isConnected() || !ni.isAvailable()) {
            System.out.println("Nessuna Connessione Internet!!!");
            return false;
        } else {
            System.out.println("Connessione Internet presente e funzionante");
            return true;
        }
    }
}
