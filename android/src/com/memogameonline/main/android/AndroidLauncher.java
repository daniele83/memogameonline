package com.memogameonline.main.android;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import android.graphics.Color;
import android.view.WindowManager;
import android.widget.RelativeLayout.LayoutParams;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.memogameonline.externalAPI.AdsController;
import com.memogameonline.main.MemoGameOnline;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class AndroidLauncher extends AndroidApplication implements AdsController {

    protected MemoGameOnline memoryGame;

    protected InternetServiceImpl internetService;
    protected FacebookServiceImpl facebookService;
    protected WhatsAppImpl whatsApp;
    protected SMSImpl sms;
    protected TwitterImpl twitter;
    protected EmailImpl email;

    private static final String AD_UNIT_ID = "ca-app-pub-9309564927126850/8962941728";
    private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-9309564927126850/7206360123";
    protected AdView adView;
    private InterstitialAd interstitialAd;

    protected View gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        //cfg.useGL20 = false;
        cfg.useAccelerometer = false;
        cfg.useCompass = false;


        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);

        AdView admobView = createAdView();
        layout.addView(admobView);
        View gameView = createGameView(cfg);
        layout.addView(gameView);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
            }
        });

        setContentView(layout);
        startAdvertising(admobView);


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookService.onActivityResult(requestCode, resultCode, data);
    }


    private View createGameView(AndroidApplicationConfiguration cfg) {
        memoryGame = new MemoGameOnline(this);

        internetService = new InternetServiceImpl(this);
        facebookService = new FacebookServiceImpl(this);
        whatsApp = new WhatsAppImpl(this);
        sms = new SMSImpl(this);
        twitter = new TwitterImpl(this);
        email = new EmailImpl(this);

        memoryGame.setInternetService(internetService);
        memoryGame.setFacebookService(facebookService);
        memoryGame.setWhatsApp(whatsApp);
        memoryGame.setSms(sms);
        memoryGame.setTwitter(twitter);
        memoryGame.setEmail(email);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        int verCode = pInfo.versionCode;
        memoryGame.setVersionCode(verCode);
        memoryGame.setVersionName(version);

        gameView = initializeForView(memoryGame, cfg);
        // gameView.setId(R.id.gameViewId);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ABOVE, adView.getId());
        gameView.setLayoutParams(params);
        return gameView;
    }

    private AdView createAdView() {
        adView = new AdView(this);
        // adView.setVisibility(View.INVISIBLE);
        adView.setBackgroundColor(0xff000000); // black
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        adView.setId(R.id.adViewId); // this is an arbitrary id, allows for relative positioning in createGameView()

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        adView.setLayoutParams(params);
        adView.setBackgroundColor(Color.BLACK);

        return adView;
    }

    private void startAdvertising(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    public void showBannerAd() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adView.setVisibility(View.VISIBLE);

                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);

                //adView.loadAd(new AdRequest.Builder().addTestDevice("462CF4B443C291C4E1CE0FF2D473D108").build());

                System.out.println("RICHIESTA EFFETTUATA!!!!!!!!!!");
            }
        });
    }

    @Override
    public void hideBannerAd() {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) adView.resume();
        if (gameView != null) gameView.requestFocus();
        if (gameView != null) gameView.requestFocusFromTouch();

        System.out.println("RESUME NATIVO RESUME  NATIVO RESUME NATIVO NATIVO NATIVO NATIVO!!!!!!!!");
    }

    @Override
    public void onPause() {
        if (adView != null) adView.pause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (adView != null) adView.destroy();
        super.onDestroy();
    }

    @Override
    public void showOrLoadInterstital() {

        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                    }
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void loadInterstital() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (!interstitialAd.isLoaded()) {

                        AdRequest interstitialRequest = new AdRequest.Builder().build();
                        interstitialAd.loadAd(interstitialRequest);

                        //interstitialAd.loadAd(new AdRequest.Builder().addTestDevice("462CF4B443C291C4E1CE0FF2D473D108").build());
                    }
                }
            });
        } catch (Exception e) {
        }
    }

    /*@Override
    public void onBackPressed() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        Button b1 = new Button(this);
        b1.setText("Quit");
        b1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        ll.addView(b1);

        Button b2 = new Button(this);
        b2.setText("TheInvader360");
        b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_URL)));
                dialog.dismiss();
            }
        });
        ll.addView(b2);

        dialog.setContentView(ll);
        dialog.show();
    }*/
}



