package com.memogameonline.main.android;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidEventListener;
import com.badlogic.gdx.utils.Timer;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.memogameonline.externalAPI.FacebookService;
import com.memogameonline.gamehelpers.TaskCallback;


public class FacebookServiceImpl extends FacebookService implements
        AndroidEventListener {

    private Activity activity;

    private CallbackManager callbackManager;

    private LoginManager loginManager;

    public FacebookServiceImpl(final AndroidLauncher androidLauncher) {
        this.activity = androidLauncher;

        FacebookSdk.sdkInitialize(this.activity.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        loginManager = LoginManager.getInstance();
        loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void logIn(final TaskCallback callback) throws IOException {

        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out
                                .println("ANDROID: LOG IN EFFETTUATO CON SUCCESSO!!!!!!!!");

                        AccessToken accessToken = AccessToken
                                .getCurrentAccessToken();

                        System.out.println("TOKEN: " + accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {

                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response) {

                                        String id = null;
                                        String name = null;
                                        String profilePicUrl = null;
                                        try {
                                            id = object.get("id").toString();
                                            name = object.get("name")
                                                    .toString();
                                            JSONObject data = response.getJSONObject();
                                            profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                            System.out.println("Ottenuro l'URL della foto!!! " + profilePicUrl);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                        callback.onSuccess(id, name, null, profilePicUrl, null);

                                    }

                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,picture");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        callback.onCancel();

                    }

                    @Override
                    public void onError(FacebookException e) {
                        callback.onError();

                    }
                });

        ArrayList<String> permissions = new ArrayList<>();
        permissions.add("user_friends");
        permissions.add("email");

        loginManager.logInWithReadPermissions(activity, permissions);

    }


    @Override
    public void getFriendList(final TaskCallback callback) {

        System.out
                .println("UTENTE GIA LOGGATO!!! EFFETTUO LA RICHIESTA DEGLI AMICI!!!");

        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
        System.out.println("TOKEN: " + accessToken);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                System.out.println("ENTRO NEL METODO RUN!!!!!");
                GraphRequest request = GraphRequest.newMyFriendsRequest(
                        accessToken, new GraphRequest.GraphJSONArrayCallback() {
                            @Override
                            public void onCompleted(JSONArray array,
                                                    GraphResponse response) {

                                String friendsList[] = new String[array
                                        .length() * 3];

                                System.out.println("Array di dimensioni:"
                                        + array.length());
                                System.out.println("Array completo:" + array);
                                System.out.println("Che oggetto è response???:"
                                        + response.getJSONObject());
                                System.out.println("Array completo:" + array);
                                try {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = array
                                                .getJSONObject(i);
                                        System.out.println("Json object numero " + i + " :" + object);

                                        friendsList[i * 3] = object.get("name")
                                                .toString();
                                        friendsList[i * 3 + 1] = object.get("id")
                                                .toString();
                                        friendsList[i * 3 + 2] = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated
                                    // catch
                                    // block
                                    e.printStackTrace();
                                }

                                callback.onSuccess(null, null, friendsList, null, null);
                            }

                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }
        });


    }

    @Override
    public void downloadProfilePicture(String url, final TaskCallback callback) {

        URL imageURL = null;
        Bitmap bitmap = null;
        try {
            imageURL = new URL(url);
            System.out.println("Creato l'oggetto URL");
        } catch (MalformedURLException me) {
            me.printStackTrace();
        }
        try {
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            System.out.println("Download effettuato!!!");
        } catch (IOException e) {
        }

        callback.onSuccess(null, null, null, null, bitmap);
    }

    @Override
    public byte[] serializePicture(Object obj) throws IOException {

        Bitmap bitmap = (Bitmap) obj;
        ByteArrayOutputStream b = null;
        try {
            b = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(b);
            writeObject(oos, bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Serializzazione avvenuta con successo!!!" + b.toByteArray());
        return b.toByteArray();
    }


    private void writeObject(ObjectOutputStream oos, Bitmap bitmap) throws IOException {
        // This will serialize all fields that you did not mark with 'transient'
        // (Java's default behaviour)
        oos.defaultWriteObject();
        // Now, manually serialize all transient fields that you want to be serialized
        if (bitmap != null) {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            boolean success = bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
            if (success) {
                oos.writeObject(byteStream.toByteArray());
            }
        }
    }

    @Override
    public boolean isLoggedIn() {

        return AccessToken.getCurrentAccessToken() != null;
    }

    @Override
    public void logOut() {
        loginManager.logOut();

    }

    public String getAccessToken() {
        return AccessToken.getCurrentAccessToken().getToken();
    }

}
