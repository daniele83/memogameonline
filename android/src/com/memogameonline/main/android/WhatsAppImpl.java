package com.memogameonline.main.android;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;

import com.memogameonline.externalAPI.WhatsApp;

public class WhatsAppImpl implements WhatsApp {

	Activity activity;

	public WhatsAppImpl(Activity activity) {
		this.activity = activity;
	}

	public void openWhatsApp() {
		Intent whatsAppIntent = new Intent();

		whatsAppIntent.setAction(Intent.ACTION_SEND);
		whatsAppIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
		whatsAppIntent.setType("text/plain");
		whatsAppIntent.setPackage("com.whatsapp");

		activity.startActivity(whatsAppIntent);
	}
}
