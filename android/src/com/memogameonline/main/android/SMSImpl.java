package com.memogameonline.main.android;

import com.memogameonline.externalAPI.SMS;

import android.app.Activity;
import android.content.Intent;

public class SMSImpl implements SMS{
	Activity activity;

	public SMSImpl(Activity activity) {
		this.activity = activity;
	}

	public void openSMS() {
		Intent smsIntent = new Intent();
		smsIntent.setAction(Intent.ACTION_SEND);
		smsIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
		//smsIntent.setType("text/plain");
		smsIntent.setType("vnd.android-dir/mms-sms");
		//smsIntent.setPackage(packageName);
		activity.startActivity(smsIntent);
	}
}
